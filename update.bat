 :: backup hexo

@git add -A
@git commit -m "backup hexo"
@git push origin master

:: deploy hexo
:: !!! make sure hexo and all the dependencies are installed
:: !!! make sure the site _config.yml and theme _config.yml is configured correctly, especially the remote branch must be `coding-pages`
:: some commands may help : `hexo init` ; `npm install --save` ; `npm i` ; etc ...

@hexo clean && hexo g && hexo d

@pause
