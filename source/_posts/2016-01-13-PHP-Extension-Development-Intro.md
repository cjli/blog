---
layout: post
title: PHP 扩展开发简介
category: PHP
tags: [PHP, 扩展开发]
---

PHP 扩展开发入门总结。

<!-- more -->

## Why

完成 PHP 自身做不到，做不好的事情。

具体而言我大致总结了下有下面几点：

### 适应高性能场景

利用其他语言的快捷性，将已有项目的部分耗时 PHP 代码以扩展形式重写。

### 改善现有扩展

实现比现有 PHP 扩展性能更高的扩展。

### 提高开发效率

极端情况下，你只需要在 PHP 脚本中，简单的调用一个扩展实现的函数，然后所有的功能都就被扩展实现，在提高性能的同时也极大减少 PHP 脚本的复杂度。

### 与外部的库做交互

假设你需要用到一个用 C/C++ 写的，很强大的库，而你并没有这个库的源码，即你无法把这个库在 PHP 中实现，这种情况下只有编写一个 PHP 扩展，来做为一个桥梁，连接 PHP 和这个库。

### 复用 Zend 词法/语法处理逻辑

在特殊领域的应用，例如 WEBSHELL 检测中，通过扩展 Hook 机制实现 Token AST 树的获取，然后在此基础上进行语法还原、和语法层面模拟执行。

### 新增扩展

扩展 PHP 尚未支持的功能。

## Drawbacks

- 复杂，对开发者水平要求更高。

需要熟练使用 C／C++ 等偏硬件语言，需要了解 PHP 内核实现，需要遵守 PHP 扩展开发规范。

- 调试、维护环节变得复杂

单独开发 PHP 程序时，改动代码刷新浏览器或者直接在 CLI 下运行，就能看到结果，而如果要采用 C/C++ 扩展形式，则每次出问题都需要先改动 C/C++ 代码，然后编译，然后 load 进 PHP，然后重启 Apache 或者 php-fpm 才能看到效果。

这失去了 PHP 自身开发速度快的优点。

## How

### 先编译一个最简单的 PHP

为了尽快得到可以测试的环境，我们仅编译一个最精简的 PHP，以后如果需要其他功能可以重新编译。

如果 configure 命令出现错误，可能是缺少 PHP 所依赖的库，各个系统的环境可能不一样，但大部分报错都可以问 Google。

``` shell
wget http://cn2.php.net/distributions/php-5.6.29.tar.gz
tar -zvzf php-5.6.29.tar.gz
cd php-5.6.29
./configure -–disable-all --enable-debug --enable-tokenizer
make
./sapi/cli/php -v
```

## References

- *[Getting Started with PHP Extension Development via PHP-CPP](https://www.sitepoint.com/getting-started-php-extension-development-via-php-cpp/)*
- *[Extension Writing Part I: Introduction to PHP and Zend](https://devzone.zend.com/303/extension-writing-part-i-introduction-to-php-and-zend/)*
- *[PHP扩展编写、PHP扩展调试、VLD源码分析、基于嵌入式Embed SAPI实现opcode查看](http://www.cnblogs.com/LittleHann/p/5164606.html)*
- *[用C/C++扩展你的PHP](http://www.laruence.com/2009/04/28/719.html)*
- *[如何基于 PHP-X 快速开发一个 PHP 扩展](https://segmentfault.com/a/1190000011111074)*