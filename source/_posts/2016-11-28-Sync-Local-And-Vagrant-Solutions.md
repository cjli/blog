---
layout: post
title: 代码同步方案：Between Local and Vagrant 
category: 折腾
tags: [Git, Shell, Linux, Workflow]
---

如果你恰好是用 Sublime Text 作为主力开发工具，并且使用 Vagrant／Linux 等虚拟机作为开发环境，那么自动同步两端代码肯定是必不可少的需求了。

<!-- more -->

> 本实践只针对 *X 系统。

## 共享目录【deprecated】

这个也是个解决办法，之前的文章也提过，就是把网站代码放到虚拟机和主机共享的目录下，直接编辑这个目录下面的文件即可。

但是我遇到有两个问题：

- 共享目录卡

具体表现是，如果进入虚拟机后想要进入网站目录进行一些操作，就会发现很卡。试过 vagrant docs 上的“加速”策略，但我发现不总是生效，且因宿主系统的不同需要进行不同的配置，一个字：累。

- 权限无法设置

对 vagrant 共享目录执行 `chown -R www:www /vagrant` 无效，且默认都是 `root:root`，这样在代码需要读写操作时候就会报错权限不足了。

## SFTP【deprecated】

SFTP？Sublime  的这个插件我试过，可是个人觉得吧，不是很好用，并不能很靠谱的同步（尽管是插件是收费的），特别是当拉取远端仓库的更新到我本地时，这些更新并不能同步到虚拟机，必须要手动打开这些更新的文件，手动保存一遍才能同步到虚拟机。

当然也有可能是我打开姿势不对吧，anyway，我最终不想再操心要如何这个插件如何配置才算正确，我开始把注意力放到 Git 上，毕竟 Git 自带强大的代码“同步”功能了，因此我始终觉得配置配置一定能让 Git 来完美完成这个任务的。

## Git

### Git 本身不支持自动同步

翻了下 Pro Git，也 Google 了一下 git daemon sync 等暗藏现成解决方案可能性的关键词，发现并没有介绍 Git 同步本地和虚拟机代码的栗子。因此肤浅地得出结论：目前 Git 本身是不支持同步功能的。

但是我不死心，革命的经验告诉我，直接不行，总能用一些 shell 命令配合 Git 实现同步效果吧。

> 使用 Git 同步的另一个好处就是，可以减少和 production 的差异，因为线上就是通过 Git 拉取最新的版本来更新的。

### 快糙猛的办法：shell + git

于是我灵光一现的愚笨思路大致是这样的：

本机和虚拟机采用 C/S 模式，两端同时使用 Git 同步。

本机后台不断检测开发分支上的改动，不断提交；虚拟机后台傻瓜式不断拉取本地分支。

貌似这样就应该可以了吧？

### 需要解决的问题

#### 如何知道分支代码是否改动？

可用的 git 指令是：

``` shell
git diff --name-status <branch_name>
```

但是，这也是需要手动执行该命令，然后根据输出结果判断，这根本不是自动触发。此外，我目前知道的唯一能触发执行的 Git Client Hooks 也并没有监听分支代码改动的钩子。

最终我的答案是无需判断，因为判断后决定要不要提交这个过程意义不大，其实开发过程中的代码基本上每分钟都要改变若干次，在很大的概率上，每时每刻分支上的代码都在变化。

因此倒不如简单点，直接运行一个后台 Shell 程序，不管代码有没有变动，每秒都执行一次提交操作，然后服务器每秒也拉取一下就行了。就算很久都没有改动，那么本机不断执行提交命名，和虚拟机不断执行拉取指令也不会对代码造成实际影响。

嗯，于是就这么愉快滴决定了。

#### 如何解决不断提交产生一堆无意义的提交记录？

那么问题来了，如果每秒代码都改变了，那么每秒提交一次的话，开发一天下来，那个提交次数是十分可怕的，且无意义，这对 code review 者来说也是一场灾难，因此如果仅仅是这样明显是行不通的。

如何解决这个问题呢？答案是：git rebase。

##### git rebase

简而言之，git rebase 可以整理很乱的 git commit tree，再简单粗暴的解释就是，git rebase 可以干掉一些不想要出现在 git log 里面的提交记录。因此，problem solved。

下面是使用示例和说明：

###### 删除／合并没意义的提交内容

``` shell
# rebase 最近 100 次提交记录
git rebase -i HEAD~100

# VIM 操作
:%s/pick/fixup/g    # 改为 fixup 模式
:1    # 跳至第一行 修改 fixup 为 pick 或 reword 必须要保留一次提交 否则 git 会终止 rebase 进程

# 终止 rebase 进程
git rebase --abort
# 继续 rebase 进程
git rebase --continue
```

###### rebase 操作的含义

``` shell
pick => use commit => 完整保留本次提交记录和信息
reword => use commit, but edit the commit message => 会保留提交记录 但是会自动指引你到改变提交信息界面
edit => use commit, but stop for amending => 保留提交记录 但是 rebase 进程会停止 需要手动 amend 后再 continue
squash => use commit, but meld into previous commit => 会保留选中的提交记录文本
fixup => like "squash", but discard this commit's log message => 会丢弃选中的提交记录文本
exec => run command (the rest of the line) using shell
drop => remove commit => 会删除提交时仓库对应的改动
```

###### rebase 进程是分支共享的

意思是，你在 test 分支进行了一半的 rebase 操作，如果要在 dev 上执行新的 rebase 命令，则必须先执行 `git rebase --abort` 才可以，否则 git 会报错。

#### 修改最后一次提交说明

当阶段性完成一个功能后，可能需要往远程 git 仓库提交一次，以更新线上代码。这时候如果完全根据脚本的提交记录，则是一些无意义的测试说明文字，因此，有必要将这段时间内开发的东西做一次重新说明。

多亏 git rebase，我们只需要修改最后一个提交说明就行了。

- 直接修改

``` shell
git commit --amend
```

- 追加一个新改动后再修改

``` shell
git add . -A
git commit -a --amend
# Or
# git commit -a --amend -m "cjli deving at `date`"
```

### 使用 Git 同步工作流脚本示例

#### 本机（开发机）同步脚本

``` shell
#! /bin/sh
#### client-git-sync.sh

dev_path="/env/vagrant/www/eme.dev"
dev_branch="cjli"
cd $dev_path

while [ true ]; do
    /bin/sleep 1
    git checkout $dev_branch
    git add . -A
    git commit -a --amend -m "cjli deving at `date`"
done
```

#### 虚拟机同步脚本

``` shell
#! /bin/sh
#### server-git-sync.sh

proj_path="/data/wwwroot/eme.dev"
cd $proj_path

while [ true ]; do
    /bin/sleep 1
    git pull local cjli --rebase=true 
    chown -R www:www $proj_path
done
```

#### 启动脚本

```shell
#!/bin/sh
#### start-sync.sh

nohup ./git-sync > /dev/null 2>&1 &
```

#### 停止脚本

```shell
#!/bin/sh
#### stop-sync.sh

ps -ef | grep 'git-sync' | grep -v grep | awk '{print $2}' | xargs kill -9
```

#### 使用说明

- client-git-sync.sh 和 server-git-sync.sh 需要排除在 git 控制中，写在 .gitignore 文件（避免命名冲突或写多余脚本），然后都命名为 git-sync，所有脚本赋予可执行权限。
- 启动和停止脚本，本地和虚拟机通用。
- 需要提交到远程服务器时，最好先暂停同步，然后修改最后一次提交记录；本地开发时再恢复次流程。

## rsync

- rsync-start-sample.sh

``` shell
#!/bin/sh

/usr/bin/nohup \
/usr/local/bin/rsync \
-avz --progress --no-o --no-g -m \
--chmod=Du=rwx,Dgo=rx,Fu=rw,Fog=r -F \
--exclude='.git/' \
/env/vagrant/www/eme.dev/ \
root@eme.dev:/data/wwwroot/eme.dev \
> /dev/null 2>&1 &
```

由于 rsync 使用 ssh 传输数据，因此虚拟机中的 Linux 如果只是被同步的话，并不需要安装 rsync 也能工作。

- stop-rsync-sample.sh

``` shell
#!/bin/sh

ps -ef | grep 'rsync-start' | grep -v grep | awk '{print $2}' | xargs kill -9
```

### Mac

#### fswatch

是一个后台服务，使用方式很简单，只需要告诉 fswatch 监听哪个路径，以及这个路径文件发生改变是执行什么操作就行了。

``` shell
/usr/bin/nohup \
fswatch -o /path/to/proj/ | xargs -n1 \
/path/to/sync-script/rsync-start > /dev/null 2>&1 &
```

## 参考

- *[Git 修改提交历史](http://glgjing.github.io/blog/2015/01/06/git-xiu-gai-ti-jiao-li-shi/)*