---
layout: post
title: 继续总结 Git 操作
category: 折腾
tag: Git
---

Git 其实真的博(ming)大(ling)精(zhen)深(duo)，继续阶段性总结最近使用到的 Git 命令。

> 快速参考我以前的记录：
>
> - [Git 命令无需记忆](http://cglai.github.io/cheatsheet/Git-Brief.html)
> - [Git 常见问题](http://cglai.github.io/auxiliary/Git-FAQ.html)
>
> (*Never ever GUI*)

<!-- more -->

## CentOS 更新 GIT

``` shell
yum install -y http://opensource.wandisco.com/centos/7/git/x86_64/wandisco-git-release-7-2.noarch.rpm

yum update -y git
```

## .gitignore

- 从版本库中移除（停止追踪）某个文件／夹

``` shell
# 停止追踪未修改且未放到暂存区的文件 file
git rm --cached file
# 停止追踪未修改且未放到暂存区的目录 dir
git rm -r --cached dir

# 停止追踪修改且已经放到暂存区的文件 file
git rm -f --cached file
# 停止追踪修改且已经放到暂存区的目录 dir
git rm -rf --cached dir
```

**说明：**一般情况下，项目最开始就需要在 .gitignore 中规定好哪些文件或者文件夹是不需要纳入 Git 控制的。但是难免会出现中途不想让 Git 控制某个文件的意图，

中途把文件／文件夹移除版本库的提交操作，仍然会存在于提交历史中。

- gitignore 文件写法示例

```gitignore
# `#` 开头为注释

# `*` 表示忽略所有文件
*

# `!` 表示排出某个文件
!.gitignore
```

### 保留子目录的某个文件

注意，如果要 gitignore 目录的所有文件，但是要保留目录（包括子目录）名，这样写是无效的：

``` gitignore
web/
!web/static/.gitkeep
```

这样依然会把 web 目录的所有文件忽略掉，要保留 web 及其子目录 static 的正确的写法如下：

```gitignore
web/*
web/static/*
!web/static/.gitkeep
```

或者：

``` shell
/vendor/*
!/vendor/a
!/vendor/a/b
!/vendor/a/b/c
```

#### 忽略子目录下的所有 swp 结尾的文件

```
/**/*.swp    # 1.8.2+
```

## git commit

- 修改最后的提交说明

```shell
git commit --amend
```

- 添加新的更改

```shell
git add . -A
git commit -a --amend
git commit -a --amend -m "deving at `date`"
```

## git fetch

``` shell
git checkout master
git fetch origin master:mater_tmp
git merge mater_tmp
```

## git rebase

- 使用 rebase 合并分支

和 merge 直接合并分支相比，rebase 可以使提交历史变得线性。比如，要把 test 分支合并到 master。

``` shell
git checkout test
git rebase master    # 这一步和 merge 相同，可能会遇到冲突，也需要人工解决，人工解决后继续 rebase
git rebase --continue

git checkout master
git merge test
```

- 删除／合并没意义的提交内容

```shell
# rebase 最近 100 次提交记录
git rebase -i HEAD~100

# VIM 操作
:%s/pick/fixup/g    # 改为 fixup 模式
:1    # 跳至第一行 修改 fixup 为 pick 或 reword 必须要保留一次提交 否则 git 会终止 rebase 进程

# 终止 rebase 进程
git rebase --abort
# 继续 rebase 进程
git rebase --continue
```

- 修改提交记录的作者信息

If your commit history is A-B-C-D-E-F with F as HEAD, and you want to change the author of C and D, then you would:

- Specify `git rebase -i B`
> if you need to edit A, use `git rebase -i --root`

- change the lines for both C and D from pick to edit
> then save and exit vim editor

- Once the rebase started, it would first pause at C
- You would `git commit --amend --author="Author Name <email@address.com>"`

- Then `git rebase --continue`
- It would pause again at D
- Then you would `git commit --amend --author="Author Name <email@address.com>"` again
- `git rebase --continue`

- The rebase would complete.

> See: <https://stackoverflow.com/questions/3042437/how-to-change-the-commit-author-for-one-specific-commit>

- rebase 操作的含义：

```shell
pick => use commit => 完整保留本次提交记录和信息
reword => use commit, but edit the commit message => 会保留提交记录 但是会自动指引你到改变提交信息界面
edit => use commit, but stop for amending => 保留提交记录 但是 rebase 进程会停止 需要手动 amend 后再 continue
squash => use commit, but meld into previous commit => 会保留选中的提交记录文本
fixup => like "squash", but discard this commit's log message => 会丢弃选中的提交记录文本
exec => run command (the rest of the line) using shell
drop => remove commit => 会删除提交时仓库对应的改动
```

- rebase 进程是分支共享的

意思是，你在 test 进行了一半的 rebase 操作，如果要在 dev 上执行新的 rebase 命令，则必须先执行 `git rebase --abort` 才可以，否则 git 会报错。

## git cherry

> Apply the changes introduced by some existing commits.

- 查看所有在 test 分支而不在 master 分支的提交

``` shell
git cherry -v master test
```

### cherry-pick

> A cherry-pick in Git is like a rebase for a single commit. It takes the patch that was introduced in a commit and tries to reapply it on the branch you’re currently on.

- 在某个分支应用整个仓库中存在的提交

``` shell
git cherry-pick <commit_hash>
```

## git revert

`git revert` is the converse of `git chery-pick`。后者用于将某次 commit 应用到某个分支，前者用于将某次 commit 从某个分支中移除。

- 移除某次非 merge-commit 并自动创建一次新的 commit hash

``` shell
git revert <commit-hash>
```

- 移除某次非 merge-commit，不自动创建新的提交记录

``` shell
git revert -n <commit-hash>
git revert --no-commit <commit-hash>
```

- 撤销当移除某次非 merge-commit 时自动创建了提交的记录的操作

``` shell
git reset HEAD~1 --soft
```

- 移除某次 merge-commit 并自动创建新的 commit hash

``` shell
# 指定 mainline 为合并提交记录中的第 1 个分支
git revert -m 1 <commit-hash>

# 指定 mainline 为合并提交记录中的第 2 个分支
git revert -mainline 2 <commit-hash>
```

> 在你合并两个分支并试图撤销时，Git 并不知道你到底需要保留哪一个分支上所做的修改。从 Git 的角度来看，master 分支和 dev 在地位上是完全平等的，只是在 workflow 中，master 被人为约定成了「主分支」。
>
> 于是 Git 需要你通过 m 或 mainline 参数来指定「主线」。merge commit 的 parents 一定是在两个不同的线索上，因此可以通过 parent 来表示「主线」。m 参数的值可以是 1 或者 2，对应着 parent 在 merge commit 信息中的顺序。

## tag

- 获取远程 tag

``` shell
# 允许 fetch 获取 tag
git config --unset-all remote.origin.fetch
git config --add remote.origin.fetch '+refs/heads/*:refs/remotes/origin/*'
git config --add remote.origin.fetch '+refs/tags/*:refs/tags/*'

# 获取远程所有 tags
git fetch --tags

# 获取远程某个 tag
git fetch origin +refs/tags/<TAG_NAME>
```

## 重置／撤销

- 重置单个已修改但未提交的文件（工作区）为上次提交时的状态

``` shell
# 1. 有风险的写法
git checkout /path/to/modified_file

# 2. 安全写法
git checkout HEAD -- /path/to/modified_file

# 3. 或者：移除未被跟踪的文件和目录
git clean -fd

# OR 对其他分支中的文件进行这种操作
# man git-checkout
```

由于 `checkout` 命令同样可以用于操作分支，因此，如果刚好出现要重置的文件名和某个存在的分支名同名的话，则最好使用第二种写法。

- 重置已经（失误）提交的某个文件到某个版本

``` shell
git checkout <commit> /path/to/file

# some modifications maybe ...

git commit - 'revert and re-update'
```

- 重置当前工作区的所有未提交的改动（**谨慎使用**）

``` shell
git reset --hard
```

- 重置已提交到版本库中的提交

``` shell
git reset --hard HEAD^      # 回退到前 1 个版本
git reset --hard HEAD^^     # 回退到前 2 个版本
git reset --hard HEAD~10    # 回退到前 10 个版本
git reset --hard <hash_long>    # 通过完整 hash 回退到某个确定的版本
git reset --hard <hash_short>   # 通过短 hash 回退到某个确定的版本
```

- 重置已推送到远程仓库的提交：

``` shell
git reset --hard <commit-hash>
git push -f origin master
```

如果推送失败，只是 GitLab 或 GitHub 中设置了保护分支，紧急情况可以暂时取消，回滚后再开启保护。

- 将改动从暂存区撤销到工作区（不会被 commit）

``` shell
git reset HEAD # 重置所有改动到工作区
git reset /path/to/file # 重置指定路径的改动到工作区
```

- 暂存改动

``` shell
git add -A
git stash
git stash clear
```

> Use git stash when you want to record the current state of the working directory and the index, but want to go back to a clean working directory.
>
> The command saves your local modifications away and reverts the working directory to match the HEAD commit.

- 查看暂存内容

``` shell
git stash show -p stash@{1}
```

## git diff

``` shell
git diff            # Diff the working copy with the index
git diff --cached   # Diff the index with HEAD
git diff HEAD       # Diff the working copy with HEAD

# 查看某个文件两次提交记录的区别
git diff <hash-1> <hash-2> > /path/to/file

# 查看某个文件与某个分支的区别
git diff origin/master:./ -- /path/to/file

# 查看某个文件和最近一次提交的区别
git diff HEAD^:./ -- /path/to/file

# 查看某个文件和某个暂存的区别
git diff stash@{0}:./ -- /path/to/file

# 查看某个文件和某次提交的区别
git diff {hash}:./ -- /path/to/file

# 查看合并分支后的冲突文件
git diff --name-only --diff-filter=U
```

## git branch

- 删除本地所有 master 和当前分支以外的分支

``` shell
git branch | grep -v "master" | xargs git branch -D
git branch | egrep -v "(master|\*)" | xargs git branch -D
```

> Note that lowercase -d won't delete a "non fully merged" branch.

- 重命名分支

``` shell
git branch -m <oldname> <newname>    # 重命名 oldname 分支为 newname
git branch -m <newname>    # 重命名当前分支
```

> `m` is for move(`mv`).

## git show

- 查看某次提交的改动详情

``` shell
git show <commit-hash>
```

- 查看某次提交的改动简要统计

``` shell
git show <commit-hash> --stat
```

## git log

- 查看历史提交改动详情

``` shell
git log -p
```

- 统计某人代码量

``` shell
git log --author="$(git config --get user.name)" --pretty=tformat: --numstat | awk '{ add += $1 ; subs += $2 ; loc += $1 - $2 } END { printf "added lines: %s removed lines : %s total lines: %s\n",add,subs,loc }' -
```

- 统计仓库所有开发者代码量

```shell
git log --format='%aN' | sort -u | while read name; do echo -en "$name\t"; git log --author="$(git config --get user.name)" --pretty=tformat: --numstat | awk '{ add += $1; subs += $2; loc += $1 - $2 } END { printf "added lines: %s, removed lines: %s, total lines: %s\n", add, subs, loc }' -; done
```

- 仓库提交者排名前 5（如果看全部，去掉 head 管道）

``` shell
git log --pretty='%aN' | sort | uniq -c | sort -k1 -n -r | head -n 5
```

- 仓库提交者（邮箱）排名前 5

``` shell
git log --pretty=format:%ae | gawk -- '{ ++c[$0]; } END { for(cc in c) printf "%5d %s\n",c[cc],cc; }' | sort -u -n -r | head -n 5 
```

这个统计可能不会太准，因为很多人有不同的邮箱，但会使用相同的名字。

- 贡献者统计

``` shell
git log --pretty='%aN' | sort -u | wc -l
```

- 提交数统计

``` shell
# 提交总数
git log --oneline | wc -l

# 每个作者的提交数
git shortlog -s
git shortlog --numbered --summary
git log --author="cjli" --oneline --shortstat

git shortlog -s -n --all --no-merges
```

- 添加或修改的代码行数

``` shell
git log --stat | perl -ne 'END { print $c } $c += $1 if /(\d+) insertions/;'

git log --shortstat --pretty="%cE" | sed 's/\(.*\)@.*/\1/' | grep -v "^$" | awk 'BEGIN { line=""; } !/^ / { if (line=="" || !match(line, $0)) {line = $0 "," line }} /^ / { print line " # " $0; line=""}' | sort | sed -E 's/# //;s/ files? changed,//;s/([0-9]+) ([0-9]+ deletion)/\1 0 insertions\(+\), \2/;s/\(\+\)$/\(\+\), 0 deletions\(-\)/;s/insertions?\(\+\), //;s/ deletions?\(-\)//' | awk 'BEGIN {name=""; files=0; insertions=0; deletions=0;} {if ($1 != name && name != "") { print name ": " files " 个文件被改变, " insertions " 行被插入(+), " deletions " 行被删除(-), " insertions-deletions " 行剩余"; files=0; insertions=0; deletions=0; name=$1; } name=$1; files+=$2; insertions+=$3; deletions+=$4} END {print name ": " files " 个文件被改变, " insertions " 行被插入(+), " deletions " 行被删除(-), " insertions-deletions " 行剩余";}'

git ls-files -z | xargs -0n1 git blame -w | ruby -n -e '$_ =~ /^.*\((.*?)\s[\d]{4}/; puts $1.strip' | sort -f | uniq -c | sort -n
```

- 生成日报

``` shell
alias dg='git log --pretty=format:"- %s" --author="$(git config --get user.name)" --since=9am > ~/.today.md && subl ~/.today.md'

git config alias.rb "log --pretty=format:'- %s' --author='$(git config --get user.name)' --since=9am"
```

### git status

- 合并分支后查看双方修改的冲突文件

``` shell
git status --short | grep "^UU "
```

## 核心概念

### branch／tag／ commit

- commit

Git 中有且只有三大类型：blob、tree object、commit。

- branch

branch 并不是 Git 中过的特殊类型，一个 branch 无非是对一个被命名 commit 的引用，一个 branch 只是一个名字。

> 因为一个提交可能有一个或者多个父代提交，而这些父代提交又有父代提交，这就允许将某一个提交就可以看作一个分支，因为它拥有全部的修改至它本身的历史。

- tag

tag 和 commit 本身也是相同的，唯一的不同是 tag 可以有自己的描述。（不然怎么能叫“标记”呢）

## 配置

``` shell
# 查看当前配置
git config --local -l    # .git/config
git config --help

# 全局配置
git config --global xxxx

# 举例：让 Git 忽略大小写
git config core.ignorecase true
git config --global core.ignorecase true
```

- 配置默认编辑器 vi 为 vim 避免插件报错：

```
git config --global core.editor "vim"

# Or：
export GIT_EDITOR=vim

export VISUAL=vim
export EDITOR="$VISUAL"
```

### Git 命令行代理

- HTTP 代理，形如 `https://github.com/owner/repo.git`

```
git config --global http.proxy "http://127.0.0.1:8080"
git config --global https.proxy "http://127.0.0.1:8080"
```

- Socks5 代理，形如 `git clone git@github.com:owner/repo.git`

```
git config --global http.proxy "socks5://127.0.0.1:1080"
git config --global https.proxy "socks5://127.0.0.1:1080"
```

- 取消代理

``` shell
git config --global --unset http.proxy
git config --global --unset https.proxy
```

- SSH 代理

修改 `~/.ssh/config` 文件，不存在则新建：

```
Host github.com
   HostName github.com
   User git
   # 走 HTTP 代理
   # ProxyCommand socat - PROXY:127.0.0.1:%h:%p,proxyport=8080
   # 走 Socks5 代理（如 Shadowsocks）
   # ProxyCommand nc -v -x 127.0.0.1:1080 %h %p
```

## FAQ

### git rebase 和 git merge

> 有一种观点认为，仓库的提交历史即是 记录实际发生过什么。 它是针对历史的文档，本身就有价值，不能乱改。从这个角度看来，改变提交历史是一种亵渎，你使用 谎言 掩盖了实际发生过的事情。 如果由合并产生的提交历史是一团糟怎么办？既然事实就是如此，那么这些痕迹就应该被保留下来，让后人能够查阅。

> 另一种观点则正好相反，他们认为提交历史是 项目过程中发生的事。 没人会出版一本书的第一版草稿，软件维护手册也是需要反复修订才能方便使用。 持这一观点的人会使用 rebase 及 filter-branch 等工具来编写故事，怎么方便后来的读者就怎么写。

> 现在，让我们回到之前的问题上来，到底合并还是变基好？希望你能明白，这并没有一个简单的答案。Git 是一个非常强大的工具，它允许你对提交历史做许多事情，但每个团队、每个项目对此的需求并不相同。 既然你已经分别学习了两者的用法，相信你能够根据实际情况作出明智的选择。

> 总的原则是，只对尚未推送或分享给别人的本地修改执行变基操作清理历史， 从不对已推送至别处的提交执行变基操作，这样，你才能享受到两种方式带来的便利。

### fatal: unable to access ssl connect error

> See: <https://stackoverflow.com/questions/48938385/github-unable-to-access-ssl-connect-error>

``` shell
sudo yum update -y nss curl libcurl
```

### `git status` utf-8 qutoted octal notation

``` shell
git config --golbal core.quotepath off
```

## 参考

- *[How to install latest version of git on CentOS 6.x/7.x](http://stackoverflow.com/questions/21820715/how-to-install-latest-version-of-git-on-centos-6-x-7-x)*
- *[GIT pull/fetch from specific tag](https://stackoverflow.com/questions/3964368/git-pull-fetch-from-specific-tag)*
- *[统计本地Git仓库中不同贡献者的代码行数的一些方法](http://eisneim.github.io/articles/2014-8-16-git-analize-caulculate-contribution.html)*
- *[“git diff” does nothing](https://stackoverflow.com/questions/3580608/git-diff-does-nothing)*
- *[Undo a particular commit in Git that's been pushed to remote repos](https://stackoverflow.com/questions/2318777/undo-a-particular-commit-in-git-thats-been-pushed-to-remote-repos)*
- *[Git 撤销合并](http://blog.psjay.com/posts/git-revert-merge-commit/)*
