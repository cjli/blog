---
layout: post
title: Web 中的认证和授权
category: Web
tags: [Auth, 认证, 授权]
---

「认证」和「授权」是两个不同的概念，同时在 Web 开发中是一个必备的需求。

<!-- more -->

## Authentication vs Authorization

Authentication，一般指身份验证，又称“验证”、“鉴权”，是指通过一定的手段，完成对用户身份的确认，即证明“你是谁”。

Authorization，一般指授权、委托、向…授予职权或权力许可、申请（操作）批准等意思，即证明“你可以做什”。

在 Web 开发中，一般顺序都是先 Authentication，比如使用账号密码登录我们的网站，然后在 Authorization，比如登录成功后可以查看自己的订单详情。

当然了，有些情况可以只需要 Authorization 而不需要 Authentication，或者说不需要显示地 Authentication，因为往往 Authorization 过程已经默认进行 Authentication 检查了。

## 为什么 Web/HTTP 需要认证和授权？

因为 HTTP 是无状态的，前一次 HTTP 请求和后一次 HTTP 请求从技术角度没有任何关联。但是我们的网站从业务上往往需要人为地确认请求的状态，最常见的就是确认本次请求的用户状态，并只对通过认证和授权用户提供所在权限范围内的服务。

## Web 认证的几种常见方式

### [HTTP Basic](http://tools.ietf.org/html/rfc2617)/Password

这是一种很旧、很简单的认证技术。基本原理就是使用用户账号 `username` 和密码 `password` 进行认证。细节上，步骤如下：

- 将 `username` 和 `password` 使用英文冒号 `:` 拼接起来得到字符串 A： `username:password` 

- 对 A 进行 Base64 编码，得到字符串 B，比如 `base64('cjli:iljc')` 得到 `Y2psaTppbGpj`

- 将 B 放到 HTTP 请求的 `Authorization` 请求头中，同时声明请求认证类型为 `Basic`：`Authorization: Basic Y2psaTppbGpj`，然后发起 HTTP 请求

- 如果服务器获取不到 `Authorization` 请求头或者获取到的内容不正确，则返回 401 响应

这种方式的缺点就是不安全，因为用户账号和密码是以明文的形式直接在网络中传输的，Base64 是可逆的编码，而不是加密。

所以这种方式现实中，最好要在 HTTPS 环境下使用，并且建议小范围使用，比如公司内网的一些简单认证。

### [Digest Access](https://en.wikipedia.org/wiki/Digest_access_authentication)

HTTP 摘要认证。简单来说，和上面的 `Basic` 形式的认证方式不同的地方在，请求头 `Authorization` 中指明的认证类型，应要求为 `Digest`，然后气候紧跟一堆定义本次请求有关的必要参数。类似如下：

``` http
GET /dir/index.html HTTP/1.0
Host: localhost
Authorization: Digest username="Mufasa",
                     realm="testrealm@host.com",
                     nonce="dcd98b7102dd2f0e8b11d0f600bfb0c093",
                     uri="%2Fcoolshell%2Fadmin",
                     qop=auth,
                     nc=00000001,
                     cnonce="0a4f113b",
                     response="6629fae49393a05397450978507c4ef1",
                     opaque="5ccc069c403ebaf9f0171e9517f40e41"
```

因为没有在网上传递用户的密码，相对会比较安全，而且，其并不需要是否TLS/SSL的安全链接。

但是，别看这个算法这么复杂，最后你可以发现，整个过程其实关键是 password，这个 password 如果不够得杂，其实是可以被暴力破解的。

而且，整个过程是非常容易受到中间人攻击——比如一个中间人告诉客户端需要的 Basic 的认证方式 或是 老旧签名认证方式（RFC2069）。

> 由此可见，复杂的东西不一定是最好的。

由于本方式复杂且不安全，所以不在这里介绍其具体步骤，即不推荐使用这样的方式。

现实应用中，最常见的是这种方式的简化版：用户登录成功后，生成一个和用户密码完全无关、完全随机的 MD5 字符串，然后后续的请求中带上这个字符串，服务器便可进行认证。

### [JWT](https://jwt.io)

JSON Web Tokens。也是一种基于 MAC 的认证方式，在客户端和服务器之间的认证场景中被广泛使用。

形式上，JWT 就是字符串，就是 Token——一个临时用户权证一样，主要由 Header、Payload、signature 三部分组成，中间以英文句点 `.` 隔开。

用户使用用户名和口令到认证服务器上请求认证，认证通过后由服务器签发 JWT 返回给客户端。认证服务器生成 JWT 步骤如下：

- 认证服务器还会生成一个 Secret Key（密钥） 

- 对JWT Header和 JWT Payload分别求Base64。在Payload可能包括了用户的抽象ID和的过期时间。

- 用密钥对JWT签名：`HMAC-SHA256(SecertKey, Base64UrlEncode(JWT-Header)+'.'+Base64UrlEncode(JWT-Payload));`

- 拼接：`base64(header).base64(payload).signature`

客户端使用 JWT 向应用服务器发送相关的请求。当应用服务器收到请求后：

- 应用服务会检查 JWT Token，确认签名是正确的。

- 然而，因为只有认证服务器有这个用户的Secret Key（密钥），所以，应用服务器得把JWT Token传给认证服务器。

- 认证服务器通过JWT Payload 解出用户的抽象ID，然后通过抽象ID查到登录时生成的Secret Key，然后再来检查一下签名。

- 认证服务器检查通过后，应用服务就可以认为这是合法请求了。

上面的这个过程，是在认证服务器上为用户动态生成 Secret Key的，应用服务在验签的时候，需要到认证服务器上去签，这个过程增加了一些网络调用，所以，JWT除了支持HMAC-SHA256的算法外，还支持RSA的非对称加密的算法。

使用RSA非对称算法，在认证服务器这边放一个私钥，在应用服务器那边放一个公钥，认证服务器使用私钥加密，应用服务器使用公钥解密，这样一来，就不需要应用服务器向认证服务器请求了。

但是，RSA 是一个很慢的算法，所以，虽然你省了网络调用，但是却费了CPU，尤其是Header和Payload比较长的时候。所以，一种比较好的玩法是，如果我们把header 和 payload简单地做SHA256，这会很快，然后，我们用RSA加密这个SHA256出来的字符串，这样一来，RSA算法就比较快了，而我们也做到了使用RSA签名的目的。然后，我们只需要使用一个机制在认证服务器和应用服务器之间定期地换一下公钥私钥对就好了。

最后，一般认证授权服务器只会检查 JWT 签名是否正确，不会存储，因此，JWT 默认是不支持主动注销的，因此在这种情况下不建议给 JWT 太长的有效期。

### [HMAC](https://en.wikipedia.org/wiki/HMAC)

Hash-based Authentication Code，基于 [MAC](https://en.wikipedia.org/wiki/Message_authentication_code) 和 Hash 的签名技术。

本质来讲，由于担心请求中的消息在传递的过程中被人修改，所以，我们需要用对消息进行一个 MAC 算法，得到一个摘要字串，然后，接收方得到消息后，进行同样的计算，然后比较这个 MAC 字符串，如果一致，则表明没有被修改过。其中的计算方法用的便是 Hash。

以 [AWS S3](https://czak.pl/2015/09/15/s3-rest-api-with-curl.html) 举例说明 HMAC 认证步骤：

- App ID

在认证之前，每个客户端都会被分配一个 App ID。

这个东西跟验证没有关系，只是用来区分是谁来调用 API 的，不是用来做身份认证的。

此外，App ID 还用来映射一个用于加密的密钥，这样一来，我们就可以在服务器端进行相关的应用 API 密钥管理，我们可以生成若干个密钥对（AppID <=> AppSecret），并可以有更细粒度的操作权限管理。

- 把 HTTP 的请求（方法、URI、查询字串、头、签名头，body）打个包叫 CanonicalRequest，作个SHA-256的签名，然后再做一个base16的编码

- 把上面的这个签名和签名算法 AWS4-HMAC-SHA256、时间戳、Scop，再打一个包，叫 StringToSign

- 准备签名，用 AWSSecretAccessKey来对日期签一个 DataKey，再用 DataKey 对要操作的Region签一个 DataRegionKey ，再对相关的服务签一个DataRegionServiceKey ，最后得到 SigningKey

- 用第三步的 SigningKey来对第二步的 StringToSign 签名

- 最后，发出HTTP Request时，在HTTP头的 Authorization 字段中放入如下的信息：

``` http
Authorization: AWS4-HMAC-SHA256 
               Credential=AKIDEXAMPLE/20150830/us-east-1/iam/aws4_request, 
               SignedHeaders=content-type;host;x-amz-date, 
               Signature=5d672d79c15b13162d9279b0855cfba6789a8edb4c82c400e06b5924a6f2b5d7
```

这种认证的方式好处在于，AppID 和 AppSecretKey 是由服务器签发的，所以，是可以被管理的。但是不好的地方在于，这个东西没有标准 ，所以，各家的实现很不一致。

最后注意：由于涉及到了签名密钥，因此 HMAC 不适用于客户端与服务器的认证，而更适用于服务器与服务器之间的签名认证和授权。

### OAuth

OAuth 是一种授权协议，只是为用户资源的授权提供了一个安全的、开放而又简易的标准。

#### OAuth 1.0

用户为了想使用一个第三方的网络打印服务来打印他在某网站上的照片，但是，用户不想把自己的用户名和口令交给那个第三方的网络打印服务，但又想让那个第三方的网络打印服务来访问自己的照片，为了解决这个授权的问题，OAuth这个协议就出来了。

在这个例子中这个协议有三个角色：User（照片所有者-用户）、Consumer（第三方照片打印服务）、Service Provider（照片存储服务）。

##### 3-legged flow

所以，又叫 3-legged flow，三脚流程。整个授权过程是这样的：

- Consumer（第三方照片打印服务）需要先上Service Provider获得开发的 Consumer Key 和 Consumer Secret

- 当 User 访问 Consumer 时，Consumer 向 Service Provider 发起请求请求Request Token （需要对HTTP请求签名）

- Service Provider 验明 Consumer 是注册过的第三方服务商后，返回 Request Token（`oauth_token`）和 Request Token Secret （`oauth_token_secret`）

- Consumer 收到 Request Token 后，使用HTTP GET 请求把 User 切到 Service Provider 的认证页上（其中带上Request Token），让用户输入他的用户和口令。

- Service Provider 认证 User 成功后，跳回 Consumer，并返回 Request Token （`oauth_token`）和 Verification Code（`oauth_verifier`）

- 接下来就是签名请求，用Request Token 和 Verification Code 换取 Access Token （`oauth_token`）和 Access Token Secret (`oauth_token_secret`)

- 最后使用Access Token 访问用户授权访问的资源。

##### 2-legged flow

OAuth 1.0 也有不需要用户参与的，只有Consumer 和 Service Provider 的，也就是 2-legged flow 两脚流程，其中省掉了用户认证的事。过程如下：

- Consumer（第三方照片打印服务）需要先上Service Provider获得开发的 Consumer Key 和 Consumer Secret

- Consumer 向 Service Provider 发起请求请求Request Token （需要对HTTP请求签名）

- Service Provider 验明 Consumer 是注册过的第三方服务商后，返回 Request Token（`oauth_token`）和 Request Token Secret （`oauth_token_secret`）

- Consumer 收到 Request Token 后，直接换取 Access Token （`oauth_token`）和 Access Token Secret (`oauth_token_secret`)

- 最后使用 Access Token 访问用户授权访问的资源。

##### 签名

有两个密钥，一个是Consumer注册Service Provider时由Provider颁发的 Consumer Secret，另一个是 Token Secret。

签名密钥就是由这两具密钥拼接而成的，其中用 &作连接符。假设 Consumer Secret 为 `j49sk3j29djd` 而 Token Secret 为 `dh893hdasih9` 那个，则签名密钥为：`j49sk3j29djd&dh893hdasih9`。

在请求 Request/Access Token 的时候需要对整个HTTP请求进行签名（使用HMAC-SHA1和HMAC-RSA1签名算法），请求头中需要包括一些OAuth需要的字段，如：

- Consumer Key： 也就是所谓的AppID
- Token： Request Token 或 Access Token
- Signature Method：签名算法比如 HMAC-SHA1
- Timestamp：过期时间
- Nonce：随机字符串
- Call Back：回调URL

#### OAuth 2.0

从Digest Access， 到AppID+HMAC，再到JWT，再到OAuth 1.0，这些API认证都是要向Client发一个密钥（或是用密码）然后用HASH或是RSA来签HTTP的请求，这其中有个主要的原因是，以前的HTTP是明文传输，所以，在传输过程中很容易被篡改，于是才搞出来一套的安全签名机制，所以，这些认证可以在HTTP明文协议下使用。

这些使用签名方式是比较复杂的，对于开发者来说，也是很不友好的，在组织签名的那些HTTP报文的时候，各种 URLEncode和Base64，还要对 Query 的参数进行排序，然后有的方法还要层层签名，非常容易出错，另外，这种认证的安全粒度比较粗，授权也比较单一，对于有终端用户参与的移动端来说也有点不够。所以，在2012年的时候，OAuth 2.0 的 RFC 6749 正式放出。

OAuth 2.0依赖于TLS/SSL的链路加密技术（HTTPS），完全放弃了签名的方式，认证服务器再也不返回什么 token secret 的密钥了，所以，OAuth 2.0是完全不同于1.0 的，也是不兼容的。

OAuth 2.0 定义了四种授权方式：授权码模式（Authorization Code）、客户端模式（Client Credentials）、简化模式（Implicit）、密码模式（Resource Owner Password Credentials）。其中最常见最常用的就是前两种授权方式。

##### Authorization Code Flow - 3 legged

Authorization Code 是最常使用的OAuth 2.0的授权许可类型，它适用于用户给第三方应用授权访问自己信息的场景。

这个 Flow 的步骤如下：

- 当用户（Resource Owner）访问第三方应用（Client）的时候，第三方应用会把用户带到认证服务器（Authorization Server）上去，主要请求的是 `/authorize API`，其中的请求方式如下：

``` http
https://login.authorization-server.com/authorize?
        client_id=6731de76-14a6-49ae-97bc-6eba6914391e
        &response_type=code
        &redirect_uri=http%3A%2F%2Fexample-client.com%2Fcallback%2F
        &scope=read
        &state=xcoiv98CoolShell3kch
```

`client_id` 为第三方应用的 App ID；
`response_type=code` 为告诉认证服务器，走 Authorization Code Flow；
`redirect_uri` 是跳转回第三方应用的 URL；
`scope` 是相关的权限；
`state` 是一个随机的字符串，主要用于防 CSRF 攻击。

- 当Authorization Server收到这个URL请求后，其会通过 `client_id` 来检查 `redirect_uri` 和 `scope` 是否合法，如果合法，则弹出一个页面，让用户授权（如果用户没有登录，则先让用户登录，登录完成后，出现授权访问页面）。

- 当用户授权同意访问以后，Authorization Server 会跳转回 Client ，并以其中加入一个 Authorization Code。 如下所示：

``` http
https://example-client.com/callback?
        code=Yzk5ZDczMzRlNDEwYlrEqdFSBzjqfTG
        &state=xcoiv98CoolShell3kch
```

这个链接，是第 1 步中的 `redirect_uri`，其中的 state 的值也和第 1 步的 `state` 一样。

- 接下来，Client 就可以使用 Authorization Code 获得 Access Token。其需要向 Authorization Server 发出如下请求：

``` http
POST /oauth/token HTTP/1.1
Host: authorization-server.com
 
code=Yzk5ZDczMzRlNDEwYlrEqdFSBzjqfTG
&grant_type=code
&redirect_uri=https%3A%2F%2Fexample-client.com%2Fcallback%2F
&client_id=6731de76-14a6-49ae-97bc-6eba6914391e
&client_secret=JqQX2PNo9bpM0uEihUPzyrh
```

- 如果没什么问题，Authorization Server 会返回如下信息：

``` json
{
  "access_token": "iJKV1QiLCJhbGciOiJSUzI1NiI",
  "refresh_token": "1KaPlrEqdFSBzjqfTGAMxZGU",
  "token_type": "bearer",
  "expires": 3600,
  "id_token": "eyJ0eXAiOiJKV1QiLCJhbGciO.eyJhdWQiOiIyZDRkM..."
}
```

其中，`access_token` 就是访问请求令牌了，`refresh_token` 用于刷新 `access_token`，`id_token ` 是 JWT，其中一般会包含用户的 OpenID。

- 接下来就是用 Access Token 请求用户的资源了。

``` http
GET /v1/user/pictures
Host: https://example.resource.com

Authorization: Bearer iJKV1QiLCJhbGciOiJSUzI1NiI
```

##### Client Credential Flow - 2 legged

Client Credential 是一个简化版的API认证，主要是用于认证服务器到服务器的调用，也就是没有用户参与的的认证流程。

Client Credentials Grant 指客户端以自己的名义，而不是以用户的名义，向"服务提供商"进行认证。严格地说，客户端模式并不属于OAuth框架所要解决的问题。在这种模式中，用户直接向客户端注册，客户端以自己的名义要求"服务提供商"提供服务，其实不存在授权问题。

这个过程非常简单，本质上就是Client用自己的 `client_id` 和 `client_secret` 向Authorization Server 要一个 Access Token，然后使用Access Token访问相关的资源。

- 客户端向认证服务器进行身份认证，并要求一个访问令牌。Client 请求示例：

``` http
POST /token HTTP/1.1
Host: server.example.com
Content-Type: application/x-www-form-urlencoded

grant_type=client_credentials
&client_id=czZCaGRSa3F0Mzpn
&client_secret=7Fjfp0ZBr1KtDRbnfVdmIw
```

`grant_type` 表示授权类型，此处的值固定为 `clientcredentials`，必选项；`scope` 表示权限范围。

- 认证服务器确认无误后，向客户端提供访问令牌。Authorization Server 返回示例：

``` json
{
  "access_token":"MTQ0NjJkZmQ5OTM2NDE1ZTZjNGZmZjI3",
  "token_type":"bearer",
  "expires_in":3600,
  "refresh_token":"IwOGYzYTlmM2YxOTQ5MGE3YmNmMDFkNTVk",
  "scope":"create"
}
```

##### Implicit Grant Type

简化模式，不通过第三方应用程序的服务器，直接在浏览器中向认证服务器申请令牌，跳过了"授权码"这个步骤，因此得名。所有步骤在浏览器中完成，令牌对访问者是可见的，且客户端不需要认证。

步骤如下：

（A）客户端将用户导向认证服务器。

``` http
GET /authorize?response_type=token&client_id=s6BhdRkqt3&state=xyz
    &redirect_uri=https%3A%2F%2Fclient%2Eexample%2Ecom%2Fcb HTTP/1.1

Host: server.example.com
```

（B）用户决定是否给于客户端授权。

（C）假设用户给予授权，认证服务器将用户导向客户端指定的"重定向URI"，并在URI的Hash部分包含了访问令牌。

``` http
HTTP/1.1 302 Found
Location: http://example.com/cb#access_token=2YotnFZFEjr1zCsicMWpAA
       &state=xyz&token_type=example&expires_in=3600
```

（D）浏览器向资源服务器发出请求，其中不包括上一步收到的Hash值。

（E）资源服务器返回一个网页，其中包含的代码可以获取Hash值中的令牌。

（F）浏览器执行上一步获得的脚本，提取出令牌。

（G）浏览器将令牌发给客户端。

##### Resource Owner Password Credentials Grant

密码模式，用户向客户端提供自己的用户名和密码。客户端使用这些信息，向"服务商提供商"索要授权。

在这种模式中，用户必须把自己的密码给客户端，但是客户端不得储存密码。这通常用在用户对客户端高度信任的情况下，比如客户端是操作系统的一部分，或者由一个著名公司出品。而认证服务器只有在其他授权模式无法执行的情况下，才能考虑使用这种模式。

步骤如下：

（A）用户向客户端提供用户名和密码。

（B）客户端将用户名和密码发给认证服务器，向后者请求令牌。

（C）认证服务器确认无误后，向客户端提供访问令牌。

``` http
POST /token HTTP/1.1
Host: server.example.com
Authorization: Basic czZCaGRSa3F0MzpnWDFmQmF0M2JW
Content-Type: application/x-www-form-urlencoded

grant_type=password&username=johndoe&password=A3ddj3w
```

##### 更新令牌

如果用户访问的时候，客户端的"访问令牌"已经过期，则需要使用"更新令牌"申请一个新的访问令牌。

客户端发出更新令牌的HTTP请求，包含以下参数：

- `grant_type`：表示使用的授权模式，此处的值固定为 `refreshtoken`，必选项。
- `refresh_token`：表示早前收到的更新令牌，必选项。
- `scope`：表示申请的授权范围，不可以超出上一次申请的范围，如果省略该参数，则表示与上一次一致。

示例：

```
POST /token HTTP/1.1
Host: server.example.com
Authorization: Basic czZCaGRSa3F0MzpnWDFmQmF0M2JW
Content-Type: application/x-www-form-urlencoded

grant_type=refresh_token&refresh_token=tGzv3JOkF0XG5Qx2TlKWIA
```

> 说明：这里的 `Authorization: Basic czZCaGRSa3F0MzpnWDFmQmF0M2JW`，应该是对于应用client的授权认证，这样就相当于有两层保护，一层是认证客户端，一层是认证用户。

## Web 中基于认证授权的应用场景

### SSO

Single Sign On，指在多系统应用群的环境下，只需要统一登录一个系统，通常是认证授权系统，便可在其他所有系统中得到授权而无需再次登录。

SSO 包括单点登录与单点注销两部分，即退出登录也只需要在一个系统统一退出。

SSO 可以统一管理账号的登录登出，其余的业务系统便可以不再重复实现登录登出的逻辑。

SSO 的缺点：认证授权系统是集中式的，所有业务系统均需要和认证授权系统通信，一定程度上增加了复杂性，认证授权服务器如果设计不合理，容易成为系统的性能瓶颈。

### 认证授权中心

所有和认证、授权有关的事务均由专门的认证授权服务器（集群）统一处理，方便管理，也使得业务子系统的开发更聚焦于其自身的业务逻辑而不用在关系认证和授权相关的东西。

## 总结

上面介绍了常用的一些和 Web 认证授权有关的方式及其原理，但是其实在现实使用中，我们完全可以借鉴其正确的思想，然后根据实际情况进行定制，本着简单和安全的原则即可。

## FAQ

### nginx 过滤掉了自定义 HTTP Header？

检查 Header 命名中是否含有下划线 `_` 以及 nginx 的 `underscores_in_headers` 配置项是否为 `off`。

如果有下划线，则要么重新选择 Header 命名，或者配置 nginx 允许包含下划线的 Header。

### Authorization 中的 Basic／Bearer 代表什么意思，是否可以省略？

只是表明认证类型。如果服务器提供了多种认证策略，而不指定类型，则服务器无法正确验证请求的合法性。

W3C 定义的 HTTP 1.0 协议中定义的原型为：`Authorization: <type> <credentials>`。

### [接口返回的 `null` 字段 crash iOS](https://blog.csdn.net/xieliang5210/article/details/50351245)

- 第一种方法：使用分类给字典添加一个类方法，将字典中的 null 值全部替换为空字符串

- 第二种方法：利用 AFNetworking 的自动解析，去除掉值为 null 的键值对

- 第三种方法：第三方 Category：<https://github.com/nicklockwood/NullSafe>

## 参考

- [HTTP API 认证授权术 | | 酷 壳 - CoolShell](https://coolshell.cn/articles/19395.html)
- [理解OAuth 2.0](http://www.ruanyifeng.com/blog/2014/05/oauth_2_0.html)
- [OAuth 2 VS JSON Web Tokens: How to secure an API](http://www.seedbox.com/en/blog/2015/06/05/oauth-2-vs-json-web-tokens-comment-securiser-un-api/)
- [How can I access request headers that don't appear in `$_SERVER`?](https://stackoverflow.com/questions/827060/how-can-i-access-request-headers-that-dont-appear-in-server)
- [Role-based access control](https://en.wikipedia.org/wiki/Role-based_access_control)
- [API Design Guide](https://cloud.google.com/apis/design/)
- [一分钟讲清楚 Authentication 和 Authorization](https://www.lijinma.com/blog/2017/02/17/authentication-and-authorization/)
- [单点登录原理与简单实现](http://www.cnblogs.com/ywlaker/p/6113927.html)
- [OAuth与SSO、REST有哪些区别与联系](http://blog.51cto.com/favccxx/1635938)
- [JSW：The Complete Guide to JSON Web Tokens](https://blog.angular-university.io/angular-jwt/)
