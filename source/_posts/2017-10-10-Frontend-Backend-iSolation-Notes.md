---
Layout: post
title: 简谈前后端分离
category: Profession
tags: [前端, 后端, 协作开发]
---

最近个人愈发认为：全栈型 Web 框架（MVC）更适用于全栈型开发者。

<!-- more -->

因为如果在工作职责上，只要有前端后端之分，那么实际中使用全栈型 Web 框架，必然会出现前后端沟通成本的问题，前端会经常来找后端问：这个模版标签怎么用来的？为什么我按照之前的例子写一个页面我看不到呢？为什么获取不到数据？.....

我个人称这种开发模式为“大前端模式”，因为在这种模式下的前端是大哥，出了什么问题都要求后端来帮忙解决。（开个玩笑）

## 大前端模式的弊端

- 强依赖、沟通成本高

前端非常依赖后端，需要帮忙配置后端环境，比如装个虚拟机，配个 MySQL，Nginx、PHP 等前端实际上根本不应该关心的东西，甚至配置 IDE 的远程编辑...

其次，配好环境才只是开始，整个开发周期内，都需要后端解决出现的各种环境问题。

- 效率低

基于第一点，前端的开发进度是受后端所限制的，因为数据需要由后端提供。

基于第一点，后端的开发进度是受前端所限制的，因为后端经常需要帮前端解决的相关的环境问题。

因此，实现前后端分离的必要性显而易见。

## 典型的前后端分离实践

本着减少前后端撕B 情况和提高双方的工作效率——可并行开发，首先需要对前后端的工作职责达成一致。

前端：

- 负责页面各种交互
- 按照接口文档从后端接口拿数据
- 根据后端返回的数据对页面进行渲染

后端：

- 出接口文档，保证接口参数和返回值的格式稳定性
- HTTP 接口开发

其中，接口文档是前端和后端沟通的唯一前提，在双方开发之前，一定要先共同定义好接口文档。

## FAQ

### 后端接口未开发完，前端无事可干？

这是经常听到的一些前端同学的“抱怨”，但实际上却是错误的想法和做法。

因为我们已经事先定义好接口文档了，因此，前端在后端接口未开发完成之前，是可以根据接口文档的定义来模拟测试数据，而根本不需要后端先行。

模拟接口数据可以采用一些前端熟悉的工具，比如 [mockjs](http://mockjs.com/) 这样的 mock 库来脱离对后端的依赖。

### 浏览器的同源限制（CORS）？

在前后端分离的模式下，用得最多的就是浏览器跨域解决办法就是 CORS。

简单来说，CORS 需要前端和后端两边同时配置一些东西：后端在返回 HTTP 响应的时候需要返回一些必要的请求头设置，前端需要在请求 HTTP 接口的时候也要带上后端允许的一些必要的请求头。

``` http
Access-Control-Allow-Credentials: true
Access-Control-Allow-Headers: *,Access-Control-Allow-Origin,AUTHORIZATION,Content-Type,Accept,APIUCID
Access-Control-Allow-Methods: *,OPTIONS,GET,HEAD,POST,PATCH,PUT,DELETE
Access-Control-Allow-Origin: *
Access-Control-Expose-Headers: APIUCID
Access-Control-Max-Age: 604800
```

### HTM5 `fetch` with cookie?

``` javascript
var headers = new Headers();
headers.append('Accept', 'application/json');
headers.append('Cache', 'no-cache');

var init = {
	method: 'GET',
	headers: headers,
	credentials: 'include',    // with cookie
};

var request = new Request('/one/of/apis', init);
setTimeout(function () {
	fetch(request).then(function (res) {
		res.json().then(function (data) {
          console.log(data);
		});
	});
}, 1000);
```

## 参考

- *[实现前后端分离的心得](http://www.cnblogs.com/chenjg/p/6992062.htm)*
- *[一个前后端分离的简单案例](https://juejin.im/entry/58aa5ccf2f301e006c32a3be)*
- *[Client-Side vs. Server-Side Rendering](http://openmymind.net/2012/5/30/Client-Side-vs-Server-Side-Rendering/)*
- *[Quora-What is the difference between JavaScript and Node.js?](https://www.quora.com/What-is-the-difference-between-JavaScript-and-Node-js)*
- *[HTTP访问控制（CORS）](https://developer.mozilla.org/zh-CN/docs/Web/HTTP/Access_control_CORS)*
- *[Laravel 5.1 API Enable Cors](https://stackoverflow.com/questions/33076705/laravel-5-1-api-enable-cors)*
- *[CORS - What is the motivation behind introducing preflight requests?](https://stackoverflow.com/questions/15381105/cors-what-is-the-motivation-behind-introducing-preflight-requests)*
- *[前端常见跨域解决方案（全）](https://segmentfault.com/a/1190000011145364)*
- *[跨域资源共享 CORS 详解](http://www.ruanyifeng.com/blog/2016/04/cors.html)*
- *[造成跨域的原因和解决方法](http://www.cnblogs.com/wangpenghui522/p/6284355.html)*
- *[Cookie not set in Request Headers, even with 'same-origin' credentials.](https://github.com/github/fetch/issues/349)*
