---
layout: post
title: 在 CentOS 6.5 上搭建 GitLab 私有服务器
category: 折腾
tags: [GitLab, Linux, CentOS, Git]
---

GitHub 对于个人开发者来说已经非常不错了，但对一家互联网软件公司来说，配置一台私用的 Git 服务器还是很有必要的，除了能够自己管理自己的源代码之外，我觉得更重要的是能够体现公司在版本控制和多人协作开发上的专业性。

<!-- more -->

本文同样属于折腾后的笔记，放在这里为自己和有需要的人提供便利。下面以在阿里云 CentOS 6.5 为例简单总结搭建 Gitlab 的步骤和遇到的坑。


## 下载安装 GitLab

GitLab 官方推荐 CentOS 安装 Omnibus 版本，我也是按照官方的建议来的：

> 按照这个指南做即可：<http://www.gitlab.cc/downloads/#centos6>。

```
# 安装配置依赖项
yum install curl openssh-server postfix cronie
service postfix start
chkconfig postfix on
lokkit -s http -s ssh

# 添加并安装GitLab软件包
wget https://mirror.tuna.tsinghua.edu.cn/gitlab-ce/yum/el6/gitlab-ce-7.12.2~omnibus.1-1.x86_64.rpm
rpm -i gitlab-ce-7.12.2~omnibus.1-1.x86_64.rpm

#  配置和使用GitLab
gitlab-ctl reconfigure
```

在最顺利的情况下，GitLab 就是这么简单就能够安装完成。

这里本来还有一种下载自动安装脚本通过管道的形式安装，但是本人那样安装失败。我是下载好 rpm 包后直接手动安装的。

如果没有报错，安装之后在 Web 登录，默认的账号密码是：

```
Username: root 
Password: 5iveL!fe
```

以 Root 身份登录进去后，如果没有修改过密码，那么会直接进入重置 Root 密码的界面。

## 配置邮箱服务器

安装好 GitLab 后默认是不能发送邮件的，如果不配置邮箱服务器，那么在注册新用户或者重置密码的时候，是无法收到确认邮件的，也就影响了正常使用，因此必须配置好邮箱服务器。

由于 GitLab 中发送邮件使用的是 postfix，所以必须确认上面已经安装好 postfix。

但为了避免干扰，先卸载 Linux 默认的邮箱工具 sendmail。

```
yum list installed | grep ^sendmail
yum remove sendmail
```

然后测试 CentOS 此时是否可以正常发送邮件：

```
echo "A Test mail from postfix on CentOS" | mail -s "Test Postfix on CentOS" test@exp.com
```

如果在邮箱 `test@exp.com` 中可以正常收到上面的信息，就证明 postfix 已经能够正常工作了。

此外，在邮箱 test@exp.com 收到系统发送来的测试邮件时，将系统的地址复制下来，如：`root@xxxx.localdomain`，其中 `xxxx` 是服务器的 hostname。

然后打开 /etc/gitlab/gitlab.rb，将：

```
gitlab_rails['gitlab_email_from'] = 'gitlab@example.com' 
```

修改为：

```
gitlab_rails['gitlab_email_from'] = 'root@xxxx.localdomain'
```

保存，执行 `gitlab-ctl reconfigure` 重新编译 GitLab。

##### 说明

1. 如果邮箱的过滤功能较强，请添加系统的发件地址到邮箱的白名单中，防止邮件被过滤。

2. 系统中邮件发送的日志保存在 /var/log/maillog 中。

## 配置仓库存储路径为独立数据盘

为了安全起见，有必要将 GitLab 的仓库地址配置为独立的数据盘。拿阿里云来说，需要先购买数据盘，然后按照手册分区、格式化然后挂载到 Linux 中。假设已经将数据盘挂载载了 /data/ 目录下，配置纪录如下：

```shell
vim /etc/gitlab/gitlab.rb
```

找到 `git_data_dir` 然后修改其中的路径为数据盘在文件系统的路径：

```shell
git_data_dir "/data/gitlab/git-data"
```

这里路径写到 git-data 就行了，然后把 /var/opt/gitlab/git-data 下面的文件和文件原样复制到 /data/gitlab/git-data/，最后更改所属组就行了：

```shell
cp -r /var/opt/gitlab/git-data/* /data/gitlab/git-data/
chown -R git:git /data/gitlab/git-data/
```

最后重新配置 Gitlab、更改 gitlab-nginx 默认端口为前台 nginx 所反代的端口即可，如果不更改就会和前台的 nginx 冲突导致 502。

```shell
## 永久修改
vim /etc/gitlab/gitlab.rb    # 找到 nginx['listen_port'] 修改即可

## 临时修改
# 更改 gitlab-lab 默认端口为 /etc/nginx/conf.d/virturl.conf 中的配置端口即可
vim /var/opt/gitlab/nginx/conf/gitlab-http.conf

gitlab-ctl reconfigure
gitlab-ctl restart
```

## 配置私有 GitLab 服务器的 IP/域名

打开 /etc/gitlab/gitlab.rb，将 `external_url = 'http://git.example.com'` 修改为自己的 IP地址或域名（子域名），然后执行下面的命令，对 GitLab 进行编译：

```
gitlab-ctl reconfigure
gitlab-rake cache:clear RAILS_ENV=production
```

## 配置 Nginx 反向代理

如果一台服务器并非专门用来做 GitLab 服务器的话，那么多站点共存是必然会出现的了。

修改 /etc/gitlab/gitlab.rb，找到  `GitLab Nginx` => `# nginx['listen_port'] = nil` ，取消前面的注释，并改为新端口号，比如 8070，然后重启。

```shell
＃ 找到并修改 gitlab-nginx 监听的端口号
nginx['listen_port'] = 8070

# 重新配置 gitlab
gitlab-ctl reconfigure
gitlab-ctl status
```

然后就可以在前台 Nginx 中配置虚拟主机及其反向代理：

```
# 如果没有安装 nginx 就先安装
# yum install nginx

vim /etc/nginx/conf.d/virtual.conf
```

添加如下内容：

```shell
server
{
    listen 80;

    server_name git.xxx.com;

    location / {

        proxy_redirect off;

        # proxy_set_header Host $host;

        # proxy_set_header X-Real-IP $remote_addr;

        # proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

        proxy_pass http://127.0.0.1:8070;    # 这里反向代理到 gitlab-nginx 所监听的端口

    }

    # access_log /var/log/nginx/gitlab.log;
}
```


然后测试非 gitlab built-in 的 nginx 语法是否正确：

```
nginx -t

nginx -s reload
# Or
# /etc/init.d/nginx restart
```

##### 注意：

- 这里不要改为 8080 因为 GitLab 的 unicorn 服务已经占用了 8080 端口。
- /var/opt/gitlab//conf/gitlab-http.conf （`listen:*:80`）是 gitlab 自动生成的，修改后重启将会失效，所以如果要永久生效则不要在这里配置。
- 如果上面 nginx 虚拟主机的配置似 gitlab 在访问时静态资源出现问题，可以删除 `proxy_set_header` 开头的只留 `proxy_pass` 就可以了。

实际应用中，肯定不止一个域名指向 gitlab 所在的这台服务器，比如顶级域名 xxx.net 及其子域名 git.xxx.net、www.xxx.net 等都可能会指向当前的服务器。

在没有配置 Nginx 反向代理之前，访问 xxx.net 和 git.xxx.net 以及 www.xxx.net （不带端口号）都会进入 gitlab 因为此时服务器上的 80 端口只有 gitlab-nginx 在监听。

在进行上述配置后，访问 git.xxx.net 进入的只会是 gitlab，而访问 xxx.net 和 www.xxx.net 进入的就可以是此服务器上的其他站点了。

## FAQ

### 重置 Root 密码后忘记密码？

如果发生了如此“不幸”的事，不要着急，[解决办法](http://roland.kierkels.net/git/reset-your-gitlab-root-password-from-a-terminal/)如下：

```
cd YOUR_GITLAB_PATH
sudo gitlab-rails console production
u = User.where(id:1).first
u.password = 'YOUR_PASSWORD'
u.password_confirmation = 'YOUR_PASSWORD'
u.save
```

### GitLab 安装好之后出现 500/502？

可能是低配的阿里云服务器内存不足，而 GitLab 官方的建议[至少要 2G (RAM+Swap)以上](http://doc.gitlab.com/ce/install/requirements.html)。

本人亲测增大虚拟内存后 500 消失。CentOS 利用磁盘空间增加 swap 分区大小操作如下：

```
# 查看当前分区情况
free -mh

# 增加 2G 左右的 Swap
dd if=/dev/zero of=/var/swap bs=1024 count=2048000

# 设置交换文件
mkswap /var/swap

# 立即激活启用交换分区
swapon /var/swap

# 添加系统引导时自启动运行
vi /etc/fstab

# 添加一行
/var/swap               swap                    swap    defaults        0 0
```

相关操作还有：

```
#收回 swap 空间
swapoff /var/swap

# 从文件系统中回收
rm /var/swap
```

其他错误具体可以查看日志文件 production.log。

### 执行 gitlab-ctl reconfigure 的时候报错导致 Web 访问 500？

修改数据库配置: postgresql 改为 redis。

### GitLab 中的 Gravatar 头像无法显示？

这是因为 Gravatar 被墙了，虽然不影响主要使用但是非常碍眼，可以逼死强迫症。解决办法就是：duoshuo CDN。

- 如果是 Omnibus 版 GitLab

编辑 /etc/gitlab/gitlab.rb，增加（或修改）下面这一行：

```
gitlab_rails['gravatar_plain_url'] = 'http://gravatar.duoshuo.com/avatar/%{hash}?s=%{size}&d=identicon'
```

然后执行：

```
gitlab-ctl reconfigure
gitlab-rake cache:clear RAILS_ENV=production
```

- 如果是普通版 GitLab

编辑 gitlab.yml ， 找到如下部分：

```
## Gravatar
gravatar:
    enabled: true                 # Use user avatar image from Gravatar.com (default: true)
    # gravatar urls: possible placeholders: %{hash} %{size} %{email}
    # plain_url: "http://..."     # default: http://www.gravatar.com/avatar/%{hash}?s=%{size}&d=identicon
    # ssl_url:   "https://..."    # default: https://secure.gravatar.com/avatar/%{hash}?s=%{size}&d=identicon
```

把 plain_url 的注释去掉，写成如下内容：

```
plain_url: "http://gravatar.duoshuo.com/avatar/%{hash}?s=%{size}&d=identicon"
```

重启服务：

```
gitlab-ctl restart
```

如果 GitLab 不是新搭建的，依然会有一些头像地址会指向原先的地址，此时需要执行下面的命令清理缓存数据：

```
gitlab-rake cache:clear RAILS_ENV=production
```

### 修改配置并重启服务后无效？

> Don't mess with /var/opt/gitlab/...because any manual configuration will be lost after a reconfigure. 

### 私用 GitLab 配置 ssh-key 失效？

```
vim /etc/passwd
```

找到 git 用户所在行：

```
git:x:497:498::/var/opt/gitlab:/bin/sh
```

这是安装好 GitLab 后的默认配置，不要将 `sh` 改为 `git-shell` 之类的就可以了。

### Plain Text Forbidden?

Omnibus GitLab:

- `vi /etc/gitlab/gitlab.rb`

- 找到 `gitlab_rails['rack_attack_git_basic_auth']` 段并取消整段注释

- `ip_whitelist` 中添加 GitLab 运行域名的 IP 地址。

- `sudo gitlab-ctl reconfigure`

## 附录：与 GitLab 相关的目录和路径

```
find / -name gitlab

	/etc/gitlab
	/var/log/gitlab
	/var/opt/gitlab
	/opt/gitlab
	/opt/gitlab/embedded/cookbooks/gitlab
	/opt/gitlab/embedded/cookbooks/local-mode-cache/backup/var/opt/gitlab
	/opt/gitlab/embedded/cookbooks/cache/cookbooks/gitlab
	/opt/gitlab/embedded/service/gitlab-rails/lib/tasks/gitlab
	/opt/gitlab/embedded/service/gitlab-rails/lib/support/nginx/gitlab
	/opt/gitlab/embedded/service/gitlab-rails/lib/support/logrotate/gitlab
	/opt/gitlab/embedded/service/gitlab-rails/lib/support/init.d/gitlab
	/opt/gitlab/embedded/service/gitlab-rails/lib/gitlab
	/opt/gitlab/embedded/service/gitlab-rails/app/views/kaminari/gitlab
	/opt/gitlab/embedded/service/gitlab-rails/app/views/import/gitlab
	/opt/gitlab/embedded/service/gitlab-rails/spec/lib/gitlab
	/opt/gitlab/embedded/service/gitlab-rails/spec/tasks/gitlab
```

此外，常用的几个文件是：

```
# 主配置文件：/etc/gitlab/gitlab.rb

# 用户仓库：/var/opt/gitlab/git-data/repositories

# 生产环境版本日志：/var/log/gitlab/gitlab-rails/production.log
```

## 参考

- <https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/install/installation.md>
- [Download GitLab Community Edition(CE)](https://about.gitlab.com/downloads/#centos6) or [下载GitLab CE](http://www.gitlab.cc/downloads/#centos6)
- [Omnibus GitLab documentation](http://doc.gitlab.com/omnibus/)
- [GitLab 安装配置笔记(CentOS+Omnibus)](https://segmentfault.com/a/1190000002722631)
- [How to Install Gitlab on CentOS/RHEL 5/6/7](http://www.techoism.com/how-to-install-gitlab-on-centosrhel/)
- [CentOS 6.5 Minimal 安装 Gitlab 7.5](http://www.restran.net/2015/04/09/gilab-centos-installation-note/)
- [安装gitlab](http://favoorr.github.io/install-gitlab-through-rpm-package/)
- [[Sever Hacks] 搭建私有 GitLab 代码托管服务器(Ubuntu+手动)](https://phphub.org/topics/576)
- [用GitLab搭建自己的私有GitHub](https://segmentfault.com/a/1190000000345686)
- [清华大学 TUNA 镜像源](https://mirror.tuna.tsinghua.edu.cn/help/gitlab-ce/)
- <https://mirror.tuna.tsinghua.edu.cn/gitlab-ce/yum/el6/>
- [gitlab修改redis、postgresql配置](http://my.oschina.net/wanglihui/blog/381130?fromerr=YsaMab9D)
- [解决Gitlab的Gravatar头像无法显示的问题](http://my.oschina.net/anylain/blog/355797?fromerr=WGJGrqju)
- [GitLab.org / omnibus-gitlab · Files](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/README.md#tcp-ports-for-gitlab-services-are-already-taken)
- [[SOLVED] 502 - GitLab is not responding](https://forum.gitlab.com/t/solved-502-gitlab-is-not-responding/258/6)
- [Gitlab Configuration Issues:: NGINX Unicorn Port Conflict](http://stackoverflow.com/questions/25011895/gitlab-configuration-issues-nginx-unicorn-port-conflict)
- [GitLab版本管理](http://www.cnblogs.com/wintersun/p/3930900.html)
- [centos 推荐使用epel源](http://blog.51yip.com/linux/1337.html)
- [Custom Git Hooks ](https://docs.gitlab.com/ee/administration/custom_hooks.html)
- [Rack Attack | GitLab](https://docs.gitlab.com/ce/security/rack_attack.html#rack-attack
