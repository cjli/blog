---
layout: post
title: 通过绝对路径得到相对路径
category: PHP
tags: [PHP, 绝对路径, 相对路径]
---

绝对路径和相对路径的使用非常频繁，有时候项目出现一些问题，比如 404，file not found 等，都是因为引用的资源路径出现了问题。

<!-- more -->

``` php
<?php

/**
* 根据绝对路径计算两个文件的相对路径
* @author cjli
*/

/**
* @param String $file1, $file2 : 文件在 Linux 文件系统中的绝对路径
* @return Array 返回一个保存了可以直接作为 cd 参数的正确相对路径的关联数组，比如 `1to2` 表示文件 1 到文件 2
*/
function get_relative_path( $file1, $file2 ) {
	if ( !is_path( $file1 ) || !is_path( $file2 ) ) {
		return false ;
	}
	$path_arr1 = explode( '/', $file1 ) ;
	$path_arr2 = explode( '/', $file2 ) ;

	$cnt1 = count( $path_arr1 ) ;
	$cnt2 = count( $path_arr2 ) ;

	$len = min( $cnt1, $cnt2 ) ;

	# 根路径都是 / 所以无需从 0 开始比较
	# 得到两个文件路径深度的最小值, 再减去文件名自身占用的次数, 作为比较次数的上限, 这样比较节省时间
	for ( $i=1; $i<$len-1; ++$i ) {
		if ( $path_arr1[ $i ] != $path_arr2[ $i ] ) {
			break ;
		}
	}

	for ( $j=1; $j<$i; ++$j ) {
		$file1 = str_replace( $path_arr1[ $j ] , '..', $file1 ) ;
		$file2 = str_replace( $path_arr2[ $j ] , '..', $file2 ) ;
	}

	# 替换掉文件名只保留相对路径
	$file1 = str_replace( $path_arr1[ $cnt1-1 ] , '', $file1 ) ;
	$file2 = str_replace( $path_arr2[ $cnt2-1 ] , '', $file2 ) ;

	# 去除首尾的 `/`, 只截取最简单的相对路径
	$to_file1 = substr( $file1, 1, -1 ) ;
	$to_file2 = substr( $file2, 1, -1 ) ;
	return array(
		'2to1' => $to_file1 ,
		'1to2' => $to_file2
		) ;
}

# 判断文件路径是否合法
function is_path( $file ) {
	$pattern = '/^\/[\w|\.]+\/(\w)+(\.?\w+)*$/' ;
	$res = preg_match( $pattern, $file ) ;
	return $res ? true : false ;
}

# 测试

$file1 = '/etc/apache2/sites-enabled/000-default.conf' ;
$file2 = '/etc/init.d/apache2' ;

$relative_path = get_relative_path( $file1, $file2 ) ;

shell_exec( 'cd '.$file2 ) ;
shell_exec( 'cd '.$relative_path[ '2to1' ] ) ;
```
