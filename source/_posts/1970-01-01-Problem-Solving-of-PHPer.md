---
layout: post
title: 关于PHP程序员解决问题的能力
category: 好文整理
tags: [程序员, PHP]
---

解决问题 > 学习能力。

<!-- more -->

这个话题老生长谈了，在面试中必然考核的能力中，我个人认为解决问题能力是排第一位的，比学习能力优先级更高。解决问题的能力既能看出程序员的思维能力，应变能力，探索能力等，又可以看出他的经验。如果解决问题能力不佳是无法通过面试的。

这里举个例子，假如我执行了一个 PHP 的脚本，如 `php test.php`，预期是可以返回一个字符串。但执行后没有任何信息输出，这时候通过什么方法能知道程序错在哪里？

这里可以将解决问题能力分为8个等级，越到后面的表示能力越强。

## Lv0：查看 PHP 错误信息

程序没有达到预期效果，证明代码出错了，看 PHP 的错误信息是第一步。如果直接忽略错误信息，表明这个人不适合担任专业的程序员岗位。

有些情况下 php.ini 配置中关闭了错误显示，需要修改 php.ini 打开错误信息，或者错误信息被导出到了日志文件，这种情况可以直接 `tail -f php_error.log` 来看错误信息。

拿到错误信息后直接定位到程序代码问题，或者到 Google/百度 搜索，即可解决问题。

PHP 打开错误显示的方法是：

- php.ini 中 `display_errors` / `display_startup_errors` 设置为 `On`

- php.ini 中 `error_reporting` 设置为 `E_ALL`

- PHP 代码中设置 `error_reporting(E_ALL)`


## Lv1：“存在多个版本的 PHP” 或 “php-cli 与 php-fpm 加载不同的配置”

存在多个版本的 PHP，懂得通过 `which php` 来看是哪个 PHP，或者加绝对路径手动指定 `php` 版本。表示此 PHPer 通过了此层级的 50% 考验。

另外一个情况就是 php-cli 与 php-fpm 得到的执行情况不一样，如在 WEB 浏览器中执行是对的，cli 下执行是错的。这时候可能是 2 个环境加载的 php.ini 不同所致。cli 下通过 `php -i | grep php.ini` 得到加载了哪个 php.ini。而 fpm 下通过 `phpinfo()` 函数可以得到 php.ini 的绝对路径。

## Lv2：`var_dump`/`die` 打印变量值信息单步调试

这是惯用的程序调试手段，也是最简单粗暴有效的解决问题方法。

高级一点的手段是使用 PHP 的 Trace类/日志类，花哨一点的可以借助 phpstorm+xdebug 在 IDE 工具里进行 Debug。

Trace 工具还可以分析脚本的耗时，进行 PHP 程序的性能优化。

这 3 个考验全部通过，表明此程序员已经具备了专业 PHP 程序员应该有的解决问题能力了。PHP 程序员只要过了这个等级，就足以应多大部分情况，在中小型网站中毫无压力。

## Lv3：使用 strace 工具跟踪程序执行

strace 可以用来查看系统调用的执行，使用 `strace php test.php`，或者 `strace -p 进程ID`。

strace 就可以帮助你透过现象看本质，掌握程序执行的过程。这个手段是在大型网站，大公司里最常用的。如果没掌握 strace，这里只能说抱歉了，我们不接受不会 strace 的 PHPer。

strace 其实也是对程序员基础的考验，如果不懂操作操作系统，完全不懂底层，肯定也达不到会用 strace 的程度。

当然 strace 对于 PHP 代码里的死循环是解决不了的。比如你发现一个 php-fpm 进程 CPU100% 了，strace 恐怕是解决不了的。因为 strace 是看系统调用，一般都是 IO类操作，既然是 IO密集，那 CPU 一定不可能是100%。

## Lv4：使用 tcpdump 工具分析网络通信过程

tcpdump 可以抓到网卡的数据通信过程，甚至数据内容也可以抓到。使用 tcpdump 可以看到网络通信过程是什么样的，如何时发起了 TCP SYN 3次握手，何时发送 FIN 包，何时发送 RST 包。这是一个基本功，如果不懂 tcpdump，证明不具备网络问题解决能力。

## Lv5：统计函数调用的耗时和成功率

使用 xhporf/xdebug 导出 PHP 请求的调用过程，然后分析每个函数调用的过程和耗时。能够分析 PHP 程序的性能瓶颈，找出可以优化的点。

另外一个对于网络服务的调用，如 MySQL 查询，cURL，其他 API 调用等，通过记录起始和结束时 `microtime`，返回的是不是 `false`，可以得到调用是否成功，耗时多少。

如果可以汇总数据，整理出调用的成功率，失败率，平均延时，证明此程序员对接口质量敏感，有大型网站项目经验。

## Lv6：gdb 使用

gdb 是 C/C++ 调试程序的利器，需要具备一定 C/C++ 功底的程序员才会能熟练使用 gdb。

上面说的 strace 无法跟踪 PHP 程序 CPU100%，而 GDB 是可以跟踪的。另外 gdb 也可以解决 PHP 程序 core dump 的问题。

通过 `gdb -p 进程ID`，再配合 php-src 的 .gdbinit，zbacktrace 等工具，可以很方便地跟踪 PHP 程序的执行。

像上面的 CPU100% 往往是 PHP 程序中发生死循环了，gdb 进行多次查看，就大致可以得到死循环的位置。

具备 gdb 解决问题能力的 PHP 程序员少之又少。如果能使用 gdb 解决 PHP 问题，这个 PHPer 百分之百可以通过面试，并且可以拿到较高的技术评级。

## Lv7：查看 PHP 内核和扩展源码

如果能熟悉 PHP 内核和扩展的源码，遇到 PHP 程序中最复杂的内存错误，也可以有解决的能力。这类PHP程序员就是凤毛麟角了。

配合 gdb 工具和对 PHP 源码的熟悉，可以查看 opcode 的信息，`execute_data` 的内存，全局变量的状态等。

## 参考

- [关于PHP程序员解决问题的能力](http://rango.swoole.com/archives/340)