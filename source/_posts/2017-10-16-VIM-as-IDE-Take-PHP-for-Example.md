---
layout: post
title: VIM as IDE take PHP for example
category: 折腾
tag: VIM
---

逐渐总结下使用 VIM 开发 PHP 项目的一些东西。

<!-- more -->

总结来说，使用 vim 开发主要就是，从你需要的功能出发，寻找合适的插件后下载配置，快捷键定义及熟悉，如果再高级点的用户就是为解决自己独有的问题而编写一些插件或函数。

> 说明：本文以下所有的配置目前均是在安装完 SpaceVim 后进行的配置，但是与不使用 SpaceVim 的区别只是插件安装的方式不一样，安装结束后的配置都是通用的。

## [SpaceVim](https://spacevim.org/cn/documentation/)

SpaceVim 是一个对新手友好的 VIM 配置框架，提供了一些开箱即用的配置选项，有时候可以节省不少时间。

SpaceVim 虽然有自己的一套配置规则，但是仍然支持自定义插件和 vimrc 文件，即我们之前习惯的所有配置项基本上也可以在安装完 SpaceVim 之后继续使用。

> 之所以说“基本上”是因为有些快捷键有可能冲突，不是之前熟悉的操作。

### vimrc

SpaceVim 安装好之后，我们也可以在 ~/.SpaceVim/vimrc 文件中配置我们没有使用 SpaceVim 之前的配置项，即 ~/.vimrc，因为可能之前都已经适应了很多自己的操作和快捷键，所以需要引进来。

### 自定义插件

在 ~/.SpaceVim.d/init.toml 配置文件中，可以自定义 GitHub 上的第三方插件，定义格式如下：

```
[[custom_plugins]]
    name = "mattn/emmet-vim"
    merged = 0
```

注意：每个插件都需要一个这样的独立的配置块，保存好配置后再次打开 vim 后 SpaceVim 就会自动安装新插件。

每个自定义安装好的插件都可以在 ~/.SpaceVim/vimrc 文件配置一些东西。

### 常用配置项

在 init.toml 中有些配置还是挺重要的，我的使用如下：

``` toml
[options]
	# 设置缩紧为 4 个空格 !!! （默认为 2 个）
	default_indent = 4

	# SpaceVim 默认的 vimfiler 不支持 git 中变动文件的标记 故这里我使用的是 nerdtree
	filemanager = "nerdtree"

	# 关闭兼容模式
	vimcompatible = false

	# 启用文件类型图标显示
	enable_tabline_filetype_icon = true
    enable_os_fileformat_icon = 1

# 禁用 SpaceVim 默认的自动补全(个人认为好难用)
[[layers]]
	name = 'autocomplete'
	enable = false
	auto-completion-return-key-behavior = "smart"
	auto-completion-tab-key-behavior = "smart"

[[layers]]
	name = 'git'
[[layers]]
    name = 'colorscheme'
[[layers]]
    name = 'core#statusline'
    enable = true
[[layers]]
    name = 'core#tabline'
    enable = true

[[custom_plugins]]
	name = 'lilydjwg/colorizer'
	merged = 0
	on_cmd = ['ColorHighlight']
```

## 自动补全

### [ervandew/supertab](https://github.com/ervandew/supertab)

### [SirVer/ultisnips](https://github.com/SirVer/ultisnips)

这个也可以在 SpaceVim 中直接安装使用，不过之前的那些文件都要复制到 ~/.vim/ 目录下。

``` shell
cp -r ~/.vim_back/bundle/ ~/.vim/
ln -s /home/cjli/src/snippets/vim/UltiSnips/ /home/cjli/.vim/
```

## 跳转到定义

IDE 典型的功能之一就是可以直接点击某个类名就直接打开该类的定义文件，Vim 也可以通过插件配置实现。

### ctags

VIM 实现定义跳转的前提是要有已经生成好的 tags 文件，所谓的 tags 生成文件指的是通过工具从你代码中筛选出类名/方法/变量等其他标识符，然后保存它们的索引到一份文件，这份索引文件就是 tags 文件。

最常用 tags 文件生成工具是 [ctags](http://ctags.sourceforge.net/)。安装也很简单：

``` shell
# CentOS
sudo yum install -y ctags
# macOS
brew install ctags
```

安装好我们可以为 ctags 工具配置一些通用的选项， 配置文件在 ~/.ctags，比如我可以为 PHP 项目配置如下：

```
--recurse=yes
--tag-relative=yes
--exclude=.git
--exclude=composer.phar
--exclude=*.js
--exclude=*.html
--exclude=*.css
--exclude=*.yml
--exclude=*.yaml
--exclude=*.vue
--exclude=*.log
--exclude=*.json
--exclude=*.vim
--langmap=php:.engine.inc.module.theme.install.php
--PHP-kinds=+cfi-vj
```

这样一来，我们在运行 ctag 命令时就不用写那么多选项了，最后就是为你的项目生成 tags 文件：

``` shell
cd /path/to/your/projects/root
ctags -R --fields=+laimS --languages=php -f ~/src/vim-tags/tags-0
```

除了标准的 ctags，还可以使用 universe-ctags，这个生成的 tag 在 tagbar 插件补全的时候可以补全更多的信息，CentOS 7 下目前只能编译安装：

``` shell
git clone https://github.com/universal-ctags/ctags.git /usr/local/src/ctags
cd /usr/local/src/ctags
./autogen.sh
./configure --prefix=/where/you/want # defaults to /usr/local
make
make install
```

然后使用新安装的 ctags 执行路径替换 `which ctags` 的旧执行路径即可。

### 使用 tags 文件

tags 文件有了后，vim 就要通过一种机制去使用这个文件，我这里还是使用 phpcomplete.vim 这个插件，上一步自动补全时配置过一些，这里可以配置下 tags 文件的位置：

``` vimrc
set tags=./tags;
let g:easytags_dynamic_files = 1
let g:easytags_file = '~/src/vim-tags/tags-0'
let g:easytags_cmd = '/usr/bin/ctags'
```

然后自动跳转定义的快捷键可以不配置使用默认的：

- `ctrl` + `]`：跳转到光标所在行定义的源文）
- `ctrl` + `t`：返回上一次跳转的地方
- `g]` 可以列出所有符合条件的 tags，你可以输入你要去的文件编号精确选择。

说明：

- 跳转有时候可能不准，是因为比如有些常见的单词在 tags 文件中的索引匹配的结果不止一个，不如 `Request` 这个单词往往出现的频率很多，因为会跳到并非你跳的文件所引用的真正文件。
- phpcomplete.vim 插件的 wiki 中也介绍了其他的可以生成 php tags 文件的有见：<https://github.com/shawncplus/phpcomplete.vim/wiki/Getting-better-tags>。

### 自动更新 tags 文件

随着代码的新增、提交、合并等过程。tags 文件里面的索引可能会有更新过，因此需要有一种机制来需要的时候更新我们的 tags 文件，不然可能会跳转到一个错误的位置。

可以直接使用 [ludovicchabant/vim-gutentags](https://github.com/ludovicchabant/vim-gutentags) 插件完成需求。

此外可选的一种操作是和 git 进行集成，合并/提交成功后，可以触发一些钩子调用更新 tags 文件的脚本，这样一来就可以基本保持我们 tags 索引和最新的代码保持同步了。

可参考:
- <http://nikhgupta.com/code/using-vim-php-ide-exuberant-ctags-code-browsing/>
- <https://tbaggery.com/2011/08/08/effortless-ctags-with-git.html>

## 文件搜索

项目中搜索文件算是使用频率最高的一个操作之一了，目前使用的是 [ctrlp.vim](https://github.com/ctrlpvim/ctrlp.vim) 类似与 Sublime Text 的 ctrl + p 快捷键，用于快速搜索文件，效果非常棒。

## vimdiff

vimdiff 是 vim 默认提供的文件 diff 工具，依赖系统的 diff 命令。

> 其他命令行 diff 工具还有 colordiff。

### 使用

``` shell
vimdiff file1 file2
vim -d file1 file2
```

#### 差异点跳转

- ]c 下一个差异点
- [c 上一个差异点
- n]c 转到下面第n个差异点,n为数字

#### 文件合并

- dp 当前差异点复制到另一个文件(diff put)
- do 差异点复制到当前文件(diff get, 不用dg是因为已被另一命令占用了)
- `:diffupdate` 修改后的更新，vimdiff 也会自动来重新比较
- u 撤销修改 

#### 上下文展开和查看

缺省会把差异处上下各6行的文本都显示出来，可通过以下修改 `:set diffopt=context:6`

- zo 展开折叠的行
- zc 重新折叠

### diff 结果界面/颜色说明

- 比较文件连续的相同行被折叠
- 只在某一文件存的行背景设置成蓝色，其他文件为绿色
- 比较文件都有并包含差异的行设置成粉色背景，差异的文字用红色背景标注

## Git 集成

SpaceVim 默认有个 git layer，可以在 init.toml 文件中配置启用：

``` toml
[[layers]]
	name = 'git'
```

然后就可以在 nerdtree 之类的文件管理插件中看到 git 中变动文件的一些特殊标记。

但是，仅仅这样还不够，我们希望可以直接在 VIM 中进行 git 常用的一些操作，比如 diff，commit 等等。

目前我使用的是 [tpope/vim-fugitive](https://github.com/tpope/vim-fugitive) 这个插件，算是封装 git 操作最好的 vim 插件了。作者还提供了一些非常有用的视频教程，详见：<http://vimcasts.org/blog/2011/05/the-fugitive-series/>。 

## PHP 专用

### 代码格式化

PHP 代码格式化我选择的是 [php-cs-fixer](https://github.com/stephpy/vim-php-cs-fixer)。 

#### git hook before commit

### Xdebug

- DBGPavim
- DBGp-X-Client
- .vimrc

### 代码折叠

## 其他插件

- [majutsushi/tagbar](https://github.com/majutsushi/tagbar)

SpaceVim 默认已经安装侧栏 tagbar 插件了，支持也不错。如果不实用 SpaceVim，可以选择这个，它支持各种文件 tags 展示。支持的文件类型详见：<https://github.com/majutsushi/tagbar/wiki>.

- [jszakmeister/markdown2ctags](https://github.com/jszakmeister/markdown2ctags) 

虽然 majutsushi/tagbar 已经可以做基本的 tags 展示了，但是 markdown2ctags 是专门为 markdown 文件生成 tags 的插件，作为使用 VIM 编辑一切文本的我，为了体验，自然不能放过它。

安装后配置 vimrc：

``` vimrc
let g:tagbar_type_markdown = {
    \ 'ctagstype': 'markdown',
    \ 'ctagsbin' : '/path/to/markdown2ctags.py',
    \ 'ctagsargs' : '-f - --sort=yes',
    \ 'kinds' : [
        \ 's:sections',
        \ 'i:images'
    \ ],
    \ 'sro' : '|',
    \ 'kind2scope' : {
        \ 's' : 'section',
    \ },
    \ 'sort': 0,
\ }
```

其中，`/path/to/markdown2ctags.py` 需要改成你安装好  markdown2ctags 之后的文件路径，比如如果是通过 SpaceVim 安装的话，那么应该改成类似：`/home/cjli/.cache/vimfiles/repos/github.com/jszakmeister/markdown2ctags/markdown2ctags.py`。

- [ack.vim](https://github.com/mileszs/ack.vim) 

文本搜索。

## FAQ

- VIM 打开文件后自动在文件末尾添加新的空行/\ No newline at end of file vim auto cancel？

``` vimrc
set binary
set noeol
```

> See: 
> - <https://stackoverflow.com/questions/729692/why-should-text-files-end-with-a-newline>
> - <https://www.infysim.org/vim-adds-new-line-utomatically-at-end-of-file/>

- SpaceVim 文件类型图标显示乱码？

字符图标的显示支持是需要终端/客户端配置为字体的。以 iTerm2 举例说明，我们需要先安装 支持字符图标的字体到系统（macOS），然后设置 iTerm2 的非 ASCII 字体（Profile/Text/Non-ASCII Font）为新安装的字符字体即可。

比如字符字体我们选择 nerd 字体：

``` shell
brew cask install font-hack-nerd-font    # 然后 iTerm2 的 Non-ASCII 字体选择它
```

> See: <https://github.com/SpaceVim/SpaceVim/issues/771>.

注意：一定要是终端软件自身设置的显示字体。如果是终端 SSH 连接到服务器/虚拟机之类的，也是要设置终端的字体，设置所连接的服务器的字体也是没用的。

## 参考

- [brookhong/DBGPavim](https://github.com/brookhong/DBGPavim)
- [用DBGPavim在Vim中调试PHP/Python程序](https://brookhong.github.io/2014/09/27/dbgpavim-cn.html)
- [在Vim中远程调试PHP代码 - 螺壳网](http://luokr.com/p/3)
- [vim-scripts/DBGp-X-client](https://github.com/vim-scripts/DBGp-X-client)
- [xdebug & VIM 调试 PHP 代码](https://blog.chenishr.com/?p=597)
- [HOW TO REMOTELY DEBUG PHP USING TERMINAL (VIM + VDEBUG + XDEBUG)](https://walty8.com/remotely-debug-php-using-command-line-vim-vdebug-xdebug/)
- [Vim As A PHP IDE - Exploring Code](https://engagor.github.io/blog/2017/02/15/vim-ide-exploring-code/)
- *[Linux - vimdiff 快速比较和合并少量文件](https://www.cnblogs.com/abeen/p/4255754.html)*
