---
layout: post
title: 在 Mac OS X(现macOS) 上批量导入 ShadowsocksX 服务器配置
category: 折腾
tags: [Mac OS X, Shadowsocks]
---

好久没有使用 [VPNCUP](https://www.vpncup.com)了，今天突然登录了一下，发现新增了不少 VPN 节点， 惊喜地，还有 Shadowsocks 的[节点](https://ssvcup.com)。

<!-- more -->

数了下，欧美＋亚洲一共大概 10 个服务器，虽然不多，但是仍然不想打开 ShadowsocksX 然后一个一个输入地址、端口、密码、加密算法等配置，作为一个有上进心，和对 GUI 不太感冒的码农（其实主要是想折腾一下），遇到重复劳动的工作条件的时候，反射地想要使用循环，从一个配置文件中一次性批量读入配置，仿佛这样才能心满意足。

#### 小飞机的配置文件说明

首先通过 Google，得知 ShadowsocksX 的配置文件是保存在：

> ~/Library/Preferences/clowwindy.ShadowsocksX.plist

这个文件下面的，因此，要使 ShadowsocksX 能够直接批量读取服务器配置，只需从正确修改这份文件开始。

plist 和 bplist ，它们都是专门保存 Mac OS X 和 iOS 上 App 属性、用户配置等信息的配置文件，只不过 plist 使用的是 XML 语法书写，而 bplist 是后来苹果为了减少 XML 文件的空间占用而使用的二进制格式配置文件，而这里的 ShadowsocksX 的配置文件就是属于 bplist。

既然是二进制，就要解决用部分文本编辑器打开后直接编辑会出现的乱码问题。

#### 将 bplist 文件中的服务器配置数据转换成 JSON

1. 备份原始配置文件

   ```shell
   cp ~/Library/Preferences/clowwindy.ShadowsocksX.plist ~/backups        # 备份
   cp ~/Library/Preferences/clowwindy.ShadowsocksX.plist ~/WWW/workshop   # 副本
   ```
   如果配置错误出现无法启动或重装小飞机也无效的问题，请使用此备份恢复。或者使用下面的命令重新生成 plist：

   ```shell
   defaults delete clowwindy.ShadowsocksX    # !重新生成后原始服务器配置将丢失
   ```

2. 将二进制 bplist 副本转换成可读 XML

   ```shell
   cd ~/WWW/workshop
   plutil -convert xml1 clowwindy.ShadowsocksX.plist -o ss.xml
   ```

   其中，plutil 是 Mac OS X 上自带的对 plist 文件进行检查和转化的工具。

   然后用编辑器打开生成的 ss.xml 文件，复制 `<data>BASE64_CODE</data>` 字段中的信息出来进行 base64 解码，保存到 ss.b64。

3. 获得 base64 解码后的服务器配置信息（json格式）

   ```shell
   base64 -D -i ss.b64 -o - > ss.json
   ```

   其中，这里得到的 JSON 文件和 Windows 下的格式还不一样，因此想要用别人现成的不行了。

#### 填写基本配置项

首先，在 ss.cnf 中预先填入格式化的配置信息，如下：

```
# !!! Make Sure that the is Line Break Style is LF(UNIX)
# eg: IP|Domain Port Method Password Remarks

123.123.123.123 6789 aes-256-cfb PASSWORD 联通优先

usa.domain.com 9999 chacha20 PASSWORD 洛杉矶-chacha20

# More Servers ...
```

需要注意的是：

- ss.cnf 文件需要使用 UNIX 换行风格，即 LF 。
- 使用  `#` 打头的行为注释行。
- 每行一个服务器完整 shadowsocks 配置。
- 每个配置项（服务器地址、端口、加密方式、密码、备注）之间只能使用一个空格隔开。

#### 批量导入配置信息后重新生成 json 和 base64 字符串到文件

这里使用 PHP 数组完成批量生成 json，然后生成 base64 编码并保存到文件的任务。

在和 ss.cnf 同一目录下创建 ss.php 文件，内容如下：

```php
<?php

/**
 * ss.php
 * 批量导入 Shadowsocks 配置
 * by @cjli
 */

# 1. 先获得 Mac OS X 下 Shadowsocks 的配置的数组格式
// $ss_json = file_get_contents( 'test/ss.json' ) ;

// $ss_arr  = json_decode( $ss_json, true ) ;

// print_r( $ss_arr ) ;    // 通过数组查看默认配置规律

# 2. 处理基本配置文件并生成格式化的数组
$servers  = file_get_contents( 'ss.cnf' ) ;
$servers  = explode( "\n", $servers ) ;
$profiles = array() ;
foreach ($servers as $k => $v) {
	if (!preg_match('/^#+/', $v) && !empty($v)) {
		$server     = explode(' ', $v) ;

		# 使用正则表达式检查配置文件每行格式是否正确
		// ...

		/**
		 * !!! 不能直接在此循环中直接一次性添加到 $profiles 数组, 否则会报 illegal offset type error
		 * 错误的写法:
		 *          $profiles[] = array(
		 *          		['password']    => $server[3] ,
		 *          		['method']      => $server[2] ,
		 *          		['server_port'] => $server[1] ,
		 *          		['remarks']     => $server[4] ,
		 *          		['server']      => $server[0] ,
		 *	         ) ;
		 */
		$profiles[] = getFormattedArr($server) ;
	}
}
$server_cnt = count($profiles) ;

$new_arr = array(
	'current'  => 0 ,
	'profiles' => $profiles
	) ;

# 3. 将数组转化为 JSON 后使用 Base64 编码
// !!! 注意这里一定加上 JSON_UNESCAPED_UNICODE 参数(PHP Version > 5.4)来规避自动 UNICODE 转码问题导致的密码格式与配置文件不符合的问题
$ss_conf = base64_encode(json_encode($new_arr, JSON_UNESCAPED_UNICODE)) ;

# 4.1 将生成的 Base64 字符串保存到文件 (手动)
// date_default_timezone_set( 'Asia/Shanghai' ) ;
// file_put_contents( date('Y-m-d-His').'.ss.x'.$server_cnt, $ss_conf ) ;

# 4.2 将生成的 Base64 字符串替换掉 ss.xml 中 <data> 标签中的信息 (自动)
$pattern  = '/<data>\s*[\w|=|\+|\/]*\s*<\/data>/' ;
$subject  = file_get_contents( 'ss.xml' ) ;
$replace  = "<data>\n\t" ;
$replace .= $ss_conf ;
$replace .= "\n\t</data>" ;
$data     = preg_replace($pattern, $replace, $subject) ;

# 5. 替换旧的 ss.xml
file_put_contents( 'ss.xml', $data) ;

/**
 * 重新获得一个格式化的数组
 * by @cjli
 */
function getFormattedArr($server) {
	$arr = array() ;

	$arr['password']    = $server[3] ;
    $arr['method']      = $server[2] ;
    $arr['server_port'] = $server[1] ;
    $arr['remarks']     = $server[4] ;
    $arr['server']      = $server[0] ;

	return $arr ;
}
```

运行此脚本（`php -f ss.php`）后，在当前目录下将生成一个类似：2016-06-23-232932.ss.x13 的文件，其中 x13 代表此配置文件中存有 13 个 shadowsocks 服务器配置信息。

#### 复制和替换后再转换为 bplist

复制 2016-06-23-232932.ss.x13 中的信息到上面第 2 步中生成的 ss.xml 中，替换掉原本 `<data>...</data>` 标签中的内容，然后将其转换为 bplist 文件：

```shell
plutil -convert binary1 ss.xml -o clowwindy.ShadowsocksX.plist
```

如果格式错误，plutil 会中止操作，使用 `plutil -p` 可以对文件进行拼写检查。

#### 重新载入 ShadowsocksX 的 plist

```shell
# 在新生成的 clowwindy.ShadowsocksX.plist 目录下执行
defaults import clowwindy.ShadowsocksX clowwindy.ShadowsocksX.plist
```

然后重新打开 ShadowsocksX 可以发现，ss.cnf 中的服务器全部已经成功导入。

#### 总结: 一键 Shell 脚本

```shell
## ShadowsocksX 批量配置 by @cjli
# 如果没有 ss.xml 请在确保具有相应权限下先执行如下命令:
# cp ~/Library/Preferences/clowwindy.ShadowsocksX.plist ~/
# plutil -convert xml1 clowwindy.ShadowsocksX.plist -o ss.xml

# 1. 读取配置文件 servers.conf 生成新的 Base64 配置字符串并替换掉 ss.xml 中的 <data> 标签
php -f ss.php

# 2. 将新的 ss.xml 转换为 plist
plutil -convert binary1 ss.xml -o clowwindy.ShadowsocksX.plist

# 3. 用新的 plist 替换掉原来的 plist
defaults import clowwindy.ShadowsocksX clowwindy.ShadowsocksX.plist

# 4. 然后重新打开 ShadowsockX ( End )
```

此脚本也放在我的 GitHub 上了：[*ss-in-bulk*](https://github.com/cglai/ss-in-bulk)，有兴趣的可以下载使用。

#### FAQ

- **PHP 出现 illegal offset type error ?**

> [*Illegal offset type* errors occur when you attempt to access an array index using an **object** or an**array** as the index key.](http://stackoverflow.com/questions/2732451/php-how-do-i-fix-this-illegal-offset-type-error)

#### 参考

- [从 ShadowsocksX 客户端配置文件说到 OS X user default plist](https://www.starduster.me/2016/04/02/mac-shadowsocksx-config-file-toos-x-user-default-plist/)
- <http://stackoverflow.com/questions/6771938/any-way-to-return-php-json-encode-with-encode-utf-8-and-not-unicode>