---
layou: post
title: iWebshop 二次开发总结
category: PHP
tags: [iWebshop, PHP, 二次开发, 电商系统]
---

iWebshop 是我在实际工作中第一个二次开发的电商系统，这里简单记录下开发经验。

<!-- more -->

## 安装使用

无需细说，解压源代码到网站根目录后访问 /install，根据提示一步步设置即可。安装结束后删除 install  目录。

如果需要的有扩展而没安装的，自行安装，然后重启 php-fpm(nginx) 或 Apache 就行了。

安装后的常用路径：

- 前台 URL 规律：/index.php?controller=C&action=A
- 后台登录：/index.php?controller=systemadmin

## 控制器&操作

### 修改默认控制器

iWebshop 默认/首页控制器：site。

通过站点入口文件 index.php 可以顺藤摸瓜到 /lib/core/application_class.php，然后找到 `protected $defaultController='test'; `  修改为自己想要的控制器即可。 

### 访问控制器和方法

``` php
// controllers/test.php
class Test extends IController {
  public function hello() {
    echo 1024 ;
  }
}
```

然后可以通过两种方式访问：

- /index.php?controller=test&action=hello（传统 URL 模式）
- /index.php/test/hello（需要开启 pathinfo 模式／伪静态）

#### `@` 符号

iWebShop 善于用 `@`符号表示包括子关系，比如 `site@index` 表示的就是site 控制器下的 index 方法。

### 访问视图和方法

在 views/ 目录下面新建一个 test 文件夹，然后在 test 文件夹下面创建一个 HTML 文件：

```html
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>hello iwebshop</title>
</head>

<body>
  <h1>hello iwebshop</h1>
</body>
</html>
```

然后访问上面第二种 URL 格式，即 /index.php/test/hello ，即可看到效果

**注意：**如果控制器中和视图中的命名冲突，那么默认会先走控制器指定的 action。

### iWebshop 路由规则

当输入的 URL 是  /index.php/test/test 的时候，会有两种情况发生：

- 通过控制器 test 访问 方法(操作) test
- 直接路由到 test 视图中的 test 模板

访问顺序是：如果第一种方式不行，则尝试第二种，如果都不行则返回 404。

当控制器文件存在而该控制器文件中没有要访问的方法时，会去 `views/主题/对应的视图控制器/` 下面查找视图 action，此时控制器中的配置仍然有效，比如 `public $layout` 属性。

如果控制器文件不存在的时候，就会直接去 `views/主题/` 下面找 url 中对应的 `控制器/action.html`。

### 控制器和视图关系

一个控制器往往对应一个视图文件夹，比如控制器 /controllers/test.php 对应的是 /views/test/。

如果是给控制器增加视图，那么视图目录的名称必须和控制器的名称相同。也就是说，你要给 test.php 这个控制器增加视图，那么必须要在 views 目录下建立相应的 test目录，并把控制器所需要的视图模板文件都放在里面。

### 控制器权限校验

在控制器中进行权限校验是一种对于操作动作的拦截配置：`Protected $checkRight = 'all';`。

这个属性的检测，**只有在后台的控制器管理权限中才有用**，主要是做管理员的角色校验的，他的检查代码，在 classes/checkrights.php 里面。因此，必须登录后台后才能进行权限验证这解释了为什么在自己写的控制器中指定 `$checkRight` 属性后在前台看不到效果。

#### 测试

在 controllers/system.php 中添加一个 test() 方法：

``` php
function test() {
	echo '权限测试' ;
}
```

然后以非超级管理员的管理员身份登录后访问该 action：/index.php/system/test。

如果没有意外，会看到如下提示信息：*发生错误 您无权限操作此功能*。

**注意：**超级管理员什么 action 都能够访问，因此这个属性对超管无效。

### 删除缓存

如果是模板 layout 增加或者取消的话，必须要清除一下缓存文件：删除 /runtime/ 下面的文件即可。

**提醒：**在调试的时候注意关闭视图引擎的缓存。

## 模版

### 给模版渲染数据

一共有2 种方法可以把控制器里面的数据传递给模板，可以通过「类属性」和 「`$this->setRenderData`」。

``` php
// 控制器
class Test extends IController {
  public $layout = 'site_mini' ;
  public $data1 = '' ;
  function hello() {
    // 第一种传值方式，通过类属性赋值操作，把字符串 '数据1' 赋值给了$this->data1【推荐】
    $this->data1 = '数据1' ;
    // 第二种传值方式，通过控制器自身方法 setRenderData() 把参数数组里面的键变成模板里面的变量
    
    $this->setRenderData(array( 'data2' => '数据2'));
    $this->redirect('hello');
  }
}

// 视图
<!DOCTYPE html> <html lang="en"> <head>
<meta charset="UTF-8">
<title>hello</title> </head>
<body> <h1>hello</h1>
<h2>{$this->data1}</h2>
<h2>{$data2}</h2>
</body> </html>
```

必须说明的几点：

如果要给视图渲染数据那么要引入的视图名字必须和action 方法名字相同，即类方法名字叫 hello，要显示的视图也必须叫 hello。
当然如果方法名称和视图名称不一致却仍想渲染数据，需要在最后的 redirect 方法中增加第二个参数为 false。

比如，要把 `$this->data1` 传递给 `demo.html` 视图，那么第三行就要写成：`$this- >redirect(‘demo’,false);` 此时就可以实现显示 demo.html，并且带着 hello 方法里面的数据了。

### 模版标签

模板中的标签是显示数据，读取数据的工具。

iWebshop 的模板标签语法：`{标签名:属性}`。

#### 输出标签

在已经通过控制器想模板中注册过数据的前提下，在模板中输出这些数据的标签格式可以是：

``` php
{$this->data1}
{$data2}
{echo:$data3}
{echo:'数据4'}
```

#### url 标签

比如你要访问 site.php 控制器下面的 index.html，就可以在模板里面写成：`<a href="{url:/site/index}">返回首页</a>`。

path 字符的前2 个默认为系统的控制器( controller )和动作( action ) ，如果需要传递url 参数，可以直接以 `/` 分隔符，按照 `/变量名/变量值` 这种键值对的形式追加到后面。

比如：` <a href="{url:/test/hello/data/1024}">测试 GET 方法</a>`。

另外也支持绝对URL 路径写法：`{url:http://www.aircheng.com}`。

总之在 iWebShop 模板里面写网页地址的时候，就必须写成这种形式，才能保证路径可以正确显示和支持伪静态等。

- `{webroot:file}` 表示从 iWebShop 根目录下的路径
- `{theme:file}`表示从当前主题目录下的路径
- `{skin:file}` 表示从当前皮肤目录下的路径
- `{js:name}` 表示 iWebShop 系统内置 JS

iWebShop 中所有系统内置的 JS 都在 `\lib\web\js\jspackage_class.php`，也可以自己扩展一些常用工具，在模板里面引用更为简单，如引用jquery 则可写成：`{js:jquery}`。

#### 自定义 PHP 标签

``` php
{set:$name = “iWebShop”;}
{set:$url = Url::createUrl(‘ddd/ddd’);} {set:break;}
```

即是说在模板中想要直接使用 PHP 语法就可以通过这个标签来完成而不要在 `<?php ? >` 这里面写(虽然这样写也可以)。不同之处在于 set 标签后面的表达式可以不带 `;`。

#### 判断类标签

``` php
{if: condition} expression {elseif:condition} expression {else:} expression {/if}
```

#### 循环标签

``` php
{while:condition}expression{/while}
{for:attribute from=v1 upto=v2 downto=v3 setp=v4 item=v5}{/for}

// items 必选 其他可选
{foreach:attribute items=v1 key=k1 item=v2}{/foreach}
```

#### 数据库查询类标签

``` php
{query:attribute}{/query}
```

这是一个十分重要的标签，属性无任何先后顺序，一定要掌握好。

先说说它的属性：

- name: 必选表名(不带表前缀)
- fields:可选默认* 表字段
- where: 可选默认无条件
- join: 可选默认无表连接
- group: 可选默认无分组
- having:可选默认无条件
- order:可选默认无排序
- limit: 可选默认500 取得的数目
- distinct: 可选默认无检索
- page: 可选默认无当前的页数，且启动分页
- pagesize: 可选默认20 每页的记录数
- pagelength: 可选默认10 分页展示栏展示的页码数
- item: 可选默认item 每一记录id: 可选默认 `$query` 每一个对象的唯一标识
- key: 可选默认key 每一项键值
- cache: 可选开启数据库缓存，缓存这个 query 查询结果，可以填写 file,memcache，如要配置 memcache 缓存，必须要保证 memcache 服务器开启，并且当前服务器支持 php 的 memcache 扩展，在 config/config.php 配置中正确设置了 cache 属性。

在属性中如果遇到下面的符号，`> = <`一定要注意转换，这一点十分重要。

##### 分页

``` php
// 分页前
{query:name=goods page=1}
	{$key}:{$item['name']} <br>
{/query}

// 分页后
{set:$page=IReq::get('page')==null?1:IReq::get('page');} {query:name=goods page=$page pagesize=5} {$key}:{$item['name']} <br>
{/query}

{$query->getPageBar()}
```

##### 多表关联

``` php
{query:name=user as u join=left join member as m on m.user_id eq u.id}
{$item[‘user_id’]}
{/query}
```

##### 关于在控制器中进行数据库读写说明

数据库读取一般用 IQuery 类库(也就是上文标签中的 `{query}`)。

数据库写入一般用 IModel 类，目前支持 update(更新)，add(添加)，del(删 除)比如要更新 goods 表的 price 字段，那么就是：

``` php
$goodsDB = new IModel(‘goods’);
$goodsDB->setData(array(‘price’ => 1000));
$goodsDB->update(‘id = 2’);
```

### 主题开发

``` php
'theme' => array(
  'pc' => array(
    'default' => 'default',
    'sysdefault' => 'green',
    'sysseller' => 'green'
),
'mobile' => array(
  'mobile' => 'default',
  'sysdefault' => 'default',
  'sysseller' => 'default'
)),
```

其中 `('pc' => array('default' => 'default','sysdefault' => 'green','sysseller' => 'green')` 代表的意思就是 当在 PC 端访问 iwebshop 前台前端界面的时候，会启用 default 主题下面的 default 皮肤。

`sysdefault` 代表的是后台系统的前端界面 `sysseller` 代表的是商家系统的前端界面。

#### 主题

views 目录下的第一层子目录，子目录名称就是主题的唯一编号。

默认iwebshop 的前台主题编号为 default，系统管理后台 sysdefault，商家管理 sysseller。

iwebshop 在运行时只能选择使用一套主题，从网站的后台，*<系统>--<网站管理>--<主题设置>* 中可以自由选择使用切换当前系统下已经存在的主题方案。

#### 模板主题层次结构图

Views 目录下的每个目录就是一个模板主题，要启用哪个主题都 在 config/config.php 里面的 theme 配置中，包括后台主题和商家主题等。

其中，shop 系统中默认前台主题是 default，默认后台主题是 sysdefault；默认商家主题是 sysseller。

主题的组成：布局模板，普通模板(控制器个性模板)，主题相关的javascript，皮肤。 

皮肤的组成：CSS，图片，皮肤相关的 javascript。如果要新建主题或皮肤只需要在指定的目录位置中新建文件夹即可( 文件夹名称就是主题或皮肤的名称)。

系统允许存放多个主题，且每个主题又可以拥有多套皮肤，在网站运行时可以从中选择一种主题和一种皮肤进行使用。

iwebshop 的模板分为两种类型：布局模板(layouts)和普通模 板。可以简单的理解为共性化的公共模板和个性化的局部模板。对于一个网站来说，很多页面的head (网站头部，包括导航， 广告，引入的js，css 等)和 foot (网站底部，包括备案信息) 往往都是相同的，不同的是页面的主体部分，针对这种情况，shop 的页面在绝大部分情况下采取布局模板和普通模板拼接组合而成(有些特殊情况可以没有布局)。

布局模板的创建有利于模板代码共用，更便于模板的升级与维护，让各个控制器或者动作都具有可塑性，简单方便容易控制等等，所以合理的使用布局是很重要的。

#### 布局

##### 设置布局的三种方式

- 在控制器中增加 `public $layout` 公共属性，表示这个控制器下的所有视图都默认用这个布局

``` php
<?php

class Test extends IController {
	public $layout = '' ;
}
```

- 在主题目录下的 config.php 文件中配置 layout 布局。

``` php
// views/default/config.php

return array(
  'name' => '锦绣前程',
  'author' => 'aircheng',
  'time' => '2014/12/29 10:03:05',
  'version' => '3.0',
  'thumb' => 'preview.jpg',
  'info' => 'aircheng旗下，IwebShop产品首款默认 主题方案，此主题适用于IwebShop 3.0.0 系列产品', 'type' => '商城前台-PC',
  'layout' => array( 'simple' => 'site', 'simple@reg' => 'site_mini' )
);
```

`'layout' => array( 'simple' => 'site', 'simple@reg' => 'site_mini' )` 代表的是控制器 simple 会使用 site 布局，而控制器 simple 下面的 reg 方法使用 site_mini 布局。

- 以在每个动作方法里面设置临时 layout。

``` php
function hello() {
	$this->layout = 'site_mini';
}
```

**注意：如果是模板 layout 增加或者取消的话，必须要清除一下缓存文件。**

##### 布局中引入复用性更高的公共模版

使用 PHP 自定义语法来完成。

假如在 site 控制器模版中要引入其他控制器模版也要复用的 footer.html 底部模版，就可以在 site 控制器所在的普通模版存放路径下新建一个模版 footer.html，然后在主题配置文件 config.php 中清空这个 footer 模版的布局为空（这样是防止重复引入其他控制器模版中的布局模版），最后在 site 控制器的模版底部引入 footer.html：`{set:include 'footer.php';}`。

注意：上面 on_sale.php 是与视图文件编译后同在的一个 路径下，而默认没有对该页面进行操作的话，是不会将 on_sale.html 编译为 on_sale.php 的，因此，在 include 此模版片段的时候需要事先手动编译出来，或者编译到一个 固定的但不是 runtime 目录的路径下，然后引入到完整的视图模版。 

举例说明:  在 index.html 中想要引入 网站公共 footer，过程如下:  首先可以在 views/theme_name/ 目录下面新建一个 includes 文件夹，里面存放的是高度复用的功能模版片段，比如 footer.html。

由于在 iwebshop 模版中，虽然可以使用自定义的 PHP 代码，但是由于视图引擎的存在，只要是模版(无论是布局模版，普通模版还是复用模版片段)，都要经过被视图引擎编译为 HTML 和 PHP 共存的动态页面，然后通过 PHP 引擎刷新后生成最终显示在浏览器上的网页。

所以上面的 `{set: include 'footer.php';}` 中 include 的文件其实是相对于编译后动态页面所在的路径下，当然，也可以使用绝对路径指向定义好的路径。 

同时，如果没有请求过某个页面，那么也不会触发试图引擎机制，因此，如果想要一来就能够成功引入所需的公共底部，那么必须在最开始就把 footer.html 编译为 footer.php。

这是可以办到的，通过 iWebshop 框架中自带的 render() 方法可以手动编译模版文件。像这样：

``` php

```

**注意：**如果写成 `include 'footer.html';` 则会报错，因为编译后的模版统一被命名为 `*.php` 了。 由于 render() 方法会产生静态页面到输出缓冲区，因此为了避免浏览器提前输出编译后的文件，需要在编译结束后使用 `ob_clean()` 清空输出缓冲区。

#### 如何开发模板主题

每个主题(比如：/views/default) 下的目录名称 (site，simple，ucenter…) 都是与控制器目录里面的 `.php` 文件名对应的，你要开发哪个控制器下的模板，就去相应的主题目录下新建与控制器名称相同的目录和与视图 action 相同的 `.html` 文件。

比如 controllers/site.php 这个前台控制器文件，如果你要二次开发主题， 就要在你自定义的主题目录里面，比如: my_theme 目录下， 新建 site 目录，然后把 controllers/site.php 里面所需要的视图都在 /views/my_theme/site/ 目录下新建 `.html` 视图。

> 在对主题和皮肤进行二次开发时，我们强烈建议不要在原有的 default 方案下进行直接修改，您完全可以在 views 目录下新建 一个属于自己的主题目录，如 test，然后把系统默认的 views /default 目录下的所有文件都复制到 test 目录中，最后进入后台 [网站管理后台->系统->网站管理->主题设置]，启用test 方案即可，此后所有的开发都基于此目录。

##### 主题配置文件说明

views / default 目录下的 config.php 文件中的信息会被后台《主题管理》页面所提取展示，以供网站管理员开启。

layout 布局模板和普通模板(前端模板设计的核心)。layout 的主要作用是呈现页面的整体轮廓和公共 head，foot 等公共部分。
两者的相同之处在于模板标签语法都是相同的。

##### 布局文件

分析 layout 文件的源码可见，每份 layout 布局文件都已经包含了 一份完整 HTML 的头部和底部(公共部分)，而普通模板只是一 个完整 HTML 页面的片段(个性部分)，最终用户可见的页面就是由 layout 布局文件 + 普通模板片段组成。

而实现布局文件+普通模板文件这个联系的关键，就是位于 layout 文件中的 `{viewcontent}` 这个标签，只有布局文件中才会有这个标签。因为系统在运行时会自动把此标签替换为浏览器要访问的普通模板内容，所以你可以把这个标签理解为是一个普通模板，他被嵌入在布局模板之中。

布局是被定义在各个控制器文件中的 `$layout` 属性中，各个控制器的文件名与主题方案(default)目录下的各个模板文件夹的名称是一一对应的，也就是说如果在某个控制器中设置了布局模板 (`$layout` 属性值)，则该控制器下的所有模板默认都会调用这个布局模板。

## 自定义 JS

### Validate 插件

首先要通过 js 标签引入此插件，然后只要有 form 表单中的 input 标签中 type 为 text, password, select-one, textarea 中添属性 pattern 和 alt 属性系统将会自动添加验证功能。

举例说明，在 hello.html 视图中想要验证 email 格式是否合法：

``` html
<!-- 首先引入 validata.js 插件 -->
{js:validate}

<form>
  <input name="eamil" pattern="email" alt="请输入正确的 email">
</form>
```

然后访问该视图文件后就可以看到验证效果了。对于 pattern 的正则系统对常用的正则都作了一些封装：

```
required，email，qq ，id，ip ，zip，phone，mobi ，url ，date，datetime ，int ，float
```

当然也可以自己写正则。比如：我想让用户输入一个 3-5 位的数字，则修改代码如下：`<input name='email' pattern='^\d{3,5}' alt='请输入一个3-5 位的数字'>`。

另外要注意，要验证时，隐藏字段会失效，不进行验证处理。

### 校验后执行自定义回调方法

这里回调的意思就是在校验结束后执行自定义的 JS 函数：

``` html
{js:validate}
<form callback='test("回调一下")'>
<input name='email' pattern='^\d{3,5}' alt='请输入一个3-5 位的数字'> <input type='submit'/>
</form>
<script>
function test(txt)
{
alert(txt);
return false;
}
</script>
```

如果返回的是 true，表单通过验证后，表单将提交，不通过将不提交。 如果返回的是 false，则无论表单是否通过表单都将不会被提交。

### 表单回填

比方说，你想让表单里的项初始化一些值：

``` html
{js:form}
<form name='test'>
<input name='email'></br>
<input type="radio" name="sex" value='1'>男
<input type="radio" name="sex" value='0'>女</br>
<input type="checkbox" name="live" value='a'>看书
<input type="checkbox" name="live" value='b'>打球</br>
<select name="code">
	<option value="c++">c++</option>
  	<option value="java">java</option>
  	<option value="php">php</option>
</select>
<input type='submit'/> </form>

<script>
var form = new Form('test'); form.init({'email':'a@a.com','sex':'1','live':'a;b','code':'php'});
</script>
```

同样也可以用 getItems 取得表单里所有数据的对象。也可以用 `setValue(name, value)` 的方式只给一个设置对应的值，也可以通过getValue(name) 得到对应项的值。如下：

``` javascript
form.setValue('email', 'test@root.com' );
alert(form.getValue('sex'));
console.log = form.getItems();
```

要注意几点：

- JS 中操作的 key 是标签里面的 name 属性值，因此必须准确指定。
- 多选的时候是用;号来分开的。
- 此 form 插件不能对 file 字段进行处理。（其他 JS 类库自行搜索使用方法）

## 数据库相关

### 关于分类

iWebShop 的分类表有两张：

- category

同时包含了父分类和子分类，如果需要查询父分类有多少，应该是这样：

``` sql
select count(*) from prefix_category where parent_id=0;
```

- category_extend

包含了子分类下的所有商品ID，根据子分类 id 到 goods 表中查询到该分类下的所有商品

``` sql
select * from prefix_category_extend where category_id=xxx;    -- 得到商品 ID 数组

select * from prefix_goods where id=xxx;    -- 得到某个具体的商品详情
```

## 已知 Bug 及其解决

### 4.4 版本

- 数据字典中的用户信息表和实际数据库中的不一致：email 字段。
- 商家后台删除咨询未回复信息无效( 默认要回复过的商品才能删除 )。   
- refer_list.html 需要替换：

``` html
<article class="module width_full">
	<header>
		<h3 class="tabs_involved">咨询列表</h3>
		<ul class="tabs">
			<li><input type="button" class="alt_btn" onclick="selectAll('id[]');" value="全选" /></li>
			<li><input type="button" class="alt_btn" onclick="delModel();" value="批量删除" /></li>
		</ul>
	</header>

	<form action="{url:/seller/refer_del}" method="post" name="goodsForm">
		<table class="tablesorter" cellspacing="0">
			<colgroup>
				<col width="25px" />
				<col />
				<col width="120px" />
				<col width="130px" />
				<col width="120px" />
				<col width="130px" />
				<col width="80px" />
			</colgroup>

			<thead>
				<tr>
					<th class="header"></th>
					<th class="header">商品名字</th>
					<th class="header">咨询人</th>
					<th class="header">咨询时间</th>
					<th class="header">回复人员</th>
					<th class="header">回复时间</th>
					<th class="header">操作</th>
				</tr>
			</thead>

			<tbody>
				{set:$seller_id = $this->seller['seller_id']}
				{set:$page = IReq::get('page') ? IFilter::act(IReq::get('page'),'int') : 1}
				{query:name=refer as re join=left join goods as go on go.id eq re.goods_id left join user as u on u.id eq re.user_id left join admin as a on a.id eq re.admin_id left join seller as se on se.id eq re.seller_id fields=se.seller_name,a.admin_name,u.username,re.*,go.name where=go.seller_id eq $seller_id page=$page id=$referObj}
				<tr>
					<td><input name="id[]" type="checkbox" value="{$item['id']}" /></td>
					<td><a href="{url:/site/products/id/$item[goods_id]}" target="_blank" title="{$item['name']}">{$item['name']}</a></td>
					<td>{if:isset($item['username'])}{$item['username']}{else:}非会员{/if}</td>
					<td>{$item['time']}</td>
					<td>{if:$item['admin_name']}{$item['admin_name']}{elseif:$item['seller_name']}{$item['seller_name']}{/if}</td>
					<td>{$item['reply_time']}</td>
					<td>
						<a href="{url:/seller/refer_edit/id/$item[id]}"><img src="{skin:images/main/icn_audio.png}" title="回复咨询" /></a>
						<a href="javascript:delModel({link:'{url:/seller/refer_del/id/$item[id]}'})"><img src="{skin:images/main/icn_del.png}" title="删除" /></a>
					</td>
				</tr>
				{/query}
			</tbody>
		</table>
	</form>
	{$referObj->getPageBar()}
</article>
```

## FAQ

- 做二次开发的话有必要重写控制器吗？比如 site、simple 这些? 

根据自己需求，如果是单纯模板样式修改一般不用，增加数据或者是业务逻辑就必须修改。

- 很多逻辑相似但是又有一些需要新加的，这种情况是重新写一份好还是直接在现有逻辑上修改好？

很多相似就在基础上修改，但是以后无法升级，也可以重新新建控制器，以后可以方便升级和代码移植。

- 新写的话有很多东西也是复制粘贴过去感觉复用性太差，继续继承下去好还是不好？

可以考虑直接在基础上修改或新增新逻辑，也可以新写控制器继承相思逻辑的控制器。

- 那二次开发的话，内核目录下面的是不是可以完全不用管？

根本不用。 

- 后台的功能如果不够用怎么不破坏原来逻辑的基础上新增逻辑? 

直接在控制器里面扩展方法，新开发的功能新建控制器。

- 对后台功能相关的控制器的修改和对前台功能相关的控制器的修改有什么不同的吗？

看继承的权限。

- 怎么在不关心内核的基础上搞懂继承权限啊？
- 管理员后台和商家后台的前端可以自定义吗? 
- 模型在 iwebshop 中指的是什么？

商品属性的集合，也可以做商品列表的查询属性，具体参考使用手册里面的说明。 

- 关于 iwebshop 二次开发？
  - 一般开发只需要了解控制器，classes，views，明确控制器和视图的关系，你重点掌握 模板开发手册，知道根 据url找到控制器和模板，可以看地址修改调试文件，你知道去哪个文件里面找，知道他的运行顺序原理。
  - 熟练掌握模板标签，query，imodel 的数据库读写。
  - 熟悉 classes下面各个模块是管理哪方面内容的，知道在控制器或者模板里面可以任意调用classes。
  - 另外，多看范例代码，比如默认版本的 default 和控制器代码，毕竟都是开源的，整个 mvc 运行模式都是固定的。 
- 商品和货品的关系？

当商品里面有规格的时候，就必须是货品。每个规格的货品有自己的独立价格，库存等。

- 规格和模型如何使用? 

规格和模型，只有规格会影响价格。所以，在为商品选择这些附加属性的时候，与价格相关的商品属性必须放在规格中去。一个商品可以有多个规格，但只能有一种模型/属性。 

- 一些商品后台并没有规格但是在结算的时候怎么还是会提示要先选择商品的规格？

检查模板里面有无规格数据生成，看看 js 在 products 里面提示的判断。

- 资金是否由买家支付到平台，交易完成后由平台转账至商家？

商家提出结算申请，平台是通过线下转款。就是比如 A 商家结算 10w，商城的财务通过支付宝，或者公司网银给商家转账。

- 支付逻辑是怎样的？

支付接口就在各个类里面，就是那么 3 步：getData，send，callback。    

- `<%%>` 是什么标签语法? 

[artTemplate](https://github.com/aui/artTemplate) 模板引擎。

- 在后台可以更换 iwebshop 默认的 logo 吗？我的意思是改了会不会出现所谓的版权问题？

你不是购买了版权费么？可以随意修改，只要是不要给别人分发和二次销售就没问题。

> 商家后台文字水印修改：/lib/web/js/source/highcharts/ highcharts.js。

- goods_attribute 表中的 attribute_value 和 attribute 表中的 value 有啥区别？

一个父一个子的关系：attribute 父，goods_attribute 子。

按商品属性筛选的时候，对比的是子属性。因为，据属性查询商品，肯定是查询的属性和商品的关系表，那就是 goods_attribute。

父属性会在设置商品模型的时候用到，因为一个属性可以有多个数据。

- 用户页面确实存在但是却提示找不到文件？

多半是被 iwebshop 的权限机制限制住了。

``` php
Class Ucenter implements userAuthorization
```

- 4.4beta 版和正式版数据库表结构有变化吗？（有）
- 那如何安全导入beta版本的数据呢？

对比一下2个版本的 install.sql。4.4 正式版 user 表里面的 email 放到了 member 表里面。

## 附录

### 4.4 countsum() 返回值

生成订单，当从购物车计算时 countsum() 返回值：

``` php
Array
(
    [final_sum] => 6723.6
    [promotion] => Array
        (
        )

    [proReduce] => 0
    [sum] => 6723.6
    [goodsList] => Array
        (
            [0] => Array
                (
                    [name] => 澳大利亚 进口牛奶 德运 （Devondale ）全脂牛奶200ml*24整箱装
                    [cost_price] => 
                    [goods_id] => 9
                    [img] => upload/2015/06/16/54744268Nce71bebf.jpg
                    [sell_price] => 129.00
                    [point] => 0
                    [weight] => 500.00
                    [store_nums] => 100
                    [exp] => 0
                    [goods_no] => 1284568
                    [product_id] => 0
                    [seller_id] => 0
                    [reduce] => 0
                    [count] => 21
                    [sum] => 2709
                )

            [1] => Array
                (
                    [name] => 供应藏族成年表演服 个性民族手工艺舞台服 接待迎宾演出服
                    [cost_price] => 
                    [goods_id] => 29
                    [img] => upload/2015/06/16/20150616232740178.jpg
                    [sell_price] => 696.00
                    [point] => 0
                    [weight] => 0.00
                    [store_nums] => 8000
                    [exp] => 0
                    [goods_no] => SD143446846079
                    [product_id] => 0
                    [seller_id] => 0
                    [reduce] => 0
                    [count] => 1
                    [sum] => 696
                )

            [2] => Array
                (
                    [name] => 荔枝纹头层牛皮双肩包 时尚休闲包 真皮女包手提包
                    [cost_price] => 
                    [goods_id] => 43
                    [img] => upload/2015/06/16/20150616233303131.jpg
                    [sell_price] => 280.00
                    [point] => 0
                    [weight] => 0.00
                    [store_nums] => 468
                    [exp] => 0
                    [goods_no] => SD143446878394
                    [product_id] => 0
                    [seller_id] => 0
                    [reduce] => 0
                    [count] => 1
                    [sum] => 280
                )

            [3] => Array
                (
                    [name] => 2014C春夏手提单肩铆钉包 欧美时尚真皮女包 休闲单肩包广州包
                    [cost_price] => 
                    [goods_id] => 95
                    [img] => upload/2015/06/16/20150616233331962.jpg
                    [sell_price] => 385.00
                    [point] => 0
                    [weight] => 0.00
                    [store_nums] => 100
                    [exp] => 0
                    [goods_no] => SD143446881185
                    [product_id] => 0
                    [seller_id] => 0
                    [reduce] => 0
                    [count] => 1
                    [sum] => 385
                )
        )

    [count] => 32
    [reduce] => 0
    [weight] => 13000
    [point] => 0
    [exp] => 0
    [tax] => 672.36
    [freeFreight] => Array
        (
        )

    [seller] => Array
        (
            [0] => 6723.6
        )

)
```

### 常用 API

- get_browser()
- get_defined_vars()
- dirname()
- defined() / define()
- or：短路作用。
- `__FILE__`
- `DIRECTORY_SEPARATOR`

### 关于二次开发

#### What

> 二次开发，简单的说就是在现有的软件上进行定制修改，功 能的扩展，然后达到自己想要的功能和效果，一般来说都不 会改变原有系统的内核。 

#### Why

> 随着信息化技术的不断发展，IT行业涌现出了一系列优秀的开源作品，其作者或是个人，或是项目小组，或是软件公司。选择和应用这些优秀的开源软件，并在此基础上进行符 合业务需求的二次开发，将给企业节省信息化成本(时间成本及开发成本)的同时，更能带来技术上的保障。
>
> 二次开发不仅仅是开发，而更重要的是吸取精华，总结经验，理顺思路，少走弯路，提升自己。 
>
> 这就是我们常听的：站在巨人的肩膀上，你将看的更远。

#### How

第一，你要有这个开源产品的所用语言的语言基础，能看懂代码是最基本的。  

##### PHP 开源产品二次开发的基本要求

> 第一， 基本要求:HTML(必须要非常熟悉)，PHP(能看懂代码， 能写一些小系统，如：留言板，小型CMS)，Mysql(至少会一种数据 库)，Javascript(能看懂，能改现成的一些代码)，Div+Css(能进行 界面的调整，明白CSS是怎么使用的) 。
>
> 第二， 熟悉开源产品的使用，比如 Dedecms，你要知道怎么登录，怎 么新建栏目，怎么添加文章，模板标签的使用方法，模型的概念和使 用方法等等一些功能。这个过程一般的开源产品都有技术手册。
>
> 第三， 要熟悉这个开源产品的数据库结构（数据字典），还要理解里面核心文件的 内容，比如：数据库类怎么使用，常用的安全过滤是怎么做的，模板 引擎又是怎么使用的等等一些核心内容。还要知道这个开源产品的目 录结构，就是说，你要知道哪是放模板的，哪里是做控制的，哪里是 放样式的，等等。
>
> 第四， 熟悉你的需求，对需求进行解读，然后确定如何对这个开源产 品进行修改和扩展 经过二次开发后，你能获取到的是什么呢？你能完 成你的需求，你能积累经验，这里的经验有你自己的，也有别人的。 所谓别人的，就是在你做这个二次开发的时候，你能吸收到这个系统 的精华，然后融入到你自己的思想里，你还能总结项目架构的经验。
>
>  有句话说的好，就是：**聪明的人会把别人的失败的经验当作自己的经验，而傻的人就是自己无数次体验失败后才作为自己的经验**。 

第二，你要对这个开源产品的功能和使用要有比较熟悉，因为你熟悉 了，你才知道一个需求下来，你要改什么，什么是系统自带的，大概要怎么改。

第三，你要熟悉这个开源产品的数据结构，代码结构，系统的框架结构，核心是哪里，附属功能是在哪里。简单点说，就是数据库，代码逻辑，文件目录的熟悉。

第四，根据你的需求，然后利用开源产品的内核，进行系统的扩展和修改，以达到你的需求。

第五，多问技术支持。因为有时候手册基本上描述得都是很少的，很多逻辑直接问技术支持最省时间。

##### 解决手段不要局限于现有框架

无论是什么框架还是系统，都逃不掉属于某个语言的范畴，实在不知道怎么办的，可以从语言本身的手段想解决办法。

总之，不要迷信框架，不要被框架束缚手脚。

