---
layout: post
title: MySQL Tricks
category: Database
tag: MySQL
---

持续记录使用 MySQL 过程中遇到过的偏冷知识点和坑。

<!-- more -->

## CLI

### enable `mysql` command on mac

``` shell
# 1. install mysql workbench
brew cask install mysqlworkbench

# 2. export environment variables
export PATH=$PATH:/Applications/MySQLWorkbench.app/Contents/MacOS

# 3. use
mysql -h {host} -u {user} -p {password}
```

### [mycli](https://www.mycli.net)

```shell
pip install mycli
brew install mycli
```

有语法提示，简直不能太赞。使用和普通 mysql 命令一样。

### over SSH Tunnel

``` shell
# -f => background
# -L => bind port between local and remote
# -N => don't execute command just forwarding ports
ssh -f user@ssh.example.com -L 3307:mysql_server.example.com:3306 -N

mysql -h 127.0.0.1 -P 3307 -u<mysql_user> -p <mysql_user_password>

# Kill SSH Tunnel progress
# Get pid
netstat -vanp tcp | grep 3307
lsoif -i:3307

kill -9 {pid}
```

## 语法

### NULL vs NOT NULL

除非真的要保存 NULL，否则尽量避免使用 NULL。

但把 NULL 列改为 NOT NULL 带来的性能提升很小，所以除非确定它引入了问题，否则就不要把它当作优先的优化措施。

### index with nullable field

当 where 条件是 `column = cond`  ，column 是 is null，MySQL 也能对其进行优化，即 MySQL 可以使用索引来查找 nullable 字段。

但是，当 where 条件种包含如下情况时，该优化不会生效：

``` sql
SELECT * FROM user WHERE `name` IS NULL
```

其中，`name` 字段的定义是 NOT NULL。该情况通常出现在联合查询时。

> https://dev.mysql.com/doc/refman/5.7/en/is-null-optimization.html

### FROM DUAL

```mysql
SELECT DATABASE() FROM DUAL;
```

> There is much discussion over whether or not FROM DUAL should be included in this or not. On a technical level, it is a holdover from Oracle and can safely be removed. If you are inclined, you can use the following instead: `SELECT DATABASE();`
>
> That said, it is perhaps important to note, that while FROM DUAL does not actually do anything, it is valid MySQL syntax. From a strict perspective, including braces in a single line conditional in JavaScript also does not do anything, but it is still a valid practice

### `having` vs `where`

``` sql
create table `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(16) NOT NULL,
  `gender` tinyint NOT NULL,
  `age` tinyint NOT NULL,
  PRIMARY KEY (`id`)
);
```

`having` 使用的是从已经筛选出来过的字段（包括别名），而 `where` 使用的是表中定义好的字段。

``` sql
select name from user where age > 18;    -- 不会报错
select name from user having age > 18;   -- 会报错 `Unknown column 'age' in 'having clause'`
select * from user having age > 18;    -- 不会报错, `*` 已包含了 `age`

-- 不会报错
select name, avg(age) as avg_age from user group by gender having avg_age > 18;
-- 报错，表 user 不存在 `avg_age` 字段
select name, avg(age) as avg_age from user where avg_age > 18 group by gender;
```

`where` 性能高于 `having`，能写在 where 限定条件中的就尽量写在 where 中。

#### 同时使用 having 和 where 的顺序

``` sql
SELECT ... FROM ... WHERE ... GROUP BY ... HAVING ...
```

### `group by` 的方便和坑

- 示例0：统计不同性别的用户个数（同一列中不同值的个数）：

``` sql
-- 1. count
select
	count(gender = 1 OR NULL) as boy,
	count(gender = 2 OR NULL) as girl,
	count(gender = 3 OR NULL) as gay,
from user

-- 2. sum
select
	sum(if(gender = 1, 1, 0)) as boy,
	sum(if(gender = 2, 1, 0)) as girl,
	sum(if(gender = 3, 1, 0)) as gay
from user;

select
	sum(gender = 1) as boy,
	sum(gender = 2) as girl,
	sum(gender = 3) as gay
from user;

-- 4. group by
select gender, count(*) as cnt from user group by gender;
```

- 示例1: 查询学生不同分数的集合 (group_concat)

> `group_concat` 在一个语句中得到 GROUP BY 被 聚合的项的每个子值的一个组合的字符串

``` sql
select
    student_name,
    group_concat(distinct score order by score desc separator '') as score_list
from student
group by student_name
```

- 示例2: 显示所有年份的总价值 (with rollup) 

> GROUP BY 子句允许一个将额外行添加到简略输出端 WITH ROLLUP 修饰符。这些行代表高层(或高聚集)简略操作。

> ROLLUP 因而允许你在多层分析的角度回答有关问询的问题，或者你可以使用 ROLLUP, 它能用一个问询提供双层分析。

> 将一个 WITH ROLLUP 修饰符添加到 GROUP BY 语句，使询问产生另一行结果。：

```
SELECT year, SUM(profit) FROM sales GROUP BY year WITH ROLLUP;

+------+-------------+
| year | SUM(profit) |
+------+-------------+
| 2000 |        4525 |
| 2001 |        3010 |
| NULL |        7535 |
+------+-------------+
```

总计高聚集行被年份列中的 NULL 值标出。

- 示例3: 一个以年、国家和产品为基础的关于 sales 表的一览表 (多重 group by + with rollup)

当有多重 GROUP BY 列时，ROLLUP产生的效果更加复杂。

这时，每次在除了最后一个分类列之外的任何列出现一个 “break” (值的改变) ，则问讯会产生一个高聚集累计行。

``` sql
SELECT year, country, product, SUM(profit)
FROM sales
GROUP BY year, country, product with rollup;
+------+---------+------------+-------------+
| year | country | product    | SUM(profit) |
+------+---------+------------+-------------+
| 2000 | Finland | Computer   |        1500 |
| 2000 | Finland | Phone      |         100 |
| 2000 | Finland | NULL       |        1600 |
| 2000 | India   | Calculator |         150 |
| 2000 | India   | Computer   |        1200 |
| 2000 | India   | NULL       |        1350 |
| 2000 | USA     | Calculator |          75 |
| 2000 | USA     | Computer   |        1500 |
| 2000 | USA     | NULL       |        1575 |
| 2000 | NULL    | NULL       |        4525 |
| 2001 | Finland | Phone      |          10 |
| 2001 | Finland | NULL       |          10 |
| 2001 | USA     | Calculator |          50 |
| 2001 | USA     | Computer   |        2700 |
| 2001 | USA     | TV         |         250 |
| 2001 | USA     | NULL       |        3000 |
| 2001 | NULL    | NULL       |        3010 |
| NULL | NULL    | NULL       |        7535 |
+------+---------+------------+-------------+
```

说明：当你使用 ROLLUP时, 你不能同时使用 ORDER BY子句进行结果排序。

> See: <https://m.jb51.net/article/40712.htm>

- 坑：性能

如果没有用到索引数据量一般大也会巨慢，甚至出现 MySQL CPU 爆高，超时等。

`group by `与 `order by` 的索引优化基本一样，`group by` 实质是先排序后分组，也就是分组之前必排序，遵照索引的最佳左前缀原则可以大大提高 `group by` 的效率。

### 表间复制

> MySQL 不支持 `select ... into ... from ...` 语法。

- `insert into ... select …` 和 `create table ... like ...`

要求插入的表存在，举例说明，往表 user_tmp 插入表 user 的部分字段：

``` sql
create table user_tmp like user;
insert into user_tmp (name, mobile) select name, mobile from user;
```

注意：如果不带 where 条件，则将 user 表中的所有记录的 name，mobile 字段逐条插入到 user_tmp 表中。

- 创建表并插入到该表

``` sql
create temporary table user_tmp (select name, mobile from user);    # 创建临时表
create table user_tmp (select name, mobile from user);    # 创建物理表
```

### JOIN 姿势的区别

- inner join：取得两个表中「存在」连接匹配关系的记录，产生的是两表交集。
- left join：取得左表完全记录，即使右表并无对应匹配记录；如果右表没有匹配的，则为 NULL。
- right join：取得右表完全记录，即使左表并无匹配对应记录；如果左表没有匹配的，则为 NULL。

## 隐式特性（坑）

### Implict Commit

#### 部分事务隐式强制性提交

It's a limitation of MySQL, not of PDO/PHP.

> [Some databases, including MySQL, automatically issue an implicit COMMIT when a database definition language (DDL) statement such as DROP TABLE or CREATE TABLE is issued within a transaction. The implicit COMMIT will prevent you from rolling back any other changes within the transaction boundary.](http://www.php.net/manual/en/pdo.begintransaction.php)

> CREATE TABLE操作背后涉及到了大量的操作，不仅仅包括对核心表的操作，还包括大量内存数据结构的更新（如Schema），以及存储系统的变更（如创建相应的数据块），工程上很难把这些操作做成原子的。
>
> 那么，应该如何做呢？比较折中的方式就是跟用户做一个约定：CREATE TABLE操作总默认COMMIT它之前的事务，这就是implict commit。

详情可参考[这篇博客](http://blog.csdn.net/maray/article/details/46410817)。

## binlog

### 查看 binlog

``` sql
mysqlbinlog -vv --base64-output=decode-rows /path/to/mysql-bin-log
```

### 恢复成SQL

``` shell
# 恢复本地 binlog

mysqlbinlog \
--no-defaults \
--base64-output=AUTO \
--verbose \
--set-charset=utf8 \
--start-datetime='2017-07-16 00:00:00' \
--stop-datetime='2017-07-17 00:00:00' \
./mysql-bin.000611 > /data/restore.sql

# 恢复远程服务器 binlog
mysqlbinlog \
--no-defaults \
-uwaimai_o2o -p \
-hrm-wz93132ahz16ttyhq.mysql.rds.aliyuncs.com \
--read-from-remote-server \
mysql-bin.000618 > restore.sql
```

> https://dev.mysql.com/doc/refman/5.7/en/mysqlbinlog.html

### 使用不小于 MySQL server 的版本

如果 mysqlbinlog 版本太低，则会出现类似报错如下：

``` shell
ERROR: Error in Log_event::read_log_event(): 'Sanity check failed', data_len: 151, event_type: 35
ERROR: Could not read entry at offset 120: Error in log format or read error.
```

## 性能

### 查询缓存

- 开启

``` mysql
show variables like '%query_cache%';
-- have_query_cache=YES 代表支持 Query Cache 而不是已开启
-- query_cache_size 的值非零时表示已开启
```

- 查看当前查询缓存使用状态

``` sql
show global status like 'qcache%';
```

> Qcache_free_blocks:表示查询缓存中目前还有多少剩余的blocks，如果该值显示较大，则说明查询缓存中的内存碎片过多了，可能在一定的时间进行整理。
>
> Qcache_free_memory:查询缓存的内存大小，通过这个参数可以很清晰的知道当前系统的查询内存是否够用，是多了，还是不够用，DBA可以根据实际情况做出调整。
>
> Qcache_hits:表示有多少次命中缓存。我们主要可以通过该值来验证我们的查询缓存的效果。数字越大，缓存效果越理想。
>
> Qcache_inserts: 表示多少次未命中然后插入，意思是新来的SQL请求在缓存中未找到，不得不执行查询处理，执行查询处理后把结果insert到查询缓存中。这样的情况的次 数，次数越多，表示查询缓存应用到的比较少，效果也就不理想。当然系统刚启动后，查询缓存是空的，这很正常。
>
> Qcache_lowmem_prunes:该参数记录有多少条查询因为内存不足而被移除出查询缓存。通过这个值，用户可以适当的调整缓存大小。
>
> Qcache_not_cached: 表示因为query_cache_type的设置而没有被缓存的查询数量。
>
> Qcache_queries_in_cache:当前缓存中缓存的查询数量。
>
> Qcache_total_blocks:当前缓存的block数量。

- `query_cache_type`
  - `0` => OFF，不缓存
  - `1` => ON，缓存除 `SELECT SQL_NO_CACHE` 之外所有查询
  - `2` => DEMAND，仅缓存 `SELECT SQL_CACHE` 查询

### 锁

#### Waiting for table metadata lock

metadata, 顾名思义，指表结构元数据, 之所以出现 metadata lock (5.5.3+), 是因为 DDL 破坏了事务的隔离级别。

> 5.5.3版本之前,MySQL事务对于表结构元数据(Metadata)的锁定是语句(statement)粒度的：即语句执行完成后，不管事务是否可以完成，其表结构就可以被其他会话更新掉！
> 引入Metadata lock后，表结构元数据(Metadata)的锁定变成了事务(transaction)粒度的,即只有事务结束时才会释放Metadata lock。

总之，出现 metadata lock 的原因主要是：MYSQL 有空闲进程，且遗留的事务没有完成。多半是操作数据的代码导致的。可查询数据库有关状态：

``` sql
show processlist;    -- 查看 mysql 实例的运行情况

show table status like 'table_name_xxx';    -- 查看某张表的数据量等状态

show engine innodb status;    -- 查看 InnoDB 引擎的状态

select * from performance_schema.events_waits_current;    -- 查询当前处理等待状态的事务

select * from information_schema.innodb_trx;    -- 查找正在运行的事务(观察 running 状态)

show open tables;    -- 查看当前 mysql 进程打开的表

kill {thread_id};    -- 杀死处理 Sleep 状态但却有运行事务的进程
```

> see: 
>
> - <https://blog.csdn.net/wlzjsj/article/details/50208957>
> - <https://www.cnblogs.com/digdeep/p/4892953.html>
> - <https://stackoverflow.com/questions/37306568/how-to-solve-mysql-innodb-waiting-for-table-metadata-lock-on-truncate-table>

### 深度翻页慢

说起翻页，很常见的做法的都是使用 `limit {start} offset {offset}`， 而 MySQL 对 limit + offset 的做法是先取出 limit + offset 的所有数据，然后去掉 limit 返回 offset，这将导致 offset 值比较大的时候，分页查询性能较低。

> See:
> -  <https://my.oschina.net/No5stranger/blog/158202>
> - <http://ourmysql.com/archives/1451>

## 版本有关问题

- **Specified key was too long; max key length is 767 bytes**

> 767 bytes is the stated prefix limitation for InnoDB tables in MySQL version 5.6 (and prior versions). It's 1,000 bytes long for MyISAM tables. In MySQL version 5.7 and upwards this limit has been increased to 3072 bytes.
>
> <https://stackoverflow.com/questions/1814532/1071-specified-key-was-too-long-max-key-length-is-767-bytes>

## MySQL 工具

### 统计

- 统计表/字段占用的磁盘空间

``` sql
SELECT table_name AS `Table`, round(((data_length + index_length) / 1024 / 1024), 2) `Size (MB)` FROM information_schema.TABLES WHERE table_schema = '{DATABASE_NAME}';

SELECT sum(char_length({FIELD_NAME}))/1024/1024 as 'Size (MB)' FROM {TABLE_NAME};
```

###  备份／恢复

``` shell
# 全库导出
mysqldump -h{MYSQL_HOST} -u{MYSQL_USER} -p {DB_NAME} | gzip > /path/to/file.sql.gz

# 导出指定数据库指定表的指定条件的数据
mysqldump -h{MYSQL_HOST} -u{MYSQL_USER} -p --databases {DB_NAME} --tables {TABLE_NAME} --where '{CONDITIONS}' | gzip > /path/to/file.sql.gz

# 导出指定 SQL 的数据记录
mysql -e '{SOME_SQLS}' -u {MYSQL_USER} -p {DB_NAME} > /path/to/file

# 从压缩文件中导入新数据库
gzip -dc /path/to/file.sql.gz | mysql -h{MYSQL_HOST} -u{MYSQL_USER} -p {DB_NAME}
```

## 主机连接

### 127.0.0.1 vs localhost

使用 mysql cli 客户端连接 docker 容器中的 mysql，使用 `127.0.0.1` 可以连接上，但是使用 `localhost` 却不能连：

``` shell
mysql -h 127.0.0.1 -uroot -p*****    # Success

mysql -h localhost -uroot -p*****    # Failed
ERROR 2002 (HY000): Can't connect to local MySQL server through socket '/tmp/mysql.sock' (2)
```

其实这个问题不属于 MySQL 范围内的问题，所有服务器程序（ssh, telnet...）都会遇到这些问题。

根本原因是由服务端程序实际上监听的地址是一个 IPv4 还是一个 hostname。

比如这里的 MySQL，如果 MySQL server 只是监听了 127.0.0.1 这个 IPv4 地址，那么客户端就只能使用 127.0.0.1 这个 IP 连接。

此外，从错误信息中还可以总结一点，unix-like 操作系统默认会把 `localhost` 当作 UNIX domain socket，而 windows 默认就认为是 TCP/IP，这样的结果是：

- 在 unix-like OS 上，如果指定地址为 `localhost` 那么 OS 会默认常识使用 Unix domain socket；如果指定地址为 `127.0.0.1`，那么 OS 才会使用 TCP/IP。
- 在 windows 上，无论指定 `localhost` 还是 `127.0.0.1`，OS 都会使用 TCP/IP。

>  UNIX socket is an inter-process communication mechanism that allows bidirectional data exchange between processes running on the same machine.
>
>  IP sockets (especially TCP/IP sockets) are a mechanism allowing communication between processes over the network. In some cases, you can use TCP/IP sockets to talk with processes running on the same computer (by using the loopback interface).
>
>  UNIX domain sockets know that they’re executing on the same system, so they can avoid some checks and operations (like routing); which makes them **faster and lighter than IP sockets**. So if you plan to communicate with processes on the same host, this is a better option than IP sockets.
>
>  <https://serverfault.com/questions/124517/whats-the-difference-between-unix-socket-and-tcp-ip-socket>

### Too many connections

``` 
max_connections=1024

wait_timeout=15    # The number of seconds the server waits for activity on a connection before closing it
```

### 重置 root 密码

``` sql
UPDATE mysql.user SET authentication_string = PASSWORD('NewPassword')
WHERE User = 'root' and Host = 'localhost';
```

重启 MySQL：

``` shell
sudo systemctl restart mysqld
# Or in docker
sudo docker restart {mysql}
```

### 新增用户

> https://dev.mysql.com/doc/refman/5.7/en/adding-users.html

``` sql
CREATE USER 'read1'@'127.0.0.1' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON *.* TO 'read1'@'127.0.0.1' WITH GRANT OPTION;

show create user read1\G

ALTER USER 'read1@'127.0.0.1' ACCOUNT UNLOCK;
-- ALTER USER 'read1'@'127.0.0.1' ACCOUNT LOCK;
```

## FAQ

- utf8mb4_unicode_ci vs utf8mb4_unicode_ci?

在对字符串排序和大小比较时，utf8mb4_unicode_ci 比 utf8mb4_general_ci 更精确，utf8mb4_general_ci 比 utf8mb4_unicode_ci 更快。 推荐 utf8mb4_unicode_ci。

> see: [What's the difference between utf8_general_ci and utf8_unicode_ci](https://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci) & [mysql使用utf8mb4经验吐血总结](http://seanlook.com/2016/10/23/mysql-utf8mb4/)

- 获得一个数据库里面表的数量

``` sql
use dbname;
show tables;
select found_rows();
```

或者 `SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = 'dbname';`。

- SELECT list is not in GROUP BY clause and contains nonaggregated column … incompatible with `sql_mode=only_full_group_by`

``` sql
SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));
```

> <https://stackoverflow.com/questions/41887460/select-list-is-not-in-group-by-clause-and-contains-nonaggregated-column-inc/41887627>

- docker mysql 设置 `ONLY_FULL_GROUP_BY`？

```
docker exec -it {docker-container} /bin/bash
echo "sql_mode=STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION">>/etc/mysql/mysql.conf.d/mysqld.cnf
exit

docker restart {mysql-container}
```

> <https://michaelzx.github.io/2016/09/21/docker-mysql-sql-mode/>

## 参考

- *[MySQL :: MySQL 5.7 Reference Manual :: B.5.4.3 Problems with NULL Values](https://dev.mysql.com/doc/refman/5.7/en/problems-with-null.html)*
- *[Are many NULL columns harmful in mysql InnoDB?](https://dba.stackexchange.com/questions/15335/are-many-null-columns-harmful-in-mysql-innodb)*
- *[how to know the date of insertion of record in SQL](https://stackoverflow.com/questions/30367982/how-to-know-the-date-of-insertion-of-record-in-sql)*
- _[DATETIME DEFAULT NOW() finally available.](http://optimize-this.blogspot.com/2012/04/datetime-default-now-finally-available.html)_
- *[SQL How to replace values of select return?](https://stackoverflow.com/questions/16753122/sql-how-to-replace-values-of-select-return)*
- *[MySQL connection over SSH tunnel - how to specify other MySQL server?](https://stackoverflow.com/questions/18373366/mysql-connection-over-ssh-tunnel-how-to-specify-other-mysql-server)*
- *[PDO: Transactions don't roll back?](https://stackoverflow.com/questions/2426335/pdo-transactions-dont-roll-back)*
- *[Difference between SET autocommit=1 and START TRANSACTION in mysql](https://stackoverflow.com/questions/2950676/difference-between-set-autocommit-1-and-start-transaction-in-mysql-have-i-misse)*s
- *[读写分离简介](https://help.aliyun.com/document_detail/51073.html)*
- *[mysql悲观锁总结和实践](http://chenzhou123520.iteye.com/blog/1860954)*
- *[MySQL索引背后的数据结构及算法原理](https://www.kancloud.cn/kancloud/theory-of-mysql-index/41846)*
- *[mysql show processlist分析](http://blog.chinaunix.net/uid-134240-id-220211.html)*
- *[MySQL的内存计算公式](http://mingxinglai.com/cn/2016/04/mysql-memory-usage-formula/)*
- *[MySQL实战系列3：视图、存储过程、函数、索引全解析](http://dbaplus.cn/news-11-810-1.html)*
