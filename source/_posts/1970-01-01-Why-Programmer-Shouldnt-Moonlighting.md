---
layout: post
title: 为什么不支持程序员接私活
category: 好文整理
---

本文非原创，由个人在网络阅读后觉得不错转载整理而来，版权归网络相应作者所有。

<!-- more -->

如果你想成为成功人士，高收入人群，那么你不应该去做兼职，不要把你宝贵的时间，浪费在兼职上。

## 简单经济学分析

### 比较优势理论

18 世纪经济学家李嘉图提出的「比较优势理论」：贸易的基础并不是绝对优势，而是比较优势。如果有绝对优势那就叫垄断，不存在比较了。

比较优势的核心理论就是：**单独一个人或者单独一个国家机构，其精力和机会成本都是有限的，哪怕你各方面都是全球第一的人才，最聪明的做法也不是四处出击，事必躬亲，而是把有限的时间、精力和资源放在自己最擅长的地方，换句话说就是最赚钱的地方。**

那么好，涉及到我们普通人，其实也是一样，你的精力和时间包括知识很有限，这都是你仅有的也最擅长的赚钱财富，做兼职也是这个道理，如果你做自己的本职工作一小时能赚200元，兼职跑滴滴一小时能赚30元，送外卖一小时能赚15元，你的最佳选择是什么?不傻的应该都会选择吧？不是去跑滴滴，而是集中精力去做好自己的本职工作。

## 社会分工

为什么亿万富翁都要创办公司请员工帮忙赚钱？因为他们一天只有 24 个小时啊，所以他们把自己每天有限的时间集中在自己最能赚钱的领域，然后请别人来做其他他不擅长或者压根不值得做的事情，这一过程，叫「社会分工」。

> 专业的人做专业的事。

社会分工是市场经济条件下，整个社会所有人都充分发挥自己劳动力的最优选择，也是最理想、最人性化的社会现象，每一个人应该只从事一份工作，而且这个工作会细分到极致，分工到极致的社会，会让一个人专门负责做其中的一件事和一种事。

## 为什么还是有很多人做兼职

结论就是社会分工极度不合理，社会资源没有调配到最优化造成的，想一下如果你一个月一天8小时是一万块，那么如果你每天加班4小时，结果还是一万块，你的努力，企业没有立刻给予你激励，你还会在公司加班吗？

但如果你下了班每天跑3个小时滴滴，一个月就可以多3000块收入，那就是月薪1万3了，那么你是不是觉得兼职很值。但如果你在单位加了班立马得到了成正比的加班收入，你还会去跑滴滴送外卖吗？那不能够

当然还有另外一种现象，做兼职是划算的，比如在某些国企或者政府单位或者个别企业的人员，工资固定制所以下班了去做兼职赚外快，但是让他们辞职去专门做兼职的那个领域他们又不愿意，这是为什么呢，因为现有的本职工作给予了他们超过应有价值的回报，其性价比超过本身在市场竞争中能获得的报酬，所以对他们个人而言，做本职拿死工资，下班了去做兼职，才是个体最优化的选择。

兼职的存在是社会分工不合理的表现，而如果你在做兼职，第一种可能就是你没有找到自己最有优势的领域，你不能在最有优势的领域赚到足够的钱，或者付出了没有得到应有的回报，再加上生活压力，你不得不做兼职。

## 我们应该怎么做

说了那么多，我想说明一个什么问题呢？那就是如果你有空闲时间，应该把精力放在提升自己职业技能和素养上，我们把时间线拉长，假如你可以做程序员15年，不要笑，现在30-40的程序员一抓一大把，如果你的私人时间都用在你最拿手的领域深耕，那么会发生什么？

大部分人可能没想过，但我告诉你，其实比你阶段性的空闲时间接个私活赚个几千块 几万块要划算的多，程序员工种是特殊的，快速多变的，而且中国公司普遍很忙，要加班，你什么时间学习和接触新技术，作总结呢？一个不做总结和不会做总结的人，他的薪资肯定不会涨的太快。

答案就是：**私人时间，去做总结，写文章，做开源，学技术，是为了以后在自己的拿手领域，更能得心应手，把价值最大利益化。**

当然了，接私活也是要有的，谁不想多挣点钱呢？顺便练练手，但请不要把所有的时间都用来接私活，因为一旦你承诺了别人的项目，你就入了坑了，所有时间都会用到私活上，因为甲方肯定会压你工期，你根本没时间学习和反思，也是一件很苦恼的事，所以阶段性的接两个私活是支持的，但请不要去做脱离你工种的其他兼职，太得不偿失了。

## 总结

既然选择了这个职业，就请坚持下去，把你的私人时间用到最擅长的领域，定能实现很多梦想。

## 附录：国内程序员接私活的平台

- 程序员客栈：程序员的经纪人

- 快码众包-让互联网产品开发更快速！认准域名Kuai.ma

- Coding 码市 - 基于云技术的软件外包服务平台 coding码市

- 开源中国众包平台 oschina众包

- 码易-高质量软件众包交付服务平台 码易众包平台

- 人人开发 - 集可视化开发，应用市场，威客众包，PaaS云于一体的企业级应用服务平台 人人开发

- 开发邦-专业互联网软件技术开发与咨询服务 开发邦