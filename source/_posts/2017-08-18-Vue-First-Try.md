---
layout: post
title: Vue First Try
category: Javascript
tags: [Vue.js, Javascript]
---

迫于生计学前端。

<!-- more -->

## 简介

### 什么是 Vue？

Vue(.js) 是基于 Javascript 写的渐进式 Web 框架。

### Vue 的优点？

- 渐进式：方便与第三方库或即有库整合，排他性弱，既可以当全家桶来开发大型项目，也可以只是改改几个组件当 jQuery 用。

- 容易上手：只关注视图层，相当于只是个轻量级的视图而已；此外，文档丰富。

- 国内社区活跃：国内外都有一大批使用 Vue 以及为 Vue 贡献代码的程序员。

- 可维护性、可测试性：Vue 提供了简洁、语义化的接口和很多辅助工具，帮助开发者编写维护性高、便于测试的代码。

- 有很多基于 Vue 的优秀库：节省很多开发时间，比如 iView/Element UI 等。

### Vue 的缺点？

说明：主要是 Vue1 的缺点，Vue2 其实已经好了很多。

- 编辑器/IDE 里的语法提示和相关插件不太完善

- template 不好 debug

- 对初学者不友好，入门比较“难”，同时要学习 webpack 等包管理工具

> vue-cli 其实已经把 webpack 隔离得很好了，需要手动配置的地方不多

- 缺乏高阶教程、文档、书籍。

- 更适合做小项目

## 概念

### 虚拟 DOM

原生 JS 或 jQuery 操作 DOM 时，浏览器会从构建 DOM 树开始从头到尾执行一遍流程。举例说明：

某次操作需要更新 10 个 DOM 节点，浏览器收到第一个 DOM 请求后就会立马执行操作，然后收到第二个 DOM 请求又马上执行操作，最终执行 10 次 DOM 操作。

这样的问题是最后一次 DOM 计算之前的 9 次 DOM 操作均为无用功。即使计算机硬件越来越高，但操作 DOM 的代价仍旧是昂贵的，频繁操作还是会出现页面卡顿，影响用户体验。

虚拟 DOM 就是为了解决浏览器的这个性能问题而被设计出来的。若一次操作中有 10 次更新 DOM 请求，虚拟 DOM 不会立即操作 DOM，而是将这 10 次更新的 diff 内容保存到本地一个 JS 对象中，最终将这个 JS 对象一次性 attch 到 DOM 树上，再进行后续操作，避免大量无谓的计算量。

所以，用 JS 对象模拟 DOM 节点的好处是，页面的更新可以先全部反映在 JS 对象(虚拟 DOM )上，操作内存中的 JS 对象的速度显然要更快，等更新完成后，再将最终的 JS 对象映射成真实的 DOM，交由浏览器去绘制。

### 双向绑定

简单来说就是视图 V 和数据 M 的任何一方发生改变，另一方也会同步改变。

Vue 数据双向绑定是通过，数据劫持结合发布者-订阅者模式的方式来实现的。

### 自底向上逐层应用

由基层开始做起，把基础的东西写好，再逐层往上添加效果和功能。

### 组件化

组件化强调的是一种抽象，允许我们使用小型、独立、可复用的组件来构建大型应用。

## 实践

### 安装-构建-运行

- 尝试 Vue

``` html
<!-- 开发环境版本，包含了有帮助的命令行警告 -->
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

<!-- 生产环境版本，优化了尺寸和速度 -->
<script src="https://cdn.jsdelivr.net/npm/vue"></script>
```

- vue-cli

``` shell
node -v    # 确保 nodejs 已安装
npm -v

# 安装 vue-cli 脚手架构建工具
npm install -g vue-cli 
vue -V
webpack -v

# 构建项目
vue init webpack my-vue-app

# 创建中小型项目
#vue init webpack-simple my-vue-app-2

# 安装项目依赖
cd my-vue-app
npm install
#cnpm install

# 热加载的方式运行我们的应用
# 热加载可以让我们在修改完代码后不用手动刷新浏览器就能实时看到修改后的效果
npm run dev
#npm run start

# 打包项目
# 会生成一个dist文件 就是我们的打包文件，点击 .html 文件能运行则成功
npm run build
#cnpm run build
```

### 开发

细节太多，总的来说就是：看文档+踩坑+搜索+填坑。

## FAQ

- 1+ 版本 API 迁移到 2+

``` shell
# install or update
npm install --global vue-migration-helper

# navigate to a Vue 1.x project directory
cd path/to/my-vue-project

# scan all files in the current directory
vue-migration-helper

# scan all files in specific sub-directories
vue-migration-helper src folder-a folder-b

# scan a specific file or files
vue-migration-helper src/app.vue
```

- `ready` => `mounted`

> created() : since the processing of the options is finished you have access to reactive data properties and change them if you want. At this stage DOM has not been mounted or added yet. So you cannot do any DOM manipulation here

> mounted(): called after the DOM has been mounted or rendered. Here you have access to the DOM elements and DOM manipulation can be performed for example get the innerHTML:

> <https://stackoverflow.com/questions/45813347/difference-between-the-created-and-mounted-events-in-vue-js>

## 参考

- [Vue.js](https://cn.vuejs.org/v2/guide/)
- [14招搞定JavaScript调试](https://blog.fundebug.com/2017/11/08/14-javascript-debugging-tips/)
- [VUX-Vue 移动端 UI 组件库](https://vux.li)
- [iView](https://www.iviewui.com/docs/guide/start)
