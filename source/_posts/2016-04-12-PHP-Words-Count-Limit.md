---
layout: post
title: PHP 判断字符串是否超过字数限制
category: PHP
tags: [PHP, mbstring, 多字节编码]
---

“开发多字节字符编码方案是为了在基于字节的常规编码系统中表达超过 256 个字符。”

<!-- more -->

引用 PHP Manual 上关于 mbstring 的几句说明：

> 虽然许多语言每个必要字符都能一对一映射到 8 比特（bit）的值，但也有好几种语言需要非常多的字符来书面通讯，以至于它们的编码范围不能仅仅包含在一个字节里。


> 在你操作（trim、split、splice 等等）多字节编码的字符串的时候，由于在这种编码方案下，两个或多个连续字节可能只表达了一个字符，所以你需要使用专门的函数。


> 否则，当你将不能检测多字节字符串的函数应用到这个字符串的时候，它可能无法检测多字节字符的起始位置，并以乱码字符串结尾，基本丢失了它原来的意思。


> mbstring 提供了针对多字节字符串的函数，能够帮你处理 PHP 中的多字节编码。 除此以外，mbstring 还能在可能的字符编码之间相互进行编码转换。


> 为了方便起见，mbstring 设计成了处理基于 Unicode 的编码，类似 UTF-8、UCS-2 及诸多单字节的编码。


> > 来自 <http://php.net/manual/zh/intro.mbstring.php>

## 举例说明

``` php
<?php

# 测试

if ( !isLimitedWordCnt( '在你操作（trim、split、splice 等等）多字节编码的字符串的时候，由于在这种编码方案下，两个或多个连续字节可能只表达了一个字符，所以你需要使用专门的函数。' ) ) {
    echo '<br>结果：字数超过限制。' ;
} ;

/**
 * 判断字符串是否超过字数限制
 * 在使用该函数之前最好使用：header( 'charset:utf-8' ) ;    // 设置返回的编码格式
 * @param $str
 * @param int $cnt
 * @return bool
 */
function isLimitedWordCnt( $str, $cnt = 50 ) {

    # 分别计算中英文字符的个数

    $cnWords = getCnWordsAndPuns( $str ) ;
    $ltWords = getLtWordsAndPuns( $str ) ;

    echo '中文及中文标点数：', $cnWords, '<br>英文及英文标点数：', $ltWords, '（含空格）<br>' ;

    return ( $cnt<($cnWords+$ltWords) ) ? false : true ;

}

/**
 * 计算字符串中的中文和中文标点个数    # 多字节
 * !!! 使用 UTF-8 编码时一个中文字符占 3 个字节; 使用 GBK 编码时一个中文字符占 2 个字节; 只有是纯英文状态下 PHP 才会判断为 ASCII 码
 * @param $str
 * @return int
 */
function getCnWordsAndPuns( $str ) {
    # 检测字符串 $str 的编码, 如果不是 UTF-8 就设置为 UTF-8
    if ( strtolower( mb_detect_encoding( $str ) != 'utf-8' ) ) {
        mb_internal_encoding( 'UTF-8' ) ;    // 设置内部字符编码
    }

    $puns = preg_match_all( '/·|，|。|《|》|‘|’|”|“|；|：|【|】|？|（|）|、/', $str ) ;    // 中文标点符号的个数( UTF-8 编码下的中文标点符号也占 2 个字节 )

    # 判断是否含有中文字符
    $mbLen = mb_strlen( $str,'utf-8' ) ;    // 等价于: mb_internal_encoding( 'UTF-8' ) ; mb_strlen( $str ) ;
    $sgLen = strlen( $str ) ;

    $cnt = 0 ;
    # 由于中文是多字节编码, 所以如果多字节字符串的长度跟单字节( 拉丁语 ) 的字符串长度不一样则认为含有中文等多字节编码的字符
    if ( $mbLen!=$sgLen ) {
//        echo '含有多字节语言或多字节编码的标点符号' ;
        $cnt = $mbLen - getLtWordsAndPuns( $str ) ;
    }

    return ($cnt+$puns) ;
}

/**
 * 计算字符串中的英文和英文标点的个数( 包含空格 )    # 单字节
 * @param $str
 * @return int
 */
function getLtWordsAndPuns( $str ) {
    $cnt = preg_match_all( '/\w|\.|\||\`|\!|\@|\#|\%|\^|\&|\*|\(|\)|\-|\=|\+|\{|\}|\:|\"|\'|\,|\.|\/|\\|\<|\>|\?|\||\;|\]|\[|\~| /', $str ) ;
    return $cnt ;
}
```

## 测试输出

```
中文及中文标点数：75
英文及英文标点数：16（含空格）

结果：字数超过限制。
```

### 总结

在不同的多字节编码方案下，每个字符占用的字节数是不一样的。

比如 UTF-8 编码下的中文字符（包含中文标点）就占用 3 个字节，GBK 编码下的中文字符（也包含中文标点）就占用 2 个字节。


使用 `mb_strlen( $str, 'utf-8' );` 可以很方便地统计 UTF-8 编码下的字符个数。

## 附录：相关 API

- strlen()

- mb_strlen()

- mb_strwidth()

- mb_detect_encoding()

- mb_internal_encoding()
