---
layout: post
title: 微服务和 DevOps
category: Profession
tags: [微服务, DevOps]
---

DevOps：“我们不仅写代码，我们同时也关心代码将如何运行。”

<!-- more -->

和微服务、DevOps 有关的，具有实际指导意义的知识点总结并记录在这里。

> TODO

## 微服务

### 单体重构

#### 停止扩张

- 不继续向单体新增功能
- 新功能调用新服务，旧功能依然调用单体
- 防护层代码：保护新服务不被旧单体所影响

停止扩张并没有解决单体存在的问题，但是却是解决单体存在问题的第一步。 

#### 前后端分离

- 前端：全权负责表现层
- 后端：专门负责业务逻辑和数据处理逻辑

前后端分离只是缩小单体的一种局部解决方案，但是仍然没有完全解决单体存在的问题。

#### 模块变服务

## DevOps

> DevOps 是软件开发、运维和质量保证三个部门之间的沟通、协作和集成所采用的流程、方法和体系的一个集合。它是人们为了及时生产软件产品或服务，以满足某个业务目标，对开发与运维之间相互依存关系的一种新的理解。

## 配置中心

### 驱动配置架构升级的痛点

#### 服务方的容量伸缩对调用方存在“反向依赖”

什么是“反向依赖”架构？随着互联网业务的复杂化，系统逐渐朝着服务化、分层方向发展，通常表现为：应用层调用服务层，服务层中又分为上游服务和底层服务。

为了保证 HA，通常情况下，一个底层服务（相对）往往由若干个节点组成集群，同时对上游提供调用。

“反向依赖”指的就是在底层服务需要伸缩时，上游调用方在逻辑和容量都无需变化的情况下，仍然需要配合底层服务的伸缩而需要修改相应的配置并重启，而无法实现上游无感知地实现流量自动迁移。

#### 服务方无法确定到底有多少个调用方调用自己

### 配置架构的演进

#### 配置私藏

> “配置私藏”是配置文件架构的最初级阶段，上游调用下游，每个上游都有一个专属的私有配置文件，记录被调用下游的每个节点配置信息。

#### 全局配置文件

> 架构的升级并不是一步到位的，先来用最低的成本来解决上述“修改配置重启”的问题一: 全局配置文件

“全局配置”法：对于通用的服务，建立全局配置文件，消除配置私藏：
1）运维层面制定规范，新建全局配置文件，例如/opt/globalconf/global.conf，如果配置较多，注意做好配置的垂直拆分
2）对于服务方，如果是通用的服务，集群信息配置在global.conf里
3）对于调用方，调用方禁止配置私藏，必须从global.conf里读取通用下游配置

这么做的好处：
1）如果下游容量变化，只需要修改一处配置global.conf，而不需要各个上游修改
2）调用方下一次重启的时候，自动迁移到扩容后的集群上来了
3）修改成本非常小，读取配置文件目录变了

不足：
如果调用方一直不重启，就没有办法将流量迁移到新集群上去了

##### 自动流量迁移

在全局配置文件这种架构中，要实现自动流量迁移，需要先实现两个并不复杂的组件：

1）文件监控组件FileMonitor
作用是监控文件的变化，起一个timer，定期监控文件的ModifyTime或者md5就能轻松实现，当文件变化后，实施回调。
2）动态连接池组件DynamicConnectionPool
“连接池组件”是RPC-client中的一个子组件，用来维护与多个RPC-server节点之间的连接。所谓“动态连接池”，是指连接池中的连接可以动态增加和减少（用锁来互斥或者线程安全的数据结构很容易实现）。

这两个组件完成后：
1）一旦全局配置文件变化，文件监控组件实施回调
2）如果动态连接池组件发现配置中减少了一些节点，就动态的将对应连接销毁，如果增加了一些节点，就动态建立连接，自动完成下游节点的增容与缩容。

#### 配置中心

> 全局配置文件是一个能够快速落地的，解决“修改配置重启”问题的方案，但它仍然解决不了，服务提供方“不知道有多少个上游调用了自己”这个问题。 

如果不知道多少上游调用了自己，“按照调用方限流”，“绘制全局架构依赖图”等需求便难以实现，怎么办，可以采用“配置中心”来解决。

对比“全局配置”与“配置中心”的架构图，会发现配置由静态的文件 升级为 动态的服务：

1）整个配置中心子系统由zk、conf-center服务，DB配置存储与，conf-web配置后台组成
2）所有下游服务的配置，通过后台设置在配置中心里
3）所有上游需要拉取配置，需要去配置中心注册，拉取下游服务配置信息（ip1/ip2/ip3）

当下游服务需要扩容缩容时：

4）conf-web配置后台进行设置，新增ip4/ip5，减少ip1
5）conf-center服务将变更的配置推送给已经注册关注相关配置的调用方
6）结合动态连接池组件，完成自动的扩容与缩容

配置中心的好处：

1）调用方不需要再重启
2）服务方从配置中心中很清楚的知道上游依赖关系，从而实施按照调用方限流
3）很容易从配置中心得到全局架构依赖关系

痛点一、痛点二同时解决。

不足：系统复杂度相对较高，对配置中心的可靠性要求较高，一处挂全局挂。

## 部署策略

### Reckless Deployment

### Rolling Update

### Blue/Green Deployment

### Canary Deployment

### Versioned Deployment

## 运维工具

> 没有银弹， 运维本身就是件很复杂 根据需求需要支出大成本代价的事情，有的人着重点是安全，有的人着重点是便捷，有的人着重点是快速集成发布。

> 上百台的话，如果环境简单，docker 打包 python 脚本部署 并不是不可能的事情。

> 但是考虑到服务的可用性，灾备又是另外一套方案，还有服务的分发又是另外一个问题。

> 总而言之，实际问题实际解决，没有大一统的方案。

- 一个便捷发布管理平台，能支持：批量发布、快速回滚、限流灰度

### Jenkins

### [Travis CI](https://travis-ci.org/)

### Puppet

### Saltstack

### Chef

### Ansible

### Docker

### Kubernetes

## 附录：常用镜像

- [淘宝 NPM 镜像](http://npm.taobao.org)

## 参考

- *[到底什么是 DevOps？-v2ex](https://www.v2ex.com/t/276618)*
- *[Deployment Strategies Defined])(http://blog.itaysk.com/2017/11/20/deployment-strategies-defined)*
- *[FeatureToggle](https://martinfowler.com/bliki/FeatureToggle.html)*
- *[How does deployment on remote servers work?](https://stackoverflow.com/questions/43278477/how-does-deployment-on-remote-servers-work)*
- *[自动化运维之架构设计六要点](http://www.yunweipai.com/archives/21236.html)*
- *[The Phoenix Project: A Novel about IT, DevOps, and Helping Your Business Win-Amazon](https://www.amazon.com/Phoenix-Project-DevOps-Helping-Business/dp/0988262592)*
- *[你所在的公司是如何实施DevOps的？](https://www.zhihu.com/question/24413538/answer/139454464)*
- _[配置”也有架构演进？看完深有痛感](https://m.w3cschool.cn/architectroad/architectroad-configuration-center.html)_
- *[The Benefits of Serverless Computing and its Impact on DevOps](https://hackernoon.com/the-benefits-of-serverless-computing-and-its-impact-on-devops-e75d82c47ac4)*
- *[乐高式微服务化改造（下）](https://mp.weixin.qq.com/s/MYNChqtCdZyaUqw89AkuAw)*
- *[为什么微服务实施那么难？如何高效推进微服务架构演进](http://gitbook.cn/books/59870d65d115e231bf3e3f5f/index.html)*
- *[微服务架构下分布式SESSION管理](http://www.primeton.com/read.php?id=2244&his=1)*
