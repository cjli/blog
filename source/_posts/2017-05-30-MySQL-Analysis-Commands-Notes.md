---
layout: post
title: MySQL 分析命令总结
category: Database
tag: MySQL
---

站在 MySQL Parser 的角度，`describe`, `desc`, `explain` 是完全等价的（synonym）。

<!-- more -->

只是通常情况，`DESCRIBE`/`DESC` 被用分析表结构，而 `EXPLIAN` 被用于分析 SQL 查询执行计划（how MySQL would execute a query）而已。

其中，`DESCRIBE` 又是 `SHOW COLUMNS`／`SHOW FIELDS` 的别名。

为了简单，下面涉及到上面 3 个命令的都在 MySQL 5.6 下以 `DESC` 进行示例。

## 获取表结构信息

获取表结构的语法为：

- desc

```sql
{EXPLAIN | DESCRIBE | DESC} tbl_name [col_name | wild]
```

其中 `col_name` 代表列名，`wild` 代表通配符。

- show

``` sql
SHOW [FULL] {COLUMNS | FIELDS} {FROM | IN} tbl_name
[{FROM | IN} db_name]
[LIKE 'pattern' | WHERE expr]
```

举例说明：

### 获取完整表结构

``` sql
DESC $tb_name;

SHOW COLUMNS FROM $tb_name FROM $db_name;
SHOW FIELDS FROM $tb_name FROM $db_name;
SHOW FULL FIELDS FROM $tb_name FROM $db_name;
SHOW FIELDS FROM $tb_name FROM $db_name WHERE 1;
SHOW FIELDS FROM $tb_name FROM $db_name LIKE '%';

SHOW CREATE TABLE $db_name.$tb_name;
```

### 获取某个字段结构

``` sql
DESC $tb_name $col_name;

SHOW FIELDS FROM $tb_name FROM $db_name WHERE `Field` = $col_name;
SHOW FULL FIELDS FROM $tb_name IN $db_name WHERE `Field` = $col_name;
```

- 获取主键字段的结构

``` sql
SHOW FIELDS FROM $tb_name FROM $db_name WHERE `Key` = 'PRI';
```

### 获取若干字段结构

可以使用通配符 `%` 和 `_` 。比如：

- 获取所有以 `a` 开头的字段结构

``` sql
DESC $tb_name 'a%';

SHOW FIELDS FROM $tb_name FROM $db_name WHERE `Field` LIKE 'a%';
SHOW FIELDS FROM $tb_name FROM $db_name LIKE 'a%';
```

- 获取包含 `a` 的字段结构

``` sql
DESC $tb_name '%a%'
SHOW FIELDS FROM $tb_name FROM $db_name WHERE `Field` LIKE '%a%';
SHOW FIELDS FROM $tb_name FROM $db_name LIKE '%a%';
```

- 获取包含 `a`，且字段名只有 3 个字符的字段结构

``` sql
DESC $tb_name 'a__';
DESC $tb_name '_a_';
DESC $tb_name 'a__';
```

- 获取属性值唯一的字段结构

```  sql
SHOW FIELDS FROM $tb_name FROM $db_name WHERE `Key` = 'UNI';
```

- 获取类型为 `int(11)`的字段结构

``` sql
SHOW FIELDS FROM $tb_name FROM $db_name WHERE `Type` = 'int(11)';
```

- 获取为可以为 NULL 的字段结构

``` sql
SHOW FIELDS FROM $tb_name FROM $db_name WHERE `Null` = 'YES';
```

## 分析查询计划

分析 SQL 查询计划的语法为：

``` sql
{EXPLAIN | DESCRIBE | DESC}
	[explain_type]
    explainable_stmt

explain_type: {
	EXTENDED
	| PARTITIONS
	| FORMAT = format_name
}

format_name: {
	TRADITIONAL
	| JSON
}

explainable_stmt: {
	SELECT statement
	| DELETE statement
	| INSERT statement
	| REPLACE statement
	| UPDATE statement
}
```

其中，`explain_type` 可以不指定，但同时只能选择一次。`format_name` 默认为 `TRADITIONAL`。

举例说明：

### 分析 SELECT

#### 不指定 `explain_type`

``` sql
DESC SELECT * FROM $tb_name;

--> 输出示例：

+----+-------------+-----------+------+---------------+------+---------+------+------+-------+
| id | select_type | table     | type | possible_keys | key  | key_len | ref  | rows | Extra |
+----+-------------+-----------+------+---------------+------+---------+------+------+-------+
| 1  | SIMPLE      | $tb_name  | ALL  | NULL          | NULL | NULL    | NULL | 45   | NULL  |
+----+-------------+-----------+------+---------------+------+---------+------+------+-------+
```

不指定 `explain_type` 或者指定为 `FORMAT = TRADITIONAL` 效果一样。

#### 指定 `explain_type`

- `EXTENDED`

``` sql
DESC extended SELECT * FROM $tb_name\G

--> 输出示例：

id           : 1
select_type  : SIMPLE
table        : $tb_name
type         : ALL
possible_keys: NULL
key          : NULL
key_len      : NULL
ref          : NULL
rows         : 45
filtered     : 100.00
Extra        : NULL
```

可见，`explain_type` 为 `extended` 时，会多返回一个 `filtered` 属性值。

此外，当指定了 `extended` 参数时，`DESC` 会生成额外的提示信息，可以紧接着通过 `SHOW WARNINGS` 查看。

- `FORMAT = json`

``` sql
DESC format = json SELECT * FROM $tb_name\G

--> 输出示例：

EXPLAIN: {
  "query_block": {
    "select_id": 1,
    "table": {
      "table_name": "$tb_name",
      "access_type": "ALL",
      "rows": 45,
      "filtered": 100
    }
  }
}
```

注意，`explain_type` 为 `format = json` 时，`DESC` 的输出已经自动地输出了 `expain_type` 既为 `extened`，又为 `partition` 的信息，可以看到，这里也返回了一个 `filtered` 属性值。（`expain_type` 为 `extened` 的输出）

### explain 输出解释

下面以 **`列名`/`JSON NAME`：对应含义** 的格式进行说明。`JSON_NAME` 指 `format = json` 时的输出字段名。

- `id`／`select_id`

  本次 SELECT 查询的序列号。

  当本次查询为 `UNION` 类型时，可以为 `NULL` 。此时的 `table` 将显示 `<unionM,N,Q>`，`select_type` 将显示 `UNION RESULT`，`M`、`N`、`Q` 分别代表 `UNION` 中所有查询的序列号。

- `select_type`/none

  SELECT 的类型。

- `table`/`table_name`：SELECT 的表名。​

## MySQL 监控工具

### 内置 `show`

``` sql
show status [like '?'];
show processlist;
show variables [like '?'];
```

### mysqlreport

`SHOW STATUS` 的输出只是简单的数据罗列，虽然也用来计算性能瓶颈的参考数据，但 mysqlreport 是对这些参考数据加以融合计算，整理成一个个优化参考点，然后就可以根据这个优化参考点的值以及该点的衡量标准，进行对应调整。

- 安装

``` shell
sudo yum install -y perl-DBD-mysql
#sudo apt-get install libdbd-mysql-perl
sudo yum install -y libdbi-dbd-mysql
sudo yum install -y mysqlreport
```

### tuning-primer.sh

tuning-primer.sh 比 mysqlreport 更进一步，除了输出报表之外，还进一步提供了修改建议。

``` shell
wget http://www.day32.com/MySQL/tuning-primer.sh
chmod +x tuning-primer.sh
./tuning-primer.sh
```

### mytop

mytop 连接到 MySQL 服务器，并总结了一个有用的格式的信息。

使用 mytop，我们可以监实时视的 MySQL 线程，查询和运行时间以及看到哪个用户正在运行的查询的数据库上，这是缓慢的查询，等等。

``` shell
sudo yum install mytop -y
```

编辑 ~/.mytop，加入以下配置:

```
host=localhost
db=mysql
delay=5
port=3306
socket=
batchmode=0
color=1
idle=1
```

配置后上面的连接信息后连接使用：

``` shell
mytop --prompt
```

- 连接报错：Can't connect to local MySQL server through socket '/var/lib/mysql/mysql.sock' mytop

取消 ~/.mytop 中是的 `socket=` 行即可。

### mysqlsla

mysqlsla 是一款帮助语句分析、过滤、和排序的功能，能够处理MySQL慢查询日志、二进制日志等。整体来说, 功能非常强大. 能制作SQL查询数据报表，分析包括执行频率, 数据量, 查询消耗等。

且该工具自带相似 SQL 语句去重的功能，能按照指定方式进行排序，比如分析慢查询日志的时候，让其按照 SQL 语句执行时间逆排序，就能很方便的定位出问题所在。

- 安装

``` shell
sudo yum -y install perl-ExtUtils-CBuilder perl-ExtUtils-MakeMaker
sudo yum -y install perl-DBI perl-DBD-MySQL
sudo yum -y install perl-CPAN
#perl -MCPAN -e shell

cd /usr/local/src
sudo git clone https://github.com/daniel-nichter/hackmysql.com.git
cd hackmysql.com/mysqlsla/
sudo perl Makefile.PL
sudo make
sudo make install
```

- 配置 mysqld slowlog

```
[mysqld]
log_slow_queries = ON
log-slow-queries = /tmp/slow_query.log
long_query_time = 1
```

## 参考

- *[MySQL :: MySQL 5.6 Reference Manual :: 13.8 Utility Statements](https://dev.mysql.com/doc/refman/5.6/en/sql-syntax-utility.html)*
- [mysqlreport - MariaDB Knowledge Base](https://mariadb.com/kb/en/library/mysqlreport/)
- [mysqlreport使用详解](http://blog.51cto.com/breezey/1529492)
- [How To Use Mytop to Monitor MySQL Performance](https://www.digitalocean.com/community/tutorials/how-to-use-mytop-to-monitor-mysql-performance)
- [mysqlsla安装与慢查询分析](http://blog.51cto.com/357712148/1961193)
