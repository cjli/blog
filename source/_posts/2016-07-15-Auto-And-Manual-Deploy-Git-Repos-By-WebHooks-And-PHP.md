---
layout: post
title: 项目部署从远程到自动：via PHP and Git Hooks
category: 折腾
tags: [Web, PHP, Git, Hooks, Linux, 微信公众号]
---

Web 项目迭代速度很快，而 Web 项目一般至少又有测试环境和生产环境，如果不想办法实现部署自动化，那么光是听 PM 等人催你更新这两个线上环境就能把耳朵听出茧。

<!-- more -->

## 微信一键远程部署

以通过在微信公众号中点击菜单按钮来完成项目更新为例，说明使用 PHP 简单实现远程部署任务的过程。

### 1. 设置微信公众号

首先在登录微信公众号管理后台，为公众号添加两个按钮，一个是 **“更新线上”**，一个是 **“更新测试”**。分别为这两个按钮绑定不同的参数，建议将分支名做参数，并指向特定的 URL，一般会路由到 PHP 的某个控制器的某个方法，最终一个带查询参数的完整的 URL 会类似如下格式：

```
http://www.domain.com/?controller=update&type=dev&is_db=0
```

绑定好之后，要实现的效果是：当我们点击 **“更新线上”** 时，会发送一个请求到 URL 中指定的控制器，然后由此控制器里面的相关方法处理后将更新结果返回到微信浏览器。点击 **“更新测试”** 同理。

`type=dev` 表示是要更新测试环境；`type=master` 则表示更新生产环境。

稍微说明一下，微信公众号菜单那里虽然填写带非80端口的域名／IP 也可以，但是每次点击此按钮都会要你确认，所以最好还是使用备案过的80端口来作为远程更新接口。

#### **注意**

线上环境和测试环境只是通过 git 同时管理的两个不同的分支，因此完全可以只在一个控制器中写一个方法，然后通过这一个方法去更新不同的环境。个人认为这样也是最省事的做法。

### 2. 响应更新操作

接下来要做的就是在服务端准备好这个方法来处理更新请求了。
代码很简单，因为任务比较单一，如下：

```php
<?php
/**
 * @brief 升级更新控制器
 */
class Update extends IController
{
    /**
     * 更新入口判断
     * by @clji
     */
    public function index()
    {
        # 0. 判断更新目标
        $type = isset($_GET['type']) ? trim($_GET['type']) : false ;
        if (!$type) die('更新目标不明确') ;

        # 1. 授权微信用户登录
        // ...

        # 2. 判断是否是管理员微信号
        // ...

        # 3. 判断更新对象
        $is_db  = (isset($_GET['is_db']) && trim($_GET['is_db'])) ? true : false ;
        $obj    = ('master' == $type) ? '线上' : '测试' ;
        $obj   .= $is_db ? '数据库' : '环境' ;

        $op_res = $is_db ? $this->updateDb($type) : $this->updateEnv($type) ;

        $stat  = is_null($op_res[0]) ? '更新出错' : '已经更新' ;
        $color = is_null($op_res[0]) ? 'red'    : 'green'   ;
        $res   = is_null($op_res[0]) ? 'NULL'   : $op_res[0]  ;
        $date  = date('Y-m-d H:i:s')  ;

        $notes = $op_res[1] ;

        $info  = <<< HTML
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>更新结果 - $obj</title>
  <style>
  div {
  text-align: center;
    width: 100%;
    height: auto;
    margin: 10% auto;
  }
  h2 {
    color: $color;
  }
  p {
    padding: 18px;
    font-size: 18px;
    background-color: #eee;
  }
  ul {
    text-align: left;
  }
  li {
    margin: 5px;
  }
  code {
    font-size: 16px;
    color: #dd524d;
  }
  </style>
</head>
<body>

<div>
  <h2> $obj $stat </h2>
  <h4> ( $date ) </h4>
  <h4>灰色区域输出说明</h4>
  <ul>
    <li>出现 <code>NULL</code> 表示更新出错。</li>
    {$notes}
  </ul>
  <h4>以下是执行的结果</h4>
  <p> $res </p>
</div>

</body>
</html>
HTML;
        exit($info) ;
    }

    /**
     * 微信更新测试环境或线上环境
     * !!! 由于在 服务器/PHP 中执行 Shell 脚本时候用户为 www
     * !!! 因此在 Linux 上一定要事先赋予 www 具有 root 的执行权限
     * !!! 否则会更新失败
     * @param $type 更新环境
     * @return Array(原始 shell 结果, 提示信息)
     * by @cjli
     */
    public function updateEnv($type)
    {
        # 先拉取当前分支最新状态
        $cmds  = "sudo cd ../\n" ;
        $cmds .= "sudo git stash\n" ;
        $cmds .= "sudo git pull origin $type\n" ;

        if ('master' == $type) {
            # 如果是要远程更新 master 分支 则拉取 Gitlab master 后需要手动更新本地 dev 然后将本地 master 与 dev 合并
            # !!! 注意 sudo 确保可执行权限
            $cmds .= "sudo git checkout dev\n" ;
            $cmds .= "sudo git stash\n" ;
            $cmds .= "sudo git pull origin dev\n" ;
            $cmds .= "sudo git checkout master\n" ;
            $cmds .= "sudo git merge dev\n" ;
        }

        # 取消执行时间限制
        set_time_limit(0) ;
        $res  = shell_exec($cmds) ;

        $html = <<< HTML
<li>出现 <code>Already up-to-date...</code> 表示只完成更新指令但代码状态并未改变。</li>
<li>出现 <code>Updating ...</code> 表示这是一次有效更新。</li>
HTML;
        return array($res, $html) ;
    }

    /**
     * 更新数据库
     * @param $type 更新目标
     * @return Array(原始 shell 结果, 提示信息)
     * by @cjli
     */
    private function updateDb($type)
    {
        # 判断并用有效 SQL 文件更新数据库
        # 遍历 docs/sql 目录下所有 sql 文件
        $res   = null ;
        $errs  = null ;
        $sqls  = $this->getSqlFiles($type) ;
        $total = count($sqls) ;
        if ($total >= 1) {
            # 自定义 MySQL 链接
            $db = IWeb::$app->config['DB'] ;
            $query = new mysqli($db['write']['host'], $db['write']['user'], $db['write']['passwd'], $db['write']['name'])
                     or die('mysql connect error.') ;

            # 取消执行时间限制
            set_time_limit(0) ;
            $res  = [] ;
            $errs = [] ;
            foreach ($sqls as $k => $v) {
                $sql_arr = json_decode(file_get_contents($v), true) ;
                $single_res = true ;
                $error_info = "sql 文件 ".$v." 中出错的语句如下:\n\n\n" ;
                if (count($sql_arr) >= 1) {
                    foreach ($sql_arr as $_k => $_v) {
                        # 执行 sql 语句
                        # !!! 不要是查询语句 因为输出和出错是一样的
                        if ($query->query($_v) === false) {
                            $single_res  = false ;
                            $error_info .= $_v."\n\n(\tError:\t".$query->error."\t)\n\n" ;
                            $errs[]      = $v.' => <code>'.$query->error.'</code>' ;
                        }
                    }
                }
                # 如果全部更新成功则将文件头的响应的标志置为 1
                if ($single_res) {
                    $this->changeSqlState($type, $v) ;
                    $res[] = $v  ;
                } else {
                    # 纪录错误日志
                    $handler = fopen(preg_replace('#runtime#', 'log', $v).'.log', 'w') or die('Unable to open file.') ;
                    fwrite($handler, $error_info) ;
                }
            }
        }

        $res = empty($res) ? (is_null($errs) ? null : implode('<br><br><br>', $errs)) : implode("\n", $res) ;

        $success = is_null($res) ? 0 : count($res) ;
        $html  = <<< HTML
<li>出现 <code>Duplicate column...</code>、<code> Table 'xxx' already exists</code> 等表示语法正确但是已经更新过, 请重新设置文件头的状态避免重复更新。</li>
<li>出现 <code> You have an error in your SQL syntax; ...</code> 表示语法出错, 请本地检查无误后重新上传到服务器。</li>
<li>本次操作了共 <code>{$total}</code> 份 sql 文件, 其中更新成功的文件数是 <code>{$success}</code>, 更新时遇到的错误已纪录到 log/ 目录。</li>
HTML;
        return array($res, $html) ;
    }

    /**
     * 获取指定目录下的所有未更新的 SQL 文件名 (不包括子目录名)
     * @param $path 文件路径
     * @return $files
     * by @cjli
     */
    private function getSqlFiles($type='dev', $path = './docs/sql/')
    {
        $handler = opendir($path);
        $files   = [] ;

        # 先清空上次保存的 index.sql
//        file_put_contents($path.'index.sql', '') ;

        # 读取路径
        // !!! 防止目录下出现类似文件名 0 或空格等情况这里必须使用全等判断
        while (false !== ($filename = readdir($handler))) {
            if (('.' != $filename) && ('..' != $filename)) {
                # 判断在当前环境下时候更新过数据库
                $cntnt  = @file_get_contents($path.$filename) ;
                $pttrn  = '/-- '.$type.' = (0|1)/' ;
                if (preg_match($pttrn, $cntnt, $arr) && $arr[1]=='0') {
                    # 将文件导入命令追加到 index.sql 文件
//                    file_put_contents($path.'index.sql', file_get_contents($path.$filename)."\n", FILE_APPEND) ;

                    # 将功能独立的 SQL 语句拆分后保存在 runtime 目录
                    $pure_sql = preg_replace('~-- {1,}.*~', '', @file_get_contents('./docs/sql/'.$filename)) ;
                    $tmp_sql  = @fopen($path.'runtime/'.$filename, 'w') or die('Unable to open file.') ;
                    @fwrite($tmp_sql, json_encode(array_filter(explode("\n\n", $pure_sql)))) ;
                    $files[]  = $path.'runtime/'.$filename ;
                }
            }
        }
        closedir($handler);
        return $files ;

    }

    /**
     * 改变 SQL 文件的更新状态
     * @param $type 测试或线上环境
     * @param $sqls 要操作的 SQL 文件
     * by @cjli
     */
    private function changeSqlState($type='dev', $sql_file)
    {
        $sql_file = preg_replace('/runtime\//', '', $sql_file) ;
        $cntnt = @file_get_contents($sql_file) ;
        $prtnt = '/(-- '.$type.' = )(0)/' ;
        # 正则搜索替换状态标志 0 => 1
        @file_put_contents($sql_file, preg_replace($prtnt, '${1}1', $cntnt)) ;
    }
}
```

### 3. 配置 PHP 和设置 Linux 用户运行权限

虽然客户端和服务端看似都已经准备就绪了，但是目前仍然无法完成远程更新的任务。还需要设置一下 PHP 和 Linux 用户权限。否则直接运行上面的代码是不能正常工作的。线上和测试环境都需要做同样的配置如下：

- **启用 shell_exec() 函数**

默认 PHP 是关闭了部分函数的，在 php.ini 中搜索 disable_functions 查看目前关闭的函数。

而现在要使用 shell_exec() 函数去执行 shell 命令，因此在搜索结果中删除 `shell_exec,` 即可。然后重启服务器。

如果 web 服务器是 Nginx 则需要重启 php-fpm，如果是 Apache 则重启 Apache。

```shell
###### CentOS 举例说明(方法不止一种)
# 重启 php-fpm
service php-fpm restart

# 重启 Apache
service httpd restart
```

- **赋予 www 用户 root 执行权限**

刚开始可能会出现这种问题：直接在 shell 中执行 `git pull origin dev` 是可以的，但是写到 PHP，用 shell_exec() 函数来执行同样的一句话的时候，却始终返回 NULL，这是因为 Linux 的用户权限问题。

PHP 是服务端脚本，因此是以 web 服务用户身份来执行 shell 脚本的。这可以通过在 php 脚本中打印 `whoami` 来得到。比如：

```php
<?php
echo shell_exec('whoami') ;
```

而直接在终端中执行命令，在现有的 Linux 服务器上大部分都是以 root 用户来执行的。在 Linux 上web 服务安装好之后，对应的用户一般都是 www（实际以上面的 php 脚本输出为准）。因此，只需让 www 用户拥有和 root 一样的执行权限就可以了。配置如下：

```shell
sudo chmod 0600 /etc/sudoers
vim /etc/sudoers
```
找到 `Allow root to run any commands anywhere`，在 root 下面按照同样的格式新增如下一行：

```shell
www     ALL=(ALL)   NOPASSWD:ALL

# 找到 `!visiblepw` 或 `requiretty` 并在其所在行行首注释掉
# Defaults   !visiblepw
```

其中的 `NOPASSWD` 的用处是，当 www 用户执行 `sudo` 命令时可以不用输入密码验证，肯定要这样配置，因为 PHP 脚本中实现 shell 交互中的输入密码操作还是比较麻烦的。

注释掉 `requiretty` 是为了解决默认情况下，执行 sudo 必须要终端的问题，因为这里是要在 PHP 脚本中执行。

到这里当我们在微信公众号中点击更新的时候，相应的线上环境都将被更新了。

### !!! 特别注意

这里为了方便开放了两处权限，当开发期结束后，关闭掉这些，因为有安全隐患。实际上，web 上的 PHP 不能给予过高权限。

最好的做法是在系统后台以 root 身份挂一个守护进程，专门根据对 PHP 的请求处理这样的操作。

- **将 git 用户加入 www 用户所在组并设置合适的权限**

如果上面赋予 root 执行权限感觉权限给的太多，还可以把 git 用户和 www 用户都放置到同一个组，然后设置组读写权限就行了。

假设 www 用户所在组也为 www。设置命令如下：

``` shell
cd /path/to/repo
sudo usermod -a -G www git
sudo usermod -a -G www mysql
chmod -R 0770 ./*
```

## 使用 Git Hooks 全自动部署

**[git hooks](https://git-scm.com/book/zh/v2/%E8%87%AA%E5%AE%9A%E4%B9%89-Git-Git-%E9%92%A9%E5%AD%90)** 可以理解为是对 git 仓库某些动作（事件）触发的监听，打个比方就是，我们能预料到会发生一些事情（比如代码肯定会推送到git服务器），而某些事情可能需要及时地处理（部署到线上），于是我们事先为其准备好解决办法（hooks），当事情真的发生了（事件触发），就**自动**按规定好的办法去解决（hooks执行）。基本上就是这么个意思。

而同一个 git 源仓库一般都有远程服务器和本地开发两个地方，那么相应的，git hooks 也有服务端 hooks 和客户端 hooks 的区分。不难理解，服务端的 hooks 就是当服务端仓库发生了某些事件的时候会被触发，客户端的 hooks 就是当客户端仓库发生了某些事件的时候被触发，然后去完成一些事情。

这里要做的就是当客户端提交代码到 git 服务器上这一事件发生，并成功后，通过 hooks 自动把开发者最新提交到代码部署到另一台线上服务器。

根据上面的说明，我们要做的只是去监听 git 服务器的某个仓库有无更新，若有且成功则执行部署操作。因此要完成这个任务只需要一个 `post-receive` hooks 就可以了。

``` shell
# 进入 git 服务器上要监听的仓库路径
cd /data/gitlab/git-data/repositories/user/repo.git/

# 进入 hooks 所在目录
cd .git/hooks

# 编辑 post-update 文件
vim post-receive
```

这里注意：

- 本地仓库和服务器仓库不同，但 hooks 路径都不受版本控制。
- post-update 等是 git 定义好的事件名，不能随便起名。

然后 post-update 的内容是：

``` shell
#! /bin/sh

# 接收标准输入参数判断更新了哪个分支
# !!! 只有是 master 或 dev 分支推送成功 才进行下一步操作
ref='refs/heads/'

while read oldrev newrev refin
do
  if [ $refin == $ref'master' ]
  then
      remote='www.master-server.com'
  elif [ $refin == $ref'dev' ]
  then
      remote='www.dev-server.com'
  else
  	  echo 'ref error'
  	  exit -1
  fi
done

echo -e "\n######## Env Updating: http://$remote\n"

# 远程登录并执行指定位置的预置更脚本
# !!! 事先配置好 SSH Key 免密码登录
# !!! 事先安装好 Tmux [防止断线导致更新失败]
# 如果要显示远程执行输出则下两行互换注释状态
# ssh -T root@$remote > /dev/null 2>&1 << ____remote_updating_res____
ssh -tt root@$remote << ____remote_updating_res____

cd ~/update
# tmux -c ./update_${remote}_env
./update_${remote}_env

exit
____remote_updating_res____

echo $'\n######## Env Updating: Done.\n'
exit 0

# unset GIT_DIR
# `pwd`
```

然后要部署的服务器相应位置要准备好名为 post-receive 中的相关脚本。

- update_www.ENVNAME_env

``` shell 
#! /bin/bash

cd /data/wwwroot/www.ENVNAME.com

# remove local changes
git add . -A
git stash

# update
git pull origin BRANCH_NAME

# 如果是通过 git hooks 则不需要执行下面的语句
# 如果是远程更新 master 则需要先更新 dev 后再合并到 master
# 
# git checkout dev
# git pull origin dev
# git checkout -
# git merge dev
```

这样一来，当开发者推送代码到 dev 或 master 成功后，就会自动部署到线上了。

### 注意事项

- 要确保 ssh 免密登录正确。

- 网络不好可能会更新失败，建议做好日志记录，便于排错。

- 由于 GitLab 对 Git Hooks 进行了自己的封装，不便于直接修改原有的 hooks，因为会印象 GitLab 自身的运行。因此，GitLab 有自定义的 Hooks 以便实现同样的功能。链接见 *参考* 。

  > Follow the steps below to set up a custom hook:
  >
  > 1. Pick a project that needs a custom Git hook.
  > 2. On the GitLab server, navigate to the project's repository directory. For an installation from source the path is usually`/home/git/repositories//.git`. For Omnibus installs the path is usually `/var/opt/gitlab/git-data/repositories//.git`.
  > 3. Create a new directory in this location called `custom_hooks`.
  > 4. Inside the new `custom_hooks` directory, create a file with a name matching the hook type. For a pre-receive hook the file name should be `pre-receive`with no extension.
  > 5. Make the hook file executable and make sure it's owned by git.
  > 6. Write the code to make the Git hook function as expected. Hooks can be in any language. Ensure the 'shebang' at the top properly reflects the language type. For example, if the script is in Ruby the shebang will probably be`#!/usr/bin/env ruby`.
  >
  > That's it! Assuming the hook code is properly implemented the hook will fire as appropriate.

## 参考

- *[How To Use Git Hooks To Automate Development and Deployment Tasks](https://www.digitalocean.com/community/tutorials/how-to-use-git-hooks-to-automate-development-and-deployment-tasks)*
- *[Custom Git Hooks](https://docs.gitlab.com/ce/administration/custom_hooks.html)*


- *[Linux Shell远程执行命令（命令行与脚本方式）](http://www.cnblogs.com/ilfmonday/p/ShellRemote.html)*
- *[利用git自动部署环境](https://argcv.com/articles/2078.c)*
- *[Git pull from a php script, not so simple.](http://jondavidjohn.com/git-pull-from-a-php-script-not-so-simple/)*

