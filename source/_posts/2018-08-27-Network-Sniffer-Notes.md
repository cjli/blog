---
layout: post
title: 网络抓包笔记
category: 折腾
tags: [抓包, Wireshark, Charles, SquidMan]
---

不管是故障诊断还是性能调优，抓包工具都是搞清楚网络问题的唯一方法。

<!-- more -->

>《发布！软件的设计与部署》 

今天简单总结下 macOS 下使用过的几种抓包工具的使用记录和一些注意事项。

## Wireshark

Wireshark 是很古老但也很权威的网络分析工具，几乎支持所有网络协议，缺点就是 UI 相对不够美观（QT 改写后还好），然后使用相对来说较复杂。

Wireshark 既有 GUI 版本也有 CLI 版本，除非你要把系统当作代理服务器，抓取其他设备的网络数据的话，一般都使用 GUI 版本的。

### 通过 Wireshark 手机抓包

> 以 iOS 为例说明。

Wireshark 只能针对某个接口抓包，因此我们必须要想办法把 iOS 手机设备的流量代理到 macOS 上的某个接口，才能使用 Wireshark 正常抓包。

#### macOS 连接有线后共享无线网络

macOS 默认提供了网络共享，只不过需要连接有线。在这种方式下，手机终端可以直接连接到共享的无线接口，然后通过 Wireshark 监听该接口即可。

#### RVI 接口

``` shell
# 0. 获取 iOS 设备的 UDID （Xcode/iTunes）

# 1. 创建 RVI（Remote Virtual Interface，远程虚拟接口）
rvictl -s 74bd53c647548234ddcef0ee3abee616005051ed

# 2. 查询新建的 RVI 接口 (rvi0, rvi1, ...)
ifconfig -l

# 3. 删除不再使用的 RVI 接口
rvictl -x 74bd53c647548234ddcef0ee3abee616005051ed
```

这种情况的缺点就是必须要连接到 MacBook，不是很方便，优点是能让 Wireshark 只监控手机的流量，因为通过接口名区分出来了。

#### [SquidMan](https://squidman.net/squidman/)

SquidMan 是一个专门为 macOS 开发的代理服务器，相比上面的 RVI 连接手机终端方式，通过 SquidMan 这样的工具，就可以在同一个无线局域网内设置手机连接代理服务器到 macOS 上，然后再通过 Wireshark 来抓取流量了。

安装使用也非常简单，简单总结如下：

- 安装 SquidMan: `brew cask install squidman`，点击 App 图标后进行后续安装步骤。

- 设置代理端口：默认 8080，可以打开软件界面后在 General -> HTTP Port 处更改。

- 设置提供服务的客户端 IP（列表）：在 Clients tab 界面，New 一条运用连接的客户端 IP 记录，比如我想代理我的局域网内所有 IP：`192.168.2.0/24`。

- 保存设置后重启。

这种方式的缺点就是 Wireshark 监控的还是 macOS 的主网卡流量，而一般 macOS 主网卡流量也不少，因此需要设置过滤条件，过滤出手机的流量，才好分析。

#### 安装 Wireshark 主机作为代理服务器

前面提到 Wireshark 有 CLI 版本，那么就可以在主机上设置为代理服务器，然后安装 Wireshark 并监听某个接口的流量，从此来抓取其他设备的网络流量。

思路其实和 SquidMan 类似，只是后续还需要把抓包的日志文件导出到 GUI 下分析。

### Wireshark 解密 HTTPS 流量

> Wireshark 的抓包原理是直接读取并分析网卡数据，要想让它解密 HTTPS 流量，有两个办法：
> 1. 如果你拥有 HTTPS 网站的加密私钥，可以用来解密这个网站的加密流量。
> 2. 某些浏览器支持将 TLS 会话中使用的对称密钥保存在外部文件中，可供 Wireshark 加密使用。

#### 通过 HTTPS 私钥

这种方式一般不可用，因为除非是你自己的网站（知道 SSL 证书私钥），否则用不了。

#### 通过 TLS 会话对称密钥

##### 注意

配置好 `SSLKEYLOGFILE` 环境变凉后一定要从终端打开浏览器和 Wireshark，否则读不到改变量仍然无法解密 HTTPS 流量。

```
echo $SSLKEYLOGFILE
```

## [Charles](https://www.charlesproxy.com/)

如果说 Wireshark 是大而全的话，那么 Charles 就是小而精了——专门的 http/https 网络调试工具，可以代理抓包、转发请求等在开发调试阶段非常有用的功能。

Charles UI 相对比较美观，操作也非常直白，默认提供代理服务器设置和 HTTPS/SSL 流量的解密，对于 Web 调试来说基本足够。

### 终端连接 Charles 代理

- 在 macOS 上启用代理，记住 macOS 内网 IP+代理的端口号
- 在手机等终端网络连接设置处填写上面的 IP 和端口

> 到这里就可以抓普通的 http 数据包了，以下步骤是设置 HTTPS 抓包的。

- 连上代理后，在浏览器输入 `http://chls.pro/ssl` 安装 CA 证书（iOS 描述文件）
- 如果是 iOS，安装完成后还要在 「通用」/「关于本机」/「证书信任设置」处手动启用刚刚安装的证书。
- Charles 启用 SSL 代理：Proxy -> SSL Proxying Settings... -> Enable SSL Proxying -> Add

#### 说明

- iOS 开启其他代理后（SS/VPN等）可能会出现抓不了包的情况。
- 不抓包请关闭手机 HTTP 代理，否则断开与电脑连接后会连不上网。

## [mitmproxy](https://mitmproxy.org/)

mitmproxy 可以当作是 Charles 的免费[开源](https://github.com/mitmproxy/mitmproxy) CLI 版本，从功能上说，Charles 有的 mitmproxy 都有，而且 mitmproxy 同时提供了 Web 界面和 Python API，更适合完成一些自动化等任务。

- 安装 mitmproxy：`brew install mitmproxy`
- 运行 mitmproxy: `mitmproxy`

> 启动参数详见：`mitmproxy --help`。比如修改默认端口：`mitmproxy -p 8899`

### 终端连接 mitmproxy 代理

基本思路和 Charles 一致，只是安装 mitmproxy 的 CA 证书时在浏览器输入的地址为 `http://mitm.it`。

## 参考

- *[How to set up a proxy server on Mac OS X](https://howchoo.com/g/mwi3ntu1mjq/how-to-set-up-a-proxy-server-on-mac-os-x#install-squidman)*
- *[Wireshark抓包iOS入门教程](http://mrpeak.cn/blog/wireshark/)*
- *[十分钟学会Charles抓包(iOS的http/https请求)](https://www.jianshu.com/p/5539599c7a25)*
- *[三种解密 HTTPS 流量的方法介绍](https://imququ.com/post/how-to-decrypt-https.html)*
- *[使用 Wireshark 调试 HTTP/2 流量](https://imququ.com/post/http2-traffic-in-wireshark.html)*
- *[和Charles同样强大的iOS免费抓包工具mitmproxy](http://mrpeak.cn/blog/mitmproxy/)*