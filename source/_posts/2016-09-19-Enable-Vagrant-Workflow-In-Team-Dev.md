---
layout: post
title: Enable Vagrant Workflow in Team Dev
category: 折腾
tags: [Vagrant, VirtualBox]
---

最近被团队开发中各种环境不一致带来的问题搞得头大，因此是时候研究下 vagrant 和 docker 了。

<!-- more -->

## 基础

- 1.安装 virtualbox 和 vagrant。［详见：<https://www.vagrantup.com/docs/installation/>］

- 2.添加／创建 box

  可以事先下载好 vagrant box 到本地，然后使用 `vagrant box add alias /path/to/box` 添加进去即可。

  也可以直接在线添加。其中 box 名字／URL 可以在“参考”中找到。

  添加成功后，可以使用 `vagrant box list` 查看已添加到 vagrant 中的镜像有哪些。

- 3.使用添加的系统镜像初始化开发环境

  ```shell
  # 假设已经添加过别名位 centos 的系统镜像到 vagrant
  vagrant init centos
  vagrant up
  ```

## 配置

- 分配虚拟内存

  ``` shell
  vim Vagrantfile

  #＃ 找到并取消注释
  config.vm.provider "virtualbox" do |vb|
  # Display the VirtualBox GUI when booting the machine
  vb.gui = false

  # Customize the amount of memory on the VM:
  vb.memory = "1024"
  end
  ```


- 分配网络 IP

  ```shell
  vim Vagrantfile
  config.vm.network "private_network", ip: "192.168.33.10"
  vagrant reload

  # 然后在本机 hosts 中加入一个映射关系
  192.168.33.10 centos.dev
  ```

- 共享目录

  默认情况下，Vagrantfile 所在目录对应于虚拟机中 /vagrant 目录。因此，为了方便，可以把虚拟机中的网站根目录配置为 /vagrant 即可。然后本地开发时，也把项目放到 Vagrantfile 所在的路径就行了。

  当然，也可以在 Vagrantfile 中自定义共享路径：

  ``` shell
  # /Users/cjli/www => 本地路径
  # /data/www       => Linux 服务器路径
  config.vm.synced_folder "/Users/cjli/www", "/data/www"
  ```

**说明**：所有配置更改后，都需要重载虚拟机生效。

- 置零系统启动 timeout

  假设系统镜像是 CentOS  Minimal 6.8，默认安装完之后系统启动会等待 5 秒(默认让用户选择启动项用)，而在执行 `vagrant up` 时启动太慢会提示连接超时。在这里无需选择，可以设置为 0，如下，重启后生效。

  ``` shell
  vim /boot/grub/grub.conf
  timeout=0
  ```

## 实践

为了方便团队几位小伙伴直接参考，以 Windows 为例，简单写下步骤如下。

前提：已经按照 [CentOS After Minimal](/2016/09/19/CentOS-After-Minimal) 打包好一份镜像，假设命名为 centos.box。

1. 安装: virtualbox + 同版本扩展包 + vagrant

2. 新建开发目录，比如：c:/dev/

3. 将 .box 文件(虚拟机镜像)，比如 centos.box 移动到 c:/dev/ 下

4. 新建目录并进入：c/dev/Vagrant，按住 shift + 点击右键，打开终端:

```
vagrant box add centos6dev c:/dev/centos.box
vagrant init centos6dev
vagrant plugin install vagrant-vbguest
```

5. 清空配置文件 Vagrantfile，用以下替代，见 *附录 2*：

6. 在当前目录下启动 Vagrant 并登录至虚拟机: 

```
vagrant up
vagrant ssh
```

后面主要是 git 操作【略】和基本 Linux 路径切换操作。常用的几个路径是：

- 网站目录：/data/wwwroot/

- 默认共享目录：/data/vagrant/

- nginx 虚拟主机配置路径：/usr/local/nginx/conf/vhost

举例说明：

```
# 1. 创建虚拟主机
~/oneinstack/vhost.sh

# 2. 按提示一步步选择
# ...

# 3. 如果是 laravel/lumen 项目则还需修改 nginx 虚拟主机配置，比如创建了 eme.dev 网站项目后
vim /usr/local/nginx/conf/vhost/sample.dev.conf

# 在 root 值后面追加 /public

# 4. 重载 nginx
nginx -s reload

# 5. 克隆本机仓库
cd /data/wwwroot/
rm -rf eme.dev
git clone /vagrant/eme.dev    # 为了简便 仓库名和网站虚拟域名要一样

# 6. 配置虚拟机内仓库
git remote remove origin
git remote add local /vagrant/eme.dev    # 跟虚拟机中的路径
git remote add origin git@git.keensword.net:cjli/eme168.git    # 跟 gitlab 上的仓库地址

# 7. 修改 windows hosts
notepad c:/windows/system32/drivers/etc/hosts
# 格式：虚拟机IP 网站虚拟域名, eg:
192.168.10.33 eme.dev

# 8. 拉取本机改动 假设你的开发分支是 cjli
git pull local cjli
```

其余操作和以前在 Windows 上的操作一致。

###### *说明*

经试验，使用 VirtualBox Share Folders 的方式会出现卡，且 Window/Mac 机器配置文件不能共享等问题，所以建议从第 `#5` 步开始就可以停止了，然后在本机 IDE 中配置好 SFTP **远程编辑**测试环境的网站目录。

经实际测试，SFTP 自动同步代码速度比使用 Git 拉去共享文件夹中的代码要快很多。这样也避免了向 Git 仓库提交过多历史记录的问题。

针对 PhpStrom 远程编辑 Vagrant  CentOS 网站项目的详细配置见：*[Mac 生产日志集合@开发@配置远程编辑+自动同步代码](/2016/09/22/Mac-Exp/#more)*。

## 附录

### 1. 常用指令

``` shell
vagrant list-commands
```

- 打包

  ``` shell
  # 不带 vagrantfile
  vagrant package
  # 带上 vagrantfile
  vagrant package --vagrantfile Vagrantfile
  ```

- 销毁虚拟机

  ```shell
  # 1. 得到 name 或 id
  vagrant global-status
  # 2. 根据 name 或 id 销毁虚拟机
  vagrant destroy [name|id]
  ```


### 2. Vagrantfile

```
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "centos6dev"

  config.ssh.username = 'root'
  config.ssh.forward_agent = true
  config.vm.box_check_update = false

  # config.vm.network "forwarded_port", guest: 80, host: 8080
  config.vm.network "private_network", ip: "192.168.33.10"

  # config.vm.network "public_network"

  # config.vm.synced_folder "./www", "/data/wwwroot"

  config.vm.provider "virtualbox" do |vb|
    vb.gui = false
    vb.memory = "1024"
  end

  config.vbguest.auto_update = false

  # config.push.define "atlas" do |push|
  #   push.app = "YOUR_ATLAS_USERNAME/YOUR_APPLICATION_NAME"
  # end

  config.vm.provision "shell", inline: <<-SHELL
    cat ~/resolv.conf > /etc/resolv.conf
    service network restart
  SHELL
end
```
## FAQ

- __Vagrant ssh authentication failure?__

  ```shell
  # 1. 权限问题
  chmod 700 ~/.ssh
  chmod 600 ~/.ssh/authorized_keys
  chown -R vagrant:vagrant ~/.ssh

  # 2. 备用方案: 使用密码
  vim Vagrantfile
  config.ssh.username = 'root'
  config.ssh.password = 'vagrant'
  config.ssh.insert_key = 'true'

  # 3. 其他尝试
  ## 使用自己的 SSH Key
  rm .vagrant/machines/core-01/virtualbox/private_key
  ssh-add ~/.ssh/id_rsa
  ssh-add -L

  # 4. 查看vagrant配置的ssh私钥是否是自己重置过的
  vagrant ssh-config # => `IdentityFile`，如果这个文件和虚拟机中的公钥不是一对则会校验失败
  ```

  - <http://dockone.io/article/339>
  - <http://stackoverflow.com/questions/22922891/vagrant-ssh-authentication-failure>

- __the guest additions on this vm do not match the installed version of virtualbox vagrant？__

  ```shell
  # For vagrant < 1.1.5:
  sudo vagrant gem install vagrant-vbguest

  # For vagrant 1.1.5+ (thanks Lars Haugseth):
  sudo vagrant plugin install vagrant-vbguest
  ```

  通过 vagrant-vbguest 插件，每次启动 vagrant 镜像时都会检查其上安装的 guest additions 和本机安装的 vitualbox 是否匹配，如果不是就会自动下载更新虚拟机上的 guest。

- __vagrant up : The following SSH command responded with a non-zero exit status__?

  根据提示中报错的 shell 命令可以推测出是在执行何种命令时出错。比如若是执行：`yum install -y kernel-devel-`uname -r` gcc binutils make perl bzip2` 出现错误(_Stderr from the command: zsh:1: command not found: sudo_) 则可以知道是因为 zsh 的原因使安装 guest 的时候出现错误。

  此时的解决办法之一是，直接重新下载安装 sudo :`yum install sudo`。

- __GuestAdditions seems to be installed (5.0.14) correctly, but not running.__

  ```
  config.vbguest.auto_update = false
  ```

- **Fedora 24 上启动 vagrant 失败?**

  ```shell
  dnf install kernel-core kerner-headers kernel-devel
  /path/to/vboxdrv.sh setup
  reboot
  ```

## 参考

- [Discover Vagrant Boxes](https://atlas.hashicorp.com/boxes/search)
- <http://www.vagrantbox.es/>
- <https://github.com/chef/bento>