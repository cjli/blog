---
layout: post
title: 用 Docker 搭建 PHP 开发环境
category: 折腾
tags: [Docker, PHP, MySQL, Dnsmasq, nginx-proxy]
---

前面我简单总结了创建一个 Apache + PHP 的基础镜像的过程，但是在实际开发中肯定要操作数据库，动态修改代码等等。

<!-- more -->

为了用 Docker 实现一个真实的开发场景，今天主要需要解决 3 个问题：

- 应用容器如何关联数据库容器，以及其他容器？
- 如何动态向运行中的容器添加修改代码？
- 如何通过本地域名访问容器中的 Web 服务？

## 关联 MySQL 容器

首先，在本地 pull 一个版本的 MySQL 镜像下来。

``` shell
docker pull mysql:5.6.36
```

然后启动：

``` shell
docker run --name mysql -e MYSQL_ROOT_PASSWORD=123456 -p 3306:3306 -d mysql:5.6.36
```

有两种方式在应用容器中关联 MySQL 容器：

### 通过查看 MySQL 容器的 IP

``` shell
docker inspect mysql | grep IPAddress
```

然后，在应用容器的代码中配置数据库地址为这个 IP 即可。

### 通过 `--link`

在启动应用容器的同时，指定 `--link` 参数，格式为：`--link <name|id>[:alias]`。

``` shell
docker run --name <APP_CONTAINER_NAME> --link <MYSQL_CONTAINER_NAME>:db -d <APP_IMAGE_NAME>
```

其中的 `MYSQL_CONTAINER_NAME` 指的就是在启动 MySQL 容器时的指定的名称，`db` 是给在应用容器中为 MySQL 容器留的别名，应用连接 MySQL 的时候，即可使用 `MYSQL_CONTAINER_NAME` 也可以使用别名。

可以通过 inspect 查看 link 有无生效：

``` shell
docker inspect -f "{{ .HostConfig.Links }}" <APP_CONTAINER_NAME>

--> 输出应类似：
[/mysql:/<APP_CONTAINER_NAME>/db]
```

然后，在应用容器中，就可以通过 `MYSQL_CONTAINER_NAME`  或别名（`db`）来连接数据库容器。比如：

``` php
new PDO('mysql:host=<MYSQL_CONTAINER_NAME|db>;port=3306;charset=utf8', 'root', 'passwd');
```

## 挂载项目路径到容器

要实现动态修改 Web 服务中的项目代码，首先需要了解 Docker 提供的数据卷。

### 数据卷

数据卷是一个可供一个或多个容器使用的特殊目录，它可以绕过 [UnionFS](https://en.wikipedia.org/wiki/UnionFS)，并具有如下特性：

- 数据卷可以在容器之间共享和重用
- 对数据卷的修改会立马生效
- 对数据卷的更新，不会影响镜像
- 数据卷默认会一直存在，即使容器被删除（也不存在垃圾回收的机制来自动删除）

#### 创建数据卷

- 在启动容器时通过 `-v` 参数创建

``` shell
docker run -d -P --name php54 -v /var/www/html php54:v1
```

这条命令将创建一个数据卷并挂载到容器的 `/var/www/html`。（类似 Linux 的 mount）

- 在 Dockerfile 中通过 `VOLUME` 创建

``` dockerfile
VOLUME /var/www/html
```

#### 删除数据卷

要在删除容器的同时删除数据卷，可以使用 `docker rm -v`。

#### 挂载主机路径作为数据卷

```shell
docker run -d -p 80:80 \
--name php54 \
-v <PATH_OF_HOST_PROJ>:/var/www/html:ro \
ckloy/php54:v1
```

对于 macOS 来说，这里指定的主机路径 `PATH_OF_HOST_PROJ` 必须是绝对路径，且其父级目录必须是在 Docker 文件共享里面设置过的。

然后在本地编辑 `PATH_OF_HOST_PROJ` 项目代码，就可以在 web 应用容器中的生效了。

上面的 `:ro` 代表容器只能读，如果不带则默认为可读可写。

> **注意**：Dockerfile 中不支持这种用法，这是因为 Dockerfile 是为了移植和分享用的。然而，不同操作系统的路径格式不一样，所以目前还不能支持。

此外，如果路径 `PATH_OF_HOST_PROJ` 是一个文件，通常的用途是用来记录容器中输入过的命令：

``` shell
docker run --rm -it -v ~/.bash_history:/.bash_history ubuntu /bin/bash
```

> **注意**：如果直接挂载一个文件，很多文件编辑工具，包括 vi 或者 sed --in- place ，可能会造成文件 inode 的改变，从 Docker 1.1 .0起，这会导致报错误信 息。所以最简单的办法就直接挂载文件的父目录。

### 数据卷容器

数据卷容器，其实就是一个正常的容器，专门用来提供数据卷供其它容器挂载的。主要用于在容器间共享一些持续更新的数据。

#### 创建数据卷容器

``` shell
docker run -d -v /share_data --name data_only training/postgres
docker run -d -v /share_data_2 --name data_only2 training/postgres
```

#### 挂载数据卷容器

``` shell
docker run -d --volumes-from data_only --name db1 training/postgres
docker run -d --volumes-from data_only --name db2 training/postgres
docker run -d --volumes-from data_only --volumes-from data_only_2 --name db3 training/postgres
```

> 使用 `--volumes-from` 参数所挂载数据卷的容器自己并不需要保持在运行状态。

#### 级联挂载数据卷容器

``` shell
docker run -d --name db4 --volumes-from db1 training/postgres
```

#### 删除数据卷容器

同数据卷，如果删除了挂载了数据卷容器的容器，数据卷容器并不会自动被删除，如果需要删除数据卷容器，在删除最后一个还挂载着它的容器时使用 `docker rm -v`。

## 配置本地开发域名

通过 Docker 创建的容器只能有一个能监听 80 端口，这就给要同时运行多个 Web 容器带来了不便：每次访问不同的 Web 容器都需要使用端口号来区分。

### nginx-proxy

我们都知道，借助 nginx 的反向代理功能，可以将来自不同域名但指向同一个 IP 的请求转发到不同的后端处理服务。

因此，要是在 docker 创建的 Web 容器之间，同样可以借助 nginx，把来自本地不同开发域名但 IP 都指向本地系统的请求转发到各自的 Web 容器就好了。

[nginx-proxy](https://github.com/jwilder/nginx-proxy) 刚好就是做这件事的。nginx-proxy 借助了 [docker-gen](https://github.com/jwilder/docker-gen) 获取所有运行的 docker 容器元数据并生成反向代理配置，每当有容器启动或停止时，docker-proxy 就会重载 nginx，使我们能够通过关联的域名访问到想要访问的容器。

#### 启动 nginx-proxy

```shell
docker run -d -p 80:80 -v /var/run/docker.sock:/tmp/docker.sock:ro jwilder/nginx-proxy
```

注意，这里必须要让 nginx-proxy 容器监听本地系统的 80 端口，这样做后虽然是 Web 容器无法直接通过 `IP:80端口` 来访问，但是却可以自定义一个指向本地系统的域名来访问（间接通过 80 端口访问），很显然，通过自定义域名的方式要比通过端口区分不同的 Web 容器来的方便。

#### 启动 Web 容器

``` shell
docker run -e VIRTUAL_HOST=php54.docker -d --rm=true -P --name php54 php54:v1
```

这里的 `VIRTUAL_HOST` 真正起作用的地方，它的值，也就后开发要用到的本地域名，会被 nginx-proxy 读取到然后自动生成一份反向代理配置。

### 配置 DNS

在上一步命令中的自定义 `php54.docker` 在没有配置任何指向前，是没有任何意义的。因此必须要让本地系统知道它的指向。

对于这个的配置，有自动管理工具比如 _[dnsmasq](http://www.thekelleys.org.uk/dnsmasq/doc.html)_，配合 macOS 的 /etc/resolver/TOP_LEVEL_DOMAIN，可以自动实现将 `TOP_LEVEL_DOMAIN` 结尾的域名指向本地系统（127.0.0.1）。

macOS 中安装配置 dnsmasq 如下：

``` shell
# 1. Install dnsmasq
brew install dnsmasq

# 2. Copy the default configuration file.
# cp $(brew list dnsmasq | grep /dnsmasq.conf.example$) /usr/local/etc/dnsmasq.conf
# 2. Create the default dnsmasq configuration file.
sudo tee /usr/local/etc/dnsmasq.conf >/dev/null <<EOF
address=/.docker/127.0.0.1
EOF

# 3. Copy the dnsmasq daemon plist into system default
sudo cp $(brew list dnsmasq | grep /homebrew.mxcl.dnsmasq.plist$) /Library/LaunchDaemons/

# 4. Auto start dnsmasq
sudo launchctl load /Library/LaunchDaemons/homebrew.mxcl.dnsmasq.plist

# 5. Start or Stop dnsmasq
sudo launchctl stop homebrew.mxcl.dnsmasq
sudo launchctl start homebrew.mxcl.dnsmasq

# 6. Create the resolver corresponding to dnsmasq configuaration
sudo tee /etc/resolver/docker >/dev/null <<EOF
nameserver 127.0.0.1
domain docker
EOF
```

##### 说明: 新版 dnsmasq

在通过 homebrew 安装完较新的 dnsmasq 后，可以直接编辑配置文件然后通过 brew 命令启用服务。

```
vi /usr/local/etc/dnsmasq.conf
sudo brew services start dnsmasq
```

然后可以测试：

- dig

  ```
  dig a.b.c.docker @127.0.0.1

  ----> 输出应类似：

  ; <<>> DiG 9.8.3-P1 <<>> a.b.c.dddd @127.0.0.1
  ;; global options: +cmd
  ;; Got answer:
  ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 28558
  ;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 0

  ;; QUESTION SECTION:
  ;a.b.c.docker.			IN	A

  ;; ANSWER SECTION:
  a.b.c.docker.		0	IN	A	127.0.0.1

  ;; Query time: 2 msec
  ;; SERVER: 127.0.0.1#53(127.0.0.1)
  ;; WHEN: Sat Jul 15 15:39:11 2017
  ;; MSG SIZE  rcvd: 44
  ```

- ping

  ``` 
  PING a.b.c.docker (127.0.0.1): 56 data bytes
  64 bytes from 127.0.0.1: icmp_seq=0 ttl=64 time=0.056 ms
  64 bytes from 127.0.0.1: icmp_seq=1 ttl=64 time=0.073 ms
  64 bytes from 127.0.0.1: icmp_seq=2 ttl=64 time=0.054 ms

  --- a.b.c.dddd ping statistics ---
  3 packets transmitted, 3 packets received, 0.0% packet loss
  round-trip min/avg/max/stddev = 0.054/0.061/0.073/0.009 ms
  ```

测试通过。

#### 配置多个本地泛域名解析

**_/usr/local/etc/dnsmasq.conf_ 文件中的 `address=/.docker/127.0.0.1` 部分的 IP 地址指的是要把 `*.docker` 解析到的 IP 地址，而 _/etc/resovler/docker_ 中的 `nameserver` 指的是 dnsmasq 所安装主机的 IP 地址。两者不要搞混淆。**

因此，比如我还要加一个 `*.centos` 的泛解析到虚拟机 IP，由于我的 dnsmasq 安装到本机，因此需要配置如下：

- 配置 dnsmasq.conf：`address=/.centos/192.168.56.42`
- 配置 /etc/resolver/centos

```
nameserver 127.0.0.1
domain centos
```

### 同步 hostname

注意，启动容器虽然指定了本地域名，但是在 Web 容器内部的 hostname 却不是，这将可能导致一些问题，因此为了统一，最好需要让容器也知道这个本地域名，可以通过 `-h` 参数指定。比如：

``` shell
docker run --rm=true -h php54.dev --name php54 -it php54:v1 /bin/bash
```

进入后 `ping -c 1 php54.dev` 可以看到当前域名已经指向了 127.0.0.1。

通过这样指定后，还有个好处就是其他容器也能知道 _php.dev_ 这个域名对应的是 _php54_ 这个容器。

### 小结

如果没有出现意外，以后开发时只需要给每个容器指定一个以 `.docker` 结尾本地域名，便可以直接在浏览器输出 _proj_name.docker_ 访问到在 `VIRTUAL_HOST` 指定为 `proj_name` 的容器。

## 总结

本文，介绍了如何给应用容器关联数据库容器、如果挂载项目代码到容器、如何配置本地开发域名。在我看来，这对于本地开发来说，已经算相当方便了。

带上上述 3 点启动容器完整的命令示例：

``` shell
docker run \
-e VIRTUAL_HOST=php54.docker \
-h php54.docker \
-d --rm=true -P \
--name php54 \
-v /path/to/php54:/var/www/html \
--link mysql:db php54:v1
```

然后用 sublime text 编写代码：`sbl/paht/to/php54 `。

最后在浏览器查看效果：`http://php54.docker`。

So Cool! (:

### 示例：构建一份 php7 镜像

``` tree .
$ tree .
.
├── Dockerfile
├── README.md
├── app
│   └── index.php
├── build
│   ├── nginx.conf
│   └── php-fpm.conf
└── docker.run

2 directories, 6 files
```

详见我的 GitHub 仓库： [php-docker-images/php7/v1](https://github.com/cglai/php-docker-images/tree/master/php7/v1)。

## FAQ

- **docker: Error response from daemon: Mounts denied:**

  ```
  The path /_proj/

  is not shared from OS X and is not known to Docker.

  You can configure shared paths from Docker -> Preferences... -> File Sharing.
  ```

  这是由于指令中设置的路径在 macOS Docker 没有设置。按提示设置即可。

- **如果启动容器的时候没有指定端口映射会怎样？**

  docker 依然会运行，如果没有明确指明端口映射，可以使用 `-P` 参数，此时 Docker 会自动给这个容器关联一个主机的端口到容器暴露端口的映射。

- **oci runtime error: container_linux.go:247: starting container process caused "exec: "/var/www/html/docker.run": permission denied".**

  这是由于执行 `docker run -d -P --name php54 -v /path/to/php54/app:/var/www/html ckloy/php54:v1 /var/www/html/docker.run` 命令时，由于已经挂在了一个本地路径到容器作为数据卷，此时要运行的 `/var/www/html/docker.run` 已经是 `/path/to/php54/app/docker.run` 了，因此检查这个文件是否具有可执行权限，如果没有请 `chomd +x /path/to/php54/app/docker.run` 即可。

- **Dockerfile 里的 `CMD` 命令失效？**

  `CMD` 的作用是用于指定容器从镜像启动时，默认会执行的命令，常用于启动 WEB 服务，比如：

  ``` dockerfile
  CMD ["nginx", "-g", "daemon off;"]
  CMD ["/usr/sbin/apachectl", "-D", "FOREGROUND"]
  ```

  `CMD` 指令常用的格式有：

  - shell 格式：`CMD 命令`
  - exec 格式：`CMD ["可执行文件", "参数1", "参数2", ...]`

  注意事项有：

  - exec 格式的参数必须用双引号：因为 exec 格式在解析时会被解析为 JSON 数组。
  - 如果在启动容器时，新指定了一个命令，则该命令会失效。
  - 一个 Dockerfile 里面只能有一个 `CMD` 指令，如果有多个则只有最后一个会生效。

- **apache2: Could not reliably determine the server's fully qualified domain name, using 127.0.1.1 for ServerName … waiting apache2:**

  ``` shell
  echo 'ServerName localhost' >> /etc/httpd/conf/httpd.conf
  ```

- **在 _ckloy/php54:v1_ 镜像中 Apache Rewrite rule not working?**

  在 _[ckloy/php54:v1](https://hub.docker.com/r/ckloy/php54/)_ 这个镜像中，Apache 配置文件中对 `<Directory "/var/www/html">` 这个默认网站根目录的 `AllowOverride` 值默认为 `None` 的（338行），也就是说不允许网站路径下 .htaccess 文件中任何重写指令生效。

  因此，必须要手动修改  httpd.conf 后重启 Apache。（注意顺序）

  ``` shell
  # 1. 启动容器，但不启动 Apache
  docker run -d -p 80:80 -it --name jhwm -v /path/to/php54:/var/www/html php54:v1 /bin/bash

  # 2. 修改 httpd.conf
  docker exec <CONTAINER_ID> sed -i '338s/None/All/' /etc/httpd/conf/httpd.conf

  # 3. 启动 Apache
  docker exec -d <CONTAINER_ID> /data/shell/docker.run
  ```

  当然，除了这种方式外，还可以通过修改 Dockerfile 在现有镜像的基础上重新 build 一份镜像。

- **php xhprof Requires: php(zend-abi) = 20100525-64**

  php7+ 版本不能直接通过 `yum install php-pecl-xhprof`，因为依赖解决不了。

  只能手动编译安装一份支持 php7 的 xhprof。详见：[github-issue:PHP 7 support](https://github.com/phacility/xhprof/issues/82).

- Docker for macOS 容器内部时间不自动同步 host 问题？

  这种问题主要出现在 MacBook Pro 合盖休眠重新登录后，此时容器内部的时间就会比主机时间落后，依然是合盖前的时间。

  已知解决办法如下：

  - 手动

    - 重启容器

      ``` shell
      docker restart <CONTAINER_ID>
      ```

    - 手动同步

      ``` shell
      docker exec <CONTAINER_ID> date -s "`date`"
      ```

  - 自动

    - [arunvelsriram/docker-time-sync-agent](https://github.com/arunvelsriram/docker-time-sync-agent/)

## 附录：常用命令

### attach

```shell
docker attach [OPTIONS] CONTAINER_NAME
```

`attach` 会立刻关联到某个运行中的容器此刻的状态。

需要注意的是，如果容器启动后，执行了一些前端挂起的命令，比如 `/usr/sbin/apachectl -D FOREGROUND`，那么执行 attach 命令后，则会出现卡住状态，这其实是正常的，因为这个容器当前的状态就是处于前置挂起状态。

一些能看到输出的例子：

- 定时输出容器内部当前的进程信息：

``` shell
docker run --name test -d centos:6 /usr/bin/top -b
docker attach test
```

- 进入容器 BASH

``` shell
docker run --name test -d -it centos:6
docker attach test
[root@fcd4b10250b3 /]# exit 1024
~ echo $?
1024
```

注意，如果已经关联到某个运行中的容器后，执行了诸如 `exit` 命令，或者按住了 CTRL+C，那么当前这个容器会立即停止运行。在这个例子中，使用 `docker ps -a | grep test`，会得到如下类似输出：

```
fcd4b10250b3  centos:6  "/bin/bash"  About 2 minutes ago  Exited (1024) 2 minutes ago  test
```

可见，STATUS 确实显示已退出。

### 镜像、容器清理

``` shell
# 1. Docker Version > 1.13
docker system prune -a
docker system prune --volumes -f    # prune junk data kept by docker

# 2. Order Docker version
docker rm $(docker ps -a -q)

# 3. Remove All dangling images
docker rmi -f $(docker images -q -a -f dangling=true)
```

其中第 1 条命令将删除掉：

- 所有停止运行的容器
- 所有没有任何容器使用的数据卷
- 所有没有任何容器使用的网络
- 所有没有任何容器关联的镜像

### 进入已运行容器的 SHELL

docker version >= 1.3：

``` shell
# by container id
docker exec -it <CONTAINER_ID> /bin/bash
# by container name
docker exec -it <CONTAINER_NAME> /bin/bash
```

说明：这种情况下，如果进入该容器 SHELL 后执行 `exit` 并不会终止正在运行的容器。

### 容器／宿主间拷贝

``` shell
# Container to local OS
docker cp mysql:/etc/mysql/my.cnf .

# Local OS to container
docker cp my.cnf mysql:/etc/mysql
```

## 参考

- *[Manage data in containers](https://docs.docker.com/engine/tutorials/dockervolumes/#mount-a-host-directory-as-a-data-volume)*
- *[docker.hub-mysql](https://hub.docker.com/_/mysql/)*
- *[Legacy container links](https://docs.docker.com/engine/userguide/networking/default_network/dockerlinks/#connect-using-network-port-mapping)*
- *[Dockerfile CMD not running at container start](https://stackoverflow.com/questions/35270493/dockerfile-cmd-not-running-at-container-start)*
- *[How to get bash or ssh into a running container in background mode?](https://askubuntu.com/questions/505506/how-to-get-bash-or-ssh-into-a-running-container-in-background-mode)*
- *[Dockerfile reference](https://docs.docker.com/engine/reference/builder/)*
- *[Best practices for writing Dockerfiles](https://docs.docker.com/engine/userguide/eng-image/dockerfile_best-practices)*
- *[.htaccess file not being read (“Options -Indexes” in .htaccess file not working)](https://stackoverflow.com/questions/10769880/htaccess-file-not-being-read-options-indexes-in-htaccess-file-not-working)*
- *[Replacing string based on line number](https://unix.stackexchange.com/questions/70878/replacing-string-based-on-line-number)*
- *[Route traffic to a docker container based on subdomain](https://stackoverflow.com/questions/24527485/route-traffic-to-a-docker-container-based-on-subdomain)*
- *[Using Dnsmasq for local development on OS X](https://passingcuriosity.com/2013/dnsmasq-dev-osx/)*
- *[求助，centos7镜像创建的容器里面安装服务后，无法使用命令启动服务](http://dockone.io/question/729)*
- *[How to Install Nginx, MySQL, PHP v7 (LEMP) stack on CentOS 7](https://www.hostinger.com/tutorials/how-to-install-lemp-centos7)*
