---
layout: post
title: Markdown 链接与 Chrome 扩展开发入门实践
category: Javascript
tags: [Markdown, Chrome, Tampermonkey, 插件]
---

用 Markdown 写文章时一直有一个小痛点：贴参考文章链接时，一般我都是先在编辑器中写好 `[文章标题](链接)` 格式（1），然后手动输入已打开页面的标题名称到 `文章标题`（2），然后手动复制浏览器地址栏上的 URL（3），最后再复制到编辑器中的`链接`中（4）。

<!-- more -->

整个过程需要 4 步骤。

此外，在参考文章标题比较长的时候，手动输入比较慢麻烦，而且有的页面如果想直接复制文章顶部的标题，因为这个标题往往都是一个超链接，因此会经常把 复制文章标题 误操作为 重新点击这个页面。

今天终于忍无可忍，寻思找一个简单粗暴一步到位的方式来彻底解决这个问题：我期望，可以直接点击一下或者快捷键按一波就可以直接复制成 `[文章标题](链接)` 这样的格式。

## Tampermonkey 还是 Chrome 插件?

这个需求不用想肯定是要用 Javascript 来完成，那么在 Chrome 上能执行用户自定义脚本的方式目前流行的就只有用 Tampermonkey 或者写个 Chrome 插件。

一开始准备用 Tampermonkey 的，单纯从复制页面为 Markdown 链接格式的目的，Tampermonkey 肯定是能办到的，但是还是有几个问题：

- Tampermonkey 安装的用户脚本要么不启用，要么就是在页面载入的时候自动执行了，无法很简单地做到根据我的需要想什么时候复制就什么复制，想复制多少次就复制多少次，连按钮或者 context menu 都实现不了

> 或许写一段比较复杂的 Javascript 交互控制逻辑能勉强实现，但是想想就觉得太麻烦啊，这个需求不应该这么难。

- 我当时没找到可以给用户脚本 定制快捷键 的地方，没快捷键何来效率？

因此想了下还是决定用 Chrome 插件来解决。我搜了下 Chrome 网上商店，下载体验了几个复制标题和链接插件觉得都够丝滑足够面向我个人需求的定制化，也没有定义快捷键的设置，所以最后还是决定自己快速先学先用实现下 Chrome 扩展开发，之前挺想搞下 Chrome 扩展开发的没动力搞过，今天就当一个入门实践了吧。

## Chrome 插件开发最少知识点

本着快速上手的原则，我目前只需要了解最少能运行的知识点就够了。看完文档后总结如下：

### 插件项目最少目录结构

新建项目目录 `chrome-ext-copy-site-title`，创建 `manifest.json`、`background.js`、`content.js` 三个文件。

```shell
mkdir chrome-ext-copy-site-title
cd chrome-ext-copy-site-title
touch manifest.json background.js content.js

tree .
.
├── background.js
├── content.js
├── icon.png
└── manifest.json

0 directories, 4 files
```

没错，就只需要这么少。下面简单总结下每个文件的作用。

#### manifest.json

这个很重要，必须要按规范编写。示例如下：

``` json
{
    "manifest_version": 2,
    "name": "Copy Site Title",
    "description": "Copy site title in different ways via context menus or keyboard shortcuts to to clipboard.",
    "version": "0.0.1",
    "author": "cjli",
    "permissions": [
        "activeTab",
        "clipboardWrite",
        "contextMenus"
    ],
    "content_scripts": [
        {
            "matches": [
                "<all_urls>"
            ],
            "js": [
                "content.js"
            ]
        }
    ],
    "icons": {
				"48": "icon.png"
    },
    "background": {
        "scripts": [
            "background.js"
        ],
        "persistent": false
    },
    "commands": {
        "title-link-markdown": {
            "suggested_key": {
                "default": "Ctrl+Shift+M",
                "mac": "Alt+Shift+M"
            },
            "description": "Copy Markdown Link"
        },
        "link-only-urldecoded": {
            "suggested_key": {
                "default": "Ctrl+Shift+L",
                "mac": "Alt+Shift+L"
            },
            "description": "Copy Link (URL decoded)"
        },
        "title-only": {
            "suggested_key": {
                "default": "Ctrl+Shift+C",
                "mac": "Alt+Shift+C"
            },
            "description": "Copy Title (Text)"
        }
    }
}
```

这个元配置文件的配置说明官方文档中都有，这里捡几个重要点选项的简单说明如下：

- `permissions`: 插件需要用到的权限标志符。因为我这里需要复制到剪切板，也需要在页面能够打开 context menu，因此 `clipboardWrite` 和 `contextMenus` 是必须的。
- `content_scripts.js`：可以操作 DOM 的内容脚本名字，可自定义但这里按常见写法为 `content.js`。
- `content_scripts.matches`：需要操作 DOM 的内容脚本的 URL 匹配规则，即符合这里指定规则的 URL 都可以使用这个插件。的可自定义但这里按常见写法为 `content.js`。
- `icons.48`：插件的图标，48 代表像素，配置值为一张 png 图片在项目中的相对路径。
- `background.scripts`：负责插件后台逻辑的脚本名字，后台脚本主要是调用 Chrome 的一些 API。约定俗成为 `background.js`。
- `background.persistent`：后台脚本是否要一直处于激活状态。一般为 `false`，只有在后台脚本需要用到 Chrome 的 `chrome.webRequest` API 阻止或者修改网络请求的时候才设置为 `true`。

> The only occasion to keep a background script persistently active is if the extension uses chrome.webRequest API to block or modify network requests. The webRequest API is incompatible with non-persistent background pages.

- `commands`：配置插件功能快捷键。

#### background.js

后台脚本，必须。

```javascript
chrome.runtime.onInstalled.addListener(() => {
	chrome.contextMenus.create({
		id: 'title-link-markdown',
		title: 'Copy Markdown Link',
		contexts: ['page'],
		documentUrlPatterns: ['<all_urls>'],
	});
	chrome.contextMenus.create({
		id: 'title-only',
		title: 'Copy Title (Text)',
		contexts: ['page'],
		documentUrlPatterns: ['<all_urls>'],
	});
	chrome.contextMenus.create({
		id: 'link-only-urldecoded',
		title: 'Copy Link (URL decoded)',
		contexts: ['page'],
		documentUrlPatterns: ['<all_urls>'],
	});
	chrome.contextMenus.create({
		id: 'link-only',
		title: 'Copy Link (Raw)',
		contexts: ['page'],
		documentUrlPatterns: ['<all_urls>'],
	});
});

chrome.contextMenus.onClicked.addListener((info, tab) => {
    chrome.tabs.sendMessage(tab.id, { text: info.menuItemId }, (title) => {
		console.log('copied site title 1: ', title);
    });
});

chrome.commands.onCommand.addListener(function(command) {
	// https://stackoverflow.com/questions/14245334/sendmessage-from-extension-background-or-popup-to-content-script-doesnt-work
	chrome.tabs.query({active: true, currentWindow: true}, function(tabs){
		chrome.tabs.sendMessage(tabs[0].id, { text: command }, function(response) {
			console.log('copied site title 2: ', command);
		});
	});
});
```

这段脚本主要的作用是：

- 在插件运行时被安装成功时创建 4 个 context menu
- 给这四个 context menu 的点击事件设置了处理逻辑的回调函数，这里是发送消息操作类型（指令）给内容页面
- 给 Chrome 快捷键出发事件设置了处理逻辑的毁掉函数，这里也是发送消息操作类型（指令）给内容页面

#### content.js

前台脚本，必须。

``` js
chrome.runtime.onMessage.addListener((msg, sender, sendResponse) => {
	execute(msg.text);
	sendResponse(document.title);
});

function execute(command) {
	switch (command) {
		case 'title-link-markdown':
			copy(`[${document.title}](${window.location.href})`)
			break;
		case 'link-only':
			copy(window.location.href)
			break;
		case 'link-only-urldecoded':
			copy(decodeURI(window.location.href))
			break;
		case 'title-only':
		default:
			copy(document.title)
			break;
	}
}

// https://stackoverflow.com/questions/3436102/copy-to-clipboard-in-chrome-extension
function copy(data) {
    var copySource = document.createElement('textarea');
    copySource.textContent = data;
    document.body.appendChild(copySource);
    copySource.select();
    document.execCommand('copy');
    document.body.removeChild(copySource);
}
```

这段脚本主要作用也很直接：

- 注册了接受 background.js 发过来的消息并处理的回调函数
- 封装了复制任意内容到剪切板的函数 `copy`
- 封装了执行复制操作类型判断的函数 `execute`

这里我为了节省代码量，将用户点击 context menu 的事件 ID 和快捷键对应的 command 命名进行统一，这样在 context.js 中就可以统一调用同一个 execute 函数来进行相同的 复制内容到剪切板 的操作。

### icon

这个不是必须的。我很懒，直接网上搜的一个在线文字转图片的网站转的。注意 icon 图片命名和路径无所谓，格式必须为 png，然后要在 manifest.json 中 `icons.48` 属性中指定好正确的路径就行了。

### 运行 & 调试

- 安装/加载插件

在 Chrome 地址栏输入 `chrome://extensions/`，启用“开发者模式”，选择“加载已解压的扩展程序”，选择已经创建好的 `chrome-ext-copy-site-title` 目录，然后就可以看到如下所示：

![copy-site-title-load-into-chrome](../../../../assets/image/copy-site-title-load-into-chrome.jpg)

如图所示，如果想要调试 background.js，则可以点击 “背景页” 按钮，然后就会出现一个 Chrome Console，你在 background.js 里面打印的东西也会输出到这里。

- 快捷键设置

![copy-site-title-shortcuts-setting](../../../../assets/image/copy-site-title-shortcuts-setting.jpg)

- 鼠标点击 context menu 复制

页面右键选择 'Copy Site Title'：

![copy-site-title-via-mouse-click](../../../../assets/image/copy-site-title-via-mouse-click.jpg)

> 说明：可以在页面进入审查模式，直接调试 content.js 的输出。

## 总结

从开始看教程和文档，到编码实现本地测试，最后提交到 GitHub，整个过程大概用了 3 个小时左右，代码加配置也就 120 行左右，时间主要花在看文档和本地测试上面了。

这篇文章用到的所有参考链接就是用的这个插件设置的快捷键直接复制出来的，从效果来说，一步到位的感觉让我的强迫症又好了一点 (:

本文最后完工的代码见：[cglai/chrome-ext-copy-site-title: Copy site title in different ways via context menus or keyboard shortcuts to to clipboard.](https://github.com/cglai/chrome-ext-copy-site-title)，可运行的环境为：macOS Mojave，Chrome 72+。

## 参考

- [Build a Chrome Extension to Streamline Your Workflow — SitePoint](https://www.sitepoint.com/build-a-chrome-extension/)
- [What are extensions? - Google Chrome](https://developer.chrome.com/extensions)
- [在线文字转图片(支持生成钢笔记,毛笔字,古印字,QQ彩虹字等)—制图网](http://ps.lrswl.com/Tool/Text2Image.html)

