---
layout: post
title: 基于本团队的开发规范
category: Profession
tags: [团队开发, 开发规范, 标准化]
---

在以前总结过的的基础上，根据现在的实际情况，重新整理一份开发规范在这里。

<!-- more -->

> 以下内容随着本团队实际开发情况可能会持续新增、修正。

## 前端

### 代码层面

#### HTML 5 优先

- 熟读 HTML5 规范 基于此规范编写标准页面。（详见：*<https://developer.mozilla.org/zh-CN/docs/Web/Guide/HTML/HTML5>*）
- doctype、lang、UTF-8、兼容 IE，参考：

```
<!DOCTYPE html>
<html lang="zh-cn">
    <head>
        <meta charset="UTF-8">    
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    </head>
    ...
</html>
```

- 如果是手机端页面则必须先统一设置好 `viewpoint` 之后才是开发

#### 正确使用常用标签及其属性

特别是表单相关所有标签。（参考文档把常用标签的的属性测试一遍，开发时多查手册）

- `a` `button` 的使用

路由改变。

#### 正确处理 CSS 和 JS

- 外部引入

虽然部署时可以压缩后放到文档里面，但开发期的 CSS 和 JS 必须强制性放到单独的文档之外，并**按功能模块分目录管理**。

此外，HTML 5 规范在引入 CSS 和 JS 时不需要指明 `type`，因为 `text/css`和 `text/javascript` 分别是他们的默认值 。

- 只引入需要的

不要不负责任的随意粘贴本页面不需要的任何 JS 和 CSS 代码。

- 避免行内样式
- 合理使用 `!important` 声明（会打乱样式优先级 增加调试难度）。

> 调试时使用 其他能不用则不用

#### 属性命名

- 优先使用 class 属性为标签命名

class 是为高可复用组件设计的，所以应处在第一位。而 id 更加具体且应该尽量少使用，所以将它放在第二位。

- **不要再出现同 ID 标签**
- 属性名全小写，用中划线做分隔符，比如：`class="a-b-c"`。

#### 减少页面不必要的标签数

能用一个搞定的，不要胡乱堆切标签。

#### 正确使用图片

- 不要滥用图片
- 引用合适大小的图片

> 比如，一个微信 logo 小图标 大概 30x30 足以，可我看到过你们引入的大概是 500x500。

- 使用字符图标

如果对图标没有特殊要求，网页采用字符图标对浏览器兼容较佳，也能减少设计／开发时间和提高网页加载速度。

- 学会使用 CSS Sprite 减少页面加载小图片的次数

#### 搞懂「语义化」

编写 SEO 友好的页面。

#### 编写模块化、可复用的组件

这其实是前端的工作目标。

### 流程层面

#### 关联设计图上的页面跳转

设计图上展示的，页面之间的切换，前端页面写好后要自行关联起来。

#### JS JS JS

- 不能让后台维护你们粘贴而来的 JS 代码


- 不能让后台为你们写过多 JS

>  现如今 JS 是前端的看家本领，只会 HTML+CSS 的只能叫 “美工”。

#### 前后端分离

要实现前后端分离，前端掌握 JS 是先决条件。

> 程序员之所以叫做「程序」员，是因为他们会「编程」。而 HTML 和 CSS 只是标记语言，并不是编程语言。
>
> 所以，如果你们真的要当一个程序员，那么请让自己学会一门编程语言。
>
> 而对于 Web 前端而言，现如今最合适，性价比最高的编程语言就是 JS。

#### 维护自己的前端仓库和分支

当实现前后端分离后，页面上的所有事情将由前端完成，这时候前端的仓库和后端的框架、仓库便不再耦合在一起。

此时，需要你们自己维护好自己的前端仓库和分支。

### 结论

- http://www.runoob.com
- https://developer.mozilla.org

就规范而言其实没什么好总结的，多查手册，按照标准来就是了。

> 或许你们会埋怨让你们学的东西有点多（目前看来），但是当你们真的做到了上面这些，那么请和你的老板商量涨工资的事情吧，因为此时的你已经无形中升「值」了。

## 后端

### 代码层面

#### PSR

还在问 PHP 规范？out 了！请 Google 「PHP PSR」后 bookmark。

##### PHP Code Sniffer

不过，如果有工具在你写代码的同时就帮你纠正是否符合规范就好了。

因此，本团队将采用 PHP Code Sniffer 来提醒开发中编码不规范的地方。安装使用说明：

- Sublime text

``` shell
# 先安装 php-code-sniffer 和 php-cs-fixer
brew search php-code-sniffer
brew install homebrew/php/php-code-sniffer
brew search php-cs-fixer
brew install homebrew/php/php-cs-fixer

# 获得 phpcs 可执行文件路径
which phpcs
which php-cs-fixer
```

安装好后 phpcs 和 php-cs-fixer 后，打开 Sublime PackageControl，搜索 *Phpcs* 并安装，然后设置插件，参考配置：

```
// ...
"phpcs_execute_on_save": true,
"phpcs_show_errors_on_save": true,
"phpcs_show_gutter_marks": true,
"phpcs_outline_for_errors": true,
"phpcs_show_errors_in_status": true,
"phpcs_show_quick_panel": false,
"phpcs_php_prefix_path": "/usr/bin/php",

"phpcs_sniffer_run": true,
"phpcs_command_on_save": true,
"phpcs_executable_path": "/usr/local/bin/phpcs",
"phpcs_additional_args": {
	"--standard": "PSR2",
	"-n": ""
},
"php_cs_fixer_on_save": false,
"php_cs_fixer_show_quick_panel": false,
"php_cs_fixer_executable_path": "/usr/local/bin/php-cs-fixer",

"php_cs_fixer_executable_path": "/usr/local/bin/php-cs-fixer",
"phpcs_linter_command_on_save": false,
"phpcs_php_path": "/usr/local/bin/phpcs",
// ...
```

- PhpStorm

参考：<https://confluence.jetbrains.com/display/PhpStorm/PHP+Code+Sniffer+in+PhpStorm> 即可。

需要说明的一点是，对 Windows 来说，下载下来的 *phpcs.bat* 不是直接就可以用的，需要将里面的 `@XXXX@` 改为实际系统上的可执行路径才行。

#### 分模块管理

laravel 中的路由、控制器、模型、视图文件、API，必须按照系统分好类的子模块名分目录管理。

#### 限定函数规模

- **单一原则**：每个函数应该，也只能完成一件事情。


- **代码行数**：每个函数的代码有效行数必须控制在 80 行以内，50 行是标准，适当换行。
- **代码列数**：每行不超过
- **合理注释**：注意「注释」和「翻译」的差别。注释，是简单描述程序流程、标注容易出错或容易引起混淆的代码，而给变量 `$origin_path` 注释为 `// 原始路径` 我认为不叫注释。

#### 面向接口编程

不关心依赖的具体实现，只关心需要什么数据，实现低耦合。

#### MVC

遵守 MVC 基本原则：对数据库的操作全部封装到模型当中，控制器通过调用模型来操作数据库，把调用结果传递到视图模版渲染为 HTML。

##### Model

- 所有对数据库的操作都封装到模型／M 层
- 控制器调用模型提供的方法，这样修改数据库的时候不用改控制器，只需要改模型方法
- 含有具体业务逻辑的数据库操作通过在控制器传递参数调用模型封装好的基本方法

##### Controller

控制器方法的返回值统一使用 `return` 返回，`echo`/`dd()` 等仅供调试使用。控制器方法主要返回：

- 视图
- JSON 接口数据
- 内部调用结果

#### 路由

##### 模块化管理路由注意事项

* 以「模块名（控制器组名）」 为 namespace
* 每个 namespace 下可能出现的控制器种类有 4 种:
  * 前台: 存放和本系统本业务逻辑相关的前台逻辑
  * 后台: 存放和本系统本业务逻辑相关的后台逻辑
  * API: 存放和本系统本业务逻辑相关的, 单独封装的 APIs, 暴露给向三方服务调用
  * Patch: 存放解决本业务逻辑测试中出现的问题的临时补丁（需求中没有与之对应的视图文件，通常由开发者本身编写简单页面调用，或者直接在终端执行）


* 路由只能关联控制器, 不能直接操作工具类、模型类

#### 封装工具类

常用的功能，代码段，花点时间封装成类、方法，避免复制粘贴类似代码（DRY），利人利己。

#### 不重复造轮子

已有的，已经能解决实际问题的现成组件、功能、类、trait、方法等等，在实际项目开发中，不到万不得已不要重复写。

把时间用在提高自定义应用开发速度，应用整体水平，和完成手中更高的目标上面。

> “ It’s silly not to take advantage of these components to build better applications more quickly instead of wasting time reinventing the wheel.”
>
> “Instead of rebuilding already-solved functionality, we use PHP components and spend more time solving our project’s larger objectives.”
>
> ( *Josh Lockhart. “Modern PHP”* )

#### 编写优雅易读的代码

一看路由命名就知道你要干嘛？

起一个好的函数名、变量名发现没必要写翻译性注释了？

合理分段后，发现不用再为了看一条完整语句到底有多长而左右拉动窗口了？一眼就看到全部？

你的代码怎么可以写的这么对称？

> 写代码的时候，要把自己当作一个作者，想象你在创作，因此，你要为你的读者负责，让他们看得懂，看的爽。

#### 防御性编程

> *“Never trust any data that originates from a source not under your direct control”*

##### 不可控的来源（*Modern PHP*）

- `$_GET`
- `$_POST`
- `$_REQUEST`
- `$_COOKIE`
- `$agrv`
- `php://stdin`
- `php://input`
- `file_get_contents()`
- 远程数据库
- 远程 API
- 客户端传来的数据

##### 过滤输入

PHP 从不可控的来源接收到的数据，在进入数据层之前，必须转义、移除不安全的字符串。

###### SQL 查询

不要在 SQL 查询中使用到未经过滤的输入数据。如果目标的 SQL 查询需要带上不可靠的输入参数，那么必须使用「预处理」语句。

> PDO 或者 mysqli 都可以，只是 mysqli 只作用于 MySQL DBS。

###### 用户属性

即是指用户邮箱、电话号码、邮编等和用户信息有关的属性。对这些过滤，请使用 PHP 内置的过滤器 `filter_var()` 和 `filter_input()` 来过滤。	详见 php.net。

###### HTML／输出转义

这个刚好相反，**对于 HTML 类型的输入数据，一般是「原样入库，输出转义」**。

```php
// 如何正确使用 `htmlentities()` 示例：
$input = '<p><script>locatiopn.href="http://evil.com"</script></p>';

echo htmlentities($input, ENT_QUOTES, 'UTF-8');
```

注意，不要使用正则表达式来过滤 HTML。原因：正则过滤 HTML 会让事情复杂化，且过滤失败的代价比较高。

> suggestions: `strip_tags()`、`nl2br()`。

##### 数据校验

校验数据和过滤输入的不同之处在于，校验数据不会从输入的数据中移除掉什么，而只是确保输入的数据和你预期的一致。

比如，过滤输入的 email，完成的是剔除接收到的 email 中可能存在的非法字符，强调的是「安全」；而数据校验 email，则是检查这个过滤过的输入「内容」还是不是 email，强调的是「正确」。

**在数据校验之前，通常已经经过过滤输入这一操作了**。举例说明过滤和校验的差别：

``` php
// 输入
$email = 'john@example.com';

// 过滤
$email_safe = filter_var($email, FILTER_SANITIZE_EMAIL);

// 校验
$is_email = filter_var($email_safe, FILTER_VALIDATE_EMAIL);

echo ($is_email !== false) ? 'success' : 'failed';
```

数据校验只是为了进一步确保到最终到数据层的数据格式是准确的，避免数据操作时出错（如：数据类型不匹配）。

> PHP 内置的过滤器并不能校验所有数据，可以在 packgist.org 上搜索 `aura/filter`，`respect/validation`，`symfony/validator` 以完成更多的校验需求。

##### 用户密码

###### 不能知道用户密码

这意味着：

- 不能以明文形式存储用户密码
- 不能用可双向算法编码用户密码

###### 不要限制最大长度

限制了最大长度，这相当于在指导 Cracker 如何破解你的密码。

###### 不能以任何形式发送明文密码给用户

如果这样做了，相当于在告诉用户：

- 我知道你的密码
- 我要么是明文保存你的密码，要么并没有使用安全的单向加密算法加密你的密码
- 我不关心保护你的密码

###### 使用带时效性 Token 的 URL 让用户重置密码

帮助用户找回密码的正确姿势是：

- 生成一个带时效性的、唯一的、和用户 ID 相关的 Token 的 URL，发送到用户邮箱或手机
- 用户点击这个 URL 重新填写新密码，点击确定重置密码
- 后台接受并校验这个 Token，如果未过期且匹配，则删除本次 Token；如果已过期，则也删除此 Token

###### hash 而不是 encrypt 用户密码

encrypt 是双向的，hash 是单向的。hash 算法根据用途可分为 2 种：

- 快速、为校验数据完整性的 hash：md5、sha1
- 慢、安全的 hash：bcrypt、scrypt

而用户密码需要的正是慢但安全的 hash。

> *“The bcrypt algorithm also consumes a large amount of time (measured in seconds) while iteratively hashing data to generate a super-secure final hash value. The number of hash iterations is called the work factor. A higher work factor makes it exponentially more expensive for a bad guy to crack password hashes. The bcrypt algorithm is future-proof, too, because you can simply increase its work factor as computers become faster.”*

对 PHP > 5.5.0 来说，常用的密码 Hash API 是 `password_hash()` 和 `password_verify()`。

> PHP < 5.5.0 时，可以使用 ircmaxell/password-compat 组件来代替。

两者正确匹配使用举例如下：

``` php
// pull plain text password from http request body
$password = filter_input(INPUT_POST, 'password');

# Create & Store
// create an bcrypt-based hash
$password_bcrypt = password_hash(
  	$password,
  	PASSWORD_DEFAULT,    // tell PHP which hash algorithm to use here, defualt, bcrypt here
	['cost' => 12]    // hash options, `cost` specifies the bcrypt work factor, 10 default, can be increased due to particular computer hardware, but ensure the process time cost in 0.1s to 0.5s.
);

// check if hash successed
if ($password_bcrypt === false) {
  	throw new Exception('Password hash failed');
} else {
  	// save or something ...
}

# Retrieve & Verify
$email = filter_input(INPUT_POST, 'email');
$user = User::findByEmail($email);

if ($user) {
  if (password_verify($password, $user->password===false)) {
  	throw new Exception('Invalid password');
  }
  
  // Re-hash password if necessary
  // This is to make sure the user record’s password hash value is up-to-date with the most current password algorithm options
  $needs_rehash = password_need_rehash(
    $user->password,
    PASSWORD_DEFAULT,
    ['cost' => 15]
  );
  
  if ($needs_rehash === true) {
    // save the new one
    $user->password = password_hash(
      $user->password,
      PASSWORD_DEFAULT,
      ['cost' => 15]
    );
    $user->update();
  }
}
```

> use `varchar(255)` field to store the password hash to be more flexible if hash res grows in future.

##### 错误和异常

在使用了现代框架的情况下，一般很少再去亲自写异常类，比如 Laravel 使用了 _[Whoops](https://github.com/filp/whoops)_ 来处理所有可能发生的错误和未经捕获的异常，并返回需要的格式。

这基本上已经足够了，但是仍然必须对特有业务代码中可能发生的错误和异常进行捕捉，返回更准确、更友好的信息。

其次，开发环境需要显示所有详尽的错误，而线上却不要显示详细的错误细节，两种环境都需要记录好错误日志。不过，框架已经为我们准备好这些事情了，我们只需要改改配置就可以。

##### 适度的防御性编程

前台和后台相交的地方，有必要防御性编程，把可能的情况都考虑进去，编写鲁棒的方法。

后端自己调用自己的方法，却不必要把时间浪费在极端情况，除非你对自己在上游方法做的防御不自信。

#### 日期、时间、时区

**不要手动计算。**原因只有一个，考虑的因素太多，太容易出错。

> *"There are too many considerations to juggle, including date formats, time zones, daylight saving, leap years, leap seconds, and months with variable numbers of days. It’s too easy for your own calculations to become inaccurate."*

使用 `DateTime`，`DateInterval`, `DatePeriod` ，`DateTimeZone` API（PHP >= 5.2.0），和现有的 *nesbot/carbon* 组件来处理，内置时间类的使用详见 php.net。

###### 设置默认时区

> 时区标志符：http://php.net/manual/timezones.php

- php.ini：`date.timezone = 'Asia/Shanghai';`
- runtime： `date_default_timezone_set('Asia/Shanghai');`

#### 多字节字符串

##### 字符编码

use UTF-8 without BOM。具体的实践如下：

- php.ini：`default_charset = "UTF-8";`

这个配置默认作用的地方有（部分函数支持改变字符编码）：

``` php
// htmlentities()
// html_entity_decode()
// htmlspecialchars()
// mb_string functions
// header()
```

- 必须清楚你正在操作数据的字符编码
- 存数据时使用 UTF-8
- 取／显示数据的时候使用 UTF-8

PHP 设计很糟糕（因为很容易让程序员出错）的地方之一，就是它默认所有的字符串都是 1 字节长度的，这意味着当你使用 PHP 原生的字符串处理函数来计算非英文字符串的时候，结果就不正确了。

``` php
echo ini_get('default_charset');    // UTF-8

$test_str = 'Iñtërnâtiônàlizætiøn';

// use php default string-manipulation functions
echo strlen($test_str);    // 27 => incorrect

// use mb_string extension
echo mb_strlen($test_str);    // 20 => correct
```

因此，为了减少潜在的危险，杜绝使用 PHP 原生字符串函数，而是用 mb_string 扩展提供的 API 来处理字符串。

此外，mb_string 扩展不仅仅能够处理 Unicode 字符串，它也能将多字节字符串在不同的字符编码之间切换。

> suggestion：`mb_detect_encoding()`，`mb_convert_encoding()`。

#### 流程图

- 逻辑简单的

先写好注释、步骤，再写代码。

- 逻辑复杂的

先画好流程图，明确你要做什么，怎么做后再敲代码。

### 测试层面

#### 单元测试

从微观的角度，把组成一个完整系统的最小组成部分，即：类、方法和函数，从整个系统中孤立出来，进行单独测试。

简而言之，就是一个方法一个方法的测试，一个类一个类的测试。

#### 黑盒测试

或称：功能测试、数据驱动测试或基于规格说明的测试。

单元测试和白盒测试结束之后，并不能保证整个系统在上层行为，即功能上表现一致，因此还需要宏观地测试功能是否可用。

> - 白盒测试
>
>
> - TDD
> - BDD => SpecBDD/StoryBDD

### 数据库层面

#### 熟悉需求是基础

数据是项目的核心，数据库作为数据的载体也是项目的核心。

不了解业务需求，是不可能设计好数据库的。

#### 关系分析清楚了吗

既然我们用的是「关系型」数据库，那么系统中角色之间的关系是否分析透彻也是决定数据库设计好坏的基本因素。

关系无非几种：一对一、一对多、多对多。

#### 三范式与反范式化设计

三范式是基础，反范式化是变通。

#### 熟练使用 SQL

SQL 基本语法、数据类型的选择、如何建索引、如何使用子查询、常用关键字和函数等等。

#### PHP 操作数据库

优先使用 PDO，原因只有一个：共用一套 API 操作各种数据库，或者说，通过 PDO 操作各种数据库，在大部分情况下，只需要写一次代码。

当然，由于各种数据库毕竟有自己的专有功能，所以，就算公用一套 API，如果必须使用某个数据库系统特有功能，相关的 SQL 仍然需要自己写。

总之，最佳实践是：使用 PDO 编写符合 ANSI/ISO 规范的 SQL。

### 流程层面

#### 和前端的对接

- 前后端分离前

如果前端叫过来的页面没有分模块管理，我们则必须根据需求、设计图按模块管理。

时间充裕的时候，我们需要多采用布局来简化代码。

- 前后端分离后

只提供接口数据即可。

#### 分支管理

- 开发在自己的分支
- 推送前拉取 dev 分支并进行合并，如果冲突则出现冲突的两方必须协商解决，切不我认为我是对的然后贸然删代码。
- 不要对 git 畏惧

你们对 git 的害怕，才是让我害怕的事情，这说明你们没有真正搞懂 git 的工作流程。

### 结论

上面只是一些「点」，技术的细节需要自己查手册、查文档了解、熟悉。

> 反正我建议所有后端都往「全栈」发展。
>
> 写后台的不一定只写后台，尽量多面手，尽量向全栈发展（减少沟通成本）。

## 参考

- *[PHP_CodeSniffer](http://pear.php.net/manual/en/package.php.php-codesniffer.intro.php)*

