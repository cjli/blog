---
layout: post
title: Start Blogging with Hexo Plus NexT
category: 折腾
tags: [Hexo, NexT, 博客]
---

很早就看到过很多人采用 [Hexo](https://hexo.io) 在 GitHub 上搭建博客了，其中配合 [NexT 主题](https://github.com/iissnan/hexo-theme-next) 更是简约美观，非常和我胃口。

<!-- more -->

由于我在 GitHub 上的 Jekyll 博客用得还算不错，到现在也已经有了百来篇文章了，实际上连续使用了半年左右的时间。

不过不足之处就是主题不够漂亮，在手机端显示也不够友好。奈何本人前端功力不深，重改够呛，也浪费时间，还是那句话，写博客最重要的还是内容为王，没有必要把主要精力单纯地放在博客的折腾上面。

今天终于花了点时间把一个简单的 Hexo + NexT 博客套装给配置好了，个人感觉还不错。所以老规矩，把步骤总结出来，给自己和需要的人节约时间。

###### *说明*

本人在 GitHub 的 [Jekyll 博客](http://cglai.github.io) 主要用于桌面端浏览，<del>而在 GitCafe 上的 [Hexo 博客](http://cjli.gitcafe.io) 主要用于手机端浏览，两者都会同步更新。</del>

( 2016年1月30日左右，Coding.NET 也开通了 Pages 服务，所以本人又在 [Coding.NET](https://coding.net/u/cjli) 备份了 GitCafe 上的 Hexo 博客，其地址是: <http://cjli.coding.me> 。 )

## 一、准备工作

下载安装 Git 和 Node.js 环境，没什么好说的去官网下就行了，安装步骤和 Windows/Linux 下普通软件包的安装没什么不同，详细过程略。

此外，最好把它们的主程序文件所在路径添加到环境变量。

## 二、本地测试

``` bash
# 下载 Hexo
npm install -g hexo
# 创建 Hexo 博客目录
mkdir hexo
# 进入 Hexo 博客目录并初始化 Hexo 博客站点
cd hexo
hexo init
# 安装依赖
npm install
# 生成静态博客文件
hexo generate
# 运行 Hexo 服务
hexo server
```

此时，在浏览器中输入 localhost:4000 ( 和 Jekyll 一样的默认端口 ) 就可以看到一个默认的 Hexo 博客了。

## 三、部署到远程服务器

如托管在 GitCafe／Coding.NET 提供的 pages 服务上。

### 1、配置部署参数

在  hexo/_config.yml 中找到 Deployment，然后添加如下内容：

```
deploy:
  type: git    # 最新版本的 Hexo 中，这里的 type 要写成 `git`，而不是 github 或 gitcafe
  repository: git@gitcafe.com:cjli/cjli.git
  branch: gitcafe-pages    # 我是把 Hexo 必要的源代码保存在 master 分支, 把部署的博客文件放到 gitcafe-pages 分支
  #repository: git@git.coding.net:cjli/cjli.git
  #branch: coding-pages
```

其中，仓库地址需要灵活修改，仓库分支固定是 gitcafe-pages，可见 GitCafe 和 GitHub 也是一样都提供免费的 Pages 服务。

###### *说明*

**这里为了防止后面在部署博客的时候重复输入用户名和密码，这里必须填写 SSH 协议而不是 HTTPS 的仓库地址，而且事先必须为部署的 Git 远程仓库服务配置好 SSH 密钥。**

### 2、部署到 GitCafe

首先，在 GitCafe 注册账户并创建项目。需要注意的是，**项目名和用户名要一致**，并且要创建公开项目而不是私有项目。

然后运行：

``` bash
hexo generate
# 安装 hexo-deployer-git
npm install hexo-deployer-git --save
# 开始部署到远程服务器
hexo deploy
```

如果没有配置过 SSH 这里可能提示需要输入 GitCafe 的用户名和密码。

###### *说明*

每次修改本地文件后，都需要运行 `hexo generate` 才能使最新改动生效，每次执行命令时，都要在站点根目录下，即是站点配置文件 ___config.yml__ 所在的目录 ( *不是主题配置文件* )。

## 四、发布文章

由于 GitCafe 上面没有 Hexo 运行环境，所以不能像在 GitHub 上的 Jekyll 博客那样直接使用 Git 发布文章。

所以在本地写完博客后，需要通过上面那种部署的方式将最新的文章同步到 GitCafe，即执行：`hexo d`。

其实，如果你查看博客站点下的 `.deploy_git` 文件夹里面的内容就知道，部署的时候也使用的是 Git，所以部署并不会每次都把博客文件全部提交，而是提交博客的改动。

但是，在执行部署前，仍然需要使用 Git 提交到远程仓库分支一次，然后再执行部署才能使得内容改动同步到 GitCafe 的项目展示主页。即需要进行类似如下步骤：

``` bash
git add -A    # 这里可根据自己情况添加文件
git commit -m "adding"    # 这里的注释灵活填写
git push alias master    # 这里的远程仓库别名和分支根据自己情况填写
hexo clean    # 为了防止部署上次的博客内容建议先清除再重新生成
hexo g
hexo d
```

可以看见，和 Jekyll 的发布相比只是最后多了一步部署工作，前面都是一样的。

## 五、迁移 Jekyll 博客文章到 Hexo

把 `_posts` 文件夹内的所有文件复制到 `source/_posts` 文件夹，并在 `_config.yml` 中修改 new_post_name 参数为：

```
new_post_name: :year-:month-:day-:title.md
```

不过这样子还是会出一些问题，因为 Jekyll 中 Liquid 语法和 Hexo 中的语法不完全兼容，所以会遇到一些报错。

## 六、配置 NexT 主题

首先当然也是先下载 NexT 主题源码到 Hexo 博客的 themes 文件夹下面，即 themes/next/，Hexo 博客的所有主题都放到 themes 文件夹下。

``` bash
git clone https://github.com/iissnan/hexo-theme-next.git
```

然后，对 Hexo 的配置主要就是配置根目录下的 _config.yml 文件，称作 **站点配置文件** 。

而对 NexT 以及其他 Hexo 主题的配置主要就是配置主题所在文件夹下的 _config.yml 文件，称作 **主题配置文件**。

### 网站图标

在 themes/next/source 文件夹下放一个你事先准备好的 favicon.ico 就行了。

### 侧边栏头像

在 themes/next/source/images 文件夹下放一个你事先准备好的 default_avatar.jpg 就行了。

此外，还可以放置 avatar.jpg，然后通过 /uploads/avatar.jpg 来引用。

或者在 themes/next/source/images 文件夹下新建一个 uploads 目录，里面存放 avatar.jpg，然后通过 /uploads/avatar.jpg 来引用。

编辑站点配置文件，新增字段 `avatar`，其值设置将成为头像的链接地址。

如果想要头像显示圆角效果，需要找到 layout/_macro/sidebar.swig，然后搜索 `site-author-image`，在这个 `<img>` 标签中加上 `style="border-radius:100%;"` 即可。

### 简体中文

Hexo 博客搭建好后默认是繁体，可以通过配置站点配置文件中的 `language` 为 `zh-Hans` 即可。

### 提示语

在 themes/next/languages 文件夹下找到你正在使用的语言类型对应的文件，比如 zh-Hans.yml，然后修改里面的相应的提示语即可。

不过需要注意的是，某些提示语好像不能修改，否则会报错。

### 侧边栏社交链接

编辑 站点配置文件，新增字段 social，然后添加社交站点名称与地址即可。例如：

```
# Social links
social:
  GitHub: https://github.com/cjli
  Google+: https://plus.google.com/xxxx
```

其中社交网站上的图标在 主题配置文件中配置，如果 NexT 主题中有的话就会正常显示社交网站的字符图标。

不过 NexT 采用的字符图标也是 Font-Awesome 的，所以 Font-Awesome 中存在的字符图标都可以配置到 NexT 的主题配置文件，然后再到站点配置文件中引用这个字符图标即可。

注意两个配置文件中的名字要一致。

### 分页和摘要

NexT 主题已经为我们准备好了，只需要我们配置一下就行。

分页只需在站点配置文件中设置分页的规模和路径：

```
# Pagination
## Set per_page to 0 to disable pagination
per_page: 10
pagination_dir: page
```

摘要只需在 NexT 主题配置文件中启用摘要和配置摘要的字数：

```
# Automatically Excerpt
auto_excerpt:
  enable: true
  length: 150
```

此外，除了自动截取指定字数的文字作为文章摘要，还有种方式就是在写文章中使用 `<!-- more -->` 标签，代表的意思就是其前面的完整内容将作为摘要。

这样比自动截取摘要有个好处就是不会出现 `...` 。

##### *注意*

如果采用的是自动截取摘要，则尽量不要使用代码段作为文章开头，否则博客首页将比较难看，手机端效果也很差。

### 标签云页面

#### 1、新建一个页面

```
# 命名为 tags
hexo new page "tags"
```

#### 2、设置页面头信息

编辑刚新建的页面，将页面的类型设置为 tags ，主题将自动为这个页面显示标签云。页面内容如下：

```
title: TagCloud
date: 2016-01-24 11:44:35
type: "tags"
---
```

##### *注意*

如果有启用多说 或者 Disqus 评论，默认页面也会带有评论。需要关闭的话，请添加字段 comments 并将值设置为 false，如：

```
title: TagCloud
date: 2016-01-24 11:44:35
type: "tags"
comments: false
---
```

#### 3、在菜单中添加链接

#### 编辑 主题配置文件，添加 tags 到 menu 中，如下:

```
menu:
  home: /
  archives: /archives
  tags: /tags
```

### 分类页面

步骤类似标签云页面，在上面的步骤中只需将 `tags` 换为 `categories` 即可。

### About 页面

新建一个 about 页面：`hexo new page "about"`。

然后在 主题配置文件 设置中将 menu 中 about 前面的注释去掉即可。

```
menu:
  home: /
  archives: /archives
  tags: /tags
  about: /about
```

更多配置详见 NexT 官网文档：<http://theme-next.iissnan.com/theme-settings.html>，以及作者 GitHub 项目中的 issues。

## FAQ

### Google 字体等导致的站点加载慢问题

和 Wordpress 差不多，Hexo 中使用的主题很多默认都加载了 Google 的字体和部分代码，导致整个站点加载很慢，可以找到相关代码并替换来源或者直接注释就行了。

比如 Hexo 默认主题 __landscape__ 中，找到 hexo/themes/landscape/layout/_partial 目录下的两个文件：

- head.ejs

``` html
<link href="//fonts.googleapis.com/css?family=Source+Code+Pro" rel="stylesheet" type="text/css">
```

这个我是直接注释掉的。

- after-footer.ejs

``` js
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
```

这个我是把一份 jQuery 的源文件放到：hexo/themes/landscape/source/js 目录下面，然后引入就行了：

``` js
<script src="/js/jquery-2.1.4.min.js"></script>
```

### TypeError: Cannot read property 'compile' of undefined

当执行 `hexo s` 的时候出现下述报错，

> ERROR Process failed: layout/_scripts/bootstrap/.gitkeep
> TypeError: Cannot read property 'compile' of undefined

这种情况会在在其他电脑上直接复制你以前电脑上的主题文件的时候发生。

可以对比 NexT 主题在 GitHub 上的源码可以找出，默认的 NexT 项目中 layout/_scripts/bootstrap/.gitkeep 没有的，于是猜测是在使用 NexT 后才生成的，这里换了环境，所以出现这种报错。

于是解决办法就是删除 layout/_scripts/**bootstrap/.gitkeep** 这个文件夹和文件即可。

在搜索的过程中也发现，Windows 上由于 .DS_Store 文件夹的存在也会产生一些报错，remove 掉就好了。

### NexT 主题中的 Google 字体

我还是喜欢 NexT 主题，真心赞。从 GitHub 上拷贝的 NexT 主题文件中也有一处使用了 Google 的字体。

在 hexo/themes/next/layout/_partials 目录下面的 head.swig 文件中，找到

``` html
{% if theme.use_font_lato %}
	  <!-- <link href="//fonts.googleapis.com/css?family=Lato:300,400,700,400italic&subset=latin,latin-ext" rel="stylesheet" type="text/css"> -->
{% endif %}
```

我也是直接注释掉这段的。

如果还是发现加载慢的问题，是我就会使用 Chrome 开发者工具查看是哪个资源加载出了问题，然后用 Sublime Text 中的在文件中搜索的功能找到该文件，然后要么替换要么注释即可。

#### Next 5+ 更新

可以直接在主题配置文件中配置字体服务器，至于 Google 字体的镜像，Google 一下就有：

```
# ...
font:
  enable: true
  # Uri of fonts host. E.g. //fonts.googleapis.com (Default)
  host: //fonts.fengqi.me
# ...
```

### NexT 主题风格与移动端适配问题

NexT 主题有 3 种 Schemes，分别是：Muse、Mist、Pisces，其中 Muse 是安装好 NexT 后默认的风格。

有一个问题就是，当我使用 Muse 和 Mist 主题的时候，移动端适配效果奇差，只有 Pisces 风格在移动端真正体现出响应式设计。

这个问题的原因我目前还没有找到，如果有知情的愿意告诉我的，还请告知一下哈。

### Git Push 时重复输入用户名密码的问题

如果 Git 中配置了 GitHub 的全局账户密码，那么在使用 Git 管理 GitCafe 上的仓库时就会重复提示输入用户名密码，此时可以通过配置 SSH 免去输入账户密码的步骤。

此外，还有一种方式。以 Windows 举例说明：

#### 1、新建用户环境变量

- 变量名：`HOME`

- 变量值：`%USERPROFILE%`

#### 2、创建配置文件

在用户个人文件夹下面，新建 `_netrc` 文件，编辑内容如下：

```
machine gitcafe.com
login cjli 
password 123456
```

亲测这种方式有效，在 CLI 下无论使用 Git 还是 hexo-deployer-git 登录 GitCafe 都没有再提示输入用户名和密码。

不过这种方式以明文保存密码，不安全，还是建议按照这里 (  [Git 命令无需记忆](http://cglai.github.io/cheatsheet/Git-Brief.html)  ) 的步骤为多个 Git 远程仓库配置 SSH-Key。

### `coding-pages` 静态资源 404

Jekyll 导致的。在 .deploy_git 部署根路径下创建一个 **.nojekyll** *文件*即可。

### fs.SyncWriteStream is deprecated [Nodejs v8.0]

``` shell
npm install hexo-fs --save
```

see: <https://github.com/hexojs/hexo/issues/2598>

### the module "xxx" was compiled against a different Node.js version using

``` shell
npm rebuild
```

## 附录：Hexo 常用命令

``` bash
hexo chean    # 删除 public 目录
hexo g <=> hexo generate
hexo d <=> hexo deploy
hexo s <=> hexo server
hexo server -p 4444    # 更改 Hexo 服务器端口
hexo server -i 192.168.1.1    # 更改 Hexo 服务器 IP
hexo n <=> hexo new
```

## 参考

- _<https://hexo.io/zh-cn/docs>_

- *[动动手指，NexT主题与Hexo更搭哦（基础篇）](http://www.arao.me/2015/hexo-next-theme-optimize-base)*

- _[hexo系列教程：（二）搭建hexo博客](http://zipperary.com/2013/05/28/hexo-guide-2)_

- _Hexo 博客主题 NexT 的作者博客：<http://notes.iissnan.com>_
