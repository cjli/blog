---
layout: post
title: 和「并发」有关的笔记
category: Programming
tags: [并发, 并行, 性能]
---

从概念、开发实践、测试三个方面总结下和「并发」相关的东西。

<!-- more -->

## 基本概念

### [并行/Parallel](https://zh.wikipedia.org/wiki/%E5%B9%B6%E8%A1%8C%E8%AE%A1%E7%AE%97): 同时执行

> 并行计算（英语：parallel computing）一般是指许多指令得以同时进行的计算模式。在同时进行的前提下，可以将计算的过程分解成小部分，之后以并发方式来加以解决[1]。

当我们说 “并行编程” 时，指的是程序使用并行的硬件架构，使计算任务得以 **更快速度** 的被执行。

因此，对于「并行」来说，提高效率，缩短计算时间是其核心关注点。

在进行并行编程时，要求我们考虑以下两点：

- 从代码层面如何将一个复杂、庞大的问题细分为一个个更独立的子问题。

- 哪种并行硬件架构更适合处理这些问题？

#### 现行并行硬件架构

##### [多核 CPU](https://zh.wikipedia.org/wiki/%E5%A4%9A%E6%A0%B8%E5%BF%83%E8%99%95%E7%90%86%E5%99%A8)

> 多核处理器（英语：Multi-core processor），又称多核微处理器，是在单个计算组件中，加入两个或以上的独立实体中央处理单元（简称核心，英语：Core），只有两个核心的处理器，称为双核处理器（dual-core processor）。这些核心可以分别独立地运行程序指令，利用并行计算的能力加快程序的运行速度。“多核心”通常是对于中央处理器（Central Processing Unit，CPU）而论的，但是某些时候也指数字信号处理器（DSP）和系统芯片（SoC）。

##### [对称多处理](https://zh.wikipedia.org/wiki/%E5%AF%B9%E7%A7%B0%E5%A4%9A%E5%A4%84%E7%90%86)

> 对称多处理（英语：Symmetric multiprocessing，缩写为 SMP），也译为均衡多处理、对称性多重处理，是一种多处理器的计算机硬件架构，在对称多处理架构下，每个处理器的地位都是平等的，对资源的使用权限相同。现代多数的多处理器系统，都采用对称多处理架构，也被称为对称多处理系统（Symmetric multiprocessing system）。在这个系统中，拥有超过一个以上的处理器，这些处理器都连接到同一个共享的主存上，并由单一操作系统来控制。在多核心处理器的例子中，对称多处理架构，将每一个核心都当成是独立的处理器。

> 在计算领域，对称多处理是一种多处理机硬件架构，有两个或更多的相同的处理机（处理器）共享同一主存，由一个操作系统控制。当前最常见的多处理机系统使用了对称多处理架构。以多核处理器为例，对称多处理架构就是这些核，它把这些核当作不同的处理器。不同的处理器之间可以由总线、矩阵开关或片上mesh网络来连接。使用总线或矩阵开关的对称多处理架构有可扩展性方面的瓶颈，它是由处理器之间连接的带宽、能耗，以及内存和磁盘阵列等引起的。使用mesh连接的架构避免了这些瓶颈。它能够支持更多数量的处理器，具有几乎线性的可扩展性，代价是牺牲可编程性。

在对称多处理系统上，在操作系统的支持下，无论进程是处于用户空间，或是核心空间，都可以分配到任何一个处理器上运行。因此，进程可以在不同的处理器间移动，达到负载平衡，使系统的效率提升。

##### [GPU](https://zh.wikipedia.org/wiki/%E5%9C%96%E5%BD%A2%E8%99%95%E7%90%86%E5%99%A8)

> GPU不同于传统的CPU，如Intel i5或i7处理器，其内核数量较少，专为通用计算而设计。 相反，GPU是一种特殊类型的处理器，具有数百或数千个内核，经过优化，可并行运行大量计算。 虽然GPU在游戏中以3D渲染而闻名，但它们对运行分析、深度学习和机器学习算法尤其有用。 GPU允许某些计算比传统CPU上运行相同的计算速度快10倍至100倍。

##### [FPGA - 现场可编程逻辑门阵列](https://zh.wikipedia.org/wiki/%E7%8E%B0%E5%9C%BA%E5%8F%AF%E7%BC%96%E7%A8%8B%E9%80%BB%E8%BE%91%E9%97%A8%E9%98%B5%E5%88%97)

> 现场可编程逻辑闸阵列（英语：Field Programmable Gate Array，缩写为FPGA），它是在PAL、GAL、CPLD等可编程逻辑器件的基础上进一步发展的产物。它是作为专用集成电路领域中的一种半定制电路而出现的，既解决了全定制电路的不足，又克服了原有可编程逻辑器件门电路数有限的缺点。

##### [计算机集群](https://zh.wikipedia.org/wiki/%E8%AE%A1%E7%AE%97%E6%9C%BA%E9%9B%86%E7%BE%A4)

> 计算机集群简称集群是一种计算机系统，它通过一组松散集成的计算机软件和/或硬件连接起来高度紧密地协作完成计算工作。在某种意义上，他们可以被看作是一台计算机。集群系统中的单个计算机通常称为节点，通常通过局域网连接，但也有其它的可能连接方式。集群计算机通常用来改进单个计算机的计算速度和/或可靠性。一般情况下集群计算机比单个计算机，比如工作站或超级计算机性能价格比要高得多。

集群分为同构与异构两种，它们的区别在于：组成集群系统的计算机之间的体系结构是否相同。

集群计算机按功能和结构可以分成以下几类：

- 高可用性集群 High-availability (HA) clusters
- 负载均衡集群 Load balancing clusters
- 高性能计算集群 High-performance (HPC) clusters
- 网格计算 Grid computing (异构)

> 网格计算或网格集群是一种与集群计算非常相关的技术。网格与传统集群的主要差别是网格是连接一组相关并不信任的计算机，它的运作更像一个计算公共设施而不是一个独立的计算机。还有，网格通常比集群支持更多不同类型的计算机集合。

网格计算是针对有许多独立作业的工作任务作优化，在计算过程中作业间无需共享数据。网格主要服务于管理在独立执行工作的计算机间的作业分配。资源如存储可以被所有节点共享，但作业的中间结果不会影响在其他网格节点上作业的进展。

#### 应用领域

适合采用并行编程的应用领域主要有：矩阵计算、数据分析、3D 渲染、粒子仿真等。

### 并发/Concurrent：同时进行

同时进行，但不一定在执行。

在并发编程中，为了提高效率，严格要求多个操作步骤必须同时执行。刚好相反地，为了保证用户可管理性，并发编程中，多个操作步骤完全没要求同时执行。

并发编程的核心关注点不是在于效率，而是：

- 组件化、低响应延迟、高可维护性

- 一个操作步骤可以在何时开始执行？

- 操作步骤之间如何交换信息？

- 代码层面如何管理对共享资源的访问？

在并发编程中，通常会或多或少地牺牲一些并行性，但是隐藏在系统背后的便捷性在并发编程中相比并行性而言更重要。

#### 应用领域

Web 服务器、用户界面、数据库等。

### 总结

根据 _The Art of Concurrency_ 书中的定义：

> A system is said to be concurrent if it can support two or more actions in progress at the same time.

> A system is said to be parallel if it can support two or more actions executing simultaneously.

> The key concept and difference between these definitions is the phrase "in progress."

This definition says that, in concurrent systems, multiple actions can be in progress (may not be executed) at the same time. Meanwhile, multiple actions are simultaneously executed in parallel systems. In fact, concurrency and parallelism are conceptually overlapped to some degree, but "in progress" clearly makes them different.

并行是解决并发问题的一种方法。

## 如何解决高并发问题

高并发场景下带来的问题主要有两种：服务器是否处理得过来、数据一致性如何保证。

### CAS: Compare And Set

CAS 是一种降低读写锁冲突，保证数据一致性的乐观锁机制。

应用场景：

- 先 SELETE 后 UPDATE 的场景。

### ABA: CAS 极端情况出现的另一个问题

### 幂等

### 通过 memcached

借助 memcached 的原子增减操作进行并发控制。

#### add

- 申请锁：在校验是否创建过活动前，执行add操作key为memberId，如果add操作失败，则表示有另外的进程在并发的为该memberId创建活动，返回创建失败。否则表示无并发
- 执行创建活动
- 释放锁：创建活动完成后，执行delete操作，删除该memberId。

缺点：memcahced 具有时效性。

#### Incr/decr (CAS)

- 预先在memcached中设置一个key值，假设为CREATKEY＝1

- 每次创建活动时，在规则校验前先get出CREATEKEY＝x；

- 进行规则校验

- 执行incr CREATEKEY操作，检验返回值是否为所期望的x＋1，如果不是，则说明在此期间有另外的进程执行了incr操作，即存在并发，放弃更新。否则

- 执行创建活动

### Linux 内核参数

``` shell
# 表示如果套接字由本端要求关闭 这个参数决定了它保持在 FIN-WAIT-2 状态的时间
net.ipv4.tcp_fin_timeout = 30

# 表示当 keepaliv e起用时 TCP 发送 keepalive 消息的频度
# 缺省是 2 小时 改为 20 分钟
net.ipv4.tcp_keepalive_time = 1200

# 开启 SYN Cookies
# 当出现 SYN 等待队列溢出时 启用 cookies 来处理 可防范少量 SYN 攻击 默认为 0
net.ipv4.tcp_syncookies = 1

# 开启重用
# 允许将 TIME-WAIT sockets 重新用于新的 TC P连接 默认为 0 表示关闭
net.ipv4.tcp_tw_reuse = 1

# 表示开启 TCP 连接中 TIME-WAIT sockets 的快速回收 默认为 0 表示关闭
net.ipv4.tcp_tw_recycle = 1

# 表示用于向外连接的端口范围 缺省情况下很小（32768~61000）改为 1024 到 65000
net.ipv4.ip_local_port_range = 1024 65000

# 表示SYN队列的长度，默认为1024，加大队列长度为8192，可以容纳更多等待连接的网络连接数。 
net.ipv4.tcp_max_syn_backlog = 8192
```

- `net.ipv4.tcp_max_tw_buckets = 5000`

表示系统同时保持 TIME_WAIT 套接字的最大数量。如果超过这个数字 TIME_WAIT 套接字将立刻被清除并打印警告信息，默认为 180000。

对于 Apache、Nginx 等服务器，上几行的参数可以很好地减少 TIME_WAIT 套接字数量。但是对于 Squid，效果却不大。

此项参数可以控制 TIME_WAIT 套接字的最大数量，避免 Squid 服务器被大量的 TIME_WAIT 套接字拖死。 

当 TIME_WAIT 太多时通过 `dmsg `可以看到 "TCP: time wait bucket table overflow" 这样的错误。

执行以下命令使配置生效：`/sbin/sysctl -p`。

## 测试工具

### [WRK](https://github.com/wg/wrk)

``` shell
wrk -t12 -c400 -d30s -s ./scripts/post.lua --latency http://localhost:3000
```

- 确保运行 wrk 的系统拥有足够的临时端口数，且在监听在这些临时端口的 socket 被关闭的之后，能够被快速回收

- 为了处理爆发的初始连接，服务器的 listen（2）backlog 队列数设置，应该大于被测试的并发连接的数量。

#### Invalid URL?

检查 hostname 格式是否符合规范，不能包含下划线 `_`。

> The Internet standards (Requests for Comments) for protocols mandate that component hostname labels may contain only the ASCII letters 'a' through 'z' (in a case-insensitive manner), the digits '0' through '9', and the minus sign ('-'). The original specification of hostnames in RFC 952, mandated that labels could not start with a digit or with a minus sign, and must not end with a minus sign. However, a subsequent specification (RFC 1123) permitted hostname labels to start with digits. No other symbols, punctuation characters, or white space are permitted.

> While a hostname may not contain other characters, such as the underscore character (`_`), other DNS names may contain the underscore.[4] Systems such as DomainKeys and service records use the underscore as a means to assure that their special character is not confused with hostnames. For example, `_http._sctp.www.example.com` specifies a service pointer for an SCTP capable webserver host (www) in the domain example.com. Notwithstanding the standard, Chrome, Firefox, Internet Explorer/Edge and Safari all actually allow underscores in hostnames although cookies in IE won't work correctly if any part of the hostname contains an underscore character.[5]

> See: <https://en.wikipedia.org/wiki/Hostname#Restrictions_on_valid_host_names>

### jmeter

> TODO

### AB

#### 安装使用

``` shell
# CentOS
yum provides /usr/bin/ab    # 查看哪个软件包包含了 ab 程序
yum install httpd-tools -y
```

#### 压测 GET 请求

GET 请求压测直接后缀 URL 就行了，参数直接挂在 URL 后面，如：`?a=1&b=2`。

``` shell
ab -c 2 -n 10  http://host/users?search=cjli&id=1
```

如果在 GET 的消息头里面增加一些参数，这个时候需要特殊选项，看以下:

``` shell
ab -c 2  -n 10  -H 'FOO: 111'  -H 'BAR: 222'  http://host/users?search=cjli
```

#### 压测 POST 请求

``` shell
ab -c 2  -n 10 -p /path/to/post-data.json -H 'FOO: BAR'  http://host/users?name=cjli&sex=boy

ab -c 200 -n 1000 -T 'application/x-www-form-urlencoded' -p post-form-data.txt http://host/users
```

其中，`post-form-data.txt` 文件的内容为表单格式的参数列表：

```
a=1&b=2&c=3&d=4
```

#### 命令行参数解释

``` shell
-n requests Number of requests to perform // 在测试会话中所执行的请求个数，默认仅执行一个请求

-c concurrency Number of multiple requests to make // 一次产生的请求个数，默认一次一个

-t timelimit Seconds to max. wait for responses // 测试所进行的最大秒数。其内部隐含值是 -n 50000。它可以使对服务器的测试限制在一个固定的总时间以内。默认没有时间限制。

-p postfile File containing data to POST // 包含了需要 POST 的数据的文件

-T content-type Content-type header for POSTing // POST数据所使用的 Content-type 头信息

-v verbosity How much troubleshooting info to print // 设置显示信息的详细程度：4或更大值会显示头信息， 3或更大值可以显示响应代码(404, 200等), 2或更大值可以显示警告和其他信息。

-V 显示版本号并退出。

-w Print out results in HTML tables // 以HTML表的格式输出结果。默认时，它是白色背景的两列宽度的一张表。

-i Use HEAD instead of GET // 执行 HEAD 请求而不是GET

-x attributes String to insert as table attributes

-y attributes String to insert as tr attributes

-z attributes String to insert as td or th attributes

-C attribute Add cookie, eg. 'Apache=1234. (repeatable) // 对请求附加一个Cookie行。 其典型形式是name=value的一个参数对，比如 `-C cookie-name=value`。此参数可以重复。

-H attribute Add Arbitrary header line, eg. 'Accept-Encoding: gzip' Inserted after all normal header lines. (repeatable)

-A attribute Add Basic WWW Authentication, the attributes are a colon separated username and password.

-P attribute Add Basic Proxy Authentication, the attributes are a colon separated username and password. // `-P proxy-auth-username:password` 对一个中转代理提供 BASIC 认证信任。用户名和密码由一个 `:` 隔开，并以 base64 编码形式发送。无论服务器是否需要(即, 是否发送了401认证需求代码)，此字符串都会被发送。

-X proxy:port Proxyserver and port number to use

-V Print version number and exit

-k Use HTTP KeepAlive feature

-d Do not show percentiles served table.

-S Do not show confidence estimators and warnings.

-g filename Output collected data to gnuplot format file.

-e filename Output CSV file with percentages served

-h Display usage information (this message)
```

#### 压测结果解释

举例说明：

```
This is ApacheBench, Version 2.0.41-dev <$Revision: 1.121.2.12 $> apache-2.0
Copyright (c) 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Copyright (c) 1998-2002 The Apache Software Foundation, http://www.apache.org/
Benchmarking 127.0.0.1 (be patient)
Completed 100 requests
Completed 200 requests
Completed 300 requests
Completed 400 requests
Completed 500 requests
Completed 600 requests
Completed 700 requests
Completed 800 requests
Completed 900 requests
Finished 1000 requests
Server Software: Apache/2.0.54    // 服务器 Apache 版本 2.0.54
Server Hostname: 127.0.0.1        // 服务器主机名
Server Port: 80    // 服务器端口
Document Path: /index.html.zh-cn.gb2312 // 测试的页面
Document Length: 1018 bytes    // 文档大小
Concurrency Level: 1000    // 并发数
Time taken for tests: 8.188731 seconds    // 本次测试持续的时间
Complete requests: 1000    // 完成的请求数量
Failed requests: 0    // 失败的请求数量
Write errors: 0
Total transferred: 1361581 bytes    // 整个场景中的网络传输量
HTML transferred: 1055666 bytes     // 整个场景中的HTML内容传输量
Requests per second: 122.12 [#/sec] (mean)    // 大家最关心的指标之一，相当于 LR 中的 每秒事务数 ，后面括号中的 mean 表示这是一个平均值
Time per request: 8188.731 [ms] (mean)        // 大家最关心的指标之二，相当于 LR 中的 平均事务响应时间 ，后面括号中的 mean 表示这是一个平均值
Time per request: 8.189 [ms] (mean, across all concurrent requests)    // 每个请求实际运行时间的平均值
Transfer rate: 162.30 [Kbytes/sec] received    // 平均每秒网络上的流量，可以帮助排除是否存在网络流量过大导致响应时间延长的问题
Connection Times (ms)
min mean[+/-sd] median max
Connect: 4 646 1078.7 89 3291
Processing: 165 992 493.1 938 4712
Waiting: 118 934 480.6 882 4554
Total: 813 1638 1338.9 1093 7785    // 网络上消耗的时间的分解，各项数据的具体算法还不是很清楚
Percentage of the requests served within a certain time (ms)
50% 1093
66% 1247
75% 1373
80% 1493
90% 4061
95% 4398
98% 5608
99% 7368
100% 7785 (longest request)
// 整个场景中所有请求的响应情况。在场景中每个请求都有一个响应时间，其中50％的用户响应时间小于1093 毫秒，60％ 的用户响应时间小于1247 毫秒，最大的响应时间小于7785 毫秒。由于对于并发请求，cpu实际上并不是同时处理的，而是按照每个请求获得的时间片逐个轮转处理的，所以基本上第一个Time per request时间约等于第二个Time per request时间乘以并发请求数
```

## 参考

- [实现了一个比nginx速度更快的HTTP服务器](http://www.cnblogs.com/clowwindy/archive/2011/09/23/a_http_server_faster_than_nginx.html)
- [Parallel Programming vs. Concurrent Programming](https://takuti.me/note/parallel-vs-concurrent/)
- [CAS下ABA问题及优化方案](https://m.w3cschool.cn/architectroad/architectroad-cas-optimization.html)
- [C10K](http://www.kegel.com/c10k.html)
- [使用memcached进行并发控制](http://blog.csdn.net/jiangbo_hit/article/details/6211704)
