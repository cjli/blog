---
layout: post
title: 关于文本搜索
category: Database 
tags: [搜索, 全文索引, MySQL, ELK]
---

搜索的需求在项目开发中非常常见，有必要单独记录一下。

<!-- more -->

## 概念

### 数据的分类

- 结构化数据

指具有固定格式或有限长度的数据，如数据库，元数据等。

- 非结构化数据

指不定长或无固定格式的数据，如邮件，word文档等。

### 搜索的分类

#### 对结构化数据的搜索

对数据库的搜索，用SQL语句。再如对元数据的搜索，如利用文件系统对文件名，类型，修改时间进行搜索等。

#### 对非结构化数据的搜索

- 顺序扫描 (Serial Scanning)

所谓顺序扫描，比如要找内容包含某一个字符串的文件，就是一个文档一个文档的看，对于每一个文档，从头看到尾，如果此文档包含此字符串，则此文档为我们要找的文件，接着看下一个文件，直到扫描完所有的文件，因此相当的慢。

- 全文检索 (Fulltext Search)

将非结构化数据中的一部分信息提取出来，重新组织，使其变得有一定结构，然后对此有一定结构的数据进行搜索，从而达到搜索相对较快的目的。

**这部分从非结构化数据中提取出的然后重新组织的信息，我们称之索引。**

> 例如：字典。字典的拼音表和部首检字表就相当于字典的索引，对每一个字的解释是非结构化的，如果字典没有音节表和部首检字表，在茫茫辞海中找一个字只能顺序扫描。然而字的某些信息可以提取出来进行结构化处理，比如读音，就比较结构化，分声母和韵母，分别只有几种可以一一列举，于是将读音拿出来按一定的顺序排列，每一项读音都指向此字的详细解释的页数。我们搜索时按结构化的拼音搜到读音，然后按其指向的页数，便可找到我们的非结构化数据——也即对字的解释。

### 什么是全文索引?

全文检索是一种将文件中所有文本与检索项匹配的文字资料检索方法。全文检索首先将要查询的目标文档中的词提取出来，组成索引，通过查询索引达到搜索目标文档的目的。

这种先建立索引，再对索引进行搜索的过程就叫全文检索。

全文检索就是把文本中的内容拆分成若干个关键词，然后根据关键词创建索引。查询时，根据关键词查询索引，最终找到包含关键词的文章。整个过程类似于查字典的过程。

### 应用场景

经过几年的发展，全文检索从最初的字符串匹配程序已经演进到能对超大文本、语音、图像、活动影像等非结构化数据进行综合管理的大型软件。

对于数据量大、数据结构不固定的数据可采用全文检索方式搜索，比如百度、Google 等搜索引擎、论坛站内搜索、电商网站站内搜索等。

## 文本全文检索常用解决方案

### [MySQL](https://dev.mysql.com/doc/refman/5.7/en/innodb-fulltext-index.html)

MySQL MyISAM 支持 fulltext，MySQL 5.6.4+ 之后的 InnoDB 引擎也开始支持基于字符类型(char/varchar/text)的全文索引。

#### 语法速览

``` sql
CREATE TABLE opening_lines (
   id INT UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY,
   opening_line TEXT(500),
   author VARCHAR(200),
   title VARCHAR(200),
   FULLTEXT idx (opening_line)
) ENGINE=InnoDB;

CREATE FULLTEXT INDEX idx ON opening_lines(opening_line);
SHOW WARNINGS;

SELECT COUNT(*) FROM opening_lines WHERE MATCH(opening_line) AGAINST('KeyWords');
SELECT COUNT(*) FROM opening_lines WHERE MATCH(opening_line) AGAINST('KeyWords' IN BOOLEAN MODE);
SELECT COUNT(*) FROM opening_lines WHERE MATCH(opening_line) AGAINST('+KeyWords1 -Keywords2' IN NATURAL LANGUAGE MODE);
SELECT id, opening_line, MATCH(opening_line) AGAINST('KeyWords' IN NATURAL LANGUAGE MODE) as score FROM opening_lines WHERE MATCH(opening_line) AGAINST('KeyWords' IN NATURAL LANGUAGE MODE);
```

搜索的时候语法有两种模式：

- natural language mode

自然语言模式，也是默认搜索模式。

> <https://dev.mysql.com/doc/refman/5.7/en/fulltext-natural-language.html>

- boolean mode

布尔模式，表现形式为搜索字符串的起始/结尾有特殊含义。该模式支持 3 中布尔逻辑。

```
+ => 逻辑 AND
- => 逻辑 NOT
无符号 => 逻辑 OR
```

> <https://dev.mysql.com/doc/refman/5.7/en/fulltext-boolean.html>

#### 分词规则

英文字母、数字、下划线组成的字符串视为一个字 (不会把下划线分割)。

会被用于分词的字符有：空白、英文逗号 `,` 和点 `.`。

#### 中文分词

MySQL Fulltext 处理中文多字节编码的文本时，需要使用其他技或术自己手动分词。

- 其他技术

> 如今网上对于中文分词的解决方案有很多，有基于MySQL插件的，有谈论算法思想的。基于插件（如海量科技的MySQL--LinuxX86-Chinese+，hightman开发的mysql--ft-hightman）的方式主要通过对MySQL数据库安装一个别人提供好的插件，在建FULLTEXT索引的字段时后面加上WITH PARSER ×××（大多都是这样）的形式。而基于算法思想的则大部分工作都要自己完成，但他们的大体思想都差不多：

> 1. 对插入的要建全文索引的中文数据进行分词；
> 2. 将原始数据和分词后的数据都存入数据库中，并以某种方式建立联系；
> 3. 在存储分词数据的字段上建立FULLTEXT索引；
> 4. 查询时以 `SELECT...MATCH...AGAINST` 的方式在分词字段上搜索，将搜到的行通过前面建立的联系找到原始数据行并返回。

> 而我们在讨论解决方案时，考虑到使用开源插件的话可控性比价差，而且插件会对MySQL做一些改变，我们决定将分词存储的工作自己写代码完成，这样虽然工作量加大，但以后的维护成本却降低了很多。

- 手动分词

可以 MySQL 能识别的字符来隔断中文文本。比如用空格将中文分词隔开。

> 网上流传的分词方法有很多，主要有基于算法的（比如二元分词算法，字节交叉切分算法）和基于词库的。基于算法是不必要维护词库的，而词库法则必须维护词库，有可能跟不上词汇的发展。实际上现在很多著名的搜索引擎都使用了多种分词的办法，比如“正向最大匹配”+“逆向最大匹配”，基于统计学的新词识别，自动维护词库等技术。

### sphInx

### Lucene

### [Elasticsearch](https://www.elastic.co/guide/cn/elasticsearch/guide/current/index.html)

## 参考

- _[1、什么是全文索引](https://blog.csdn.net/u013115157/article/details/53607622)_
- _[Elasticsearch的配置与使用，为了全文搜索](https://www.qiehe.net/posts/4/the-configuration-and-use-of-elasticsearch-for-full-text-search)_
- _[MySQL中文全文检索解决方案](http://www.cnblogs.com/tanxin/archive/2012/11/05/2754645.html)_
