---
layout: post
title: 迟来的 2016 年终总结
category: 胡思乱想
tags: [年终总结, 2016]
---

不知不觉 2017 已经过了 1 个月零 8 天...... 迟来的年终总结～

<!-- more -->

## 2016 年报

总的来说，2016 年还是经历了很多事情，其中很多个“第一次”。下面根据回忆得起的先后顺序先简单列个年报：

- 1月
  - 大学最后一个寒假没回老家，在温州寂寞度过
  - 经历最严重的一次分手风波
  - 看完《深度学习的艺术》
- 2月
  - 第一次正式为工作准备
  - 第一次过年还在学习技术
  - 觉得一直使用二级域名没意思，买了个属于自己的顶级域名
  - 第一次投简历
- 3月
  - 大四最后一期没回学校，第一次找工作
  - 第一次胆大心粗地来到广州
  - 第一次做公司项目
  - 第一次体验到现实社会版的编程工作
- 4月
  - 兴奋地拥有了属于自己的 MacBook Pro
  - 不再使用 Windows，迅速熟悉 Mac 工作流工作和生活，强烈感受到 macOS 对我而言的流畅和高效
  - 第一次在公司过生日，那天女朋友为我做了一桌丰盛的菜，大家都来了(:
  - 开始负责管理公司 Git／GitLab 工作流
  - 第一次争论版本控制、Git 对互联网项目的重要性
  - 编写 macOS 批量管理 SS 节点配置小工具
- 5月
  - 挽回女友
  - 第一次经历通宵赶项目
  - 第一次独立二次开发一个大型项目后台
  - 第一次觉得粤语歌好听
- 6月
  - 大学双证毕业
  - 第一次坐高铁商务舱
  - 转正
  - 第一次涨工资
  - 开始带领后台小组开发
- 7月
  - 开始正式使用 Laravel/Lumen
  - 第一次与 iOS 开发者合作完成 App 接口开发
  - 第一次感受到老板有“画大饼”嫌疑
  - 为女朋友过生日，用我自己挣的钱为把她的 8G 4s 换成了 64G 6s
  - 第一次感受到自己经常“选择性”注意不到电话和短信（因此买了一对手环...）
  - 第一次面试别人
- 8月
  - 带领后台开发者学习使用 Laravel 重写一个刚开始不久的 ThinkPHP 项目
  - 通过新项目第一次体验到小公司混乱的开发现状和各种不规范的弊端
  - 深刻意识到流程、规范、执行力的重要性并进行相关学习和总结
  - 面试了几位 PHP 开发者
- 9月
  - 第一次正式成为公司技术负责人
  - 第一次想带领好开发者做好新项目
  - 看完《Moder PHP》
  - 第二次涨工资
  - 制定并实施开发规范、Git 规范、软件项目开发参考流程
  - 第一次负责开会、负责检查开发者的日报
  - 意识到环境不一致带来的问题，在团队中引入 Vagrant 工作流统一开发环境
  - 开始经历比较长的，客户、设计、产品、程序之间的摸索、磨合期
- 10月
  - 负责优化项目，数据库设计，解决同事出现的技术性问题
  - 回老家庆祝爷爷 70 寿辰
  - 强制性要求开发者重构不规范、难懂的、无用的功能代码
  - 第一次能确定老板在“画大饼”
  - 第一次有离开公司的想法
  - 第一次被坑：前老板之一离开公司，由于未事先签合同，为其做的项目被其拷走，对应奖金最终泡汤
  - 开始特别重视老板对员工承诺的兑现情况
  - 看完《周伟鸿自述：我的互联网方法论》
- 11月
  - 继续带领技术组完成上个项目的第二期功能
  - 大家都开始重视开发规范，整个协作流程上开始规范化，前后端开发者编码明显规范很多
  - 进一步学习 PhantomJS 爬虫，开始和淘宝反爬虫机制作斗争
  - 陪女友回海南处理她考研报名等事项
  - 开始整理 2016 年的学习笔记
  - 开始重视规范、高效的知识管理
- 12月
  - 继续带领技术组完成上个项目的第二期功能遗留工作
  - 测试新项目现有功能
  - 评估第三期工作计划
  - 整理完 2016 年间学习笔记和文章
- 2017年1月
  - 阶段性突破淘宝手机端截图痛点问题
  - 公司年会被评为「年度最佳技术奖」
  - 携女友飞回老家过年
  - 开始重视、关注房地产和汽车知识、资讯
- 2017年2月
  - 第一次买黄牛票):
  - 开始研究 PHP 扩展开发
  - 第一家公司离职
  - 为域名续费
  - 将个人主页搬到搬瓦工 VPS 并使用 HTTPS
  - 回学校那边把拖了一年多的驾照考完

## 学习

> 下面指的「学习」均非学校中的学习和考试，我不擅长也讨厌这种学习方式。

大概从大二下开始，我很喜欢也很重视学习，当然，主要是自学。

我也一直在坚持自学，2016 年间主要依然是在自学编程和英语，大量使用了 Google 和 Stack overflow，以及开发手册。

### 现阶段自学方式的好与坏

- 好处：目的性强、效率高；坏处：不系统，目的驱动型。

我是贯彻 Learn By Doing 的人，这样的好处学什么用什么，不会浪费时间在暂时用不上的东西上。

但是，从长远的角度来说，这样的结果是各个知识结构中的知识点零散分布，容易形成多而不精的尴尬局面。

这种情况如果不进行后期总结，则很难把学过的知识点串联起来，也就很难发挥完善的知识系统的威力。（大家都知道散兵游勇是没有战斗力的，而一支训练有素的军队才具有杀伤力）

- 解决之道：自学前先了解该知识结果，规划知识框架，自学时学什么就填充、完善哪个子部位；自学后再从知识结构的宏观角度，对该知识框架中已有的部分进行总结和梳理，对框架中知识点进行查漏补缺。

### 如何通过读书学习

我认为读书学习是一个间隔期逐渐增大的循环过程。我的读书流程如下：

- 标注对现在的你来说新的概念，暂时不懂无所谓。
- 每隔几天重看一遍，无需强行记忆，只要理解。
- 一直看，当你再次看到标注过的内容时觉得已经理所当然了，就可以删除这些标注。
- 如果又忘了，请从 0 开始。

### 程序员如何学习英语

``` 
while (true) { 
	++积累; // 语言学习就是个长期积累 不断重复的过程 
}
```

- 口语和听力

日常看美剧，培养语感，觉得有兴趣的表达可以学着多念几遍。

> 不在外企和出国其实没多大实际用场，如果在外企活着国外的话，周边环境刺激下学得也快。

- 阅读理解

对程序员来说，英语阅读能力更重要也必要。你自己要有意识到学英语的好处，然后给自己创造机会（ google so quora hn 等、博客、原著等）去阅读英文。然后查单词是必不可少的 。

- 复杂句型

我建议每次遇到复杂点的句型就记下来搞清楚一次，想办法（搜索、问专业同学）搞懂这一类句子的断句、理解方式。长期下来常用的句型肯定就能举一反三了。（我个人不相信计算机相关的英语会天天遇到到不同的逻辑复杂的表达方式） 

- 用外国人思维学英语

理解英文时最好不要靠翻译成的中文解释再想这个中文表达的意思，而是直接从英文想到指的什么。

比如，apple，一般人是先翻译成“苹果”，然后再联想到“苹果”的一切。而最好，最自然的记忆方式是，看到 apple 就能直接联想到苹果的画面。这其实也是老外学英语能学好，中国人学中文能学好的思维。

- 坚持 & 重复

最后，如果没有主动坚持下去的意识，说什么都是无用功。

### 2016 已读书籍

- 《深度学习的艺术》
- 《Modern PHP》
- 《周伟鸿自述：我的互联网方法论》

## 工作

2016 是我毕业后正式工作的第一年，这一年我自认为是认真负责，工作量多的。

> 由于公司较小，人手不足（尤其是前几个月），我干的很杂很多的。

主要体现在：

### 主动性

主动一直是我在坚持技术路线上引以为傲的优点。2016 年我在工作中的主动性主要包括：

- 下班后如果没处理完，仍会主动处理工作上的事情
- 主动规范自己的代码
- 为提升软件质量和自身水平，主动重构部分复杂、有明显缺陷、不够完美的功能（在时间允许的情况下）
- 主动揽下技术难点
- 主动推行对团队开发有益的工作流：Git；Vagrant
- 主动事先学习项目马上要用到而没使用过的技术

### 我对「责任心」的定义

责任心，不是 7x24 小时待命，随时恭候老板的电话（虽然这对升职加薪有帮助），让你干啥就干啥，而是在自己工作时间内保证自己的工作质量、进度和效率。

责任心，有时候，也在于敢于否定老板。老板不如你懂技术，可能有时候会提出一些不切实际想法，这时候，为了项目，感觉用自己技术带来的自信去否定老板的想法，我认为，这对于项目大局上来说也是一种责任心的体现。

### 大公司，小公司

从本人差不多 1 年的在小公司工作的经验来说，小公司的好处在于，你表现的机会多，晋升的难度相对较低。

但是坏处也十分明显：不正规，不规范，技术水平欠缺；老板喜欢画大饼（动不动就提什么股票、期权、丰厚奖金的等等），物质兑现能力弱，福利少。

当然并非指所有小公司都是这样，从我个例上总结而已。

> 如果这家创业公司倒了，那么你的下家就不好找了。反之，你从大公司要进入小公司，则是轻轻松松地一件事。

### 钱

**拿到手上的钱才叫钱。**动不动谈理想、未来的老板多半都是坑，因为未来变数太多，谁都无法预料。

## 生活及其相关领域

关于生活，为人处事，基本法律，社会民生，经济学常识等与社会人来说戚戚相关的各方面的关注以及必要的知识积累，一直是我很欠缺的地方。大学期间的我，眼里只有技术这个狭隘的（相对于人需要掌握的完整知识系统来说）领域，见识被仅有的专业知识所限制。

随着年龄，经历的增长和心智的成熟，我也不得不开始认可：这些我所欠缺的领域，对于一个人来讲，在现代社会生存中的重要程度不亚于主要的专业技能。因此，我也要分配一些时间去了解、学习它们，至少掌握一些常识性的东西。

### 房产知识

购房人必须具备良好的知识和技术素质。良好的知识技术素质要求购房者具有丰富的房地产及买卖知识，熟悉交易程序和方法，能够在初始阶段就能预知房地产市场的行情和区域背景。

## 其他

### 关于个人计划

我个人觉得，计划都必须要有，但不同于工作计划，个人计划最好不要忙于告诉别人，也不要分享讨论。

自己默默坚持，专注于每天的积累，相信量变会引起质变就好了。因为在没有实现之前喊一些漂亮的口号很是奇怪，讨论你的个人计划给人的感觉就像真的已经实现了一样。

靠说出来的计划，不务实、夸夸其谈的计划也多半实现不了。我个人始终坚持，心里有计划才是真的会朝计划去做。

### 从头来过

想起前公司一位老板曾对我说过，如果我离开他们公司，就要从头来过。而我其实想要说，2016 我尚未开始，何来从头来过。

2017，才是真的开始。

## 结束

最后，附上我最喜欢的一段话之一结束本文：

> 这些年我一直提醒自己一件事情，千万不要自己感动自己。大部分人看似的努力，不过是愚蠢导致的。什么熬夜看书到天亮，连续几天只睡几小时，多久没放假了，如果这些东西也值得夸耀，那么富士康流水线上任何一个人都比你努力多了。人难免天生有自怜的情绪，唯有时刻保持清醒，才能看清真正的价值在哪里。 （于宙，《我们这一代人的困惑》）

## 参考

- [劳动合同和劳务合同的5项区别](http://jingyan.baidu.com/article/8ebacdf0e1a47d49f65cd5b7.html)
- [学习深研究](http://cglai.github.io/auxiliary/Comprehensive-to-Learning.html)