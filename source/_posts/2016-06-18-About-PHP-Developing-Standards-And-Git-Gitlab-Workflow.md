---
layout: post
title: 关于 PHP 团队开发规范和 Git 使用规范
category: Profession
tags: [团队协作, 开发规范, Git,  PHP]
---

今天公司在广州开了个开发者会议，还是总结了不少东西。以下是我总结的关于 PHP 团队开发规范和 Git 使用规范。

<!-- more -->

## PHP 开发规范

### 为什么？

同普通话存在的意义一样，不同地方的人有自己的家乡话，同一个地方的人说方言这当然没有问题，但是但凡要和外地的人沟通，就不得不说普通话，不然互相都不知道在表达什么，怎么能一起干好事情呢。同理，每个程序员应该都有自己写代码的习惯，有的喜欢写

所以，对一个团队而言之所以需要这么一个规范主要是为了在软件开发过程中大家便于交流和协作。由于软件是人写的，也是人维护的，所以在写和读代码的同时，如果都遵循统一的规范，那么代码的可重用性和易读性可以随之增强，因为都能互相理解，那么别人在使用你的代码或修改你的代码的时候也会容易很多，因此也会使软件便于维护，从总体上来说，间接地也就提高软件开发质量和缩短开发周期。另外，别人在阅读你们团队的代码的时候，统一、整洁的代码也会给人留下专业的形象。

那么关于我们要采用的 PHP 开发规范，是采用的 PHP 互操作性框架制定小组（FIG）制定的 [PHP-PSR](https://github.com/PizzaLiu/PHP-FIG) ，也就是传说中的 PHP 推荐标准，之所以用它，是因为它受到了很多优秀的 PHP 框架开发者和社区的关注，如果我们遵守它，不光在自己团队之间可以按照这个标准协作，在研究学习其他优秀框架的时候也会在编码风格层面上容易理解很多，在 PHP 圈子中，可以把它比作英语，因为是国际语言，所以走到哪里基本上都能沟通。

下面我要讲的，就是在这个的基础上简单总结出来的要点。主要有下面这些：

### 程序文件层面

#### 文件编码

可以在各编程语言间通用的。UTF-8、不带字节序标记（BOM）。因为 字节序标记 会阻止应用程序设置它的头信息，从而影响 PHP 的输出。

#### 换行符

使用 Unix 格式换行符（LF）。因为 PHP 基本上都是跑在 Linux 上的（其实大部分网络服务都是跑在 Linux 上的），LAMP，LNMP 这些被广泛采用的流行 Web 架构都是运行在 Linux 服务器上，而 Linux 衍生于 UNIX，所以使用 UNIX 换行符能从脚本文件层面上杜绝不必要的麻烦。麻烦在于，Windows 换行使用的是 CR＋LF（回车和换行），UNIX/Linux 使用的是 LF，Mac OS X 使用的是 CR，虽然大部分现代编辑器和 IDE 都具有自动转换这种差异的功能，但是如果被不小心弄错，导致本地开发的换行格式和 Linux 服务器上的不一致，比如在 Windows 上开发好后没问题，但是一放到 Linux 服务器上后却不能正常运行，这很可能就是因为 Windows 下的文件到 Linux 上后每次换行的地方都多了一个回车符号被显式地，以字符串 \M 的形式输出到原本的 PHP 脚本中，而这个 \M 是 PHP 解析器无法理解的语言结构，因此导致解析错误。

#### 用空行代替 PHP 结束标签

PHP 结束标签 `?>` 对于 PHP 解析器来说是可选的，但是只要使用了，结束标签之后的空格有可能会导致不想要的输出，这个空格可能是由开发者或者用户又或者 FTP 应用程序引入的，甚至可能导致出现 PHP 错误，就算通过修改配置抑制输出 PHP 错误，仍然会出现空白页面。基于这个原因， 所有的 PHP 文件将不使用结束标签，而是以一个空行代替。

#### 文件命名

类名必须以大写开头的驼峰法命名，比如：*SiteController.php*，或带 *.class* 的下划线加小写命名，比如：site_controller.class.php。但是，**同一个项目中，只能选用其一**（下面所有可以有2种选择的都必须这样）。

默认推荐第一种，因为可以少敲几次键盘。

其他文件，比如视图、配置、一般的脚本文件等等只能使用小写加下划线命名，比如：home.html，admin_login.js，article_footer.css，site_config.php 等等。

#### 文件夹和前缀

同一个逻辑下面的程序文件放在同一个有意义名称的文件夹下，如果不得不放在一个文件夹，那么使用前缀来区分。

比如：system 控制器下的所有视图必须放在 views 文件夹下的 system 目录下。如果非要放在 views 文件夹下，那么必须要带上 `system_` 前缀。默认采用第一种，因为可以缩短文件名。

### 代码层面

#### 命名

>  所有命名都要 **见名知意**！

- 类名必须大写是开头的驼峰法（匈牙利命名法），比如：SiteController。
- 常量名必须是全大写，并且单词之间使用下划线，比如：APP_ROOT。
- 函数／方法名必须是小写打头的驼峰法或下划线＋小写，比如：getVersion() 和 get_version()。默认使用第一种，因为为了和 PHP 系统函数区分。
- 变量／属性名必须使用 $ 打头的下划线＋小写，比如：$order_by， $is_same_key，或者 驼峰法：$orderBy，$OrderBy。

> 上面含有多种可选方式的规范中，无论遵循哪种命名方式，都应该在一定的范围内保持一致。最好是同一个项目中只使用默认的一套，函数名和变量名不能使用同一套。

#### 注释

> 在写代码的那一刻，只有程序员和上帝能看懂，写完之后，如果没有注释，就只有上帝能看懂了。

- 函数或方法名的注释必须在函数定义的上方，使用多行注释风格，每个方法必须要写注释。形如：

```php
/**
 * what this function do?
 * @para1 Type, like String
 * @para2 Type, like Boolean
 * @return Type, like Array
 * by @cjli
   */
public static function getVersion(&$arr, $flag=false)
{
 # step 1
 ...    // what this variable means?
 # step 2
 ...    // what should be noticed here?
 # step 3
 if ( $flag ) {
 	...    // what does this line do ?	
 } elseif ( $cond ) {
 	...    // do something here
 }
}
```
关于上面的示例代码，有几点需要说明的：

-    过程／流程注释使用 `#` 号，因为 `#` 有代表数字顺序都含义，所以适合描述步骤。
-    每行代码的注释写在同一行的右边，使用双斜线，上下行对齐。

#### 关键字使用

-    PHP所有 关键字必须全部小写。包括常量关键字 `true` 、`false` 和 `null` 也必须全部小写。

-    所有类方法都必须添加访问修饰符。

-    需要添加 `abstract` 或 `final` 声明时， 必须写在访问修饰符前，而 `static` 则必须写在其后。 比如：

     ```php
                       <?php
                       namespace Vendor\Package ;

                       use OtherVendor\OtherPackage\OtherClass ;

                       class Site
                       {
                         abstract protected function drink() ;
                           
                         /**
     * what this function do ?
     */
          final public static function getVersion()
     {
       // do sth here
     } 
        }
     ```

#### 空格、行和花括号

- 控制结构的开始花括号({)必须写在声明的同一行，而结束花括号(})必须写在主体后自成一行。

- 类和方法的起始花括号都要自成一行。

- 使用逗号的地方，逗号前不能有空格而逗号后必须有空格。

- 函数名与紧跟的括号之际要有空格，函数括号与相邻的参数不能有空格。

- 代码必须使用4个空格符的缩进，一定不能用 tab键 。

  > 因为不同的编辑器、IDE对 tab 键的理解不一样，有的是 4 个空格有的是8个空格，严格使用空格缩进可以避免这种差异带来的排版混乱。

- 每行不应该多于80个字符，大于80字符的行应该折成多行。

- 非空行后一定不能有多余的空格符。

- 每行一定不能存在多于一条语句。

### 其他方面

> 新项目默认选择最新稳定版。

- 开发工具：PHPStrom + Sublime Text/Atom。
- PHP 版本：5.5+
- MySQL 版本：5.5+。


上面说的这些只是一些最常用到的概要性总结，更多更完整的规范在 gitlab 上 README 项目中可以看到，因为时间关系以及说那么久大家也不可能全记住，所以我已经把你们拉进这个项目，你们可以直接从你们的账号中进入查看，主要看代码示例基本上就懂了。希望大家在尽快的时间内熟悉这些规范，并共同配合、互相遵守。

如果以后遇到一些新的，但是没有与之对应的规范来解决的问题，到时候大家再讨论，统一意见后，再追加到这个当前的这份开发规范中去就行了。

最后，用一句话总结关于开发规范的话题：

> 统一开发规范的价值在于我们都遵循这个编码风格，而不是在于它本身。

## Git 项目管理规范

Git 工作流程大概有三种：

- Git 工作流（含功能分支，补丁分支，和预发分支）


- GitHub 工作流（只有一个长期分支）


- Gitlab 工作流（上游优先；区分开发环境，预发环境和生产环境）。

由于 Git 工作流更适合隔段时间才发布新版本的开发模式，而我们所做的网站开发属于*“持续发布”*型开发，通常一个很小的改动就要立马上线，这时候很多分支内容实际上是差不多的，因此，区分功能，补丁和预发分支等显得比较多余，徒增维护难度。

而对于 Gitlab 工作流来说，结合我们所接手的网站项目规模和公司条件：规模普遍较小、公司目前并没有专门的测试环境，所以此工作流暂时不适合我们公司。

鉴于此，我将在 GitHub 工作流的基础上总结我们在 Gitlab 上管理我们的开发项目要经过的一种基本流程，目的是能够快速搞懂然后融入正常多人开发，更多 git 操作建议阅读参考中的个人认为总结地不错的博文，或者是直接查阅官方的 Pro-Git 一书。

首先，由团队负责人在 Gitlab 上创建一个 Group，然后以团队的名义创建一个私有项目和两个保护分支：master 和 dev，master 代表最稳定的版本，dev 代表最新开发的版本，并将 dev 分支设置为默认分支。然后拉入将参与此项目的开发人员并分配 Developer 权限。只有项目负责人或上级或专门的测试人员在测试 dev 的代码后，才决定 dev 分支的代码是否要合并到 master 分支。

当项目分工结束后，所有开发者都需要在 dev 分支的基础上新建一个不同于当前仓库已存在分支名的分支，并在该分支下进行开发，在自己的任务完成后，必须拉取 dev 分支上最新的代码到自己的任务所对应的那个分之下进行合并，确认无冲突后提交到 Gitlab 上，然后发起一个源分支为自己新建的分支，目标分支为 dev 分支的 Merge Request，只有项目负责人决定是否接受开发者的 Merge Request。

如果同时有多个开发者发起了合并请求，项目负责人最多只能同意其中一个，每次同意之后删除请求源分支，并拒绝其他的合并请求，然后要求被拒绝的开发者重新拉取 dev 分支的最新代码与自己的任务分支进行本地合并，确保无冲突后再次发起合并请求，以此类推。

在上述流程中，developer 将涉及到的 Git 操作将会有：

- 拉取项目源码到本地

  ```shell
  git clone git@git.keensword.net:hp-php/hpmall.git
  ```


- 在 dev 分支基础上创建新分支

  ```shell
  git checkout dev    # 确保在 dev 分支上
  git checkout -b taskname-developername
  ```

- 开发完成提交任务代码

  ```shell
  # 1. 添加
  git add -A
  # // 或者部分: git add file1 file2 ...

  # 2. 提交
  git commit -m "comments for this commit that make sense" 
  # // 或者使用 git commit 进入 vim 编辑器书写详细注释

  # 3. 拉取最新 dev 分支代码并合并
  git fetch origin dev   # 若使用的是别名请把 origin 改为你设置的别名
  git merge origin/dev   # 若出现冲突请与其他开发者协商解决
  # // 也可以用 git pull origin dev 一步到位但是不推荐因为当代码出错很难 debug

  # 4. 查看当前是否存在没提交的文件
  git status
  # // 如果有则重复 1 和 2 然后到 5

  # 5. 确认无冲突后提交到 gitlab
  git push origin taskname-developername
  ```

- 在 Gitlab 上发起 Merge Request

  Source Branch 一定是自己的任务分支，Target Branch 一定是项目的 dev 分支。

- 一次任务完成后删除上次创建的本地任务分支

  ```shell
  git branch -d taskname-developername
  ```

  每次做任务的时候建议都删除旧的任务分支然后新建新任务分支。

### 项目负责人将涉及的额外工作

- 决定是否接受开发者的合并请求、拒绝未与 dev 最新代码合并的合并请求。
- 定期将 dev 分支上的最新代码给上面组长或技术总监 review，如果都满意后将合并到 master 分支。
- 将 master 分支的代码部署、并定期更新到服务器。

### Git 提交规范

每次提交，Commit message 都包括三个部分：Header，Body 和 Footer，Header 必须，其余可选。

```
<type>(<scope>): <subject>
// 空一行
<body>
// 空一行
<footer>
```

- 特殊情况：

```
revert: feat(pencil): add 'graphiteWidth' option

This reverts commit 667ecc1654a317a13331b17617d973392f415f02.
```

> 如果当前 commit 用于撤销以前的 commit，则必须以revert:开头，后面跟着被撤销 Commit 的 Header。

> Body部分的格式是固定的，必须写成This reverts commit &lt;hash>.，其中的hash是被撤销 commit 的 SHA 标识符。

> 如果当前 commit 与被撤销的 commit，在同一个发布（release）里面，那么它们都不会出现在 Change log 里面。如果两者在不同的发布，那么当前 commit，会出现在 Change log 的Reverts小标题下面。

#### Header

##### type
    
type用于说明 commit 的类别，只允许使用下面7个标识:

- feature：新功能
- fix：修补bug
- docs：文档（documentation）
- style： 格式（不影响代码运行的变动）
- refactor：重构（即不是新增功能，也不是修改bug的代码变动）
- test：增加测试
- chore：构建过程或辅助工具的变动

> 如果 type 为feature 和 fix，则该 commit 将肯定出现在 Change log 之中。其他情况（docs、chore、style、refactor、test）由你决定，建议不要。

##### scope

scope用于说明 commit 影响的范围，比如数据层、控制层、视图层等等，视项目不同而不同。

##### subject

subject是 commit 目的的简短描述，不超过50个字符。遵守以下规范：

- 以动词开头，使用第一人称现在时，比如change，而不是changed或changes
- 第一个字母小写
- 结尾不加句号（.）

#### Body

Body 部分是对本次 commit 的详细描述，可以分成多行。注意：

- 使用第一人称现在时，比如使用change而不是changed或changes。
- 应该说明代码变动的动机，以及与以前行为的对比。

#### Footer

Footer 部分只用于两种情况:

- 不兼容变动

如果当前代码与上一个版本不兼容，则 Footer 部分以BREAKING CHANGE开头，后面是对变动的描述、以及变动理由和迁移方法。

- 关闭 Issue

如果当前 commit 针对某些 issue，那么可以在 Footer 部分关闭这个 issue 。

```
Closes #123, #245, #992
```

## 团队建设

本来要说一些关于“未来的团队期盼“之类的，说出自己的不足期望能怎么养”之类的话题，但是没有怎么准备，所以简单总结了以下几句话：

提交效率，减少加班。

加强沟通，取长补短。

研究技术，共同进步。

#### 参考

- [PHP-The Right Way](http://www.phptherightway.com/)
- [《Clean Code》]()
- [Git 使用规范流程](http://www.ruanyifeng.com/blog/2015/08/git-use-process.html)
- [Git 工作流程](http://www.ruanyifeng.com/blog/2015/12/git-workflow.html)
- [Git分支管理策略](http://www.ruanyifeng.com/blog/2012/07/git.html)
- [常用 Git 命令清单](http://www.ruanyifeng.com/blog/2015/12/git-cheat-sheet.html)
- [Git工作流规范](https://calvingit.gitbooks.io/appscomm-coding-guidelines-for-cocoa/content/Git_Flow.html)
- [Commit message 和 Change log 编写指南](http://www.ruanyifeng.com/blog/2016/01/commit_message_change_log.html)
