---
layout: post
title: 搭建本地 HTTPS 环境
category: 折腾
tags: [HTTPS, SSL]
---

随着 HTTPS 的日渐普及，我们的网站现已基本运行在 HTTPS 环境下。

<!-- more -->

为了保证网站项目能在 HTTPS 环境下正常工作，我们需要一个本地的 HTTPS 开发环境，避免本地开发环境上没问题，上线后却因为 HTTPS 出现问题这样的情况发生。

## 本文示例环境

- 主机（客户端）是 macOS，开发环境（服务端）是 CentOS 虚拟机。
- macOS 已事先配置所有以 `.centos` 结尾的域名都指向到 CentOS。
- CentOS 上的 Web 服务器：nginx。

## [FiloSottile/mkcert](https://github.com/FiloSottile/mkcert)

### 安装 mkcert

``` shell
sudo yum install nss-tools
go get -u github.com/FiloSottile/mkcert
```

### 生成泛域名证书

``` shell
mkdir -p /data/ssl && cd /data/ssl
mkcert '*.dev.centos'    # 必须是二级域名而不是顶级域名
mkcert --install
```

### 信任 mkcert 根证书

执行完后会生成 `~/.local/share/mkcert/rootCA.pem`，mkcert 的根证书文件，需要导入要访问本地 HTTPS 环境的浏览器所在的操作系统的系统证书列表并信任，比如 macOS 的 Keychain。

### 网站服务器设置 SSL

以 nginx 为例说明，主要在 `server` 块里面加人以下三行代码就好：

``` nginx
listen 443 ssl;
ssl_certificate /data/ssl/_wildcard.dev.centos.pem;
ssl_certificate_key /data/ssl/_wildcard.dev.centos-key.pem;
```

然后重启 nginx：

``` shell
nginx -s reload
```

然后就可以访问你任何以 `.dev.centos` 结尾的本地域名了，比如 `https://app1.dev.centos`。

## [Fishdrowned/ssl](https://github.com/Fishdrowned/ssl.git)

### 安装 Fishdrowned/ssl

``` shell
git clone https://github.com/Fishdrowned/ssl.git && cd ssl
```

### 生成泛域名证书

``` shell
./gen.cert.sh dev.centos    # *.dev.centos 都可以使用的泛域名证书
```

### 信任 mkcert 根证书

同 mkcert，只是根证书的路径变了：`/data/src/ssl/out/dev.centos/root.crt`。

### 网站服务器设置 SSL

同 mkcert，只是需要修改下证书和证书密钥路径。

``` nginx
ssl_certificate /data/src/ssl/out/dev.centos/dev.centos.crt;
ssl_certificate_key /data/src/ssl/out/dev.centos/dev.centos.key.pem;
```

## FAQ

### Chrome 允许本地主机地址无 SSL 证书就可以 https 访问？

找到 Chrome 实验功能之“允许不安全的 localhost”：

```
chrome://flags/#allow-insecure-localhost
```

### Chrome 加载 http 资源时会自动加载 https 资源？

检查 HTML 头部有无如下标签：

``` html
<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
```
