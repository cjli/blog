---
layout: post
title: 浏览器串行化相同 URL 的请求问题
category: Web
tags: [浏览器, URL串行化]
---

今天遇到个“诡异”的问题，用浏览器连续打开相同的 URL 时，最后一个页面的请求总会比第一个请求慢。

<!-- more -->

## 结论

事实证明这是由于部分浏览器行为（Browser Behavior）导致的，命令行工具 cURL, wget, ab 等没有这个行为。

## 复现

我用一段 PHP 代码来简单复现一下。

``` php
// index.php

$q = $_GET['q'];
$n = 10;

while ($n-- > 0) {
    sleep(1);
}

echo $q;
```

启动服务器 `php -S localhost:6666 -t .`，然后在 Chrome 打开两个 window/tab，关注 Network -> Timing，连续访问 _http://localhost:6666_。

其中第一个页面带了查询参数 `q=1`，第二个页面带了查询参数 `q=2`。

可以看到第一个打开的页面，等待了大约 10.04 秒输出了 1，第二个页面打开，并没有立马输出 2，而是等待了大约 18.66 秒才输出 2。

可以看出，第二个页面在第一个页面未结束以前，是处于阻塞状态的，否则第二个页面的等待时间也是在 10 秒左右。

至于为什么是 18.66 秒而不是 20 秒，这差的 1.34 秒是我手动切换两个页面所花费的时间。

## 原理和对现实的指导意义

每种浏览器自己的实现细节有关，可能是 feature——为了性能减少重复请求, 也可能是 bug——在动态网页的请求这种情况下的考虑不周。

> And two concurrent requests are issued from different windows of the same browser (for those browsers that have this bug/feature), the browser will actually issue only one request and won't run the second request till the first one is finished.

> 这个功能其实是给网页上的图片等静态资源准备的。

> 只要把浏览器的缓存禁用，就会发现不存在串行化的问题，这时多次请求是同时发起的

> 如果浏览器的缓存不禁用，浏览器是期望相同的 URL 服务器返回 304 的响应，但可惜的是第一次响应内容没有缓存，因此每次请求都是串行。

## 附录：浏览器请求页面的几种方式

- 通过浏览器地址栏／点击超链接（可消灭实际请求）

允许浏览器使用最少的请求来获取网页数据，浏览器会对所有没有过期的内容直接使用本地缓存，即 `Expired` 在此时（也仅此时）有效。

- F5 刷新／浏览器刷新按钮

允许必要的缓存协商，但是不会用到本地缓存，即 `Expires` 不会生效，`Last-Modified` 有效。

- Ctrl + F5／强制刷新／Ctrl + 浏览器刷新按钮

所有 Web 组件直接向服务器发送请求，不使用任何缓存协商。

## 参考

- [浏览器多tab打开同一URL串行化的问题](http://www.laruence.com/2011/07/16/2123.html)
- [关于请求被挂起页面加载缓慢问题的追查](http://fex.baidu.com/blog/2015/01/chrome-stalled-problem-resolving-process/)
