---
layout: post
title: Head into Laravel
category: PHP
tags: [Laravel, Lumen, PHP, Framework]
---

Lately I'm starting with another new project, I was studied on kinds of PHP frameworks before, since the Chinese version of _[PHP: THE Right Way](http://www.phptherightway.com/)_ is upheld by Laravel China, so I think Laravel is kind of meaningful for chinese PHP developer, finally, I decide to learn and use Laravel.

<!-- more -->

**说明**

之所以我没写中文笔记是因为我看的是 Laravel 的作者 Taylor Otwell 在 _[laracasts](https://laracasts.com/series/laravel-5-from-scratch)_ 的入门教程，听英文教程的时候不想来回切换输入法，因为在中英转换之间，如果漏听了几句关键性的说明，很可能就跟不上作者的思路，保持全英文状态更容易融入课程，所以就索性全部写英文了。不过课后自己总结的一些问题都是写的中文。

还是老规矩，内容不会照搬手册，只是个人笔记，记录的是个人实践后认为需要说明的一些东西，如果没有提到的，一般情况照着手册走就行了。

## Get Ready

#### Globally Install Composer

``` shell
curl -sS https://getcomposer.org/installer | php -- --install-dir=bin --filename=composer

# Or
# php -r "readfile('https://getcomposer.org/installer');" | php

# Or manually rename composer.phar to composer and link the composer binary file to $PATH
```

#### Globally Install Laravel/Lumen

``` shell
composer global require laravel/installer
composer global require "laravel/lumen-installer=~1.0"

vim ~/.zshrc

# add `~/.composer/vendor/bin` to the $PATH
# ...

source ~/.zshrc
```

#### Run Without Special Server

``` shell
laravel new site
# or Lumen: `lumen new site`

cd site
php artisan serve --port 8888
```

##### Notes

- All the shell commands we listed below were in the path of the new laravel project `site`, as the root directory.
- when install laravel via `laravel new proj_name`, it will automatically install the latest active version of laravel. So, if you need some specific version, like 5.1.* LTS, you can via composer:

``` shell
composer create-project laravel/laravel blog "5.1.*"
```

And this will install the lateset version of `5.1.*`, which you typed in this command.

## Blade

- **In layout view**

``` php
<!-- layouts/main.blade.php -->

@yield('title')
@yield('header')
@yield('content')
@yield('footer')

```

- **In normal view**

``` php
<!-- index.blade.php -->

@extends('layouts.main')

@section('title', 'PAGE NAME')
@section('header')
  <!-- header detail -->
  {{ $variable->attr }}

<!-- Include sub view -->
@include('shared.navbar')

@section('content')
  <!-- content detail -->
	@if (...)
		{{ time() }}
	@elseif (...)
    	{{ some php code here }}
	@else
      	{{ php }} or html all ok
  	@endif
      
@section('footer')
  <!-- footer detail -->
@stop
<!-- or @endsection -->
```

More Demos are here: <https://cs.phphub.org/#blade>. and here: <http://laravel-china.org/docs/5.1/blade>.

## Data

#### Pass Data to View

- Normal Array

``` php
Route::get('/', function () {
	
	$data = [1, 2, 3] ;

    return view('welcome', ['data' => $data]);
});
```

- `compact()`

``` php
Route::get('/', function () {
	
	$data = [1, 2, 3] ;

    return view('welcome', compact('data'));
});
```

- `with()`

``` php
Route::get('/', function () {
	
	$data = [1, 2, 3] ;

    return view('welcome')->with('data' => $data);
});
```

- Dynamic Methods

``` php
Route::get('/', function () {
	
	$data = [1, 2, 3] ;

    return view('welcome')->withData($data);
});
```

#### Database Driver

Switch it in config/database.php, find `'default' => env('DB_CONNECTION', 'mysql')`, and choice one db driver that laravel support.

If use sqlite(file based database for small project), please comment the `DB_*` fields in .env file.

Otherwise, like mysql, plese configure right in the general way that we always do.

### Migration: The Version Controll of Database

``` shell
# 1. define the table we want to create
php artisan make:migration create_users_table --create=user

# 2. custom the table structure in /database/migrations/DATATIME_create_user_table.php@up() function

# 3. actual create the table we defined before into our configured database
php artisan migrate

# 4. delete tables if we want modify it, migrate it to others env, etc.
php artisan migrate:rollback
```

In future, if we needs to modify the migration we create before, what we need to do is to `rollback` that migration and modify the `up()` method in migration file, and `re-migrate` it to make new modifications works. And this also helpful for collaboration.

One `rollback` operation only rollback one last migrate record, not one table record, because one migrate operation can migrate many tables at the same time.

When rollbacked, even re-migrated, the actual data of previous table all erased and can not recover any more.

Note that the best practice of naming a new migration should be the words just explained what we want to do.

If we want to create a table for users, we will say 'create users table', so the migration name should be `create_users_table`. Do like this.

#### Tinker

Tinker is the shell that can interact with the laravel application, besides the basic commands support, all the php methods in your laravel project are capable to be executed in Tinker, and in a tinker way to play around.

``` shell
php artisan tinker
DB::table('users')->get();
```

#### Namespace Review

Namespace equals the folder structure.

In some programming languages like PHP, namespace are used to place class, and avoid ambiguity when class name are the same.

In laravel, two ways to locate the class:

- use `use \Path\To\Class;` before the class defination.

- use `\Classname` in the function block.

Note that when you not use any namespaces, PHP will search the class you need in the current namespace.

### Query Builder

It's a tranditional way of laravel to interact with data layer.

No more to explian, just follow the demos in here: <https://cs.phphub.org/#db>, and here: <http://laravel-china.org/docs/5.1/queries>.

### Eloquent: ORM of Laravel

In simple, it's just the `M` layer in the MVC design pattern.

Differently, it's the Active Record implementation in PHP way to interact with the table of database, rather than using the tranditional Query Builder.

The result is, each table has a Model, Model represent the table and table's CRUD options are more convenient and pleasant via Model.

The Steps to use Eloquent in laravel:

###### 1. __Create table via `make:migration`__

###### 2. __Insert data into the table__

###### 3. __Make a Model reference to the table__

``` shell
php artisan make:model Models/User -m # --migration
```

`-m` option only used when we haven't create the table of this model response to.

Notice that here is a rule:

__Model name must be singular, but table name must be plurals.__

For example, model name of table `histories` should be `history`.

###### 4. __Using Model in controller__

For example, we have created a table `users`, and a model `User` in namespace `App\Models\User`, contents of App\Http\routes.php like these:

``` php
Route::get('users', 'Users@index');

# below two routes are same intend, just for explain here
# use only one way if you like
// Will show specific user by id
Route::get('users/{id}', 'Users@show');

// Will show specific user by instance
Route::get('users/{user}', 'Users@show');
```

and we use it in controller Users like these:

``` php
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;

class Users extends Controller
{
	/**
	 * get all users
	 */
	public function index()
	{
		return User::all() ;   // Automatically JSON Response
	}

	/**
	 * 1. show specific user by id
	 * !!! param name `$id` don't have to response to the `{id}` in routes defination
	 */
    public function show($id)
    {
    	$user = User::find($id) ;    // Also JSON if has the result

    	return $user ? $user : 'user not found' ;
    }

    /**
	 * 2. show specific user by instance
	 * !!! param name `$user` must response to the `{user}` in routes defination
	 */
    public function show(User $user)
    {
    	return $user ;    // JSON
    }
}
```

###### 5. Pass the result data to blade views when necessary

Talked before, only notice that `.` is the directory separation, like `view('users.show')` means `resources/views/users/show.blade.php`.

## Defining Relationships with Eloquent

Take one simple example, we have create tables: users, cards, notes.

``` shell
php artisan make:model Models\User -m
php artisan make:model Models\Card -m
php artisan make:model Models\Note -m
```

What we defined in each `up()` method are:

- CreateUsersTable:

  ``` php
  Schema::create('users', function (Blueprint $table) {
              $table->increments('id');
              $table->string('name');
              $table->timestamps();
          });
  ```

- CreateCardsTable:

  ``` php
  Schema::create('cards', function (Blueprint $table) {
              $table->increments('id');
              $table->integer('user_id')->unsigned()->index();
              $table->string('title');
              $table->timestamps();
          });
  ```

- CreateNotesTable:

  ``` php
  Schema::create('notes', function (Blueprint $table) {
              $table->increments('id');
              $table->integer('card_id')->unsigned()->index();
              $table->text('content');
              $table->timestamps();
          });
  ```

Obviously, the relationships between these are: user has cards, cards has notes. Before testing, we migrate them right now:

``` shell
php artisan migrate
```

Ok then. How to define the relationship with Eloquent are basically the same, so we take the _cards has notes_ relationship to explain how it works.

### Get/Query

- No Eloquent

``` shell
php artisan tinker

# 1. Prepare one card whose id = 1
$card = new App\Models\Card;
$card->user_id = 1;    // assume it's 1 here
$card->title   = '1st card';
$card->save();

# 2. Create a note with it's card_id = 1
$note = new App\Models\Note;
$note->card_id = 1;
$note->content = '1st note';
$note->save();
```

Yes, it just manually relate their relationship. It's a little bit inelegant.

- **With Eloquent**

  - 1. define the `notes()` method in Card Model

    ``` php
    public function notes()
    {
    	return $this->hasMany(Note::class);    // either `Note::class` or `\App\Models\Note`
    }

    public function path()
    {
      return '/cards/' . $this->id;
    }
    ```

    If use `\App\Models\Note` then treate as an object, else use `Note::class` will trate as string representation of the class.

  - 2. call the `notes()` method in Card Model instance

    ``` shell
    php artisan tinker

    $card = App\Models\Card::first()

    $card->notes->first()        # as collection
    # Or
    # $card->notes[0]        # as array

    # Or
    # $card->notes()->first()    # as object
    ```

    The difference between `collection` and `object` is in the finnal query sql.

    We can prove this by test:

    ``` shell
    php artisan tinker   # refresh the modifications in project

    # Setup a listener when query event occurs
    >>> DB::listen(function ($query){ echo $query->sql; }

    >>> $card = new App\Models\Card::first()
    select * from "cards" limit 1;

    # First execute the get-card-notes query
    >>> $card->notes
    select * from "notes" where "notes"."card_id" = ? and "note"."card_id" is not null

    # Re-execute the same query
    >>> $card->notes
    # You will see nothing here because the last query result is stored in the $card object and fetch them directly from object rather than the database

    # Check if the $card object has the notes relationships here
    >>> $card
    # You will see the notes of this card here

    # Check if notes relationship still exists after we refresh the object
    >>> $card->fresh()
    select * from "cards" where "id" = ? limit 1
    >>> $card
    # You wil note see the notes of this card here any more because the resuls cache are erased after `fresh()`

    # If we use as collection, the sql of get first note record might be like this:
    >>> $card->fresh()->notes->first()
    select * from "cards" where "id" = ? limit 1
    select * from "notes" where "notes"."card_id" = ? and "notes"."card_id" is not null

    # If we use as object method, the sql of get first note record might be like this:
    >>> $card->fresh()->notes()->first()
    select * from "cards" where "id" = ? limit 1
    select * from "notes" where "notes"."card_id" = ? and "notes"."card_id" is not null limit 1
    ```

  You will see that with eloquent it is much clear and natual, just like what we said.

### hasMany and belongsTo

Moreover, we can say that note also belongs to card. So here is how eloquent deal with this `belongs-to` relationship here.

- 1. define the `card()` method in Note Model:

  ``` php
  public function card()
  {
    return $this->belongsTo(Card::class);
  }
  ```

- 2. call the `card` method in Note Model instance

  ``` shell
  php artisan tinker
  $note = App\Models\Note::first()
  $note->card
  ```

  **Be aware that:** `hasMany()` related method is `notes()`, which is resonable because one card can has many notes, and `belongsTo()` related method is `card()`, which is resonable because one note only belongs to one card.

  You shouldn't create the method name as `note()` or `cards()`, because it's does make any sense, and must response to what method you have called in it.

  Remember that `hasMany()` related to plurals methods, and `belongsTo()` related to singular methods.

###  Create/save

``` shell
php artisan tinker

# Create a new Note
$note = new App\Models\Note
$nots->content = 'second note'

# Decide which card to associate with
$card = App\Models\Card::first()

# Save a exists note by `save()`
$card->notes()->save($note)    # we cann't use $card->notes->save() here

# Or create a new note by `create()`
$card->notes()->create(['content'=>'third note'])    # make sure that `content` is fillable in Note Model

# See the new note associated with first card
$card->notes
# Yout will see new note was already in $card object
```

Notice that here we didn't set the card_id for $note, because eloquent automatically did this for us.

**When we creating new record, we cann't use collections any more, because it is only used for query-like operations.**

Besides, when we directly create a new note by `create()`, the fields of notes table, here only the `content`, must be fillable in Note Model protected attributes `$fillable` defination, or we cann't create success. Like this: 

``` php
class Note extends Model
{
	protected $fillable = ['content'];
	
    public function card()
    {
    	return $this->belongsTo(Card::class);
    }
}
```

And as we all know, it's a protected way to avoid attacks in laravel.

## Form

Based on the examples before, suppose we have a resources/views/cards/show.blade.php here:

``` html
@extends('layouts.main')
@section('title', 'show card')
@section('content')

<div class="row">
	<div class="col-md-6">
		<h1>
			{{ $card->title }}
		</h1>

		<ul class="list-group">
			@foreach ($card->notes as $note)
				<li class="list-group-item">
					{{ $note->content }}
				</li>
			@endforeach
		</ul>
		<hr>
		<h2>
			Add More Notes Here
		</h2>

		<form method="POST" action="/cards/{{ $card->id }}/notes">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="form-group">
				<textarea name="content" class="form-control"></textarea>
			</div>

			<div class="form-group">
				<button type="submit" class="btn btn-primary">
					Add Note
				</button>
			</div>
		</form>

	</div>
</div>

@stop
```

And our routes.php:

``` php
Route::post('cards/{card}/notes', 'Notes@store');
```

And controller Notes@store: 

``` php
public function store(Request $request, Card $card)
    {
        // return $request->all();
        // return \Request::all();
        // return request()->all();
        // return $card;

  		# 1
        // $note = new Note;
        // $note->content = $request->content;
        // $card->notes()->save($note);

        # 2
        // $card->notes()->save([
        //     new Note([
        //         'content' => $request->content
        //         ])
        //     ]);

        # 3
        // $card->notes->create([
        //         'content' => $request->content
        //     ]);
        
        # 4
        $card->notes()->create(request()->all());      
  
        // return \Request::to('path/to/new/uri');
        // return redirect()->to('path/to/new/uri');

        return back();
    }
```

__Be careful that: __

- __If you want use eloquent as an instance, the param name `$card` must equals to the route catch name `{card}`, here `card` equals `card`. Or $card in method will be nothing, which is unexpected.__
- __When we use `create()`to insert an relationship, we must make sure that we had stipulated the `$fillable` in related model, as we talked before__.

Since card and note has relationship here, so we can add a method to describle more clearer what we want to do, we can add a method `addNote()` in model Card, like this:

``` php
public function addNote(Note $note)
{
 	 $this->notes()->save($note);
}
```

Finally, our controller Notes@store():

``` php
public function store(Request $request, Card $card)
{
  	$card->addNote(
                new Note($request->all())
            );
	return back();
}
```

### RESTful Principles Review

If the verb of the HTTP request is one of these: GET, POST, PATCH/PUT, DELETE, it's no need to add more verbs in the URI and route rules, but if we want do something like edit, update or some meaningful options, it's fine we add this verd into URI.

And the whole explainations of RESTful interfaces are in this table:
<http://laravel-china.org/docs/5.1/controllers#restful-resource-controllers>

Besides, since the browser only understand the GET and POST method directly, so to follow the RESTful convention we can trick like this: 

``` html
<form method="POST">
  {{ method_field('PATCH | DELETE | PUT') }}
  <!-- which equals to: -->
  <!-- <input type="hidden" name="method" value="PATCH"> -->
  <!-- more form elements -->
</form>
```

### Update And Eager Loading

Eager loading is an effective way to avoid copious database query which the object data can be get at one query through the relationship of laravel Model we created.

Before we using eager loading, if we want to get both the notes of one card, and the user this card belongs to, we may query N+1 times like this (__`notes` table must has fileds named with `user_id` and `card_id`__):

``` php
public function showByInstance(Card $card)
{
	return $card->notes[0]->user;
}
```

If we do like this, we want to get the notes and the user associated with this card, but we must query two times for both of them here, which is inefficient. 

And with eager loading, we can organize this two queries into one, like this:

``` php
public function showByInstance(Card $card)
{
  return $card->load('notes.user');    // notes means Card@notes() user means Note@user()
  # Or
  // return $card->with('notes.user');
  
  # Or only load notes of this card
  // return $card->load('notes');
  // return $card->with('note')->find(1);    // 1 means this id of the card we want to find
}
```

### Validation

- Form input

``` php
public function addNote(Request $request, Card $card)
{
  $this->validate($request,[
    'content' => 'required|min:10'
  ]);     // Controller => ValidatesRequests@validate
}
```

- CSRF: See detial in FAQ.
- Middleware

``` php
Route::group([
	'middleware' => ['web'],
], function () {
	Route::post('cards/{card}/notes', 'Cards@addNote');
	// Route::post('cards/{card}/notes', 'Notes@store');
  	// More routes
});
```

- Errors

Every views can share the $errors variable.

- `old('field_name')`

When form check fails, the preview wrong data are `withInput()` in the redirect, and we can put it into the right elements.

### FAQ

- [php artisan migrate:reset failed to open stream: No such file or directory](http://stackoverflow.com/questions/25237415/php-artisan-migratereset-failed-to-open-stream-no-such-file-or-directory)?

``` shell
composer dump-autoload
```

- [TokenMismatchException in VerifyCsrfToken.php Line 67](http://stackoverflow.com/questions/34866404/tokenmismatchexception-in-verifycsrftoken-php-line-67)?

``` html
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<!-- Or -->
{{ csrf_field() }}

<!-- Or set into meta tag -->
<meta name="csrf-token" content="{{ csrf_token() }}">
```

- [What's the differences between PUT and PATCH?](https://laracasts.com/discuss/channels/general-discussion/whats-the-differences-between-put-and-patch?page=1)

```
PUT = replace the ENTIRE RESOURCE with the new representation provided (no mention of related resources in the spec from what i can see)

PATCH = replace parts of the source resource with the values provided AND|OR other parts of the resource are updated that you havent provided (timestamps) AND|OR updating the resource effects other resources (relationships)
```



#### Reset and Refresh Commands list

- `php artisan clear-compiled`: Remove the compiled class file

- `php artisan auth:clear-resets` : Flush expired password reset tokens

- `php artisan cache:clear`: Flush the application cache

- `php artisan config:clear`: Remove the configuration cache file

- `php artisan migrate:refresh`: Reset and re-run all migrations

  __Same with `migrate:rollback`, they will erase the data in database.__

- `php artisan migrate:reset`: Rollback all database migrations

- `php artisan queue:flush`: Flush all of the failed queue jobs

- `php artisan queue:forget`: Delete a failed queue job

- `php artisan route:clear`: Remove the route cache file

- `php artisan view clear`: Clear all compiled view files

## References

- <https://laracasts.com/series/laravel-5-from-scratch>

- <https://laravel.com/docs/5.2>

- <http://laravel-china.org/docs/5.1/>

- <https://cs.phphub.org/#routing>

- <https://en.wikipedia.org/wiki/Laravel>

- <http://lumen.laravel-china.org/docs/5.1/installation>

- <http://code.tutsplus.com/tutorials/your-one-stop-guide-to-laravel-commands--net-30349>

