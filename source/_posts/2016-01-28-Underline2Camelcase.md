---
layout: post
title: 下划线和驼峰式命名风格之间的相互转换
category: PHP
tags: [PHP, 下划线命名, 驼峰命名法]
---

编程语言中对变量的命名主要有两种风格：一种是下划线法，一种是驼峰法 ( 也称匈牙利法 ) 。

<!-- more -->

``` php
<?php

/**
* 将下划线风格的命名转换为驼峰式 和 将驼峰式命名转换为下划线风格
*/

function underline2camelcase( $underline_str ) {
	if ( !is_string( $underline_str ) ) {
		return false ;
	}
	$str_arr = str_split( $underline_str ) ;
	$len = count( $str_arr ) ;

	if ( '_' != $str_arr[ 0 ] ) {
		$str_arr[ 0 ] = strtoupper( $str_arr[ 0 ] ) ;
	}
	foreach ( $str_arr as $key => $val ) {
		if ( '_' == $val ) {
			# 逻辑比较有先后顺序, 对于 `&&` 来说, 如果前面的条件满足后, 便不再进行后面的判断
			if (  $key<$len-1 && '_' != $str_arr[ $key+1 ] ) {
				$str_arr[ $key+1 ] = strtoupper( $str_arr[ $key+1 ] ) ;
			}
			$str_arr[ $key ] = '' ;
		}
		# 拼接字符串的效率和 implode() 函数的速度相比较慢, 因此不用这种方式
		// $camelcase_str .= $str_arr[ $key ] ;
	}
	return implode( '' , $str_arr ) ;
}

function camelcase2underline( $camelcase_str ) {
	if ( !is_string( $camelcase_str ) ) {
		return false ;
	}
	$str_arr = str_split( $camelcase_str ) ;
	foreach ( $str_arr as $key => $val ) {
		if ( 0 == $key ) {
			$str_arr[ 0 ] = strtolower( $val ) ;
		} else {
			if ( is_upper( $val ) ) {
						$str_arr[ $key ] = '_'.strtolower( $val ) ;
					}
		}
	}
	return implode( '', $str_arr ) ;
}

/**
* 判断一个字符是否为大写
* 这里必须是单个字符而不能是字符串
*/
function is_upper( $char ) {
	# 如果传入的字符是一个字符才进行下一步判断
	if ( is_string( $char ) ) {
		$str_arr = str_split( $char ) ;
	} else {
		return -1 ;
	}
	# 如果传入的参数是单个字符才进行下一步判断
	if ( 1 == count( $str_arr ) ) {
		$flag = 0 ;
		# 可以通过 ASCII 码和正则表达式来判断
		$ascii = ord( $char ) ;
		# A-Z 的 ASCII 码取值是: 65~90; a-z 的 ASCII 码取值是: 97~122; 
		if ( ( 64 < $ascii && $ascii < 91 ) || ( preg_match( '/[A-Z]/' , $char ) ) ) {
			$flag = 1 ;
		}
		return $flag ;
	} else {
		return -2 ;
	}
}

# 测试

$underline_str1 = 'open_____door_____ok___' ;
$underline_str2 = '______make___by_id______' ;

$camelcase_str3 = 'GGiGGetElementByIdDDDD' ;

$res1 = underline2camelcase( $underline_str1 ) ;
$res2 = underline2camelcase( $underline_str2 ) ;
$res3 = camelcase2underline( $camelcase_str3 ) ;

$string = <<< RES
	###### 测试 1 - \t 将下划线风格的命名转换为驼峰式命名

	下划线风格命名为: \t {$underline_str1}
	转换为驼峰法风格后的命名为: \t {$res1}

	下划线风格命名为: \t {$underline_str2}
	转换为驼峰法风格后的命名为: \t {$res2}

	###### 测试 2 - \t 将驼峰式风格的命名转换为下划线风格

	驼峰风格命名为: \t {$camelcase_str3}
	转换为下划线风格后的命名为: \t {$res3}
RES;

echo $string ;
```

上述函数的运行结果如下：

```
###### 测试 1 - 	 将下划线风格的命名转换为驼峰式命名

下划线风格命名为: 	 open_____door_____ok___
转换为驼峰法风格后的命名为: 	 OpenDoorOk

下划线风格命名为: 	 ______make___by_id______
转换为驼峰法风格后的命名为: 	 MakeById

###### 测试 2 - 	 将驼峰式风格的命名转换为下划线风格

驼峰风格命名为: 	 GGiGGetElementByIdDDDD
转换为下划线风格后的命名为: 	 g_gi_g_get_element_by_id_d_d_d_d
```
