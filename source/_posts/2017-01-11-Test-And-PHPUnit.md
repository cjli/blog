---
layout: post
title: 开发流程规范化之：软件测试
category: Programming
tags: [单元测试, PHPUnit, PHP, Xdebug]
---

说来有点像借口，上面给的开发时间一直很紧（keng），大部分时间属于边上线边改那种类型的，有空又去研（zhe）究（teng）其他的东西了，导致项目中一直没写过单元测试。Anyway，先面壁三秒钟。

<!-- more -->

嗯，总之今天先总结下，以后团队应该要搞上去了。

## 测试，是软件开发的重要一环

测试本应是完整软件开发流程中的一个重要组成部分，开发前，中，后都需要进行测试。

### 认为不需要测试的原因

- “浪费”过多时间，换来的价值不高，开发进度负担不起
  - 程序员的一时懒惰：一时懒惰，得到的是各种各样的被忽略的 bug，其实最终浪费了更多时间。
  - 老板的短视，团队开发流程的不规范和低效


- 测试工具太多，不知道选哪个，学习成本高
  - 没有学习意识
  - 没有投资概念：学习测试工具是在为项目后期投资，而且肯定有回报：系统的可靠性；自信。
- 开发者不知道测试这一个概念：这就尴尬了，需要多看书补补基础。

> 如果只知道一味地敲代码，看不见其他同样重要的东西，永远只能是低级码农。
>
> 同时注重软件质量、设计思想的才是一名合格的程序员。
>

### 为什么需要测试？

很简单：为了确保应用能运行、持续运行，并且运行结果在我们掌握之中的。

从长期的角度看，测试换来的是：

- 节省时间，节省资金
- 项目健壮性，更少的问题
- 自信和良好习惯

如果你在把代码拉到线上环境的时候会感到不靠谱，甚至是紧张，那么基本上肯定会出问题，这也反映了你的代码是没有经过充分测试过的。反之，如果你对自己的测试自信，那么写代码的时候也会更自信。

没有经过充分测试过的代码不能部署到线上缓解，这是作为程序员的责任感体现。

### 什么时候测试？

编码前的流程设计阶段，编码阶段，编码结束之后，都需要测试。

#### 交差(chai)型程序员

指的是只是把测试当作不得不完成的任务，而非真心想要通过测试来改良代码质量的人。

他们通常在代码写完的最后，从测试结果中匆忙拉几个成功用例给管理者检查，或者说服自己，然后结束一天。

> *Somehow, 感觉「翻译味」贼浓，我感觉我达到了中英切换之走火入魔境界...*
>
> *Well, so tm what, my blog, my place, my rules. (:*

#### 编码前

安装、配置好测试工具，把测试工具当作开发应用时的重要依赖，选择业内人士评价高的工具即可。

Laravel 初始化之后就已经集成了 PHPUnit，因此，可以很快的通过这个流程。

#### 编码中

在开发每个模块的时候，比如新建了一个类，则同时需要为这个模块编写单元测试，运行测试。

及时测试，及时反馈，及时修改，稳扎稳打。

编码过程中重构的代价是最低的，特别是如果需要重构那些破坏现有功能的新代码。

> 软件开发真的很像盖房子，底层出问题，上层基本上要垮掉，然后重来。而不理解这个过程的人往往都自认为和步步高点读机一样，哪里不行改哪里。
>
> 这就是沟通成本。

#### 编码后

因为编码过程中不可能测试到系统的所有内容，有些问题往往是在某些特别的运行情景下才能发现的。

这时候需要为这个新问题写一个测试用例，这个新测试用例通过后，这个新问题基本上才能被解决。

注意，测试用例不是写完就了了的，而是和系统本身一样，是在持续改善的。因此，**更新代码的同时，也要更新相关的测试用例。**

### 测试什么？

#### 单元测试

测试系统的每个组成部分。

对于 PHP 应用来说，每个系统都有类、方法和函数，因此对于每个类，每个方法／函数，都需要跑单元测试。

#### 黑盒测试

黑盒测试测试的目的：验证应用的功能是否能用，因此也称功能测试。

对每个最小的组成部分单元测试通过后，并不能 100% 保证从整个系统的角度上不出现问题。

因此需要从使用者的角度，宏观地测试整个系统。该过程通常由自动化测试工具来完成。

## 如何测试？

前戏啰嗦清楚了，下面进入重头戏。How exactly？

测试的类型分很多种，但是它们之间并不互斥。下面就 from the point of view of a PHPer 总结下常用的测试套路。

### 单元测试

单元测试：把每个类、方法、函数孤立出来单独测试，看是否符合我们预期。

通过单元测试是保障整个系统中工作正常的前提，但不是充要条件。反之必出问题。

#### 为什么要使用自动化工具？

举个例子，假设都在开发同一个功能模块，你如何确保你一周后写的代码不会破坏一周前的代码？

有了单元测试，每个单元的测试用例是被版本控制了的，这样的好处是，一周后，你可以继续跑之前单元的测试，如果通过，则基本上认为，现有代码没有影响到之前的代码。

那么完成这个过程，显然不能用人工来完成。一个模块少则几个，多则几十个单元，如果项目较大，要跑的测试将不是人该做的事情。因此，自动化工具最适合完成这种任务。

#### PHPUnit

你或许听过这货，也在 Laravel 等现代 PHP 框架中见过，之所以这么流行，因为 PHPUnit 是 PHP 单元测试框架中的事实标准，其他出名点的还有 PHPSpec 等，再其他就使用得很少了。

总之，既然要学当然学大家都认可的，因为可以参考的讨论和资料都多，也经过大家多年的考验可靠性很高。

##### 测试流程

单元测试组成测试用例，那些有关联的测试用例将组成一个测试组，PHPUnit 使用 test runner 运行测试组，test runner 输出测试结果。

> - 一般大项目才会有很多测试组。
>
>
> - test runner 在 cli 下运行。

##### 测试原理

PHPUnit 测试原理直接参考 《Moder PHP》 的说明：

- A test case class name must end with `Test`, and its filename must end with `Test.php`.
- A test case is a single PHP class that extends the `PHPUnit_Framework_TestCase` class. 
- Each test case contains public methods whose names begin with `test`.
- These methods are individual tests that assert specific scenarios to be true.
- Each assertion can pass or fail.
- You want all assertions to pass.

以 Laravel 来进行说明：

Laravel 的 PHPUnit 单元测试默认都放在 /tests/ 下，/tests/ 下的每个类属于一个测试用例，这些类的每个方法（`test`开头）都属于单独测试对象。/tests/ 下的所有测试用例属于一个测试组。

其中默认的 TestCase.php 和 ExampleTest.php 是一个关联测试用例。

每个单独测试对象，使用断言去验证已给的条件。每个断言要么通过要么不通过。

和 /tests 同级的还有其他源码（比如如果是 Composer 组件的话则一般是 /src 目录)、composer.json，以及phpunit.xml。

phpunit.xml 对 PHPUnit 是必须的，里面保存的是 test runner 的配置细节。

> This configuration file also lets us apply the same PHPUnit settings on remote continuous testing servers like Travis CI. 

##### 安装配置 PHPUnit

``` shell
composer require --dev phpunit/phpunit    # --dev 选项对于 phpunit 来说一般是默认要选择的
```

配置 PHPUnit 主要就是配置 phpunit.xml。

- bootstrap 属性

指明运行 test runner 前需要引入的东西。

对于 Laravel 来说，这个属性值对应的文件就是整个框架的 Composer 依赖，PHPUnit 只有引入该启动文件后，在测试的时候才能使用到需要的依赖。

> 测试前确保 Composer 依赖已经安装。

- testsuite 标签

指明需要跑的测试组文件所在路径。

- whitelist 标签

告诉 PHPUnit 只需要为这个路径下的源码生成代码覆盖。

##### 安装 Xdebug

PHPUnit 跑单元测试，Xdebug 则分析并生成代码覆盖率等信息。可以查看 PHP 扩展相关信息：

``` shell
# 查看当前 PHP 扩展的编译路径
php-config --extension-dir
# Or
php -i | grep extension_dir

# 查看是否安装 xdebug
php -m | grep xdebug
```

如果是 Linux：

- 如果是用软件包管理安装的 PHP，则也使用包管理器安装 Xdebug ：

``` shell
# CentOS
sudo yum -y --enablerepo=epel,remi,remi-php56 install php-xdebug

# Debian
sudo apt-get install php5-xdebug
```

- 如果是从源码安装的 PHP，则使用 PECL 安装 Xdebug：

``` shell
pecl install xdebug
```

然后更新 php.ini，加载编译好的 xdebug.so 路径。以 Mac 默认 PHP 启用 Xdebug 为例说明：

``` shell
sudo vim /etc/php.ini
```

找到 *Dynamic Extensions* 后追加：

``` ini
zend_extension="/path/to/xdebug.so"
```

注意，**对于 xdebug 扩展的引入需要使用 `zend_extension` 而不是 `extension`**。

重启 Apache 即可：`sudo apachectl restart`。

##### 使用 PHPUnit 进行单元测试示例

- 测试对象 /src/Demo.php

``` php
class Demo
{
  private $attr;    // Assume unique required
  
  public function __construct($attr = 'demo')
  {
    $this->attr = mb_strtolower($attr);
  }
  
  public function getAttr()
  {
    return 'The unique attr of class Demo is: '.$this->attr;
  }
  
  public function setAttr($attr)
  {
    $attr = mb_strtolower($attr);
   	if ($attr == $this->attr) {
      throw new Exception(
        sprintf(
          'Warning: %s is already set.',
          $this->attr
        )
      );
   	}
	$this->attr = $attr;

    return 'success';
  }
}
```

- 测试用例 /tests/DemoTest.php

在编写测试用例之前，必须先清楚我们要测试的每个独立对象的可能性有什么，或称「API 表现」。

在测试用例 Demo.php 中，对于测试对象 `__construct()` 来说，要保证它接受的参数会成为唯一的属性值；对于 `	getAttr()` 来说，要保证其返回值为类的唯一属性值。

对于 `setAttr` 来说，要保证两种情况下的如期返回：如果要设置的属性值已经存在，则会抛出一个异常，反之返回 success。

**知道每个方法要做什么，有哪些可能性后，才能开始写单元测试。**

``` php
// require_once dirname(__DIR__).'/src/Demo.php';

class DemoTest extends PHPUnit_Framework_TestCase
{
  public function testSetUniqueAttrInConstructor()
  {
    $demo = new Demo('cjli');
    // 断言 $demo 对象的 `attr` 属性等于 'cjli'
    $this->assertAttributeEquals('cjli', 'attr', $demo);
  }
  
  public function testGetAttrReturn()
  {
    $demo = new Demo('cjli');
    $this->assertEquals('The unique attr of class Demo is: cjli', $demo->getAttr());
  }
  
  public function testSetAttrSuccess()
  {
    $demo = new Demo('cjli');
    $option = 'cjli2';
   	$this->assertEquals('success', $demo->setAttr($option));
  }
  
  /**
   * @expectedException Exception
   */
  public function testSetAttrException()
  {
    $demo = new Demo('cjli');
    $option = 'cjli';
   	$demo->setAttr($option);
  }
}
```

> laravel 直接创建测试类 artisan 命令：`php artisan make:test DemeTest`。

###### 测试的孤立性

为什么上面例子中的 `testSetUniqueAttrInConstructor()` 方法没有使用 Demo 类的方法 `getAttr()` 呢？不都是为了获得 `$demo` 对象的 `attr` 属性值吗？

这就是单元测试的孤立性：编写单元测试的时候，必须把需要测试的对象孤立出来，而不能依赖要测试的其他对象。

> The PHPUnit assertion `assertAttributeEquals()` receives three arguments. The first argument is the expected value; the second argument is the property name; and the final argument is the object to inspect.
>
> What’s neat is that the `assertAttributeEquals()` method can inspect and verify protected properties using PHP’s reflection capabilities.
>
> The `assertAttributeEquals()` assertion lets us inspect the object’s internal state without relying on a separate, untested getter method.

对于第 4 个单元测试，通过的条件是它抛出一个异常，失败的条件是没有。`@expectedException` 注释必须要，用于控制单元测试。

- 运行测试

运行的要求是所有单元测试都通过。在项目根目录下执行：

``` shell
vendor/bin/phpunit -c phpunit.xml
```

对于这个例子来说如果成功将会输出：

```
PHPUnit 4.8.31 by Sebastian Bergmann and contributors.

....

Time: 87 ms, Memory: 6.00MB

OK (4 tests, 4 assertions)
```

假设改变测试方法  `testSetAttrException()` ，故意使其不通过：

``` php
/**
 * @expectedException Exception
 */
public function testSetAttrException()
{
  $demo = new Demo('cjli');
  $option = 'non-cjli';
  $demo->setAttr($option);
}
```

重新测试则会输出：

``` 
PHPUnit 4.8.31 by Sebastian Bergmann and contributors.

...F

Time: 91 ms, Memory: 6.00MB

There was 1 failure:

1) DemoTest::testSetAttrException
Failed asserting that exception of type "Exception" is thrown.

FAILURES!
Tests: 4, Assertions: 4, Failures: 1.
```

可很清楚那些测试没通过，以及没通过的原因。

以上就是使用 PHPUnit 进行单元测试的基本流程。

### 代码覆盖

人是不靠谱的。

你不可能做到一丝不漏的为系统代码的每个片段都写过单元测试，总会因为一些因素：懒、忘了、遗漏等等不可靠因素导致代码没有测试完全。而相比下，程序来做这件事就显得靠谱很多。

源代码被测试可以理解为代码被测试覆盖，因此，代码覆盖描述的就算源代码被测试的程度。测试过的单元占系统总单元数的比例就是代码覆盖率。

PHPUnit 提供了代码覆盖率报告工具，在运行单元测试的时候可以带上 `--coverage-html` 选项来生成代码覆盖报告：

``` shell
./vendor/bin/phpunit -c phpunit.xml --coverage-html coverage    # coverage 为保存路径
```

测试运行结束后，项目根目录下就会有一个 coverage 目录，里面就是本次测试详细的测试报告。

打开 coverage/index.html 中的内容后，我们期望看到的是满满的绿色和 100%，但是却不能硬性规定必须达到 100%。因为不同的项目，代码覆盖率为多少才合适是很主观的，需要视情况而定。

总之，代码覆盖率的作用是指导你改善你的代码，而非要成为一个强制性规定。

### 测试驱动开发

即所谓的 TDD（Test Driven Development），其父概念都是一些听起来高大上的方法学名词：极限编程（Extreme programming）、敏捷开发（Agile software development）等等。这些概念都有几本书的专门介绍，需要的自行了解。

Anyway，简而言之，TDD 的基本概念是：先写好单元测试，然后编码实现功能。

测试用例，一个接一个，从不通过到最终通过的过程，就算测试驱动开发。

> 测试驱动开发是戴两顶帽子思考的开发方式：先戴上实现功能的帽子，在测试的辅助下，快速实现其功能；再戴上重构的帽子，在测试的保护下，通过去除冗余的代码，提高代码质量。测试驱动着整个开发过程：首先，驱动代码的设计和功能的实现；其后，驱动代码的再设计和重构。

#### TDD 的优点

- 帮助你有目的性的编程

可以提前提醒你自己知道你要做的是什么，以及要做成什么样子。

- 快速迭代性

TDD 是一个持续的过程，这个任务做完可以很快继续下一个，节奏感强。

- 避免开发前的过度设计

过度设计个人认为是不科学的，因为不可能一来就设计得完美无瑕。软件开发不应该由病态的完美主义者驱动，不应该一来就大而全，做什么完美的宏观规划等，而是一点一点，一个测试一个测试地通过，以此构建出来的。

有时候快速反馈更重要。

#### TDD 的负面评价

中心放到了测试的代码上，而忽略了实际需求的实现。（可用结对编程解决）

## FAQ

- **What's the 'dead code' in phpunit code coverage report?**

> XDebug parsing issue, so the closing brace is not executed after the return statement since the code leaves that function and does not actually execute the closing brace. This is a known issue in the XDebug/PHP/PHPUnit parsing/executing space. ([see here](http://stackoverflow.com/questions/25721574/phpunit-why-is-this-a-dead-code))

## 参考

- *Modern PHP: Chapter 10. Testing*
- *[PHPUnit Manual](https://phpunit.readthedocs.io/zh_CN/latest/index.html)*
- *[PHPUnit annotations that can control a given test.](https://phpunit.de/manual/current/en/appendixes.annotations.html)*
- *[PHP Warning: Xdebug MUST be loaded as a Zend extension](stackoverflow.com/questions/26221690/php-warning-xdebug-must-be-loaded-as-a-zend-extension)*

