---
layout: post
title: 在 macOS 上给树莓派安装 Linux 
category: 折腾 
tags: [Linux, 树莓派]
---

今天整理东西发现了吃灰很久的树莓派 2，觉得应该还有点利用价值（破个Wi-Fi啥的）才对。

<!-- more -->

本文简单记录下在 macOS 上给 Raspberry Pi 2 安装 Kali 和 CentOS 的过程和注意事项。

## 准备工作

- Raspberry Pi 2
- RJ45 网线一根
- 5V 1A micro USB 充电器和数据线
- 8G+ TF 卡/micro SD 卡及其 USB 读卡器
- 无线路由器以 DHCP 方式分配联网设备的 IP

## 安装过程


- 下载 Linux 镜像，一定要选择 ARM  CPU 架构下的树莓派专用镜像

  - Kali Linux for Raspberry Pi 2：[Kali Linux ARM Images](https://www.offensive-security.com/kali-linux-arm-images/#1493408272250-e17e9049-9ce8)。
  - CentOS minimal：[Download - CentOS Wiki](https://wiki.centos.org/Download)。找到 AltArch Releases 下面的 `Minimal image for RaspberryPi 2/3`。

Kali 没有针对树莓派提供 minimal 镜像，因此系统安装后占用的空间比较大，如果决定使用 Kali 则 TF 卡容量要买大点的 32G+。


- 将 TF 卡插入 USB 读卡器，再把读卡器插入 macOS 的 USB 端口
- 查看磁盘列表，找到 TF 卡挂在到 macOS 上的路径。
```shell
diskutil list
```

我这里的挂载路径是 `/dev/rdisk3`。

- 取消挂载

``` shell
sudo diskutil unmountDisk /dev/disk3
```

注意不要直接在 Finder 点弹出 SD 卡的 USB 读卡器。

- 刻录镜像到 TF 卡（别刻错目标路径了～）

``` shell
sudo dd if=/path/to/kali-linux-rpi2.img of=/dev/rdisk3 bs=4m
```

当下载的 Linux 镜像很大时，一定要加  `bs=4m` 不然刻录会很慢，或者至少 `bs=2m`。

注意：在 macOS 下用 `dd` 时 `bs` 的单位是小写的  `m`，而不是 `M`。

- 镜像写入完成后，弹出 TF 卡

``` shell
sudo diskutil eject /dev/rdisk3
```

- 拔出 TF 卡，插入树莓派，插上网线，最后通电

这里注意要确保树莓派网线连接的网络和 macOS 是同一个局域网（网段相通）。否则后面 macOS SSH 连不上。

- 等待树莓派系统启动和初始化，等路由器管理界面出现新的连接设备后（如 kali）

除了在路由器管理页面查看树莓派的 IP，还可以使用 `arp -a` 命令扫描局域网内的主机来查看。

- 在 macOS 上 SSH 到树莓派的 IP

Kali 安装完成后默认账号密码为：`root`/`toor`；CentOS 为 `root`/`centos`。

- Linux 系统更新

``` shell
# Kali
apt-get update    # 取回更新的软件包列表信息

# CentOS
yum update
```

## CLI 方式让树莓派连接无线网络

下面以 Kali 进行演示。

### 无线网卡驱动

首先，你有一个比较通用的无线网卡和比较新的 Linux 内核版本，最好是免驱的，不然又要折腾半天网卡驱动。

> 大学时期折腾低版本内核和冷门网卡驱动的痛苦，记忆犹新。

将无线网卡插入树莓派 2 的 USB 接口，查看是否驱动成功：

``` shell
root@kali:~# lsusb
Bus 001 Device 005: ID 0bda:8812 Realtek Semiconductor Corp. RTL8812AU 802.11a/b/g/n/ac 2T2R DB WLAN Adapter
Bus 001 Device 003: ID 0424:ec00 Microchip Technology, Inc. (formerly SMSC) SMSC9512/9514 Fast Ethernet Adapter
Bus 001 Device 002: ID 0424:9514 Microchip Technology, Inc. (formerly SMSC) SMC9514 Hub
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
```

我这里用的无线网卡是 Realtek RTL8812AU，输出包含了表示网卡驱动成功。`ifconfig` 中也可以看到多了一个网络接口：

```shell
root@kali:~# ifconfig
wlan0: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        ether b6:d7:92:85:58:48  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```

由于是刚刚插入网卡，系统未连接过任何 WI-FI，因此 `wlan0` 尚未获得 IP 地址（无 `inet`/`inet6`）。

### 扫描附近可用 Wi-Fi

无线网络配置命令，旧的 `iwconfig` 已被弃用，现如今使用 `iw` 比较多。

- 查看当前已经连接到系统的 Wi-Fi 适配器（网卡）

``` shell
root@kali:~# iw dev
phy#0
	Interface wlan0
		ifindex 3
		wdev 0x1
		addr c2:da:61:45:45:46
		type managed
		channel 1 (2412 MHz), width: 20 MHz, center1: 2412 MHz
		txpower 18.00 dBm
```

- 查看无线网卡接口的启用状态

``` shell
root@kali:~# ip link show wlan0
3: wlan0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether b6:2d:d7:90:c6:28 brd ff:ff:ff:ff:ff:ff
```

这里 `<BROADCAST,MULTICAST,UP,LOWER_UP>` 中包含 `UP` 代表无线网卡接口已启用。如果没启用使用 `ip link set wlan0 up` 启用即可。

- 扫描网卡 `wlan0` 附近 Wi-Fi 列表

``` shell
root@kali:~# iw wlan0 scan
BSS 74:12:bb:88:06:81(on wlan0)
	TSF: 25195431438 usec (0d, 06:59:55)
	freq: 2432
	beacon interval: 100 TUs
	capability: ESS Privacy ShortSlotTime RadioMeasure (0x1411)
	signal: -55.00 dBm
	last seen: 0 ms ago
	Information elements from Probe Response frame:
	SSID: ChinaNet-759d
	Supported rates: 1.0* 2.0* 5.5* 11.0* 18.0 24.0 36.0 54.0
	DS Parameter set: channel 5
	ERP: Barker_Preamble_Mode
	Extended supported rates: 6.0 9.0 12.0 48.0
	RSN:	 * Version: 1
		 * Group cipher: TKIP
		 * Pairwise ciphers: CCMP TKIP
		 * Authentication suites: PSK
		 * Capabilities: 16-PTKSA-RC 1-GTKSA-RC (0x000c)
	BSS Load:
		 * station count: 2
		 * channel utilisation: 53/255
		 * available admission capacity: 0 [*32us]
	HT capabilities:
		Capabilities: 0x83c
			HT20
			SM Power Save disabled
			RX Greenfield
			RX HT20 SGI
			No RX STBC
			Max AMSDU length: 7935 bytes
			No DSSS/CCK HT40
		Maximum RX AMPDU length 65535 bytes (exponent: 0x003)
		Minimum RX AMPDU time spacing: 8 usec (0x06)
		HT RX MCS rate indexes supported: 0-15
		HT TX MCS rate indexes are undefined
	HT operation:
		 * primary channel: 5
		 * secondary channel offset: no secondary
		 * STA channel width: 20 MHz
		 * RIFS: 0
		 * HT protection: no
		 * non-GF present: 1
		 * OBSS non-GF present: 0
		 * dual beacon: 0
		 * dual CTS protection: 0
		 * STBC beacon: 0
		 * L-SIG TXOP Prot: 0
		 * PCO active: 0
		 * PCO phase: 0
	Extended capabilities:
		 * Extended Channel Switching
		 * BSS Transition
		 * Operating Mode Notification
	WPS:	 * Version: 1.0
		 * Wi-Fi Protected Setup State: 2 (Configured)
		 * Response Type: 3 (AP)
		 * UUID: e2e36e1a-4247-e85e-87e2-2819aaa69aa1
		 * Manufacturer: Broadcom
		 * Model: Broadcom
		 * Model Number: 123456
		 * Serial Number: 1234
		 * Primary Device Type: 6-0050f204-1
		 * Device name: BroadcomAP
		 * Config methods: Label, Display
		 * RF Bands: 0x1
		 * Unknown TLV (0x1049, 6 bytes): 00 37 2a 00 01 20
	WPA:	 * Version: 1
		 * Group cipher: TKIP
		 * Pairwise ciphers: CCMP TKIP
		 * Authentication suites: PSK
	WMM:	 * Parameter version 1
		 * u-APSD
		 * BE: CW 15-1023, AIFSN 3
		 * BK: CW 15-1023, AIFSN 7
		 * VI: CW 7-15, AIFSN 2, TXOP 3008 usec
		 * VO: CW 3-7, AIFSN 2, TXOP 1504 usec
```

附近 Wi-Fi 信号太多，只列举了要连接的一个。这么多信息中主要关注 `SSID` ——Wi-Fi 名字（这里是 `ChinaNet-759d`），和 `RSN`——加密方式（这里是 WPA2）。

### 连接 Wi-Fi

到目前为止还没有连接 Wi-Fi：

``` shell
root@kali:~# iw wlan0 link
Not connected.
```

我们要让 `wlan0` 网卡接口以 `WPA2` 加密方式连接上上面的 `ChinaNet-759d` Wi-Fi。

说明：macOS 连接的也是这个 Wi-Fi，当树莓派通过无线网卡连接到这个 Wi-Fi 的时候，macOS 也可以 SSH 新的无线网卡的 IP 进入树莓派。

- 生成 wpa/wpa2 配置文件

``` shell
wpa_passphrase 'ChinaNet-759d' >> /etc/wpa_supplicant.conf
123456
```

这里执行第一行命令的时候会等待键盘输入 Wi-Fi 密码，这里假设是 `123456`。然后查看刚才生成的配置文件：

``` shell
root@kali:~# cat /etc/wpa_supplicant.conf
# reading passphrase from stdin
network={
	ssid="CMGG"
	#psk="123456"
	psk=l9oumhhdwctzboof31jqzfqeemqzrtepla8olisowq3muyebso5clscrvxsiv006
}
```

- 连接 Wi-Fi

``` shell
# 前台连接
wpa_supplicant -i wlan0 -c /etc/wpa_supplicant.conf

# 后台连接
wpa_supplicant -B -i wlan0 -c /etc/wpa_supplicant.conf
wpa_supplicant -B -D wext -i wlan0 -c /etc/wpa_supplicant.conf
wpa_supplicant -B -D nl80211 -i wlan0 -c /etc/wpa_supplicant.conf

# 断开连接
killall wpa_supplicant
```

查看连接成功与否：

``` shell
root@kali:~# iw wlan0 link
Connected to 74:12:bb:88:06:81 (on wlan0)
	SSID: ChinaNet-759d
	freq: 2427
	RX: 162829 bytes (341 packets)
	TX: 7318 bytes (21 packets)
	signal: -14 dBm
	tx bitrate: 144.4 MBit/s

	bss flags:	short-slot-time
	dtim period:	0
	beacon int:	100
```

这个表示已经连接成功了，传输速率为 `144.4 MBit/s`。

### 无线网卡接口获取 IP

通过上面 `wpa_supplicant` 方式连接上 Wi-Fi 后，并不是平时我们所谓的连上 Wi-Fi 了，这时只是无线网卡接口和Wi-Fi 关联起来了，但是无线接口要能上网还要获取 IP 地址。

``` shell
dhclient wlan0
```

然后查看无线网卡接口已获得的 IP 地址：

```shell
ip addr
```

### 证明无线网卡可以连接互联网

前面我们 macOS 是以 SSH 方式连接到树莓派的，macOS 和树莓派都是同一个局域网，但是 macOS 连接的是 Wi-Fi，树莓派使用网线直接和路由器连接的。

这里为了测试树莓派无线网卡已经连接到 macOS 正在连接的 Wi-Fi 且能够联网，我们需要断掉 macOS 和树莓派的 SSH 会话，然后拔掉树莓派的网线，重新 SSH 树莓派的新的无线网卡接口的 IP。

### 开机自动连接 Wi-Fi

TODO...

## FAQ

### disk vs rdisk

前面使用 `dd` 命令时之所以用 `/dev/rdisk3` 而不是用 `/dev/disk3` 是因为前者比后者写入速度快很多。

> /dev/rdisk nodes are character-special devices, but are "raw" in the BSD sense and force block-aligned I/O. They are closer to the physical disk than the buffer cache. /dev/disk nodes, on the other hand, are buffered block-special devices and are used primarily by the kernel's filesystem code.

### armhfp vs aarch64

32 位选择 armhfp/ARMv7/arm-32，64 位选择 aarch64/ARMv8/arm-64。

### RSN vs WPA2

> WPA implemented a subset of a draft of 802.11i. The Wi-Fi Alliance refers to their approved, interoperable implementation of the full 802.11i as WPA2, also called RSN (Robust Security).

### wlan0: Association request to the driver failed

检查网卡接口是否启用，没启用重新关闭启用下：

``` shell
ifconfig wlan0 down    # ifdown wlan0
ifconfig wlan0 up      # ifup wlan0
ip link set wlan0 down
ip link set wlan0 up
```

### `dbclient` 获取 IP 失败

## 参考

- [sd card - Why is “/dev/rdisk” about 20 times faster than “/dev/disk” in Mac OS X - Super User](https://superuser.com/questions/631592/why-is-dev-rdisk-about-20-times-faster-than-dev-disk-in-mac-os-x)
- [Raspberry Pi 2 | Kali Linux Documentation](https://www.kali.org/docs/arm/kali-linux-raspberry-pi-2/)
- [Connect to WiFi network from command line in Linux - blackMORE Ops](https://www.blackmoreops.com/2014/09/18/connect-to-wifi-network-from-command-line-in-linux/)
- [IEEE 802.11i-2004 - Wikipedia](https://en.wikipedia.org/wiki/IEEE_802.11i-2004)
- [[solved] ip link set device up seems not working / Newbie Corner / Arch Linux Forums](https://bbs.archlinux.org/viewtopic.php?id=195522)
- [Getting started with Docker for Arm on Linux - Docker Blog](https://www.docker.com/blog/getting-started-with-docker-for-arm-on-linux/)