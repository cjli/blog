---
layout: post
title: Nginx 通用日志
category: 折腾
tag: Nginx
---

Nginx 能做的事实在太多，先遇到什么记录下什么吧。

<!-- more -->

和本博客其他很多汇总性日志一样，本文内容也会不定期更新。

> http://nginx.org/en/docs/

## 基本

### 泛域名配置

#### 自动关联子域名到目录名

``` nginx
server {
  # ...
  server_name ~^(?<subdomain>.+).example.com$;
  root /data/wwwroot/example.com/$subdomain;
  # ...
}
```

如果 nginx 后面是 PHP，只是这样配置会出现 `$_SERVER['host']` 一字不差的为 `~^(?<subdomain>.+).example.com$`，因此还需要添加一个 `fastcgi_param` 的  `SERVER_NAME` 配置：

``` nginx
location ~ [^/]\.php(/|$) {
  #fastcgi_pass remote_php_ip:9000;
  fastcgi_pass unix:/dev/shm/php-cgi.sock;
  fastcgi_index index.php;
  include fastcgi.conf;
  fastcgi_param SERVER_NAME $host;    # 重新赋值 SERVER_NAME 参数覆盖前面 `server_name` 的初始定义
}
```

### 监听 IPv6

``` nginx
listen 80;
listen [::]:80 ipv6only=off;
listen [::]:443 ssl;
listen 443 ssl http2;
```

注意，根据 nginx 版本的不同，`ipv6only=on` 不是必须的，且 `ipv6only` 只能设置一次。

> `ipv6only`=`on`|`off`
>
> this parameter (0.7.42) determines (via the IPV6_V6ONLY socket option) whether an IPv6 socket listening on a wildcard address [::] will accept only IPv6 connections or both IPv6 and IPv4 connections. This parameter is turned on by default. **It can only be set once on start.**
>
> Prior to version 1.3.4, if this parameter was omitted then the operating system’s settings were in effect for the socket.

## 安全

### IP 访问频率限制

``` nginx
geo $whitelist {
   default 0;
   # CIDR in the list below are not limited
   1.2.3.0/24 1;
   9.10.11.12/32 1;
   127.0.0.1/32 1;
}

map $whitelist $limit {
    0     $binary_remote_addr;
    1     "";
}

# 2. The directives below limit concurrent connections from a 
# non-whitelisted IP address to five
limit_conn_zone      $limit    zone=connlimit:10m;
limit_conn           connlimit 5;
limit_conn_log_level warn;   # logging level when threshold exceeded
limit_conn_status    503;    # the error code to return

# 3. Limits the number requests from a non-whitelisted IP to
# one every two seconds with up to 3 requests per IP delayed
# until the average time between responses reaches the threshold. 
# Further requests over and above this limit will result 
# in an immediate 503 error
limit_req_zone       $limit   zone=one:10m  rate=30r/m;
limit_req            zone=one burst=3;    # burst => 超过请求频率限制后最多允许多少次强制性访问
limit_req_log_level  warn;
limit_req_status     503;
```

配置好后可以简单通过命令来测试结果：`ab -n 5000 -c 100 http://www.example.com/`，会显示绝大部分请求均返回 503。

配置说明：

- `geo` 定义了白名单 `$whitelist`：默认值为 0；如果请求 IP 与白名单中的 IP 匹配，则`$whitelist` 值为 1。
- `map` 指令是将 `$whitelist` 值为 0 的，也就是受限制的 IP，映射为请求 IP。将 `$whitelist` 值为 1 的，也就是白名单 IP，映射为空的字符串。
- `limit_conn_zone` 和 `limit_req_zone` 将忽略键值为空请求 IP，从而实现对白名单中的 IP 不限制而对其他 IP 做限制。

### 设置 nginx 密码认证

``` nginx
location /path {
  auth_basic "Restricted";
  auth_basic_user_file /path/to/nginx/.htpasswd;
}
```

### IP 黑／白名单

``` nginx
location /path {
  allow whitelist;    # whitelist 事先定义过
  deny all;
}
```

被 `deny` 掉的 IP 会得到 403 状态码。可以制定一个拒绝访问的页面：

``` nginx
location / {
  error_page 403 = @deny;
  allow whitelist;
  deny all;
}
localtion @deny {
  return 301 http://site.com/cominglater;
}
```

#### `allow all` 和 `deny all` 的顺序问题

> 和 Apache 的有区别，因此不要搞混淆。

- 先 `allow` 部分后 `deny all`，表示只允许部分 IP （被 `allow` 指定过的）访问


- 先 `deny all` 后 `allow` 部分，表示拒绝所有，后续 `allow` 不再生效：

``` nginx
location / {
  deny all;
  allow 10.1.1.1;
  allow 10.1.1.2;
  allow 10.1.1.3;
}
```

所有 IP 均不能访问 `/`，返回 403。

#### 禁止指定 IP 或 IP 段

``` nginx
# /path/to/blacklist.ip
deny 192.168.33.111;
deny 192.168.44.222;
deny 192.168.55.0/24;    # 禁止 192.168.10.1~192.168.10.254

--------------------------

# /path/to/nginx.conf
include /path/to/blacklist.ip    # 绝对路径
# include blacklist.ip    # 相对于 nginx.conf 路径
```

### 限制特定目录访问

``` nginx
localtion ~ .*(config|cache)/.*\.php$ {
  deny all;
}
```

### 限制 UA

``` nginx
location / {
  if ($http_user_agent ~ * 'ApacheBench|bingbot/2.0|Spider/3.0') {
    return 403;
  }
}
```

### 客户端真实 IP

主要记录请求头 [X-Forwarded-For](https://zh.wikipedia.org/wiki/X-Forwarded-For)，这一 HTTP 头一般格式如下:

```
X-Forwarded-For: client1, proxy1, proxy2
```

其中的值通过一个 逗号+空格 把多个 IP 地址区分开，最左边（client1）是最原始客户端的IP地址，代理服务器每成功收到一个请求，就把请求来源IP地址添加到右边。

在上面这个例子中，这个请求成功通过了三台代理服务器：proxy1, proxy2 及 proxy3。请求由client1发出，到达了proxy3（proxy3可能是请求的终点）。请求刚从client1中发出时，XFF是空的，请求被发往proxy1；通过proxy1的时候，client1被添加到XFF中，之后请求被发往proxy2；通过proxy2的时候，proxy1被添加到XFF中，之后请求被发往proxy3；通过proxy3时，proxy2被添加到XFF中，之后请求的的去向不明，如果proxy3不是请求终点，请求会被继续转发。

鉴于伪造这一字段非常容易，应该谨慎使用 `X-Forwarded-For` 字段。正常情况下 XFF 中最后一个 IP 地址是最后一个代理服务器的 IP 地址，这通常是一个比较可靠的信息来源。

nginx 访问日志记录客户端真实 IP 配置：

``` nginx
http {
  log_format main '$http_x_forwarded_for - $remote_user [$time_local] '
'"$request" $status $body_bytes_sent "$http_referer" '
'"$http_user_agent"';
}

server {
  access_log    /path/to/sitelogs/access.log main;
}
```

### 子域名 Cookie 隔离

比如：`*example.com` 和 `*www.example.com` 想要共享 cookie 而 `*test.example.com` 使用自己的 cookie 怎么办？

- 在域名的 DNS 解析纪录中，将 `*example.com` 重定向到 `*www.example.com` 即可。

- 或者通过设置 nginx 301 重定向

``` nginx
server {
  listen 80;
  server_name domain.com;
  return 301 $scheme://www.domain.com$request_uri;
}
server {
  listen 80;
  server_name www.domain.com;
  # ...
}
```

## Rewrite 规则

### 别忘了限制条件

**如果不在限制条件下使用 rewrite 规则，容易造成死循环**。

- PHP 伪静态

``` nginx
if (!-e $request_filename) {
  rewrite ^(.*)$ /index.php last;
}
```

- HTTP 重定向到 HTTPS

``` nginx
server {
  listen 80;
  server_name www.example.com;
  rewrite ^/(.*) https://$server_name$1 permanent;   # 301
  # rewrite ^/(.*) https://$server_name$1 redirect;    # 302
}
```

- 顶级域名自动跳转到 www 子域名

``` nginx
server {
  listen 80 default_server;
  listen [::]:80 default_server;
  server_name example.com;
  rewrite ^/(.*)$ $scheme://www.example.com$request_uri permanent;
}
```

### PHP 项目常用 rewrite 规则

- laravel

``` nginx
location / {
  try_files $uri $uri/ /index.php?$query_string;
}
```

- 禅道项目管理系统

``` nginx
server {
    listen       80;
    server_name  zentao.example.com;
    access_log /data/wwwlogs/zentao.example.com_nginx.log combined;
    index index.html index.htm index.php;
    root /data/wwwroot/zentao.example.com/www;
    charset utf-8;

    location / {
        try_files $uri /index.php$uri;
    }

    location ~ \.php(/.+)?$ {
        fastcgi_pass unix:/dev/shm/php-cgi.sock;
        fastcgi_index index.php;
        include fastcgi.conf;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_param PATH_INFO $fastcgi_path_info;
    }
}
```
## 日志配置

### 关闭对 favicon 和 robot.txt 的日志记录

``` nginx
location = /favicon.ico {
  access_log off;
  log_not_found off;
}
location = /robot.txt {
  access_log off;
  log_not_found off;
}
```

### 只记录特定状态的访问日志

``` nginx
http {
    map $status $normal {
        ~^2  1;
        default 0;
    }
    map $status $abnormal {
        ~^2  0;
        default 1;
    }
    map $remote_addr $islocal {
        ~^127  1;
        default 0;
    }
  
    server {
        access_log logs/access.log combined if=$normal;
        access_log logs/access_abnormal.log combined if=$abnormal;
        access_log logs/access_local.log combined if=$islocal;
    }  
}
```

## With PHP

### nginx 与 fastcgi／php-fpm 之间的关系

- Nginx：Web 服务器。
- FastCGI：同 cgi 一样只是一个协议，负责规范 Web 应用程序和 Web 服务器的连接，规定了传递给 PHP 等动态程序的有哪些参数以及通过什么形式传递。
- [php-fpm](http://php.net/manual/zh/install.fpm.php)：PHP 官方采纳并使用的，实现了 FastCGI 协议的进程管理器，负责管理和调度 php-cgi 进程。

> 这里的「Web 应用程序」指的是 PHP 开放的应用程序，虽然我们常说 PHP 服务器后端开发，但是站在 Nginx 角度，PHP 开发的后端程序依然是应用程序的范畴。

HTTP 请求到达服务器之后，首先经过专门的 Web 服务器，然后 Web 服务器会将该请求简单处理后将按照 CGI／FastCGI 规定的标准数据格式交给 PHP-FPM 处理。

php-fpm 的安装，旧版本 PHP 中需要下载对应 PHP 版本的 php-fpm 源码以内核补丁的形式编译到 PHP 中，PHP 5.4 之后 PHP 核心已内置 php-fpm，只需在安装的时候启用 `--enable-fpm` 就行了。

### nginx+fpm 高并发优化思路

- 启用 opcache（no zend guard loader）
- nginx 和 fpm 的通信方式使用 UNIX socket 替换 TCP 端口

``` nginx
fastcgi_pass 127.0.0.1:9000;
# 换为：
fastcgi_pass unix:/dev/shm/php-fpm.sock;
```

其中，`/dev/shm/php-fpm.sock` 是在 php-fpm.conf 中配置的。

- 优化 Linux 内核参数

修改 _/etc/sysctl.conf_（_/proc/sys/net/core/_somaxconn）后使用 `sysctl -p` 重启：

```Conf
net.ipv4.tcp_max_syn_backlog = 16384
net.core.netdev_max_backlog = 16384 
net.core.somaxconn = 2048
```

- 优化 php-fpm 参数

  ``` ini
  listen.backlog = 8192;     ; 默认为 -1（由系统决定）
  listen 80 backlog = 8192;  ; 默认为 511
  ```

## 反代与 LB

``` nginx
upstream service_a {
  server 10.10.10.100:80;
  server 10.10.10.200:80;
}

server {
  listen 80;
  server_name _;
  location / {
    proxy_pass http://service_a/;
    proxy_set_header Host   $host;
    proxy_set_header X-Real-IP      $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
  }
}
```

## 高并发原理

Nginx 是典型的“高并发+事件驱动” 型 Web 服务器。其主要原理简述如下：

### 多（个独立单）进程模式

> 这里的多进程有别于 Apache 的多进程，要从一个进程中实际运行的执行流／线程的「多」少去看待。
>
> Apache 的多进程适合于 CPU 密集型服务（减少进程间切换）。

Nginx 多进程的工作模式指的是：

- 一个 master 进程 + 多个 worker 进程。
- master 进程不处理请求，而是接受请求后向各个 worker 发送消息，导致每个 worker 都有可能来处理当前的请求。
- master 进程会监控每个 worker 进程的运行状态，并在 worker 异常终止后启用新进程。
- worker 在 `accept()` 连接后创建一个“已连接描述符”，然后通过这个连接描述符与客户端通信：处理数据、返回给客户端、断开连接。

多进程的好处是：worker 进程间不互相影响，避免了高并发场景下进程内部通信和共享状态带来的复杂度和可用性，也方便支持平滑重启。

当然，多进程的缺点有：

- 进程间竞争 CPU 会带来上下文切换的开销，因此需要根据 CPU 核心数合理设置 nginx 的 worker 数量
- “惊群现象”会带来较多的资源浪费，因此，nginx 默认开启了 accept_mutex 互斥锁来解决这个问题。

> 惊群现象：由于所有 worker 都是从 master fork 而来的，继承了 master 用于监听请求的 socket 文件描述符，当连接到来时，所有的 worker 均会收到通知并开始争夺连接权，导致大量的进程在激活后被挂起，消耗较多的系统资源。
>
> accept_mutex：即每个 worker 必须先获得一把互斥锁后才能 `accept()`  请求。

### 多路 IO 模型：epoll

多进程模式下，每个 worker 进程一次只能处理一路 I/O，表现为 `accept()` 请求或者转发请求到 upstream 后，进程处于阻塞状态，直到重新收到连接或者缓冲区收到数据。

为了使单个 worker 中能够表现为同时处理多个请求，nginx 采用了 Linux 2.5.44 之后的多路复用 IO 模型： [epoll](https://zh.wikipedia.org/zh-hans/Epoll)。

> 因为一个进程里只有一个线程，所以一个进程同时只能做一件事，但是可以通过不断地切换来“同时”处理多个请求。

### 事件驱动

epoll 允许 worker 进程在接受到客户端请求或者转发请求到后端服务器后立即返回，并通过 `epoll_ctl()  `注册一个事件，当请求处理结束后再以注册事件回调的形式主动通知 worker，以完成请求响应的发送和连接的关闭等操作，期间该 worker 重新进入待命状态。

事件驱动适合于 I/O 密集型服务，比如静态文件的读写（磁盘IO）和传输（网络IO），而非 CPU 密集型服务，因为进程在一个地方进行计算（CPU密集）时，那么这个进程就不能处理其他事件了。

> Nginx 典型应用之反向代理服务器：代理后端服务器返回响应，可以在客户端与后端服务器之间起一个数据中转作用，比如缓存，纯 I/O 操作，自身并不涉及到复杂计算。

## FAQ

### 客户端获得 403 原因

> "403" is actually an HTTP status code that means that the web server has received and understood your request, but that it cannot take any further action.

- 客户端访问目录而 `autoindex` 设置成 `off`。

  由于安全原因，目录索引默认是关闭的。打开方式：

  ``` nginx
  location /path {
    autoindex on;
    autoindex_exact_size_off;
  }
  ```

  `autoindx` 必须出现在 `location` 块中。

- 客户端访问了只能在服务端内部访问的资源。

- nginx 无需要的网站路径权限

  文件要被能 nginx `read`，目录要能被 nginx `execute`。

- 目录索引文件未定义

  以 PHP 为例，如果只定义 `index index.html index.htm` 则 nginx 会直接返回 403 而无论是否真实存在 index.php。

- 索引定义的文件名大小写弄错

  `index` 指令使大小写区分的，如果 `index.php` 错写成 `Index.php` 依然会出现 403.

- nginx 屏蔽了指定的客户端／IP，通常通过 `deny` 等指令设置。

## 参考

- *[Module ngx_http_limit_conn_module](http://nginx.org/en/docs/http/ngx_http_limit_conn_module.html)*
- *[Mozilla SSL Configuration Generator](https://mozilla.github.io/server-side-tls/ssl-config-generator/)*
- *[How to rate-limit in nginx, but including/excluding certain IP addresses?](https://serverfault.com/questions/177461/how-to-rate-limit-in-nginx-but-including-excluding-certain-ip-addresses)*
- *[nginx定制header返回信息模块ngx_headers_more](http://www.ttlsa.com/nginx/nginx-custom-header-to-return-information-module-ngx_headers_more/)*
- *[Nginx下禅道的伪静态(rewrite)规则](http://yaxin-cn.github.io/Nginx/configure-static-access-for-zentao-under-nginx.html)*
- *[Nginx Access Log日志统计分析常用命令](http://www.cnblogs.com/coolworld/p/6726538.html)*
- *[nginx防止DDOS攻击配置（2）](https://www.52os.net/articles/nginx-anti-ddos-setting-2.html)*
- *[How to Secure Nginx Using Fail2ban on Centos-7](https://hostpresto.com/community/tutorials/how-to-secure-nginx-using-fail2ban-on-centos-7/)]*
- *[nginx通过云负载均衡后作反向代理并限制制定ip访问](http://www.voidcn.com/article/p-durjqmpt-bro.html)*
- *[IP.cn - 每日中国大陆 IP 列表, 电信 IP 列表，联通 IP 列表，移动 IP 列表](ip.cn/chnroutes.html?capely=85zku1)*
- *[Online IP CIDR / VLSM Supernet Calculator](www.subnet-calculator.com/cidr.php)*
- *[Nginx监听IPv6地址端口的正确操作方法](https://www.librehat.com/the-right-way-to-setup-nginx-monitor-ipv6-address-port/)*
- *[Nginx中配置Access Control](http://blog.liulantao.com/nginx-access-control-allow-deny.html)*
- *[Nginx拒绝或允许指定IP](https://www.centos.bz/2011/12/nginx-deny-or-allow-ip/)*
- *[nginx访问控制allow、deny（ngx_http_access_module）](http://www.ttlsa.com/linux/nginx-modules-ngx_http_access_module/45/)*
- *[epoll使用详解（精髓）](http://www.cnblogs.com/fnlingnzb-learner/p/5835573.html)*
- *[使用NGINX+LUA实现WAF功能](http://blog.oldboyedu.com/nginx-waf/)*
- *[CDN下nginx获取用户真实IP地址](http://www.ttlsa.com/nginx/nginx-get-user-real-ip/)*
- *[TCP三次握手图解／SYN flood攻击／四次挥手](https://www.jianshu.com/p/5e3601bdee2a)*
- *[The definitive guide to cookie domains and why a www-prefix makes your website safer](http://erik.io/blog/2014/03/04/definitive-guide-to-cookie-domains/)*
