---
layout: post
title: 在 VirtualBox／CentOS 中设置 Host-Only 网络
category: 折腾
tags: [VirtualBox, CentOS, Host-Only]
---

最近重装了一次系统，在使用 Vagrant 配置 private_network 时总出现问题。

<!-- more -->

后来我跳过 Vagrant 直接在 VirtualBox 中安装 CentOS，配置开发环境时，遇到了一个 Host-Only 网络配置问题，下面将步骤记录如下。

## 安装

- Virtualbox && Extension Pack：*https://www.virtualbox.org/wiki/Downloads*。略。


- CentOS Minimal：为了方便可以到 *[CentOS | VirtualBoxes - Free VirtualBox® Images](https://virtualboxes.org/images/centos/)* 下载可直接使用的 VirtualBox ova 镜像文件，导入命令为：

  ``` shell
  vboxmanage import /path/to/centos.ova
  ```

  在 virtualboxes.org 中下载的 ova 镜像初始用户名密码为：`root => reverse`。

  当然，也可以直接从*[官方 iOS 镜像](https://wiki.centos.org/Download)* 中下载 Minimal 的 IOS 镜像进行全新安装。

## VirtualBox 添加 Host-Only 网络接口

- 在 VirtualBox 设置中找到「网络」=> 「仅主机（Host-Only）网络」

- 点击右侧添加图标，首次将创建一个名为 `vboxnet0` 的接口名，然后点击右侧设置图标

- 在打开的「主机虚拟网络界面」设置 IPv4/IPv6 参数：

  ``` 
  IPv4 地址：192.168.56.1
  IPv4 网络掩码：255.255.255.0
  ```

- 「DHCP 服务器」设置：由于我只需要一个指向虚拟机的静态 IP，因此没有启用 DHCP。

- 点击「OK」保存。

### 什么是 Host-Only 网络？

在上面的例子中，在宿主系统中创建一个接口 *vboxnet0*，这个接口将与 Guest 系统的某个接口关联（下面会设置），充当了一个路由网关，可以转发网络请求，目的是为了实现主机上访问 Guest 系统的内网 IP 就可以访问到虚拟机中的某个服务的效果。（我是为了开发）

## 在 Guest 系统中关联接口

没有配置好之前，只能通过 GUI 的形式启动虚拟机，而不能 SSH，因为宿主系统和虚拟机之间并没有网络连接。

这当然是很麻烦的，而且 VirtualBox GUI 操作 CentOS 虚拟机体验很不好，最好的操作，当然就是用宿主系统中的终端 SSH 到虚拟机中进行操作就好了。

关联步骤如下：

### GUI 模式登录 CentOS

在 VirtualBox 界面中启动 CentOS 虚拟机，输入账号密码后登录。

### 添加一个网络接口 

``` shell
cd /etc/sysconfig/network-scripts/
ifconfig -a    # 获得真实的 MAC 地址 用于下面的配置
vi ifcfg-eth1
```

在 eth1 接口配置文件中输入以下信息后保存退出：

```
DEVICE=eth1
BOOTPROTO=static
HWADDR=08:00:27:A1:65:48
NM_CONTROLLED=yes
ONBOOT=yes
TYPE=Ethernet
NETMASK=255.255.255.0
IPADDR=192.168.56.42
```

#### 注意

- `HWADDR` 一定要是上面得到的真实 MAC 地址，否则后面启用该接口时会提示 *Unable to activate eth1 - device eth1 has different MAC address than expected.*
- 这里的 `IPADDR` 一定要和前面在 VirtualBox 中设置 Host-Only 接口时配置的 IPv4 地址在一个网段，否则仍然无法连通。

### 启用该接口

``` shell
ifup eth1
```

没有报错则启用成功，可以通过 `ifconfg` 或者 `ip addr` 命令查看虚拟机的 IP 地址，成功的情况下，则会看到 eth1 接口的 inet 值会是上述配置的 192.168.56.42。

配置好 Host-Only 网络后，以后对虚拟机的操作就和远程登录 Linux 服务器完全一样了。

## 虚拟机内访问互联网

如果我们的 CentOS 虚拟机只是配置了 Host-Only 网络的话，虚拟机内部是无法访问公网的。因此，我们还需要在虚拟机设置中新启用一个网卡接口，并设置为 NAT 模式。

启用完成后，重启虚拟机，使用 `ip addr` 可以看到新增了一个网络接口。

## 开发模式下的常用操作

- 以 headless 模式启动虚拟机

``` shell
VBoxManage startvm "centos-dev" --type headless
```

这里的 *centos-dev* 是安装在 VirtualBox 中的虚拟机的名称。下同。

- 关闭 headless 虚拟机

``` shell
VBoxManage controlvm "centos-dev" poweroff
```

- 登录 CentOS

``` shell
ssh root@192.168.56.42
```

## FAQ

-guest 与 host 时间不同步？

一种解决办法：虚拟机设置。

关闭 virtualbox guest。

``` shell
# 启用扩展时间同步
VBoxManage setextradata "centos-dev" "VBoxInternal/Devices/VMMDev/0/Config/GetHostTimeDisabled" 0

# 开启时间同步的时候设置时间
VBoxManage guestproperty set "centos-dev" "/VirtualBox/GuestAdd/VBoxService/--timesync-set-start" 1

# 设置虚拟机从已保存状态恢复时同步时间
VBoxManage guestproperty set "centos-dev" "/VirtualBox/GuestAdd/VBoxService/----timesync-set-on-restore" 1

# 设置 host/geust 时间同步间隔上限（单位：微秒）
VBoxManage guestproperty set "centos-dev" "/VirtualBox/GuestAdd/VBoxService/--timesync-set-threshold" 10000
```

然后重启虚拟机。

> See: 
> - <https://www.virtualbox.org/manual/ch09.html#idm8496>
> - <https://stackoverflow.com/questions/19490652/how-to-sync-time-on-host-wake-up-within-virtualbox>

说明：以上几个配置项目也可以直接改动文件 .vbox 配置文件，比如 ~/VirtualBox VirtualBox\ VMs/centos-dev/centos-dev.vbox。

第二种解决办法：docker ntpd+crontab。

首先在 macOS 上启动一个 ntpd 容器，监听 123 端口。

``` shell
docker run --name ntpd -d -p 123:123/udp pablogarciaarevalo/ntpd-local
```

> 不建议去折腾 macOS 原生 ntpd，会因 macOS 版本不同而不太好搞。

然后在 Linux 虚拟机中配置 cron job，每分钟去 macOS 本地更新时间。

``` crontable 
* * * * * /sbin/ntpdate 192.168.56.1 > /dev/null 2>&1
```

- 静态 IP 设置自动重置？

CentOS 6.4 之后使用前面的静态 IP 设置会出现这样的情况，解决办法：

```
NM_CONTROLLED=no
BOOTPROTO="none"
```

然后重启网络服务：`systemctl restart network`.

See: <https://serverfault.com/questions/514340/interface-ip-address-is-removed-automatically-on-centos-running-inside-virtualbo>

## 参考

- *[Host-Only Networking with VirtualBox](http://christophermaier.name/2010/09/01/host-only-networking-with-virtualbox/)*
- *[Adding a new interface?](https://www.centos.org/forums/viewtopic.php?t=8181)*
- *[Unable to activate eth1 - device eth1 has different MAC address than expected.](http://www.linuxquestions.org/questions/linux-networking-3/unable-to-activate-eth1-device-eth1-has-different-mac-address-than-expected-906295/)*
