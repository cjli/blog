---
layout: post
title: SQLite 使用日志 
category: Database
tags: [SQL, SQLite, Database]
---

最近搞腾的一个小项目选择了轻量级的 SQLite，简单总结下使用笔记。

<!-- more -->

## 简介

SQLite 是一个完全开源（作者正式放弃其版权）、专为内嵌式应用设计的关系型数据库。

### SQLite 优点

- 简单而实用：因为是内嵌的，因此不需要网络配置和管理（零配置，无 DBA）。数据库的服务器和客户端运行在同一个进程中。这样可以减少网络的消耗，简化数据库管理和部署。但是，SQLite 还支持 ANSI SQL92 的一个大子集，包括事务、视图、检查约束、关联子查询和复合查询等关系型数据库具有的特性。

- 兼容性：SQLite 兼容各种各样的 PC 操作系统，大量的嵌入式平台，16位、32位、64位体系结构，字节序大端小端格式等环境。此外，SQLite 的兼容性不只是表现在代码上，还表现在其数据库文件上。SQLite 的数据库文件在其所支持的所有操作系统、硬件体系结构和字节顺序上都是二进制一致的，即一份 SUN 工作站的 SQLite 数据库可以随意用到 macOS、Windows 等系统，而不需要做任何转换和修改。

- 紧凑性：SQLite 的设计只有 1 个头文件，1 个库文件，不需要扩展的数据库服务。

- 适应性：SQLite 在以下两个方面都做得很好：强而有力的关系型数据库前端，简单而紧凑的 B-tree 后端。

- 自由免费：SQLite 全部代码没有附加版权要求，任何人均可以修改、合并、发布、出售或者将这些代码用于任何目的（包括商业性），完全免费。

- 可靠：SQLite 核心代码大约 3W 行，干净、模块化、完好注释，易理解、易定制。此外，回归测试代码也有 3W+ 行，覆盖率达 97%。可以说是相当可靠了。

- 易用：SQLite 提供了一些独特的功能来提升易用性，比如动态类型，冲突解决和附加多个数据库到一个连接的能力。

- 高性能（部分情况）：和大多数数据库一样，SQLite 实用 B-tree 处理索引，实用 B+tree 处理表数据。因此，在对单表进行查询时，SQLite 要快于（至少相等于）其他数据库的速度。

- 具有学习价值：因为开源，通过 SQLite 可以研究很多计算机科学的课题：分析器、分词器、虚拟机、Btree 算法、高整缓存、程序体系结构等经典概念。

### SQLite 缺点

- 并发：基本没有并发。

> 这是情有可原的，SQLite 就是对同一个文件的读写。

- 数据库大（tai）小

- 网络功能不完善

- 不支持触发器

- 不支持完整的 alter table

- 不支持事务嵌套

- 不支持 RIGHT/FULL OUTER JOIN

- 不可以修改视图

- 不支持 GRANT 和 REVOKE

## 常用命令

### 进入 Sqlite SHELL（CLP）

``` shell
sqlite3    # 进入临时基于内存的数据库

sqlite3 /path/to/db.sqlite    # 指定进入某个数据库（文件路径）

sqlite>
```

### `.` 语法

`.` 语法只能在终端下进入 SQLite 才能使用。

``` shell
.help    # 查看帮助

.tables   # 查看当前所有表

.schema {table}   # 查看表定义

.table {table}    # 查看表是否存在

.exit/.quit  # 退出 Sqlite 命令行

.headers on    # 显示 SHELL 输出头信息

.mode column    # 设置 SHELL 输出列模式，改进输出格式

.prompt ' sqlite3> '   # 改变 CLP Shell 提示符

.indices {table} # 查看表索引

.shell pwd      # 执行外部 SHELL 命令

.nullvalue NULL    # 使用字符串来显示 NULL 值

.exit    # 退出 CLP
```

## 语法

### DDL 

- 创建/删除数据库

一个文件就是一个数据库。即执行 `sqlite3 /path/to/file.db` 时就创建来一个称为 file.db 的数据库了。

不过，只是执行这一个命令但数据库并不存在的话，SQLite 并没有真正创建它，直到我们真正向其中增加了数据库对象（表/视图等）之后才真正创建它。原因是给时间设置数据库，比如页大小，字符集等，这些设置一旦创建之后就不能修改了。

删除数据库很直接，删掉数据库文件就行了。

- 创建/删除表

``` sql
create table user (id integer primary key, info text);
drop table user;
```

- 创建/删除表索引

``` sqlite
create index info_idx on user(info);
drop index info_idx;
```

- 创建视图

``` sqlite
create view schema as select * from sqlite_master;
```

### DML

- 查询表字段属性列表

```sqlite
PRAGMA table_info({table});
```

- 创建记录

``` sqlite
insert into user values(1, 'aaaa'), (2, 'bbbb');
select last_insert_rowid() as last_id;
```

- 查询表记录

``` sqlite
select * from user;
```

### 注释

单行 `--`，多行 `/* ... */`：

``` sql lite
/* CREATE TABLE User
        -- A table comment
(
        uid INTEGER,    -- A field comment
        flags INTEGER   -- Another field comment
); */
```


### `sqlite_master`

``` sql lite
-- 查询表定义 等价于 `.schema {table}`
select `sql` from sqlite_master where name = 'job';

-- 查询表是否存在 等价于 `.table {table}`
select count(*) from sqlite_master where type = 'table' and name = '{table}';
```

## 数据备份与恢复

- 导出数据

``` sqlite
.output /path/to/save.sql    # 指定导出文件格式
.dump [table_name|view_name] # 不指定参数则导出整个数据库
.output stdout    # 指定输出目标为标准输出
```

以上语法功能等价于 SHELL 命令：`sqlite3 file.db .dump > /path/to/save.sql`

此外，还可以用复制的方法得到一个二进制数据库文件拷贝，但也许在复制之前，你想先抽空原始数据库文件——释放数据库文件中未使用的部分，得到文件大小更小的数据库文件：

``` shell
sqlite3 file.db VACUUM
cp test.db test.backup
```

说明：二进制备份不如 SQL 备份兼容性好，虽然 SQLite 兼容性不错，但是 SQL 格式更保险。

- 导入数据

``` sqlite
# 导入由 .dump 命令创建的文件
.read /path/to/file.sql    # 如果导入的数据已经存在 则需要先删除

# 导入逗号分隔的值（comma-separated values，CVS）
.import /path/to/file.csv [file][table]
```

以上语法功能等价于 SHELL 命令：`sqlite3 file.db < /path/to/save.sql`

## Python 操作 Sqlite

``` python
#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import sqlite3, os

dbname = 'test.db'
conn = sqlite3.connect(dbname)
cr = conn.cursor()
print 'Sqlite db opened: ', dbname

cr.execute('''
    create table user (id int primary key, name text)
''')

print 'Table created: user'

cr.execute('''
    insert into user values (1, "cjli"), (2, "li1"), (3, "lll")
''')

print 'Records added.'

cr.execute('''
    update user set name = "li2" where id = 2
''')

print 'User id 2 name updated to: li2'

cr.execute('''
    delete from user where id = 3
''')

result = cr.execute('''
    select * from user
''')
for row in result:
    print 'id = ', row[0]
    print 'name = ', row[1]

conn.commit()
conn.close()
os.remove(dbname)
```

## FAQ

- **No such table though it does exist**

首先检查文件路径是否正确，因为就算文件路径不存在 SQLite 默认也会创建一个空数据库文件，所以会导致找不到表等问题。

- **HY000 error: 17, database schema has changed **

通常是由于有多个 writer 在修改 SQLite 数据库文件导致的。

## 参考

- *[Appropriate Uses For SQLite](https://sqlite.org/whentouse.html)*
- *[SQLite-Docs](https://www.sqlite.org/docs.html)*
- *[SQlite源码分析](http://huili.github.io/index.html)*
- *[iOS/Android SQLite全文检索——FTS (full text search)](http://www.jianshu.com/p/32563e843cc0)*
- *[《SQLite 权威指南》，Michael Owens]()*
