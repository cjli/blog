---
layout: post
title: 为什么互联网上几乎没有大小超过 2G 的图片？
category: 折腾
tags: [图片, PNG, JPEG]
---

前几天在研究网页截图的时候偶然发现了一个问题，当网页无限懒加载导致截图图片过长时，截图程序会无法生成图片或者直接崩溃。

<!-- more -->

> 有点标题党了，应该改成“为什么互联网上几乎没有超大图片”更准确点，2G 只是我在寻找问题的时候得出的一个有其他前提的结论。

## 截图程序的内存限制

我之前使用的 PhantomJS 截图的，因此如果对它（NodeJS 进程）有内存限制，那么截图代码开始执行时，能截出图片的大小上限肯定不会超过这个限制。

## 32 位程序所能使用的内存有限

互联网上运行的一些程序仍然是在 32 位操作系统上开发的，一般来讲 32 bit 的程序能使用的内存上限默认是 2GB（尽管理论极限是 4GB）。

## PNG 图片的大小上限？

> A naive implementation of resizing would require the image to be blown up to 2.7GB in size before it is displayed. This would clearly be too large for a normal 32-bit program to handle.

> The PNG specification doesn't appear to place any limits on the width and height of an image; these are 4 byte unsigned integers, which could be up to 4294967295.

## 参考

- [How much memory can a 32 bit process access on a 64 bit operating system?](https://stackoverflow.com/questions/639540/how-much-memory-can-a-32-bit-process-access-on-a-64-bit-operating-system)
- [File format limits in pixel size for png images?](https://stackoverflow.com/q/4109447/4696448)
- [Portable Network Graphics (PNG) Specification (Second Edition)](http://www.libpng.org/pub/png/spec/iso/index-object.html)
- [LodePNG crashes when encoding PNG files with size greater than 15000 * 15000 pixels](https://stackoverflow.com/questions/25490332/lodepng-crashes-when-encoding-png-files-with-size-greater-than-15000-15000-pix)
