---
layout: post
title: 新版 macOS 下软件破解情况测试
category: 折腾
tags: [破解, macOS]
---

本着折腾的心态，又把 macOS 抹掉重来了，心血来潮，安装了一批软件来尝试新系统（macOS）破解情况。

<!-- more -->

> **声明：个人破解仅供折腾测试。请支持正版或者选择开源免费软件！**

## 准备工作

因为 macOS 默认只信任从 App Store 和拥有开发者 ID 签名的应用，为了避免出现“打不开XXX，因为它来自身份不明的开发者”，首先确保系统设置（「安全性与隐私」=>「任何来源」）中已经允许任何软件来源。

也可以用如下命令设置：

``` shell
sudo spctl --master-disable
```

其他：

- 可以将复制软件然后在终端粘贴，或者直接把软件拖进终端，代表其完整路径。

## Alfred

使用 Keygen 破解。

会出现的问题是，在高版本 macOS，比如 Sierra 中可能会出现打开 Keygen 失败的现象。

解决办法是，使用 upx 为其脱壳，然后再用 Keygen 激活。

``` shell
# !!! 确保 upx 的可执行权限
# 命令参数：/path/to/upx -d /path/to/keygen.app/Contents/MacOS/CORE\ Keygen
./upx -d CORE\ Keygen.app/Contents/MacOS/CORE\ Keygen
                       Ultimate Packer for eXecutables
                          Copyright (C) 1996 - 2016
UPX 3.92-BETA   Markus Oberhumer, Laszlo Molnar & John Reiser   Sep 22nd 2016

        File size         Ratio      Format      Name
   --------------------   ------   -----------   -----------
    105804 <-     46704   44.14%   macho/amd64   CORE Keygen

Unpacked 1 file.
```

然后运行 Keygen，选择已安装的 Alfred.app，点击 Patch、Save，重启 Alfred，即可。

## Dash

使用破解补丁破解。

只需用下载的破解补丁 HockeySDK 覆盖到 Dash.app 安装路径下面的：Contents/Frameworks/HockeySDK.framework/Versions/A/HockeySDK，即可。

## Moom

使用 Special-K 破解。

``` shell
# 命令参数
/path/to/patcher /path/to/Moom.app /path/to/eyePatch /path/to/Moom.app

# !!! patcher 和 eyePatch 都是在 Moom 3.x [SP].app/Contents/MacOS 下
```

当出现 *code_alloc_xc_ios: ....../codesign_allocate* 字样的时候表示破解成功。

然后重启 Moom 即可。

如果需要汉化，只需要将下载的汉化包 `zh_CN.lproj` 拷贝到已安装的 Moom.app/Contents/Resources/，注销后重新登录系统即可。

### 已知问题

破解成功后无法正常运行。总是要求系统辅助功能权限，暂时无解。

## Monodraw

未完待续...

## 参考

- [MacOSX分屏软件Moom 3.2.5汉化破解版 ，亲测10.11.3完美运行](http://bbs.feng.com/read-htm-tid-10486036.html)
- [解决macOS Sierra下注册机无法运行的问题](http://www.jianshu.com/p/0d3e9a4ebaf1)
- [Moom 3.2.5](http://pan.baidu.com/s/1hrVDrJQ)
- [moom3.2.1.dmg-xe4y](http://pan.baidu.com/share/link?shareid=2216213754&uk=4263946228)
- [upx](https://pan.baidu.com/s/1bptBCAV)
- [最简单的Hopper Disassembler玩转Mac逆向 - 简书](https://www.jianshu.com/p/c04ac36c6641)
- [cracks_monodraw [Wizardry and Steamworks]](http://grimore.org/cracks/monodraw)