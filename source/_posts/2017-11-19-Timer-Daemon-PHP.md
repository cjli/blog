---
layout: post
title: 定时器和守护进程在 PHP 中的应用
category: Linux
tags: [定时器, 守护进程, PHP]
---

开发工作中，关于「定时」的需求是非常常见的。

<!-- more -->

> 控制了时间，就控制了一切。

## 定时器应用场景

### interval

一段时间后周期性执行某个任务，每隔一定的时间重复执行某种操作。

### timeout

一段时间后执行一次任务。等待一定的时间后执行某种操作，只执行一次。

属于延时消息任务，为了高效性可以使用环形队列（数组）+ 任务集合 + timer 实现。

### 在某个确定的时间点周期性执行任务

> TODO

## 守护进程

### supervisord

之前介绍过 Laravel 的队列主要是借助 `queue:listen`／`--daemon`  和 supervisor 来实现的，前者简单模拟了守护进程，而 supervisord 本身就是一种（超级）守护进程。

#### 安装配置

``` shell
pip install supervisor
# Or: yum install -y supervisor

# 生成主要配置文件
echo_supervisord_conf > /etc/supervisord.conf

# 创建进程配置文件路径
mkdir -p /etc/supervisor.d/

# 修改配置文件
[include]
files = /etc/supervisor.d/*.ini
```

#### 主进程管理

``` shell
# 启动 supervisor
supervisord -c /etc/supervisord.conf

# 重载配置
supervisorctl reload

# 停止 supervisor
ps aux | grep supervisor    # get pid of supervisord
kill -s SIGTERM <pid_of_supervisord>
```

#### 子进程管理

``` shell
supervisorctl status    # 查看已监控进程状态
supervisorctl stop <program_name>    # 停止某个子进程
supervisorctl stop all    # 停止所有已管理进程
```

#### 交互模式

交互模式可以和进程管理实现一样的效果，只是在一个交互的 SHELL 中完成，并且不用在指定 `supervisorctl` 命令。

``` shell
supervisorctl -c /etc/supervisord.conf
> status    # 查看已监控进程状态
> stop all  # 停止所有已管理进程
```

#### 子进程配置示例

``` shell
[program:php-cli-script]
directory = /path/to/executable
command = php do-sth.php
autostart = true
startsecs = 5
autorestart = true
startretries = 3
user = www
redirect_stderr = true
stdout_logfile = /path/to/php-cli-script.log
loglevel = info

[inet_http_server]
port=127.0.0.1:9002
username=php
password=php
```

### systemd

CentOS 7 之后的版本中，systemd 作为 sysv init 和 upstart 的替代。

#### systemd 规则

如果我们想让自己的程序或服务被 systemd 管理，则编写守护进程时要遵循一些 systemd 的规则：

- 后台服务进程代码不需要执行两次派生来实现后台进程，只需要实现服务本身的主循环即可。（传统编写守护进程要至少调用一次fork，然后停止父进程）

- 不要调用 setsid()，交给 systemd 处理

- 不再需要维护 pid 文件。（传统编写守护进程，会自己在某个目录下，生成pid文件，一是记录本守护进程pid，另外一点是防止本守护进程被多次重启而导致出错或者导致多个实例在运行）

- Systemd 提供了日志功能，服务进程只需要输出到 stderr 即可，无需使用 syslog。（传统编写守护进程我们要将标准输出、出错、输入关闭或者重定向，日志信息都是发往rsyslog）

- 处理信号 SIGTERM，这个信号的唯一正确作用就是停止当前服务，不要做其他的事情。（传统守护进程一般SIGTERM也是用来做这种事情的）

- SIGHUP 信号的作用是重启服务。（传统数据进程一般SIGHUP也是做这种事情的）

- 需要套接字的服务，不要自己创建套接字，让 systemd 传入套接字。（这个承接systemd快速启动优点而设立的，可以实现这个特点，也可以不实现）

- 使用 `sd_notify()` 函数通知 systemd 服务自己的状态改变。一般地，当服务初始化结束，进入服务就绪状态时，可以调用它。

#### 实例 my-daemon-demo

- 编写自己的服务代码 my-daemon-demo.php

``` php
echo date('Y-m-d H:i:s'), PHP_EOL;
sleep(1);
```

- 编写 systemd service 文件 my-daemon-demo.service

在 /etc/systemd/system 目录下新建 my-daemon-demo.service 文件，内容如下：

```
[Unit]
Description=My PHP Daemon Demo
After=home.mount network.target

[Service]
User=cjli
Group=www
ExecStart=/path/to/my-daemo-demo.php
ExecStop=/bin/kill -TERM $MAINPID
ExecReload=/bin/kill -HUP $MAINPID
KillMode=process
Restart=on-failure

[Install]
WantedBy=multi-user.target
```

- 启用服务

``` shell
systemctl daemon-reload
systemctl enable my-daemon-demo    # 服务自启动
systemctl start my-daemon-demo
systemctl status my-daemon-demo
systemctl reload my-daemon-demo

# 查看日志
journalctl --unit my-daemon-demo
```

### [start-stop-daemon](http://man7.org/linux/man-pages/man8/start-stop-daemon.8.html)

start-stop-daemon 是一个 Debian 派的守护进程管理程序，如果是 RedHat 派 Linux 则需要先安装，以 CentOS 举例说明：

``` shell
wget http://ftp.de.debian.org/debian/pool/main/d/dpkg/dpkg_1.17.6.tar.xz
tar -xf dpkg_1.17.6.tar.xz
cd dpkg_1.17.6
./configure  # 这一步如果提示缺少 curses，需要先 `yun install ncurses-devel`
make    # 这一步会报错，忽略即可
cd utils
make   # 可能不需要执行这个
sudo cp start-stop-daemon /usr/local/bin/start-stop-daemon
```

- 开启一个daemon进程

``` shell
start-stop-daemon --start --background --exec /myapp/daemon-demo.php
```

- 关闭一个daemon进程

``` shell
start-stop-daemon --stop --name daemon-demo.php

```

### PHP CLI 实现守护进程

参考《UNIX环境高级编程》，基本思路就是两次 fork，调用 `setsid()` 脱离终端（如果有的话），标准输出入、标准输出、标准出错重定向等等。

``` php
function daemonize()
{
    $pid = pcntl_fork();
    if ($pid == -1) {
        die("fork(1) failed!\n");
    } elseif ($pid > 0) {
        //让由用户启动的进程退出
        exit(0);
    }

    //建立一个有别于终端的新session以脱离终端
    posix_setsid();

    $pid = pcntl_fork();
    if ($pid == -1) {
        die("fork(2) failed!\n");
    } elseif ($pid > 0) {
        //父进程退出, 剩下子进程成为最终的独立进程
        exit(0);
    }
}

daemonize();
sleep(1000);
```

## FAQ

### 定时器和队列的关系

interval 和 timeout 对时间大小都有严格的要求，指定多少就要是多少。与 interval 和 timeout 不同，队列可以借助定时器实现，但是却对时间大小没有严格的限制，只是按顺序一个一个处理队列任务。

### supervisor: "program: ERROR (no such process)"

首先，检查 program 定义是否含有空格，即 `test` 任务的定义只能是：`[program:test]` 而不能是 `[program: test]`。

然后，检查 supervisord.conf 中 文件包含配置是否正确：末尾的 `[include]` 和 `files` 都要取消 `;` 注释，以及 `files` 指定的路径是否正确。

### crond 调度 PHP 程序容易遇到的问题

crontable 执行的工作目录是执行 cron job 用户的家目录开始的相对路径，因此如果是调用其他语言进行目录相关操作，可能与理想中的不一样，举例说明：

- Cron Job

```shell
# crontable
* * * * * php /path/to/1.php
```

- PHP 程序

```php
// PHP => 1.php
file_put_contents('1.log', date('Y-m-d H:i:s').PHP_EOL, FILE_APPEND);
```

如果以 `root` 身份执行该 cron job，那么实际生成 _1.log_ 的路径是 _/root/1.log_ 而非 _/path/to/1.log_。

> 说明：`crontable -e` => 无须重启即可生效。

## 参考

- [浅析 Linux 初始化 init 系统，第 3 部分: Systemd](https://www.ibm.com/developerworks/cn/linux/1407_liuming_init3/index.html)
- *[Supervisor: A Process Control System — Supervisor 3.3.3 documentation](http://supervisord.org/index.html)*
- *[使用 supervisor 管理进程](http://liyangliang.me/posts/2015/06/using-supervisor/)*
- *[纯PHP实现定时器任务（Timer）](http://blog.p2hp.com/archives/2035)*
- *[systemd](https://www.freedesktop.org/wiki/Software/systemd/)* && *[Rethinking PID 1](http://0pointer.de/blog/projects/systemd.html)*
- *[Archlinux-Systemd(简体中文)](https://wiki.archlinux.org/index.php/systemd_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87))*
- *[Linux 守护进程的启动方法](http://www.ruanyifeng.com/blog/2016/02/linux-daemon.html)*
- *[Systemd 入门教程：命令篇](http://www.ruanyifeng.com/blog/2016/03/systemd-tutorial-commands.html)*
- *[Ubuntu Manpage: start-stop-daemon - start and stop system daemon programs](http://manpages.ubuntu.com/manpages/zesty/en/man8/start-stop-daemon.8.html#contenttoc5)*
- *[Creating a daemon in Linux](https://stackoverflow.com/questions/17954432/creating-a-daemon-in-linux)*
- *[How to run process as background and never die?](https://stackoverflow.com/questions/4797050/how-to-run-process-as-background-and-never-die)*