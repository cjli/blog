---
layout: post
title: MySQL 使用日志
category: Database
tags: [MySQL, 数据库]
---

一些 MySQL 在开发、测试、正式环境使用记录，主要是语法/命令应用方面，强调实用性。

<!-- more -->

> 不定时更新。

## DDL

### 修改表定义

``` sql
ALTER TABLE {table_name}
MODIFY {col_name} <CHAR | VARCHAR | TEXT>(col_length)
[CHARACTER SET {charset_name}]
[COLLATE {collation_name}]
```

### 创建指定编码格式的数据库 

``` sql
CREATE DATABASE test CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
```

### 基于 SELECT 结果创建新表并插入数据

``` sql
CREATE TABLE bar (m INT) SELECT n FROM foo;
```

## DCL

### 创建数据库用户并授予部分数据库的权限 

``` sql
GRANT ALL PRIVILEGES ON database.* TO 'user'@'localhost'IDENTIFIED BY 'password' WITH GRANT OPTION;
```

## DML

### JOIN UPDATE

``` sql
UPDATE tbnameA a
LEFT JOIN tbnameB b ON a.id = b.aid

SET a.fdValue = '******'
```

### 查看当前连接到 MySQL 服务器的用户权限

``` sql
SHOW GRANTS FOR CURRENT_USER();
SHOW GRANTS FOR CURRENT_USER;
SHOW GRANTS;
```

### 从 SELECT 结果中插入数据

``` sql
INSERT INTO table_1 (fd1, fd2)
SELECT fd11, fd22 FROM table_2 
WHERE id=1;
```

### 查看表定义

- 简洁

``` sql
desc {table}
show columns from {table}
```

- 详细

``` mysql
select *
from information_schema.columns
where table_schema = {database} and table_name = {table}
```

## 命令行

### 备份所有数据库到 SQL 文件

``` shell
mysqldump --all-databases --all-routines -u root -p > /path/to/fulldump.sql
```

### 从 SQL 备份中恢复所有数据库

``` shell
mysql -u root -p  < /path/to/fulldump.sql
```

### 只导出数据库结构信息

``` shell
mysqldump --no-data -uusername -p the-database > dump_file
```

### 只导出数据库数据

``` shell
mysqldump --no-create-info -uusername -p the-database > dump_file
```

### 导出满足条件的部分记录

``` shell
mysqldump -h127.0.0.1 -uroot -p --database DB_NAME --tables TABLE_NAME --where="id=1024 limit 1"
```

### 备份某个数据库整库到文件

``` shell
mysqldump -uusername -ppassword DATABASE > db.sql
```

### 备份某个数据库整库到压缩文件

``` shell
mysqldump -uusername -ppassword the-database | gzip -9 > db.sql
```

## 常见问题

###  MySQL server has gone away

- fork 子进程场景

fork 了子进程，子进程退出后，使得同一个数据库链接被关闭，而父进程中仍然保留对数据库链接的引用。

此时如果在父进程中使用这个实际已被关闭的数据库连接，则会报此错。

> 同样的场景在 SQLite 下不会出现。

## 参考

- *[What is a SPATIAL INDEX and when should I use it?](https://stackoverflow.com/questions/2256364/what-is-a-spatial-index-and-when-should-i-use-it)*
- *[Improving Performance with MySQL Index Columns](https://logicalread.com/improve-perf-mysql-index-columns-mc12/)*
- *[MySQL - UPDATE query based on SELECT Query](https://stackoverflow.com/questions/1262786/mysql-update-query-based-on-select-query)*
- *[MySQL server has gone away](https://dev.mysql.com/doc/refman/5.6/en/gone-away.html)*
