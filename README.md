## Hexo Version

``` shell
hexo: 3.7.1
hexo-cli: 1.1.0
os: Linux 3.10.0-957.1.3.el7.x86_64 linux x64
http_parser: 2.7.1
node: 6.14.3
v8: 5.1.281.111
uv: 1.23.2
zlib: 1.2.7
ares: 1.10.1-DEV
icu: 50.1.2
modules: 48
napi: 3
openssl: 1.0.2k-fips
```

## 安装 Hexo

``` shell
npm install
npm install hexo --save
npm install hexo-deployer-git --save
npm install hexo-generator-searchdb --save
```

也可以不使用 `hexo-deployer-git` 工具部署，而把本地生成好的 public 文件夹下的文件推送到 `*-pages` 分支即可。

## 重新安装 `node_modules` 目录

- Linux

``` shell
rm -rf node_modules/
npm install # 或 `npm i`
```

- Windows

``` shell
npm uninstall hexo-deployer-git
npm install # 或 `npm i`
```

看 `node_modules` 下面还有多少个依赖包，依次 `uninstall`, 否则 Windows 下删不干净 `node_modules` 。

## 自定义主题

如果你是自定义主题包，比如我的是在 NexT 基础上做了一些小自定义化，则 `clone` NexT 仓库到 Hexo 主题目录：

``` shell
git clone git@gitlab.com/cjli/hexo.git themes/next
```

## 在其他电脑上迁移会遇到的问题

首先是确保站点配置文件 `_config.yml` 中的 `root:` 设置的值要与运行环境/主机/域名下的 Web 目录正确绑定。**

然后可能会用到的操作步骤如下：

``` shell
# 1. 先 clone 本仓库 master 分支的代码到本地
git clone git@gitlab.com:cjli/blog.git

# 2. 确保本机安装好 node.js 环境后然后进入 cjli 目录安装 Hexo
npm install -g hexo # 或者 npm install hexo --save

# 3. 然后安装依赖
npm install # 或者 npm i

# 4. 如果想从新生成后用 clone 下来的代码覆盖替换则先初始化一个 hexo 站点
hexo init    # 会自动安装和生成一些必要的文件, 需要的只是替换掉博客文章、站点配置文件和主题配置文件

# 5. 本地预览
hexo s --debug

# 6. 确保 `_config.yml` 配置无误后部署
hexo d

# 6. 如果产生合并冲突可以暴力解决
git push origin --delete master
git add -A
git commit -m "updated"
git push -f origin master
```
