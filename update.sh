cd $1

rm -rf .git
git init
git remote add origin git@gitlab.com:cjli/blog.git
git add . -A
git commit -m "updated"
git checkout -b src
git branch -D master
git push -f origin src

hexo clean
hexo g
rm -rf .deploy_git 2>&1 >> /dev/null
mkdir .deploy_git
cp gitlab-ci.yml .deploy_git/.gitlab-ci.yml
cd .deploy_git
git init
cd ..
hexo d
